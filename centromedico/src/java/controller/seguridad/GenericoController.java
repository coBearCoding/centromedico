
package controller.seguridad;

import util.CategoriaMaestro;
import jpa.modelo.CnClasificaciones;
import jpa.facade.CnClasificacionesFacade;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import util.Sesion;
import jpa.modelo.CnPerfilesUsuario;
import jpa.modelo.HcTipoReg;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CnCie10;
import jpa.facade.CnPerfilesUsuarioFacade;
import jpa.facade.HcTipoRegFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CnCie10Facade;

/**
 *
 * @author kenlly
 */
@ManagedBean(name = "genericoController")
@ApplicationScoped
public class GenericoController {

    @EJB
    CnClasificacionesFacade clasificacionesFacade;
    @EJB
    CnPerfilesUsuarioFacade perfilesUsuarioFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    HcTipoRegFacade tipoRegCliFacade;
    @EJB
    CnCie10Facade cie10Facade;

    private List<SelectItem> listaTipoIdentificacion;
    private List<SelectItem> listaGenero;
    private List<SelectItem> listaOrientacionSexual;
    private List<SelectItem> listaIdentidadGenero;
    private List<SelectItem> listaNacionalidad;
    private List<SelectItem> listaNacionalidades;
    private List<SelectItem> listaGruposPrioritarios;
    private List<SelectItem> listaTipoEdad;
    private List<SelectItem> listaAfiliado;
    private List<SelectItem> listaZona;
    private List<SelectItem> listaEstadoCivil;
    private List<SelectItem> listaGrupoSanguineo;
    private List<SelectItem> listaEtnia;
    private List<SelectItem> listaTipoAfiliado;
    private List<SelectItem> listaProvincia;
    private List<SelectItem> listaParroquias;
    private List<SelectItem> listaCanton;
    private List<SelectItem> listEspecialidades;
    private List<SelectItem> listaOcupaciones;
    private List<SelectItem> listaTipoUsuario;
    private List<SelectItem> listaFinalidadConsulta;
    private List<SelectItem> listaFinalidadProcedimiento;
    private List<SelectItem> listaMotivoConsulta;
    private List<SelectItem> listaMotivoCancelacionCitas;
    private List<SelectItem> listaPerfilesUsuario;
    private List<SelectItem> listaTipoDiagnosticoConsulta;
    private List<SelectItem> listaGenero3;
    private List<SelectItem> listaTipoConsulta;
    private List<SelectItem> listaTipoPago;
    private List<SelectItem> listaFormacionProfesional;
    private List<SelectItem> listaParentesco;
    private List<SelectItem> listaCategoriaPaciente;
    private List<SelectItem> listaGestacion;
    private List<HcTipoReg> listaTipoRegistroClinico;
    private List<CnCie10> listaEnfermedades;
    private List<CnUsuarios> listMedicos;
    private List<CnUsuarios> listaUsuarios;  

    private ArrayList<Sesion> sesionesActuales = new ArrayList<>();
    private List<SelectItem> listaMeses;
    
     
    public GenericoController() {
     
    }

    public void init() {

        if (listaGenero == null) {       

            cargarCategoria(CategoriaMaestro.Afiliado);
            cargarCategoria(CategoriaMaestro.Canton);
            cargarCategoria(CategoriaMaestro.Provincia);
            cargarCategoria(CategoriaMaestro.Enfermedades);
            cargarCategoria(CategoriaMaestro.Etnia);
            cargarCategoria(CategoriaMaestro.EstadoCivil);
            cargarCategoria(CategoriaMaestro.Especialidad);
            cargarCategoria(CategoriaMaestro.FinalidadConsulta);
            cargarCategoria(CategoriaMaestro.GrupoSanguineo);
            cargarCategoria(CategoriaMaestro.GrupoPrioritario);
            cargarCategoria(CategoriaMaestro.Sexo);
            cargarCategoria(CategoriaMaestro.IdentidadGenero);
            cargarCategoria(CategoriaMaestro.Meses);
            cargarCategoria(CategoriaMaestro.MotiCancCitas);
            cargarCategoria(CategoriaMaestro.MotivoConsulta);
            cargarCategoria(CategoriaMaestro.Nacionalidad);
            cargarCategoria(CategoriaMaestro.Nacionalidades);
            cargarCategoria(CategoriaMaestro.OrientacionSexual);
            cargarCategoria(CategoriaMaestro.Parroquia);
            cargarCategoria(CategoriaMaestro.Parentesco);
            cargarCategoria(CategoriaMaestro.PerfilesUsuario);
            cargarCategoria(CategoriaMaestro.Medicos);
            cargarCategoria(CategoriaMaestro.TipoConsulta);
            cargarCategoria(CategoriaMaestro.TipoEdad);
            cargarCategoria(CategoriaMaestro.TipoIdentificacion);
            cargarCategoria(CategoriaMaestro.TipoRegistroClinico);
            cargarCategoria(CategoriaMaestro.TipoUsuario);
            cargarCategoria(CategoriaMaestro.Usuarios);
            cargarCategoria(CategoriaMaestro.Zona);
            cargarCategoria(CategoriaMaestro.Gestacion);

        }
    }

    public void cargarCategoria(CategoriaMaestro maestro) {
        
        
        switch (maestro) {
            
            case Afiliado:
                listaAfiliado = cargarCategoria(maestro.toString());
                break;
            case Parroquia:
                listaParroquias = cargarCategoria(maestro.toString());
                break;
            case Provincia:
                listaProvincia = cargarCategoria(maestro.toString());
                break;
            case Enfermedades:
                listaEnfermedades = cie10Facade.buscarOrdenado();
                break;
            case Especialidad:
                listEspecialidades = cargarCategoria(maestro.toString());
                break;
            case EstadoCivil:
                listaEstadoCivil = cargarCategoria(maestro.toString());
                break;
            case Etnia:
                listaEtnia = cargarCategoria(maestro.toString());
                break;
            case IdentidadGenero:
                listaIdentidadGenero = cargarCategoria(maestro.toString());
                break;
            case OrientacionSexual:
                listaOrientacionSexual = cargarCategoria(maestro.toString());
                break;
            case FinalidadConsulta:
                listaFinalidadConsulta = cargarCategoria(maestro.toString());
                break;
            case GrupoSanguineo:
                listaGrupoSanguineo = cargarCategoria(maestro.toString());
                break;
            case GrupoPrioritario:
                listaGruposPrioritarios = cargarCategoria(maestro.toString());
                break;
            case Meses:
                cargarMeses();
                break;
            case MotiCancCitas:
                listaMotivoCancelacionCitas = cargarCategoria(maestro.toString());
                break;
            case MotivoConsulta:
                listaMotivoConsulta = cargarCategoria(maestro.toString());
            case Canton:
                listaCanton = cargarCategoria(maestro.toString());
                break;
            case Nacionalidad:
                listaNacionalidad = cargarCategoria(maestro.toString());
                break;
            case Nacionalidades:
                listaNacionalidades = cargarCategoria(maestro.toString());
                break;
            case Parentesco:
                listaParentesco = cargarCategoria(maestro.toString());
                break;
            case Medicos:
                listMedicos = usuariosFacade.findMedicos();
                break;
            case PerfilesUsuario:
                listaPerfilesUsuario = cargarPerfiles();
                break;
            case Sexo:
                listaGenero = cargarCategoria(maestro.toString());
                break;
            case TipoRegistroClinico:
                listaTipoRegistroClinico = tipoRegCliFacade.buscarTiposRegstroActivos();              
                break;
            case TipoIdentificacion:
                listaTipoIdentificacion = cargarCategoria(maestro.toString());
                break;
            case TipoEdad:
                listaTipoEdad = cargarCategoria(maestro.toString());
                break;
            case TipoUsuario:
                listaTipoUsuario = cargarCategoria(maestro.toString());
                break;
            case TipoAfiliado:
                listaTipoAfiliado = cargarCategoria(maestro.toString());
                break;
            case TipoConsulta:
                listaTipoConsulta = cargarCategoria(maestro.toString(), "TRUE");
                break;
            case Usuarios:
                listaUsuarios = usuariosFacade.buscarOrdenado();
                break;
            case Zona:
                listaZona = cargarCategoria(maestro.toString());
                break;
            case Gestacion:
                listaGestacion = cargarCategoria(maestro.toString());
                break; 
        }
    }
    
    private List<SelectItem> cargarPerfiles() {
        List<SelectItem> listaRetorno = new ArrayList<>();
        List<CnPerfilesUsuario> listaPerfiles = perfilesUsuarioFacade.findAll();
        for (CnPerfilesUsuario perfil : listaPerfiles) {
            listaRetorno.add(new SelectItem(perfil.getIdPerfil(), perfil.getNombrePerfil()));
        }
        return listaRetorno;
    }

    private List<SelectItem> cargarCategoria(String maestro) {
        List<SelectItem> listaRetorno = new ArrayList<>();
        List<CnClasificaciones> listaCategorias = clasificacionesFacade.buscarPorMaestro(maestro);
        for (CnClasificaciones clasificacion : listaCategorias) {
            listaRetorno.add(new SelectItem(clasificacion.getId(), clasificacion.getCodigo() + " - " + clasificacion.getDescripcion()));
        }
        return listaRetorno;
    }

    private List<SelectItem> cargarCategoria(String maestro, String observacion) {
        List<SelectItem> listaRetorno = new ArrayList<>();
        List<CnClasificaciones> listaCategorias = clasificacionesFacade.buscarPorMaestroObservacion(maestro, observacion);
        for (CnClasificaciones clasificacion : listaCategorias) {
            listaRetorno.add(new SelectItem(clasificacion.getId(), clasificacion.getCodigo() + " - " + clasificacion.getDescripcion()));
        }
        return listaRetorno;
    }

    public void cargarMeses() {
        listaMeses = new ArrayList<>();
        listaMeses.add(new SelectItem("0", "Enero"));
        listaMeses.add(new SelectItem("1", "Febrero"));
        listaMeses.add(new SelectItem("2", "Marzo"));
        listaMeses.add(new SelectItem("3", "Abril"));
        listaMeses.add(new SelectItem("4", "Mayo"));
        listaMeses.add(new SelectItem("5", "Junio"));
        listaMeses.add(new SelectItem("6", "Julio"));
        listaMeses.add(new SelectItem("7", "Agosto"));
        listaMeses.add(new SelectItem("8", "Septiembre"));
        listaMeses.add(new SelectItem("9", "Octubre"));
        listaMeses.add(new SelectItem("10", "Noviembre"));
        listaMeses.add(new SelectItem("11", "Diciembre"));
    }

    public boolean estaLogueado(int idUsuario) {
        //determina si un usario tiene una sesion iniciada //idUser= identificador del usuario en la base de datos
        for (Sesion sesion : sesionesActuales) {
            if (sesion.getIdUsuario() == idUsuario) {
                return true;
            }
        }
        return false;
    }

    public void addSession(int idUser, String idSession) {
        //adicionar a la lista de sesiones activas //
        sesionesActuales.add(new Sesion(idSession, idUser));
    }

    public void removeSession(int idUsuario) {
        //eliminar de la lista de sesiones activas dependiento del id del usuario
        for (Sesion sesion : sesionesActuales) {
            if (sesion.getIdUsuario() == idUsuario) {
                sesionesActuales.remove(sesion);//toca ver si funciona sino usar un index
                //sesionesActuales.remove(i);
                return;
            }
        }
    }

    public void removeSession(String idSesion) {
        //eliminar de la lista de sesiones actuales dependiento del id de la sesion
        for (Sesion sesion : sesionesActuales) {
            if (sesion.getIdSesion().equals(idSesion)) {
                sesionesActuales.remove(sesion);
                return;
            }
        }
    }

    public boolean buscarPorIdSesion(String idSesion) {
        //buscar una session segun su id
        for (Sesion sesion : sesionesActuales) {
            if (sesion.getIdSesion().equals(idSesion)) {
                return true;
            }
        }
        return false;
    }

    public List<SelectItem> getListaTipoIdentificacion() {
        return listaTipoIdentificacion;
    }

    public void setListaTipoIdentificacion(List<SelectItem> listaTipoIdentificacion) {
        this.listaTipoIdentificacion = listaTipoIdentificacion;
    }

    public List<SelectItem> getListaGenero() {
        return listaGenero;
    }

    public void setListaGenero(List<SelectItem> listaGenero) {
        this.listaGenero = listaGenero;
    }

    public List<SelectItem> getListaOrientacionSexual() {
        return listaOrientacionSexual;
    }

    public void setListaOrientacionSexual(List<SelectItem> listaOrientacionSexual) {
        this.listaOrientacionSexual = listaOrientacionSexual;
    }

    public List<SelectItem> getListaIdentidadGenero() {
        return listaIdentidadGenero;
    }

    public void setListaIdentidadGenero(List<SelectItem> listaIdentidadGenero) {
        this.listaIdentidadGenero = listaIdentidadGenero;
    }

    public List<SelectItem> getListaNacionalidad() {
        return listaNacionalidad;
    }

    public void setListaNacionalidad(List<SelectItem> listaNacionalidad) {
        this.listaNacionalidad = listaNacionalidad;
    }

    public List<SelectItem> getListaNacionalidades() {
        return listaNacionalidades;
    }

    public void setListaNacionalidades(List<SelectItem> listaNacionalidades) {
        this.listaNacionalidades = listaNacionalidades;
    }

    public List<SelectItem> getListaGruposPrioritarios() {
        return listaGruposPrioritarios;
    }

    public void setListaGruposPrioritarios(List<SelectItem> listaGruposPrioritarios) {
        this.listaGruposPrioritarios = listaGruposPrioritarios;
    }
    
    public List<SelectItem> getListaAfiliado() {
        return listaAfiliado;
    }

    public void setListaAfiliado(List<SelectItem> listaAfiliado) {
        this.listaAfiliado = listaAfiliado;
    }

    public List<SelectItem> getListaTipoEdad() {
        return listaTipoEdad;
    }

    public void setListaTipoEdad(List<SelectItem> listaTipoEdad) {
        this.listaTipoEdad = listaTipoEdad;
    }

    public List<SelectItem> getListaZona() {
        return listaZona;
    }

    public void setListaZona(List<SelectItem> listaZona) {
        this.listaZona = listaZona;
    }

    public List<SelectItem> getListaEstadoCivil() {
        return listaEstadoCivil;
    }

    public void setListaEstadoCivil(List<SelectItem> listaEstadoCivil) {
        this.listaEstadoCivil = listaEstadoCivil;
    }

    public List<SelectItem> getListaGrupoSanguineo() {
        return listaGrupoSanguineo;
    }

    public void setListaGrupoSanguineo(List<SelectItem> listaGrupoSanguineo) {
        this.listaGrupoSanguineo = listaGrupoSanguineo;
    }

    public List<SelectItem> getListaEtnia() {
        return listaEtnia;
    }

    public void setListaEtnia(List<SelectItem> listaEtnia) {
        this.listaEtnia = listaEtnia;
    }

    public List<SelectItem> getListaTipoAfiliado() {
        return listaTipoAfiliado;
    }

    public void setListaTipoAfiliado(List<SelectItem> listaTipoAfiliado) {
        this.listaTipoAfiliado = listaTipoAfiliado;
    }

    public List<SelectItem> getListaProvincia() {
        return listaProvincia;
    }

    public void setListaProvincia(List<SelectItem> listaProvincia) {
        this.listaProvincia = listaProvincia;
    }

    public List<SelectItem> getListEspecialidades() {
        return listEspecialidades;
    }

    public void setListEspecialidades(List<SelectItem> listEspecialidades) {
        this.listEspecialidades = listEspecialidades;
    }

    public List<SelectItem> getListaOcupaciones() {
        return listaOcupaciones;
    }

    public void setListaOcupaciones(List<SelectItem> listaOcupaciones) {
        this.listaOcupaciones = listaOcupaciones;
    }

    public List<SelectItem> getListaTipoUsuario() {
        return listaTipoUsuario;
    }

    public void setListaTipoUsuario(List<SelectItem> listaTipoUsuario) {
        this.listaTipoUsuario = listaTipoUsuario;
    }

    public List<SelectItem> getListaPerfilesUsuario() {
        return listaPerfilesUsuario;
    }

    public void setListaPerfilesUsuario(List<SelectItem> listaPerfilesUsuario) {
        this.listaPerfilesUsuario = listaPerfilesUsuario;
    }

    public List<CnUsuarios> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<CnUsuarios> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<SelectItem> getListaGenero3() {
        return listaGenero3;
    }

    public void setListaGenero3(List<SelectItem> listaGenero3) {
        this.listaGenero3 = listaGenero3;
    }

    public List<HcTipoReg> getListaTipoRegistroClinico() {
        return listaTipoRegistroClinico;
    }

    public void setListaTipoRegistroClinico(List<HcTipoReg> listaTipoRegistroClinico) {
        this.listaTipoRegistroClinico = listaTipoRegistroClinico;
    }

    public List<SelectItem> getListaMotivoConsulta() {
        listaMotivoConsulta.clear();
        for (SelectItem servicio : cargarCategoria("MotivoConsulta")) {
            if (servicio.getValue().toString().equals("475")) {
                listaMotivoConsulta.add(servicio);
            }
        }
        for (SelectItem servicio : cargarCategoria("MotivoConsulta")) {
            if (!servicio.getValue().toString().equals("475")) {
                listaMotivoConsulta.add(servicio);
            }
        }
        return listaMotivoConsulta;
    }

    public void setListaMotivoConsulta(List<SelectItem> listaMotivoConsulta) {
        this.listaMotivoConsulta = listaMotivoConsulta;
    }

    public List<SelectItem> getListaTipoDiagnosticoConsulta() {
        return listaTipoDiagnosticoConsulta;
    }

    public List<SelectItem> getListaTipoDiagnosticoConsultaHCRCV() {
        List<SelectItem> n = new ArrayList<>();
        for (SelectItem f : listaTipoDiagnosticoConsulta) {
            if (f.getValue().toString().equals("1759")) {
                n.add(f);
            }
        }
        for (SelectItem f : listaTipoDiagnosticoConsulta) {
            if (!f.getValue().toString().equals("1759")) {
                n.add(f);
            }
        }
        listaTipoDiagnosticoConsulta.clear();
        listaTipoDiagnosticoConsulta = n;
        return listaTipoDiagnosticoConsulta;
    }

    public void setListaTipoDiagnosticoConsulta(List<SelectItem> listaTipoDiagnosticoConsulta) {
        this.listaTipoDiagnosticoConsulta = listaTipoDiagnosticoConsulta;
    }

    public List<SelectItem> getListaTipoConsulta() {
        return listaTipoConsulta;
    }

    public void setListaTipoConsulta(List<SelectItem> listaTipoConsulta) {
        this.listaTipoConsulta = listaTipoConsulta;
    }

    public List<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(List<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public List<SelectItem> getListaMotivoCancelacionCitas() {
        return listaMotivoCancelacionCitas;
    }

    public void setListaMotivoCancelacionCitas(List<SelectItem> listaMotivoCancelacion) {
        this.listaMotivoCancelacionCitas = listaMotivoCancelacion;
    }

    public List<SelectItem> getListaParentesco() {
        return listaParentesco;
    }

    public void setListaParentesco(List<SelectItem> listaParentesco) {
        this.listaParentesco = listaParentesco;
    }

    public List<SelectItem> getListaCategoriaPaciente() {
        return listaCategoriaPaciente;
    }

    public void setListaCategoriaPaciente(List<SelectItem> listaCategoriaPaciente) {
        this.listaCategoriaPaciente = listaCategoriaPaciente;
    }

    public List<SelectItem> getListaTipoPago() {
        return listaTipoPago;
    }

    public void setListaTipoPago(List<SelectItem> listaTipoPago) {
        this.listaTipoPago = listaTipoPago;
    }


    public List<CnCie10> getListaEnfermedades() {
        return listaEnfermedades;
    }

    public void setListaEnfermedades(List<CnCie10> listaEnfermedades) {
        this.listaEnfermedades = listaEnfermedades;
    }

    public List<SelectItem> getListaMeses() {
        return listaMeses;
    }

    public void setListaMeses(List<SelectItem> listaMeses) {
        this.listaMeses = listaMeses;
    }

    public List<SelectItem> getListaFinalidadConsulta() {
        return listaFinalidadConsulta;
    }

    public List<SelectItem> getListaFinalidadConsultaHCRCV() {
        List<SelectItem> n = new ArrayList<>();
        for (SelectItem f : listaFinalidadConsulta) {
            if (f.getValue().toString().equals("1848")) {
                n.add(f);
            }
        }
        for (SelectItem f : listaFinalidadConsulta) {
            if (!f.getValue().toString().equals("1848")) {
                n.add(f);
            }
        }
        listaFinalidadConsulta.clear();
        listaFinalidadConsulta = n;
        return listaFinalidadConsulta;
    }

    public void setListaFinalidadConsulta(List<SelectItem> listaFinalidadConsulta) {
        this.listaFinalidadConsulta = listaFinalidadConsulta;
    }

    public List<SelectItem> getListaFinalidadProcedimiento() {
        return listaFinalidadProcedimiento;
    }

    public void setListaFinalidadProcedimiento(List<SelectItem> listaFinalidadProcedimiento) {
        this.listaFinalidadProcedimiento = listaFinalidadProcedimiento;
    }

    public List<SelectItem> getListaParroquias() {
        return listaParroquias;
    }

    public void setListaParroquias(List<SelectItem> listaParroquias) {
        this.listaParroquias = listaParroquias;
    }

    public List<SelectItem> getListaCanton() {
        return listaCanton;
    }

    public void setListaCanton(List<SelectItem> listaCanton) {
        this.listaCanton = listaCanton;
    }

    public List<SelectItem> getListaGestacion() {
        return listaGestacion;
    }

    public void setListaGestacion(List<SelectItem> listaGestacion) {
        this.listaGestacion = listaGestacion;
    }


    public CnClasificacionesFacade getClasificacionesFacade() {
        return clasificacionesFacade;
    }

    public void setClasificacionesFacade(CnClasificacionesFacade clasificacionesFacade) {
        this.clasificacionesFacade = clasificacionesFacade;
    }

    public CnPerfilesUsuarioFacade getPerfilesUsuarioFacade() {
        return perfilesUsuarioFacade;
    }

    public void setPerfilesUsuarioFacade(CnPerfilesUsuarioFacade perfilesUsuarioFacade) {
        this.perfilesUsuarioFacade = perfilesUsuarioFacade;
    }

    public CnUsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }

    public void setUsuariosFacade(CnUsuariosFacade usuariosFacade) {
        this.usuariosFacade = usuariosFacade;
    }

    public HcTipoRegFacade getTipoRegCliFacade() {
        return tipoRegCliFacade;
    }

    public void setTipoRegCliFacade(HcTipoRegFacade tipoRegCliFacade) {
        this.tipoRegCliFacade = tipoRegCliFacade;
    }

    public ArrayList<Sesion> getSesionesActuales() {
        return sesionesActuales;
    }

    public void setSesionesActuales(ArrayList<Sesion> sesionesActuales) {
        this.sesionesActuales = sesionesActuales;
    }

    public List<SelectItem> getListaFormacionProfesional() {
        return listaFormacionProfesional;
    }

    public void setListaFormacionProfesional(List<SelectItem> listaFormacionProfesional) {
        this.listaFormacionProfesional = listaFormacionProfesional;
    }
}
