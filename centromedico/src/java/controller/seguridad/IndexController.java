
package controller.seguridad;

import jpa.modelo.CnOpcionesMenu;
import jpa.modelo.CnUsuarios;
import jpa.facade.CnOpcionesMenuFacade;
import jpa.facade.CnUsuariosFacade;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultSubMenu;
import util.MetodosGenerales;
import util.Tab;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import controller.configuraciones.PacienteController;
import controller.historias.HistoriaClinicaController;
import controller.historias.SignosVitalesController;
import jpa.modelo.CnEmpresa;
import jpa.modelo.CnPacientes;
import jpa.facade.CnEmpresaFacade;
import org.apache.commons.lang3.SystemUtils;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author kenlly
 */
@Named("indexController")
@SessionScoped
public class IndexController extends MetodosGenerales implements Serializable {

    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    CnOpcionesMenuFacade opcionesFacade;
    @EJB
    CnEmpresaFacade empresaFacade;

    private CnUsuarios usuarioActual;
    private CnEmpresa empresaActual;

    private String clave = "";
    private String login = "";
    private boolean autenticado = false;
    private boolean superUsuario = false;
    private List<Tab> listadoDeTabs = new ArrayList<>();
    private String indexTabActiva = "0";
    private Set<CnOpcionesMenu> opcionesParaUsuarioSegunPerfil;
    private List<CnOpcionesMenu> opcionesList;
    private DefaultMenuModel simpleMenuModel = new DefaultMenuModel();
    private String urlTmp = "";
    private String urlFile = "";
    private String urlFileTf = "";//url del directorio para archivos de ordenes de terapia fisica
    private boolean esFisioterapia = false;
    private String idSession = "";
    private GenericoController genericoController;
    private PacienteController pacienteController;
    private HistoriaClinicaController historiaClinicaController;
    private SignosVitalesController signosvitalesController;
    private String rutaCarpetaImagenes;
    private String urlTabActual = "";
    private CnPacientes cnPacientes;

    public IndexController() {
        genericoController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{genericoController}", GenericoController.class);
        rutaCarpetaImagenes = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("uploadDirectory");
        if (SystemUtils.IS_OS_WINDOWS) {
            rutaCarpetaImagenes = "C:/imagenesSistemaMedico/";
        } else {
            rutaCarpetaImagenes = "/home/imagenesSistemaMedico/";
    }
    }

    public void init() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();       
    }

    @PreDestroy
    public void beforeSignOut() {
        genericoController.removeSession(idSession);
    }

    public void cerrarSesion() {
        try {
            genericoController.removeSession(idSession);
            ExternalContext exPath = FacesContext.getCurrentInstance().getExternalContext();
            String sPath = ((ServletContext) exPath.getContext()).getContextPath();
            ((HttpSession) exPath.getSession(false)).invalidate();
            exPath.redirect(sPath + "/index.html");
        } catch (IOException ex) {
            imprimirMensaje("Error", "Error: " + ex.toString(), FacesMessage.SEVERITY_ERROR);
        }
    }

    public String loginUser() {       

        if (clave.trim().length() == 0 || login.trim().length() == 0) {
            imprimirMensaje("Error", "Debe ingresar usuario y contraseña", FacesMessage.SEVERITY_ERROR);
            return "";
        }
        usuarioActual = usuariosFacade.buscarPorLoginClave(login, clave);
        empresaActual = empresaFacade.find(1);
        if (usuarioActual == null) {
            imprimirMensaje("Error", "Usuario o clave de acceso no válido", FacesMessage.SEVERITY_ERROR);
            clave = "";
            return "";
        } else if (!usuarioActual.getEstadoCuenta()) {
            imprimirMensaje("Error", "La cuenta de este usuario no se encuentra activa", FacesMessage.SEVERITY_ERROR);
            clave = "";
            return "";
        } else if (genericoController.estaLogueado(usuarioActual.getIdUsuario())) {
            RequestContext.getCurrentInstance().execute("PF('closeSessionDialog').show();");
            return "";
        } else {
            superUsuario = usuarioActual.getIdPerfil().getNombrePerfil().compareTo("SuperUsuario") == 0;
            cargarMenu();
            configurarSesion();
            if (usuarioActual.getIdPerfil().getNombrePerfil().matches("Medico")) {
                openTab("Inicio", "InicioEspecialidades.xhtml");
            } else if(usuarioActual.getIdPerfil().getNombrePerfil().matches("Prestador")) {
                openTab("Inicio", "InicioEspecialidades.xhtml");
            } else if (usuarioActual.getIdPerfil().getNombrePerfil().matches("Pediatria")) {
                openTab("Inicio", "InicioEspecialidades.xhtml");
            } else if (usuarioActual.getIdPerfil().getNombrePerfil().matches("Estadistica")) {
                openTab("Inicio", "InicioEstadistica.xhtml");
            } else if (usuarioActual.getIdPerfil().getNombrePerfil().matches("Enfermera")) {
                openTab("Inicio", "InicioEnfermeria.xhtml");
            } else if (usuarioActual.getIdPerfil().getNombrePerfil().matches("Terapia")) {
                openTab("Inicio", "InicioTerapiaFisica.xhtml");
            } else {
                openTab("Inicio", "Inicio.xhtml");
            }

            if (usuarioActual.getEspecialidad().getDescripcion().equals("FISIOTERAPIA")) {
                setEsFisioterapia(true);
            }else{
                setEsFisioterapia(false);
            }
            autenticado = true;
            return "home";
        }
    }

    public String cerrarSesionAbrirNueva() {
     
        genericoController.removeSession(usuarioActual.getIdUsuario());
        cargarMenu();
        configurarSesion();
        autenticado = true;
        return "home";
    }

    private void configurarSesion() {
        
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username", usuarioActual.getLoginUsuario());
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        idSession = session.getId();
        genericoController.addSession(usuarioActual.getIdUsuario(), idSession);


        urlFile = rutaCarpetaImagenes + "Archivoshistoria/";
        urlFileTf = rutaCarpetaImagenes + "ArchivosTerapiaFisica/";

        File directorio = new File(urlFile);
        if (!directorio.exists()) {
            directorio.mkdirs();
        }

        File directorioTf = new File(urlFileTf);
        if (!directorioTf.exists()) {
            directorioTf.mkdirs();
        }
        
        pacienteController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{pacienteController}", PacienteController.class);
        pacienteController.setIndexController(this);

        historiaClinicaController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{historiaClinicaController}", HistoriaClinicaController.class);
        historiaClinicaController.setIndexController(this);
        historiaClinicaController.setBtnEditarRendered(superUsuario);

        signosvitalesController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{signosvitalesController}", SignosVitalesController.class);
        signosvitalesController.setIndexController(this);
        signosvitalesController.setBtnEditarRendered(superUsuario);
    }

    private void cargarMenu() {
        simpleMenuModel = new DefaultMenuModel();
        if (usuarioActual != null) {
            opcionesParaUsuarioSegunPerfil = usuarioActual.getIdPerfil().getCnOpcionesMenuList();
            opcionesList = new ArrayList<>(opcionesParaUsuarioSegunPerfil);
            Collections.sort(opcionesList, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    CnOpcionesMenu a = (CnOpcionesMenu) o1;
                    CnOpcionesMenu b = (CnOpcionesMenu) o2;
                    return a.getIdOpcionMenu().compareTo(b.getIdOpcionMenu());
                }
            });

            for (CnOpcionesMenu opcion : opcionesList) {
                if (opcion.getIdOpcionMenu() != 0 && opcion.getIdOpcionPadre().getIdOpcionMenu() == 0) {
                    if (opcion.getUrlOpcion() == null || opcion.getUrlOpcion().length() == 0) {
                        DefaultSubMenu sm = new DefaultSubMenu();
                        sm.setLabel(opcion.getNombreOpcion());
                        if (opcion.getStyle() != null) {
                            sm.setIcon(opcion.getStyle());
                        }
                        for (CnOpcionesMenu op : opcion.getCnOpcionesMenuList()) {
                            cargarSubMenu(op, sm);
                        }
                        simpleMenuModel.addElement(sm);
                    } else {//no tiene hijos es menu item
                        DefaultMenuItem mi = new DefaultMenuItem();
                        mi.setTitle(opcion.getNombreOpcion());
                        mi.setValue(opcion.getNombreOpcion());
                        mi.setUpdate(":IdFormCenter");
                        if (opcion.getStyle() != null) {
                            mi.setIcon(opcion.getStyle());
                        }
                        mi.setCommand("#{indexController.openTab('" + opcion.getNombreOpcion() + "','" + opcion.getUrlOpcion() + "')}");
                        simpleMenuModel.addElement(mi);
                    }
                }
            }
        }
    }

    private void cargarSubMenu(CnOpcionesMenu opcion, DefaultSubMenu submenu) {
        if (tienePermisoMenu(opcion)) {
            if (opcion.getUrlOpcion() == null || opcion.getUrlOpcion().length() == 0) {
                DefaultSubMenu sm = new DefaultSubMenu();
                sm.setLabel(opcion.getNombreOpcion());
                if (opcion.getStyle() != null) {
                    sm.setIcon(opcion.getStyle());
                }
                for (CnOpcionesMenu op : opcion.getCnOpcionesMenuList()) {
                    cargarSubMenu(op, sm);
                }
                submenu.getElements().add(sm);
            } else {
                DefaultMenuItem mi = new DefaultMenuItem();
                mi.setTitle(opcion.getNombreOpcion());
                mi.setValue(opcion.getNombreOpcion());
                mi.setUpdate(":IdFormCenter");
                if (opcion.getStyle() != null) {
                    mi.setIcon(opcion.getStyle());
                }
                mi.setCommand("#{indexController.openTab('" + opcion.getNombreOpcion() + "','" + opcion.getUrlOpcion() + "')}");
                submenu.getElements().add(mi);
            }
        }
    }

    private boolean tienePermisoMenu(CnOpcionesMenu opcion) {
        for (CnOpcionesMenu op : opcionesList) {
            if (Objects.equals(op.getIdOpcionMenu(), opcion.getIdOpcionMenu())) {
                return true;
            }
        }
        return false;
    }

    public void cargarPacienteDesdeHistorias() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> params = context.getExternalContext().getRequestParameterMap();
        String titulo = params.get("titulo");
        String url = params.get("url");
        String idPaciente = params.get("idPaciente");
        pacienteController.cargarPacienteDesdeHistorias(idPaciente);
        openTab(titulo, url);
    }

    public void loadTab() {
        
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> params = context.getExternalContext().getRequestParameterMap();
        String titulo = params.get("titulo");
        String url = params.get("url");
        String id = params.get("id");
        switch (titulo) {
            case "Pacientes":
                pacienteController.cargarPacienteDesdeHistorias(id);
                break;
            case "Historias Clinicas":
                historiaClinicaController.cargarDesdeTab(id);
                break;
            case "Signos Vitales":
                signosvitalesController.cargarDesdeTab(id);
                break;
           
        }
        openTab(titulo, url);
    }

    public void openTab(String titulo, String url) {
        try {
            boolean tabYaEstaAgregada = false;
            int posicion = -1;
            for (Tab tabActual : listadoDeTabs) {
                posicion++;
                if (tabActual.getTitulo().equals(titulo) && tabActual.getUrl().equals(url)) {
                    tabYaEstaAgregada = true;
                    break;
                }
            }
            if (tabYaEstaAgregada) {
                indexTabActiva = String.valueOf(posicion);
                urlTabActual = listadoDeTabs.get(posicion).getUrl();
            } else {
                listadoDeTabs.add(new Tab(titulo, url));
                urlTabActual = listadoDeTabs.get(posicion + 1).getUrl();
                indexTabActiva = String.valueOf(posicion + 1);
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Setear Pagina: " + e.toString(), ""));
        }
    }

    public void changeTab(TabChangeEvent event) {
        indexTabActiva = String.valueOf(((TabView) event.getSource()).getChildren().indexOf(event.getTab()));
        urlTabActual = listadoDeTabs.get(Integer.parseInt(indexTabActiva)).getUrl();
    }

    public int closeTab(TabCloseEvent event) {
        try {
            int posicionTabEliminar = -1;
            int posicionTabActiva = ((TabView) event.getSource()).getChildren().indexOf(event.getTab());
            boolean esLaActiva = false;
            boolean esLaPrimera = false;
            boolean esLaUltima = false;
            boolean hayMasDeUna = false;
            boolean esMayorActivaQueEliminar = false;

            if (event.getTab() == null) {
                return 0;
            }
            String titulo = event.getTab().getTitle();
            for (Tab tabActual : listadoDeTabs) {
                posicionTabEliminar++;
                if (tabActual.getTitulo().equals(titulo)) {
                    if (posicionTabActiva == posicionTabEliminar) {
                        esLaActiva = true;
                    } else if (posicionTabActiva > posicionTabEliminar) {
                        esMayorActivaQueEliminar = true;
                    }
                    if (listadoDeTabs.size() > 1) {
                        hayMasDeUna = true;
                    }
                    if (posicionTabEliminar == 0) {
                        esLaPrimera = true;
                    }
                    if (posicionTabEliminar == (listadoDeTabs.size() - 1)) {
                        esLaUltima = true;
                    }
                    if (esLaPrimera) {
                        if (hayMasDeUna) {
                            if (esLaActiva) {
                                indexTabActiva = "0";
                                listadoDeTabs.remove(tabActual);
                                urlTabActual = listadoDeTabs.get(0).getUrl();
                                return 0;
                            } else {
                                indexTabActiva = String.valueOf(posicionTabActiva - 1);
                                listadoDeTabs.remove(tabActual);
                                urlTabActual = listadoDeTabs.get(posicionTabActiva - 1).getUrl();
                                return 0;
                            }
                        } else {
                            urlTabActual = "";
                        }
                    }
                    if (esLaUltima) {
                        if (hayMasDeUna) {
                            if (esLaActiva) {
                                indexTabActiva = String.valueOf(listadoDeTabs.size() - 2);
                                listadoDeTabs.remove(tabActual);
                                urlTabActual = listadoDeTabs.get(listadoDeTabs.size() - 1).getUrl();
                                return 0;
                            }
                        }
                    } else
                    if (esLaActiva) {
                        indexTabActiva = String.valueOf(posicionTabActiva - 1);
                        listadoDeTabs.remove(tabActual);
                        urlTabActual = listadoDeTabs.get(posicionTabActiva - 1).getUrl();
                        return 0;
                    } else if (esMayorActivaQueEliminar) {
                        indexTabActiva = String.valueOf(posicionTabActiva - 1);
                        listadoDeTabs.remove(tabActual);
                        urlTabActual = listadoDeTabs.get(posicionTabActiva - 1).getUrl();
                        return 0;
                    }
                    listadoDeTabs.remove(posicionTabEliminar);
                    return 0;
                }
            }
        } catch (NumberFormatException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cerrar Tab: " + e.toString(), ""));
        }
        return 0;
    }

    public List<Tab> getListadoDeTabs() {
        return listadoDeTabs;
    }

    public void setListadoDeTabs(List<Tab> listadoDePaginas) {
        this.listadoDeTabs = listadoDePaginas;
    }

    public String getIndexTabActiva() {
        return indexTabActiva;
    }

    public void setIndexTabActiva(String indexTabActiva) {
        this.indexTabActiva = indexTabActiva;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isAutenticado() {
        return autenticado;
    }

    public void setAutenticado(boolean autenticado) {
        this.autenticado = autenticado;
    }

    public CnUsuarios getUsuarioActual() {
        return usuarioActual;
    }

    public void setUsuarioActual(CnUsuarios usuarioActual) {
        this.usuarioActual = usuarioActual;
    }

    public DefaultMenuModel getSimpleMenuModel() {
        return simpleMenuModel;
    }

    public void setSimpleMenuModel(DefaultMenuModel simpleMenuModel) {
        this.simpleMenuModel = simpleMenuModel;
    }


    public String getUrltmp() {
        return urlTmp;
    }

    public void setUrltmp(String urltmp) {
        this.urlTmp = urltmp;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public GenericoController getAplicacionGeneralMB() {
        return genericoController;
    }

    public void setAplicacionGeneralMB(GenericoController genericoController) {
        this.genericoController = genericoController;
    }

    public String getRutaCarpetaImagenes() {
        return rutaCarpetaImagenes;
    }

    public void setRutaCarpetaImagenes(String rutaCarpetaImagenes) {
        this.rutaCarpetaImagenes = rutaCarpetaImagenes;
    }

    public String getUrlTabActual() {
        return urlTabActual;
    }

    public void setUrlTabActual(String urlTabActual) {
        this.urlTabActual = urlTabActual;
    }

    public CnEmpresa getEmpresaActual() {
        return empresaActual;
    }

    public void setEmpresaActual(CnEmpresa empresaActual) {
        this.empresaActual = empresaActual;
    }

    public boolean isSuperUsuario() {
        return superUsuario;
    }

    public void setSuperUsuario(boolean superUsuario) {
        this.superUsuario = superUsuario;
    }

    public CnPacientes getCnPacientes() {
        return cnPacientes;
    }

    public void setCnPacientes(CnPacientes cnPacientes) {
        this.cnPacientes = cnPacientes;
    }

    public HistoriaClinicaController getHistoriasMB() {
        return historiaClinicaController;
    }
  
    public void setHistoriasMB(HistoriaClinicaController historiaClinicaController) {
        this.historiaClinicaController = historiaClinicaController;
    }

    public PacienteController getPacientesMB() {
        return pacienteController;
    }

    public void setPacientesMB(PacienteController pacienteController) {
        this.pacienteController = pacienteController;
    }

    public SignosVitalesController getSignosvitalesMB() {
        return signosvitalesController;
    }

    public void setSignosvitalesMB(SignosVitalesController signosvitalesController) {
        this.signosvitalesController = signosvitalesController;
    }
    
    public String getUrlFileTf() {
        return urlFileTf;
    }

    public void setUrlFileTf(String urlFileTf) {
        this.urlFileTf = urlFileTf;
    }

    public boolean isEsFisioterapia() {
        return esFisioterapia;
    }

    public void setEsFisioterapia(boolean esFisioterapia) {
        this.esFisioterapia = esFisioterapia;
    }
}
