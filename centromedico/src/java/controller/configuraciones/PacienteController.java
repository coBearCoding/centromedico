
package controller.configuraciones;

import controller.seguridad.ValidaCedula;
import controller.seguridad.ValidarRUC;
import util.LazyPacienteDataModel;
import controller.seguridad.IndexController;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnPacientes;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnPacientesFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import util.MetodosGenerales;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.GenericoController;
import jpa.modelo.CnGruposprioritarios;
import jpa.modelo.CnGruposprioritariosPK;
import jpa.facade.CnGruposPrioritariosFacade;
import org.jboss.weld.util.collections.ArraySet;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "pacienteController")
@SessionScoped
public class PacienteController extends MetodosGenerales implements Serializable {

    @EJB
    CnPacientesFacade pacientesFacade;
    @EJB
    CnClasificacionesFacade clasificacionesFacade;
    @EJB
    CnGruposPrioritariosFacade gruposFacade;
    
    @Inject
    private IndexController indexController;
    private GenericoController genericoController;

    private LazyDataModel<CnPacientes> listPacientes;
    
    private CnPacientes pacienteSeleccionado;
    private CnPacientes pacienteSeleccionadoTabla;
    private CnPacientes nuevoPaciente;    
  
    private List<SelectItem> listaCanton;
    private List<SelectItem> listaParroquia;
    
    private ValidarRUC validarrucep= new ValidarRUC();
    private ValidaCedula validarcedula= new ValidaCedula();
    
    private boolean tieneGestacion;
    private boolean estaenGestacion;
    private boolean nuevoRegistro = true;
    
    private String tabActiva = "0";
    private String tituloTab = "Datos nuevo paciente";
    private String tipoIdentificacion = "";
    private String identificacion = "";
    private String tipoIdentificacionrepresentante = "";
    private String identificacionrepresentante = "";    
    private String edad = "-";
    private String genero = "";
    private String orientacionsexual = "";
    private String identidadgenero = "";
    private String grupoSanguineo = "";
    private String primerNombre = "";
    private String segundoNombre = "";
    private String primerApellido = "";
    private String segundoApellido = "";
    private String estadoCivil = "";
    private String telefonoResidencia = "";
    private String telefonoOficina = "";
    private String celular = "";
    private String provincia = "";
    private String nacionalidad = "";
    private String nacionalidades = "";
    private String canton = "";
    private String zona = "";
    private String barrio = "";
    private String desBarrio = "COMUNA Y BARRIO";
    private String direccion = "";
    private String email = "";
    private String tipoAfiliado = "";
    private String categoriaPaciente;
    private String etnia = "";
    private String semanagestacion = "";
    private String responsable;
    private String telefonoResponsable;
    private String parentesco;
    private String parentesco_a;
    private String acompanante;
    private String telefonoAcompanante;
    private String observaciones = "";
    private String labelcheck = "...";
    private String gestacion;
    private String[] gruposprioritarios;    
    private Date fechaNacimiento = null;
    private int años = 1;    

    private final static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final static Pattern EMAIL_COMPILED_PATTERN = Pattern.compile(EMAIL_PATTERN);
  
    public PacienteController() {
        genericoController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{genericoController}", GenericoController.class);
        indexController = (IndexController) FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{indexController}", IndexController.class);
    }       
       
    @PostConstruct
    public void init() {
        nuevoRegistro = true;
        listPacientes = new LazyPacienteDataModel(pacientesFacade);
        limpiarFormulario();
    }

    public void seleccionGenero() {
        tieneGestacion = false;
        if (genero != null && genero.equals("2008")) {
            tieneGestacion = true;
        } else {
            gestacion = "";
        }
    }
    public void seleccionGestacion() {
        estaenGestacion = false;
        if (gestacion != null && gestacion.equals("1921")) {
            estaenGestacion = true;
        } else {
            semanagestacion = "";
        }
    }

    public void cambiaFechaNacimiento(SelectEvent event) {
        if (fechaNacimiento != null) {
            edad = calcularEdad(fechaNacimiento);
            años = calcularEdadInt(fechaNacimiento);

        } else {
            años = 1;
            edad = "-";
        }
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
    }

    public Date getFechaMaxima() {
        return clasificacionesFacade.FechaServidorddmmyyhhmmTimestamp();
    }


    public void cargarCanton() {
        listaCanton = new ArrayList<>();
        if (provincia != null && provincia.length() != 0) {
            List<CnClasificaciones> listaM = clasificacionesFacade.buscarCantonPorProvincia(clasificacionesFacade.find(Integer.parseInt(provincia)).getCodigo());
            for (CnClasificaciones mun : listaM) {
                listaCanton.add(new SelectItem(mun.getId(), mun.getId() + "-" + mun.getDescripcion()));
            }
        }
    }
    
    public void cargarParroquias() {
        listaParroquia = new ArrayList<>();
        if (canton != null && canton.length() != 0) {
            List<CnClasificaciones> listaM = clasificacionesFacade.buscarParroquiaPorCanton(clasificacionesFacade.find(Integer.parseInt(canton)).getId().toString());
            for (CnClasificaciones mun : listaM) {
                listaParroquia.add(new SelectItem(mun.getId(), mun.getDescripcion()));
            }
        }
    }


    public void limpiarNuevoRegistro() {
        limpiarFormulario();
        nuevoRegistro = true;
    }

    public void limpiarFormulario() {
        tituloTab = "Datos nuevo paciente";
        pacienteSeleccionado = null;
        tipoIdentificacion = "";
        identificacion = null;
        fechaNacimiento = null;
        edad = "-";
        grupoSanguineo = "";
        genero = "";
        primerNombre = "";
        segundoNombre = "";
        primerApellido = "";
        segundoApellido = "";
        nacionalidad = "";  
        estadoCivil = "";
        celular = "";
        email = "";
        direccion = "";
        orientacionsexual = "";
        identidadgenero = "";
        telefonoResidencia = "";
        telefonoOficina = "";
        canton = "";
        provincia = "";
        zona = "";
        barrio = "";
        nacionalidades = "";
        etnia = "";
        gestacion = "";
        semanagestacion = "";
        tieneGestacion = false;
        estaenGestacion = false;
        gruposprioritarios = null;
        tipoAfiliado = "";
        observaciones = "";
        responsable = "";
        identificacionrepresentante = "";
        telefonoResponsable = "";
        parentesco = "";
        acompanante = "";
        telefonoAcompanante = "";
        categoriaPaciente = "";       
        
        cambiaFechaNacimiento(null);
        cargarCanton();
    }

    public void nuevoPaciente() {
        nuevoRegistro = true;
        tituloTab = "Datos nuevo paciente";
        pacienteSeleccionado = null;
        fechaNacimiento = null;
        edad = "-";
        genero = "";
        tieneGestacion = false;
        estaenGestacion = false;
        grupoSanguineo = "";
        primerNombre = "";
        segundoNombre = "";
        primerApellido = "";
        segundoApellido = "";
        estadoCivil = "";
        telefonoResidencia = "";
        telefonoOficina = "";
        celular = "";
        provincia = "";
        canton = "";
        zona = "";
        barrio = "";
        direccion = "";
        email = "";
        tipoAfiliado = "";
        categoriaPaciente = "";
        etnia = "";
        responsable = "";
        telefonoResponsable = "";
        parentesco = "";
        acompanante = "";
        telefonoAcompanante = "";
        observaciones = "";
        gestacion = "";
        gruposprioritarios = null;
        cambiaFechaNacimiento(null);
        cargarCanton();
    }

    public void cargarPacienteDesdeHistorias(String id) {
        pacienteSeleccionadoTabla = pacientesFacade.find(Integer.parseInt(id));
        setIdentificacion(id);
        cargarPaciente();
    }

  

    public void cargarPaciente() {
        if (pacienteSeleccionadoTabla == null) {
            imprimirMensaje("Error", "Se debe seleccionar un paciente de la tabla", FacesMessage.SEVERITY_ERROR);
            return;
        }
        limpiarFormulario();
        pacienteSeleccionado = pacientesFacade.find(pacienteSeleccionadoTabla.getIdPaciente());
        tituloTab = "Datos paciente: " + pacienteSeleccionado.nombreCompleto();

        identificacion = pacienteSeleccionado.getIdentificacion();
        if (pacienteSeleccionado.getTipoIdentificacion() != null) {
            tipoIdentificacion = pacienteSeleccionado.getTipoIdentificacion().getId().toString();
        } else {
            tipoIdentificacion = "";
        }
        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            fechaNacimiento = pacienteSeleccionado.getFechaNacimiento();
            edad = calcularEdad(fechaNacimiento);
        } else {
            fechaNacimiento = null;
            edad = "-";
        }
        if (pacienteSeleccionado.getGrupoSanguineo() != null) {
            grupoSanguineo = pacienteSeleccionado.getGrupoSanguineo().getId().toString();
        } else {
            grupoSanguineo = "";
        }
        if (pacienteSeleccionado.getSexo()!= null) {
            genero = pacienteSeleccionado.getSexo().getId().toString();
        } else {
            genero = "";
        }        
        primerNombre = pacienteSeleccionado.getPrimerNombre();
        segundoNombre = pacienteSeleccionado.getSegundoNombre();
        primerApellido = pacienteSeleccionado.getPrimerApellido();
        segundoApellido = pacienteSeleccionado.getSegundoApellido();
        cambiaFechaNacimiento(null);
        if (pacienteSeleccionado.getNacionalidad()!= null) {
            nacionalidad = pacienteSeleccionado.getNacionalidad().getId().toString();
        } else {
            nacionalidad = "";
        }
        if (pacienteSeleccionado.getEstadoCivil() != null) {
            estadoCivil = pacienteSeleccionado.getEstadoCivil().getId().toString();
        } else {
            estadoCivil = "";
        }
        celular = pacienteSeleccionado.getCelular();
        email = pacienteSeleccionado.getEmail();
        if (pacienteSeleccionado.getDireccion() != null) {
            direccion = pacienteSeleccionado.getDireccion();
        }
        if (pacienteSeleccionado.getOrientacionsexual()!= null) {
            orientacionsexual = pacienteSeleccionado.getOrientacionsexual().getId().toString();
        } else {
            orientacionsexual = "";
        }
        if (pacienteSeleccionado.getIdentidadgenero()!= null) {
            identidadgenero = pacienteSeleccionado.getIdentidadgenero().getId().toString();
        } else {
            identidadgenero = "";
        }        
        telefonoResidencia = pacienteSeleccionado.getTelefono();
        telefonoOficina = pacienteSeleccionado.getTelefonofamiliar();        
        if (pacienteSeleccionado.getProvincia()!= null) {
            provincia = pacienteSeleccionado.getProvincia().getId().toString();
            cargarCanton();
            canton = pacienteSeleccionado.getCanton().getId().toString();
        } else {
            provincia = "";
            canton = "";
        }
        if (pacienteSeleccionado.getParroquia()!= null) {
            zona = pacienteSeleccionado.getParroquia().getId().toString();
        } else {
            zona = "";
        }
        barrio = pacienteSeleccionado.getBarrio();
        if (pacienteSeleccionado.getNacionalidades()!= null) {
            nacionalidades = pacienteSeleccionado.getNacionalidades().getId().toString();
        } else {
            nacionalidades = "";
        }
        if (pacienteSeleccionado.getEtnia() != null) {
            etnia = pacienteSeleccionado.getEtnia().getId().toString();
        } else {
            etnia = "";
        }
        
        if (pacienteSeleccionado.getTipoAfiliado() != null) {
            tipoAfiliado = pacienteSeleccionado.getTipoAfiliado().getId().toString();
        } else {
            tipoAfiliado = "";
        }
        if (pacienteSeleccionado.getGestacion()!= null) {
            gestacion = pacienteSeleccionado.getGestacion().getId().toString();
        } else {
            gestacion = "";
        }
        semanagestacion = pacienteSeleccionado.getSemanagestacion();

        if (pacienteSeleccionado.getTipoAfiliado()!= null) {
            tipoAfiliado = pacienteSeleccionado.getTipoAfiliado().toString();
        } else {
            tipoAfiliado = "";
        }
        observaciones = pacienteSeleccionado.getObservaciones();        
        responsable = pacienteSeleccionado.getResponsable();
        identificacionrepresentante = pacienteSeleccionado.getIdentificacionresponsable();        
        telefonoResponsable = pacienteSeleccionado.getTelefonoResponsable();
        if (pacienteSeleccionado.getParentesco() != null) {
            parentesco = pacienteSeleccionado.getParentesco().getId().toString();
        } else {
            parentesco = null;
        }
        acompanante = pacienteSeleccionado.getAcompanante();
        telefonoAcompanante = pacienteSeleccionado.getTelefonoAcompanante();
        if (pacienteSeleccionado.getParentescoA()!= null) {
            parentesco_a = pacienteSeleccionado.getParentescoA().getId().toString();
        } else {
            parentesco_a = null;
        }
        
        
        List<CnGruposprioritarios> listGrupo = gruposFacade.getGruposByIdPaciente(pacienteSeleccionado.getIdPaciente());
        gruposprioritarios = new String[listGrupo.size()];
        if(listGrupo != null){
            for (int i = 0; i < listGrupo.size(); i++) {
                gruposprioritarios[i] = listGrupo.get(i).getCnGruposprioritariosPK().getIdgruposprioritarios().toString() ;//+ (clasificacionesFacade.find(listGrupo.get(i).getCnGruposprioritariosPK().getIdgruposprioritarios())).getDescripcion();
                
            }
        }
        
        nuevoRegistro=false;
        tabActiva = "0";
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
    }

    public void guardarPaciente() {
        CnPacientes pacienteTmp;
        gestacion = "";
        if(genero == "1"){
            
        }
        
        if(años <= 17 && tipoIdentificacion.matches("1769")){
            if (validacionCampoVacio(identificacion + "", "Identificación")) {
                return;
            }
        }else{
            if (validacionCampoVacio(identificacion + "", "Identificación")) {
                return;
            }
            if (validacionCampoVacio(tipoIdentificacion, "Tipo de identificación")) {
                return;
            }else{
                if(tipoIdentificacion.matches("1764")){
                    if(identificacion.length()==10){
                        if(!validarcedula.validacionCedula(identificacion)){
                            imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                            return;
                        }
                    }else{
                        imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                        return;
                    }

                }else if(tipoIdentificacion.matches("2004")){
                    if(identificacion.length()==13){
                        if(!validarrucep.validacionRUC(identificacion)){
                            imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                            return;
                        } 
                    }else{
                        imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                        return;
                    }                
                }else if(tipoIdentificacion.matches("1769")){
                    if(identificacion.length()==10){
                        if(!validarrucep.validacionRUC(identificacion)){
                            imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                            return;
                        } 
                    }else{
                        imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                        return;
                    }                
                }
            }
        }
        
        if (fechaNacimiento == null) {
            imprimirMensaje("Error", "La fecha de nacimiento es obligatoria", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaNacimiento.after(pacientesFacade.FechaServidorddmmyyhhmmTimestamp())) {
            imprimirMensaje("Error", "La fecha de nacimiento no puede ser mayor a la actual", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (validacionCampoVacio(primerNombre, "Primer Nombre")) {
            return;
        }
        if (validacionCampoVacio(primerApellido, "Primer Apellido")) {
            return;
        }
        if (años <= 17){
           if (validacionCampoVacio(responsable, "Responsable")) {
              return;
           }
           if (validacionCampoVacio(identificacionrepresentante, "Identificación Responsable")) {
              return;
           }
           if (validacionCampoVacio(tipoIdentificacionrepresentante, "Tipo Identificación Responsable")) {
              return;
           }else{
                if(tipoIdentificacionrepresentante.matches("1764")){
                    if(identificacionrepresentante.length()==10){
                        if(!validarcedula.validacionCedula(identificacionrepresentante)){
                            imprimirMensaje("Error", "La identificacion del representante no es valida " , FacesMessage.SEVERITY_ERROR);
                            return;
                        }
                    }else{
                        imprimirMensaje("Error", "La identificacion del representante no es valida " , FacesMessage.SEVERITY_ERROR);
                        return;
                    }

                }else if(tipoIdentificacionrepresentante.matches("2004")){
                    if(identificacionrepresentante.length()==13){
                        if(!validarrucep.validacionRUC(identificacionrepresentante)){
                            imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                            return;
                        } 
                    }else{
                        imprimirMensaje("Error", "La identificacion no es valida " , FacesMessage.SEVERITY_ERROR);
                        return;
                    }                
                }else if(tipoIdentificacionrepresentante.matches("1769")){
                        imprimirMensaje("Error", "El representante debe tener identificacion" , FacesMessage.SEVERITY_ERROR);
                        return;
                                  
                }
            }
        }
        if(gestacion.matches("SI")){
           if (validacionCampoVacio(semanagestacion, "Semana de gestación")) {
              return;
           } 
        }
        if (!email.equals("")) {
            Matcher matcher = EMAIL_COMPILED_PATTERN.matcher(email);

            if (!matcher.matches()) {
                imprimirMensaje("Error", "Formato de Email Invalido", FacesMessage.SEVERITY_ERROR);
                return;
            }
        }
        if (pacienteSeleccionado == null) {                
            if (pacientesFacade.buscarPorIdentificacion(identificacion + "") != null) {
                imprimirMensaje("Error", "Ya existe un paciente con esta identificación", FacesMessage.SEVERITY_ERROR);
                return;
            }
            guardarNuevoPaciente();
        } else {           
            pacienteTmp = pacientesFacade.buscarPorIdentificacion(identificacion + "");
            if (pacienteTmp != null && !pacienteSeleccionado.getIdentificacion().equals(pacienteTmp.getIdentificacion())) {
                imprimirMensaje("Error", "Existe un paciente diferente con esta identificación", FacesMessage.SEVERITY_ERROR);
                return;
            }
            actualizarPacienteExistente();
        }
        tabActiva = "0";
        nuevoRegistro = true;

    }

    private void guardarNuevoPaciente() {
        
        try {
            UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
            
            transaction.begin();
            
            nuevoPaciente = new CnPacientes();
            
            nuevoPaciente.setIdPaciente(pacientesFacade.siguienteid());
            nuevoPaciente.setIdentificacion(identificacion + "");
            if (validarNoVacio(tipoIdentificacion)) {
                nuevoPaciente.setTipoIdentificacion(clasificacionesFacade.find(Integer.parseInt(tipoIdentificacion)));
            }
            
            if (fechaNacimiento != null) {
                nuevoPaciente.setFechaNacimiento(fechaNacimiento);
            }
            if (validarNoVacio(genero)) {
                nuevoPaciente.setSexo(clasificacionesFacade.find(Integer.parseInt(genero)));
            }
            if (validarNoVacio(grupoSanguineo)) {
                nuevoPaciente.setGrupoSanguineo(clasificacionesFacade.find(Integer.parseInt(grupoSanguineo)));
            }
            nuevoPaciente.setPrimerNombre(primerNombre.toUpperCase());
            nuevoPaciente.setSegundoNombre(segundoNombre.toUpperCase());
            nuevoPaciente.setPrimerApellido(primerApellido.toUpperCase());
            nuevoPaciente.setSegundoApellido(segundoApellido.toUpperCase());
            if (validarNoVacio(estadoCivil)) {
                nuevoPaciente.setEstadoCivil(clasificacionesFacade.find(Integer.parseInt(estadoCivil)));
            }
            if (validarNoVacio(orientacionsexual)) {
                nuevoPaciente.setOrientacionsexual(clasificacionesFacade.find(Integer.parseInt(orientacionsexual)));
            }
            if (validarNoVacio(identidadgenero)) {
                nuevoPaciente.setIdentidadgenero(clasificacionesFacade.find(Integer.parseInt(identidadgenero)));
            }
            if (validarNoVacio(nacionalidad)) {
                nuevoPaciente.setNacionalidad(clasificacionesFacade.find(Integer.parseInt(nacionalidad)));
            }
            if (validarNoVacio(nacionalidades)) {
                nuevoPaciente.setNacionalidades(clasificacionesFacade.find(Integer.parseInt(nacionalidades)));
            }
            
            if (validarNoVacio(gestacion)) {
                nuevoPaciente.setGestacion(clasificacionesFacade.find(Integer.parseInt(gestacion)));
            }
            
            nuevoPaciente.setSemanagestacion(semanagestacion);
            nuevoPaciente.setTelefono(telefonoResidencia);
            nuevoPaciente.setTelefonofamiliar(telefonoOficina);
            nuevoPaciente.setCelular(celular);
            
            if (provincia != null && provincia.length() != 0) {
                nuevoPaciente.setProvincia(clasificacionesFacade.find(Integer.parseInt(provincia)));
                nuevoPaciente.setCanton(clasificacionesFacade.find(Integer.parseInt(canton)));
            }
            if (validarNoVacio(zona)) {
                nuevoPaciente.setParroquia(clasificacionesFacade.find(Integer.parseInt(zona)));
            }
            nuevoPaciente.setBarrio(barrio.toUpperCase());
            nuevoPaciente.setDireccion(direccion.toUpperCase());
            nuevoPaciente.setEmail(email);
            
            nuevoPaciente.setIdentificacionresponsable(identificacionrepresentante + "");
            if (validarNoVacio(tipoIdentificacionrepresentante)) {
                nuevoPaciente.setTipoIdentificacionrepresentante(clasificacionesFacade.find(Integer.parseInt(tipoIdentificacionrepresentante)));
            }
            
            if (validarNoVacio(tipoAfiliado)) {
                nuevoPaciente.setTipoAfiliado(clasificacionesFacade.find(Integer.parseInt(tipoAfiliado)));
            }
            
            if (validarNoVacio(etnia)) {
                nuevoPaciente.setEtnia(clasificacionesFacade.find(Integer.parseInt(etnia)));
            }
            
            nuevoPaciente.setResponsable(responsable.toUpperCase());
            nuevoPaciente.setTelefonoResponsable(telefonoResponsable);
            if (validarNoVacio(parentesco)) {
                nuevoPaciente.setParentesco(clasificacionesFacade.find(Integer.parseInt(parentesco)));
            }
            if (validarNoVacio(parentesco_a)) {
                nuevoPaciente.setParentescoA((clasificacionesFacade.find(Integer.parseInt(parentesco_a))));
            }
            nuevoPaciente.setAcompanante(acompanante.toUpperCase());
            nuevoPaciente.setTelefonoAcompanante(telefonoAcompanante);
            
            nuevoPaciente.setObservaciones(observaciones.toUpperCase());
            
            
            
            Set<CnGruposprioritarios> grupos = new ArraySet<>();
            if (gruposprioritarios!=null) {
                if (gruposprioritarios.length > 0) {
                    for (String u : gruposprioritarios){
                        CnGruposprioritarios nuevogruposprioritarios = new CnGruposprioritarios();
                        nuevogruposprioritarios.setCnGruposprioritariosPK(new CnGruposprioritariosPK((clasificacionesFacade.find(Integer.parseInt(u))).getId(), nuevoPaciente.getIdPaciente()));
                        grupos.add(nuevogruposprioritarios);
                    }
                    nuevoPaciente.setCnGruposprioritariosCollection(grupos);
                }
            }
            nuevoPaciente.setFechainsc(pacientesFacade.FechaServidorddmmyyhhmmTimestamp());
            nuevoPaciente.setUsuarioCreacion(indexController.getUsuarioActual().getIdUsuario());
            pacientesFacade.create(nuevoPaciente);
            transaction.commit();
            
            imprimirMensaje("Correcto", "Nuevo paciente creado correctamente", FacesMessage.SEVERITY_INFO);
            listPacientes = new LazyPacienteDataModel(pacientesFacade);
            limpiarFormulario();
            
            
            this.init();
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void validarIdentificacion() {
        nuevoRegistro = true;
        int cedula = pacientesFacade.numeroCedulas(identificacion + "");
        if (cedula > 0) {
            pacienteSeleccionadoTabla = pacientesFacade.findPacienteByTipIden(Integer.parseInt(tipoIdentificacion), identificacion);
            if (pacienteSeleccionadoTabla != null) {
                cargarPaciente();
                imprimirMensaje("Informacion", "El paciente ya se encuentra registrado, se carga información", FacesMessage.SEVERITY_INFO);
            } else {
                imprimirMensaje("Informacion", "El paciente no existe, ingrese los datos para crearlo o verifique el número de documento", FacesMessage.SEVERITY_INFO);
                nuevoPaciente();
            }
        } else {
            imprimirMensaje("Informacion", "El paciente no existe, ingrese los datos para crearlo o verifique el número de documento", FacesMessage.SEVERITY_INFO);
            nuevoPaciente();
        }
    }

     public void crearCodigoIdentificacion() {
         if(tipoIdentificacion.matches("1769")){
            String sql = "select * from generar_identificacion()" ;
            identificacion = pacientesFacade.findBySqlNativo(sql);
         }
         
    }

    private void actualizarPacienteExistente() {
        
        try {
            UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
            
            transaction.begin();
            
            pacienteSeleccionado.setIdentificacion(identificacion + "");
            if (validarNoVacio(tipoIdentificacion)) {
                pacienteSeleccionado.setTipoIdentificacion(clasificacionesFacade.find(Integer.parseInt(tipoIdentificacion)));
            }
            
            if (fechaNacimiento != null) {
                pacienteSeleccionado.setFechaNacimiento(fechaNacimiento);
            }
            if (validarNoVacio(genero)) {
                pacienteSeleccionado.setSexo(clasificacionesFacade.find(Integer.parseInt(genero)));
            }
            if (validarNoVacio(grupoSanguineo)) {
                pacienteSeleccionado.setGrupoSanguineo(clasificacionesFacade.find(Integer.parseInt(grupoSanguineo)));
            }
            pacienteSeleccionado.setPrimerNombre(primerNombre.toUpperCase());
            pacienteSeleccionado.setSegundoNombre(segundoNombre.toUpperCase());
            pacienteSeleccionado.setPrimerApellido(primerApellido.toUpperCase());
            pacienteSeleccionado.setSegundoApellido(segundoApellido.toUpperCase());
            if (validarNoVacio(estadoCivil)) {
                pacienteSeleccionado.setEstadoCivil(clasificacionesFacade.find(Integer.parseInt(estadoCivil)));
            }
            if (validarNoVacio(orientacionsexual)) {
                pacienteSeleccionado.setOrientacionsexual(clasificacionesFacade.find(Integer.parseInt(orientacionsexual)));
            }
            if (validarNoVacio(identidadgenero)) {
                pacienteSeleccionado.setIdentidadgenero(clasificacionesFacade.find(Integer.parseInt(identidadgenero)));
            }
            if (validarNoVacio(nacionalidad)) {
                pacienteSeleccionado.setNacionalidad(clasificacionesFacade.find(Integer.parseInt(nacionalidad)));
            }
            if (validarNoVacio(nacionalidades)) {
                pacienteSeleccionado.setNacionalidades(clasificacionesFacade.find(Integer.parseInt(nacionalidades)));
            }
            
            if (validarNoVacio(gestacion)) {
                pacienteSeleccionado.setGestacion(clasificacionesFacade.find(Integer.parseInt(gestacion)));
            }
            
            pacienteSeleccionado.setSemanagestacion(semanagestacion);
            pacienteSeleccionado.setTelefono(telefonoResidencia);
            pacienteSeleccionado.setTelefonofamiliar(telefonoOficina);
            pacienteSeleccionado.setCelular(celular);
            
            if (provincia != null && provincia.length() != 0) {
                pacienteSeleccionado.setProvincia(clasificacionesFacade.find(Integer.parseInt(provincia)));
                pacienteSeleccionado.setCanton(clasificacionesFacade.find(Integer.parseInt(canton)));
            }
            if (validarNoVacio(zona)) {
                pacienteSeleccionado.setParroquia(clasificacionesFacade.find(Integer.parseInt(zona)));
            }
            pacienteSeleccionado.setBarrio(barrio.toUpperCase());
            pacienteSeleccionado.setDireccion(direccion.toUpperCase());
            pacienteSeleccionado.setEmail(email);
            
            pacienteSeleccionado.setIdentificacionresponsable(identificacionrepresentante + "");
            if (validarNoVacio(tipoIdentificacionrepresentante)) {
                pacienteSeleccionado.setTipoIdentificacionrepresentante(clasificacionesFacade.find(Integer.parseInt(tipoIdentificacionrepresentante)));
            }
            
            if (validarNoVacio(tipoAfiliado)) {
                pacienteSeleccionado.setTipoAfiliado(clasificacionesFacade.find(Integer.parseInt(tipoAfiliado)));
            }
            
            if (validarNoVacio(etnia)) {
                pacienteSeleccionado.setEtnia(clasificacionesFacade.find(Integer.parseInt(etnia)));
            }
            
            pacienteSeleccionado.setResponsable(responsable.toUpperCase());
            pacienteSeleccionado.setTelefonoResponsable(telefonoResponsable);
            if (validarNoVacio(parentesco)) {
                pacienteSeleccionado.setParentesco(clasificacionesFacade.find(Integer.parseInt(parentesco)));
            }
            if (validarNoVacio(parentesco_a)) {
                pacienteSeleccionado.setParentescoA((clasificacionesFacade.find(Integer.parseInt(parentesco_a))));
            }
            pacienteSeleccionado.setAcompanante(acompanante.toUpperCase());
            pacienteSeleccionado.setTelefonoAcompanante(telefonoAcompanante);
            
            pacienteSeleccionado.setObservaciones(observaciones.toUpperCase());
            
            if(gruposprioritarios!=null){
                if (gruposprioritarios.length > 0) {
                    List<CnGruposprioritarios> listGrupo = gruposFacade.getGruposByIdPaciente(pacienteSeleccionado.getIdPaciente());
                    
                    String[] listgrupo2 = new String[listGrupo.size()];
                    for (int i = 0; i < listGrupo.size(); i++) {
                        listgrupo2[i] = listGrupo.get(i).getCnGruposprioritariosPK().getIdgruposprioritarios().toString();
                    }
                    
                    Collection<String> listOne = Arrays.asList(gruposprioritarios);
                    Collection<String> listTwo = Arrays.asList(listgrupo2);
                    Collection<String> similar = new HashSet<String>( listOne );
                    Collection<String> different = new HashSet<String>();
                    
                    different.addAll( listOne );
                    different.addAll( listTwo );

                    similar.retainAll( listTwo );
                    different.removeAll( similar );
                    
                    Collection<String> eliminar = new HashSet<String>( different );
                    Collection<String> agregar = new HashSet<String>();
                    
                    eliminar.retainAll( listTwo );
                    
                    agregar.addAll( different );
                    agregar.removeAll( eliminar );
                    
                    List<CnGruposprioritarios> cn = new ArrayList<>();
                    
                    for(String c: eliminar){
                        CnGruposprioritarios grupos = gruposFacade.getGruposById(Integer.parseInt(c), pacienteSeleccionado.getIdPaciente());
                        cn.add(grupos);
                    }
                    
                    for(CnGruposprioritarios g: cn){
                        gruposFacade.remove(g);                        
                    }

                    Set<CnGruposprioritarios> grupos = new ArraySet<>();

                    for (String u : agregar){
                        CnGruposprioritarios nuevogruposprioritarios = new CnGruposprioritarios();
                        nuevogruposprioritarios.setCnGruposprioritariosPK(new CnGruposprioritariosPK((clasificacionesFacade.find(Integer.parseInt(u))).getId(), pacienteSeleccionado.getIdPaciente()));
                        grupos.add(nuevogruposprioritarios);
                    }
                    pacienteSeleccionado.setCnGruposprioritariosCollection(grupos);

                } 
            }
            
            pacienteSeleccionado.setUsuarioEditar(indexController.getUsuarioActual().getIdUsuario());
            pacienteSeleccionado.setFechaEditar(pacientesFacade.FechaServidorddmmyyhhmmTimestamp());
            pacientesFacade.edit(pacienteSeleccionado);
            
            transaction.commit();
            
            imprimirMensaje("Correcto", "Paciente actualizado correctamente", FacesMessage.SEVERITY_INFO);
            listPacientes = new LazyPacienteDataModel(pacientesFacade);
            limpiarFormulario();//limpiar formulario
            
            this.init();
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void eliminarPaciente() {
        if (pacienteSeleccionado == null) {
            imprimirMensaje("Error", "No se ha cargado ningún paciente", FacesMessage.SEVERITY_ERROR);
            return;
        }
        try {
            UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
            
            transaction.begin();
            
            List<CnGruposprioritarios> listGrupo = gruposFacade.getGruposByIdPaciente(pacienteSeleccionado.getIdPaciente());
            for(CnGruposprioritarios g: listGrupo){
                gruposFacade.remove(g);
            }
            pacientesFacade.remove(pacienteSeleccionado);
            
            transaction.commit();
            
            pacienteSeleccionado = null;
            listPacientes = new LazyPacienteDataModel(pacientesFacade);
            limpiarFormulario();//limpiar formulario
            imprimirMensaje("Correcto", "El registro fue eliminado", FacesMessage.SEVERITY_INFO);
        } catch (Exception e) {
            imprimirMensaje("Error", "El paciente que se intenta eliminar tiene actividades dentro del sistema; por lo cual no puede ser eliminado.", FacesMessage.SEVERITY_ERROR);
        }
    }


    public LazyDataModel<CnPacientes> getListPacientes() {
        return listPacientes;
    }

    public void setListPacientes(LazyDataModel<CnPacientes> listPacientes) {
        this.listPacientes = listPacientes;
    }

    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public CnPacientes getPacienteSeleccionadoTabla() {
        return pacienteSeleccionadoTabla;
    }

    public void setPacienteSeleccionadoTabla(CnPacientes pacienteSeleccionadoTabla) {
        this.pacienteSeleccionadoTabla = pacienteSeleccionadoTabla;
    }

    public CnPacientes getNuevoPaciente() {
        return nuevoPaciente;
    }

    public void setNuevoPaciente(CnPacientes nuevoPaciente) {
        this.nuevoPaciente = nuevoPaciente;
    }

    public CnPacientesFacade getPacientesFacade() {
        return pacientesFacade;
    }

    public void setPacientesFacade(CnPacientesFacade pacientesFacade) {
        this.pacientesFacade = pacientesFacade;
    }

    public CnClasificacionesFacade getClasificacionesFacade() {
        return clasificacionesFacade;
    }

    public void setClasificacionesFacade(CnClasificacionesFacade clasificacionesFacade) {
        this.clasificacionesFacade = clasificacionesFacade;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
    
    public String getIdentificacionRepresentante() {
        return identificacionrepresentante;
    }

    public void setIdentificacionRepresentante(String identificacionrepresentante) {
        this.identificacionrepresentante = identificacionrepresentante;
    }

    public String getTipoIdentificacionRepresentante() {
        return tipoIdentificacionrepresentante;
    }

    public void setTipoIdentificacionRepresentante(String tipoIdentificacionrepresentante) {
        this.tipoIdentificacionrepresentante = tipoIdentificacionrepresentante;
    }


    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String getOrientacionSexual() {
        return orientacionsexual;
    }

    public void setOrientacionSexual(String orientacionsexual) {
        this.orientacionsexual = orientacionsexual;
    }
    
    public String getIdentidadGenero() {
        return identidadgenero;
    }

    public void setIdentidadGenero(String identidadgenero) {
        this.identidadgenero = identidadgenero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }
    
    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
    public String getNacionalidades() {
        return nacionalidades;
    }

    public void setNacionalidades(String nacionalidades) {
        this.nacionalidades = nacionalidades;
    }


    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public void setGrupoSanguineo(String grupoSanguineo) {
        this.grupoSanguineo = grupoSanguineo;
    }

    public String getEtnia() {
        return etnia;
    }

    public void setEtnia(String etnia) {
        this.etnia = etnia;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoResidencia() {
        return telefonoResidencia;
    }

    public void setTelefonoResidencia(String telefonoResidencia) {
        this.telefonoResidencia = telefonoResidencia;
    }

    public String getTelefonoOficina() {
        return telefonoOficina;
    }

    public void setTelefonoOficina(String telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTipoAfiliado() {
        return tipoAfiliado;
    }

    public void setTipoAfiliado(String tipoAfiliado) {
        this.tipoAfiliado = tipoAfiliado;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }


    public List<SelectItem> getListaCanton() {
        return listaCanton;
    }

    public void setListaCanton(List<SelectItem> listaCanton) {
        this.listaCanton = listaCanton;
    }

    public List<SelectItem> getListaParroquia() {
        return listaParroquia;
    }

    public void setListaParroquia(List<SelectItem> listaParroquia) {
        this.listaParroquia = listaParroquia;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }


    public String getSemanagestacion() {
        return semanagestacion;
    }

    public void setSemanagestacion(String semanagestacion) {
        this.semanagestacion = semanagestacion;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getTelefonoResponsable() {
        return telefonoResponsable;
    }

    public void setTelefonoResponsable(String telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
    }

    public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

    public String getTelefonoAcompanante() {
        return telefonoAcompanante;
    }

    public void setTelefonoAcompanante(String telefonoAcompanante) {
        this.telefonoAcompanante = telefonoAcompanante;
    }

    public String getCategoriaPaciente() {
        return categoriaPaciente;
    }

    public void setCategoriaPaciente(String categoriaPaciente) {
        this.categoriaPaciente = categoriaPaciente;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }


    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTabActiva() {
        return tabActiva;
    }

    public void setTabActiva(String tabActiva) {
        this.tabActiva = tabActiva;
    }

    public String getTituloTab() {
        return tituloTab;
    }

    public void setTituloTab(String tituloTab) {
        this.tituloTab = tituloTab;
    }

   

    public String[] getGruposprioritarios() {
        return gruposprioritarios;
    }
    
    public void setGruposprioritarios(String[] gruposprioritarios) {
        this.gruposprioritarios = gruposprioritarios;
    }

    public String getGestacion() {
        return gestacion;
    }

    public void setGestacion(String gestacion) {
        this.gestacion = gestacion;
    }


    public boolean isNuevoRegistro() {
        return nuevoRegistro;
    }

    public void setNuevoRegistro(boolean nuevoRegistro) {
        this.nuevoRegistro = nuevoRegistro;
    }


    public String getDesBarrio() {
        return desBarrio;
    }

    public void setDesBarrio(String desBarrio) {
        this.desBarrio = desBarrio;
    }

    public String getParentesco_a() {
        return parentesco_a;
    }

    public void setParentesco_a(String parentesco_a) {
        this.parentesco_a = parentesco_a;
    }

    public int getAños() {
        return años;
    }

    public void setAños(int años) {
        this.años = años;
    }

    public boolean isTieneGestacion() {
        return tieneGestacion;
    }

    public void setTieneGestacion(boolean tieneGestacion) {
        this.tieneGestacion = tieneGestacion;
    }

    public boolean isEstaenGestacion() {
        return estaenGestacion;
    }

    public void setEstaenGestacion(boolean estaenGestacion) {
        this.estaenGestacion = estaenGestacion;
    }

    public String getLabelcheck() {
        return labelcheck;
    }

    public void setLabelcheck(String labelcheck) {
        this.labelcheck = labelcheck;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }
    
}

