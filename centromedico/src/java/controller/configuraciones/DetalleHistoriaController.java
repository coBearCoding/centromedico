
package controller.configuraciones;

import util.MetodosGenerales;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import jpa.modelo.CnHistoriaCamposPredefinidos;
import jpa.modelo.HcCamposReg;
import jpa.modelo.HcTipoReg;
import jpa.facade.CnHistoriaCamposPredefinidosFacade;
import jpa.facade.HcCamposRegFacade;
import jpa.facade.HcTipoRegFacade;
import org.primefaces.context.RequestContext;
/**
 *
 * @author kenlly
 */
@Named(value = "detalleHistoriaController")
@ViewScoped
public class DetalleHistoriaController extends MetodosGenerales implements java.io.Serializable{

    
    @EJB
    private CnHistoriaCamposPredefinidosFacade cnHistoriaCamposPredefinidosFacade;
    @EJB
    private HcTipoRegFacade hcTipoRegFacade;
    @EJB
    private HcCamposRegFacade hcCamposRegFacade;
    
    private CnHistoriaCamposPredefinidos cnHistoriaCamposPredefinidos;
    private CnHistoriaCamposPredefinidos campoSeleccionado;
    
    private int id;
    private int idHcTipoReg;
    private int idCampo;
    private String valor;
    private boolean renderAgregar;
    
    private List<HcTipoReg> listaHistoriasClinicas;
    private List<HcCamposReg> listaCamposHistorias;
    private List<CnHistoriaCamposPredefinidos> listaCampos;
    private List<CnHistoriaCamposPredefinidos> listaValores;  
   
    
    
    public DetalleHistoriaController() {
    }
    
    @PostConstruct
    public void init(){
       listaHistoriasClinicas = hcTipoRegFacade.buscarTiposRegstroFichaMedica();
       cnHistoriaCamposPredefinidos = new CnHistoriaCamposPredefinidos();
       listaCamposHistorias= new ArrayList<>();
       listaCampos= cnHistoriaCamposPredefinidosFacade.findAll();
       idHcTipoReg = 0;
       idCampo=0;
       valor="";
       id=0;
       listaValores = new ArrayList<>();
       renderAgregar = false;
    }
    
    public void cargarCamposHistorias(){
        if(idCampo!=0){
            listaValores = cnHistoriaCamposPredefinidosFacade.getCamposDefinidosXHistoriaClinicaXCampo(idHcTipoReg, idCampo);
            renderAgregar = true;
        }else{
            renderAgregar = false;
        }
        
    }
    
    public void nuevo(){
        idHcTipoReg = 0;
        listaCamposHistorias.clear();
        cnHistoriaCamposPredefinidos = new CnHistoriaCamposPredefinidos();
        idHcTipoReg = 0;
        idCampo=0;
        valor="";
        id=0;
        listaCampos= cnHistoriaCamposPredefinidosFacade.findAll();
        renderAgregar =false;
        listaValores= new ArrayList<>();
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
    }

    
    public void editar(CnHistoriaCamposPredefinidos campo){
        id=  campo.getId();
        campoSeleccionado = campo;
        idCampo = campo.getIdCampo().getIdCampo();
        valor = campo.getValor();
        RequestContext.getCurrentInstance().update("formularioNuevoValor");
        RequestContext.getCurrentInstance().update("IdValorNuevo");
        RequestContext.getCurrentInstance().execute("PF('dialogoNuevoValor').show()");
        RequestContext.getCurrentInstance().update("formularioNuevoValor");
    }
    public void eliminar(CnHistoriaCamposPredefinidos campo){
        try {
            cnHistoriaCamposPredefinidosFacade.remove(campo);
            listaValores = cnHistoriaCamposPredefinidosFacade.getCamposDefinidosXHistoriaClinicaXCampo(idHcTipoReg, idCampo);
            RequestContext.getCurrentInstance().update("IdFormPrincipal");
        } catch (Exception e) {
        }
    }
    
    
    
    public void cargar(){
        id= cnHistoriaCamposPredefinidos.getId();
        idCampo = cnHistoriaCamposPredefinidos.getIdCampo().getIdCampo();
        HcCamposReg camposReg = hcCamposRegFacade.find(idCampo);
        idHcTipoReg = camposReg.getIdTipoReg().getIdTipoReg();
        listaCamposHistorias = hcCamposRegFacade.buscarPorTipoRegistro(idHcTipoReg);
        valor = cnHistoriaCamposPredefinidos.getValor();
        RequestContext.getCurrentInstance().execute("PF('dialogoBuscarCampos').hide(); PF('wvTablaCampos').clearFilters(); PF('wvTablaCampos').getPaginator().setPage(0);");
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
    }
    
    public void eliminar(){
        if(cnHistoriaCamposPredefinidos!=null)cnHistoriaCamposPredefinidosFacade.remove(cnHistoriaCamposPredefinidos);
        else imprimirMensaje("Seleccione un campo", "Consulte un campo", FacesMessage.SEVERITY_FATAL);
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
        nuevo();
    }
    
    public void guardar(){
        
        if(validar()){
            try {
                UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");

                transaction.begin();
                
                if(id==0){
                    cnHistoriaCamposPredefinidos = new CnHistoriaCamposPredefinidos();
                    cnHistoriaCamposPredefinidos.setDefaultValor(false);
                    cnHistoriaCamposPredefinidos.setIdCampo(new HcCamposReg(idCampo));
                    cnHistoriaCamposPredefinidos.setValor(valor);
                    cnHistoriaCamposPredefinidosFacade.create(cnHistoriaCamposPredefinidos);
                }else{
                    campoSeleccionado.setValor(valor);
                    cnHistoriaCamposPredefinidosFacade.edit(campoSeleccionado);
                }
                    imprimirMensaje("Guardado", "Guardado Correctamente", FacesMessage.SEVERITY_INFO);
                    
                transaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
                imprimirMensaje("Error al guardar", e.getLocalizedMessage(), FacesMessage.SEVERITY_FATAL);
            }
            
        }
    }
    
    public void setDefault(CnHistoriaCamposPredefinidos predefinido){
        try {
            for(CnHistoriaCamposPredefinidos campo : listaValores){
                campo.setDefaultValor(false);
                cnHistoriaCamposPredefinidosFacade.edit(campo);
            }
            predefinido.setDefaultValor(true);
            cnHistoriaCamposPredefinidosFacade.edit(predefinido);
            RequestContext.getCurrentInstance().update("IdFormPrincipal");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private boolean validar(){
        if(this.idCampo==0){
            imprimirMensaje("Seleccione campo", "Campo Vacío", FacesMessage.SEVERITY_WARN);
            return false;
        }else if(this.valor.equals("")){
            imprimirMensaje("Digite Valor", "Valor Predefinido Vacío", FacesMessage.SEVERITY_WARN);
            return false;
        }
        return true;
    }
    
    public void cancelarValor(){
        valor="";
        RequestContext.getCurrentInstance().update("formularioNuevoValor");
    }
    
    public void cargarCampos(){
        if(idHcTipoReg!=0){
            listaCamposHistorias = hcCamposRegFacade.buscarPorTipoRegistroPredefinido(idHcTipoReg);
        }else {
            renderAgregar = false;
            listaCamposHistorias.clear();
        }
    }
    
    
    
    public void guardarValor(){
        
        guardar();
        valor="";
        listaValores = cnHistoriaCamposPredefinidosFacade.getCamposDefinidosXHistoriaClinicaXCampo(idHcTipoReg, idCampo);
        RequestContext.getCurrentInstance().update("IdFormPrincipal");
        RequestContext.getCurrentInstance().update("formularioNuevoValor");
        RequestContext.getCurrentInstance().execute("PF('dialogoNuevoValor').hide()");
    }
    
    
    
    public List<HcTipoReg> getListaHistoriasClinicas() {
        return listaHistoriasClinicas;
    }

    public void setListaHistoriasClinicas(List<HcTipoReg> listaHistoriasClinicas) {
        this.listaHistoriasClinicas = listaHistoriasClinicas;
    }

    public HcTipoRegFacade getHcTipoRegFacade() {
        return hcTipoRegFacade;
    }

    public void setHcTipoRegFacade(HcTipoRegFacade hcTipoRegFacade) {
        this.hcTipoRegFacade = hcTipoRegFacade;
    }

    public List<HcCamposReg> getListaCamposHistorias() {
        return listaCamposHistorias;
    }

    public void setListaCamposHistorias(List<HcCamposReg> listaCamposHistorias) {
        this.listaCamposHistorias = listaCamposHistorias;
    }
    
     public List<CnHistoriaCamposPredefinidos> getListaCampos() {
        return listaCampos;
    }

    public void setListaCampos(List<CnHistoriaCamposPredefinidos> listaCampos) {
        this.listaCampos = listaCampos;
    }

    public CnHistoriaCamposPredefinidos getCnHistoriaCamposPredefinidos() {
        return cnHistoriaCamposPredefinidos;
    }

    public void setCnHistoriaCamposPredefinidos(CnHistoriaCamposPredefinidos cnHistoriaCamposPredefinidos) {
        this.cnHistoriaCamposPredefinidos = cnHistoriaCamposPredefinidos;
    }
    
    public List<CnHistoriaCamposPredefinidos> getListaValores() {
        return listaValores;
    }

    public void setListaValores(List<CnHistoriaCamposPredefinidos> listaValores) {
        this.listaValores = listaValores;
    }

    public int getIdHcTipoReg() {
        return idHcTipoReg;
    }

    public void setIdHcTipoReg(int idHcTipoReg) {
        this.idHcTipoReg = idHcTipoReg;
    }  

    public int getIdCampo() {
        return idCampo;
    }

    public void setIdCampo(int idCampo) {
        this.idCampo = idCampo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    public boolean isRenderAgregar() {
        return renderAgregar;
    }

    public void setRenderAgregar(boolean renderAgregar) {
        this.renderAgregar = renderAgregar;
    } 
    
}
