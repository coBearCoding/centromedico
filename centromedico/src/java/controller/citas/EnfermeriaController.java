
package controller.citas;

import util.LazyAgendaModel;
import util.MetodosGenerales;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import controller.seguridad.IndexController;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author kenlly
 */
@ManagedBean(name = "enfermeriaController")
@SessionScoped
public class EnfermeriaController extends MetodosGenerales implements Serializable {

    private LazyAgendaModel evenModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private String nombreCompleto;
    private boolean rend = false;
    private boolean haySesionEnfermeria;
    private boolean rendBtnSignosVitales = false;
    private boolean rendBtnAtendida = false;
    private boolean rendAgenda;
    private String idTurno;
    private String minTime;
    private String maxTime;

    private CnUsuarios enfermeroActual;
    private CtCitas citCita;
    private CtTurnos ctTurnos;
    
    @EJB
    CtCitasFacade citasFacade;

    @EJB
    CtTurnosFacade turnosfacade;

  

    @ManagedProperty(value = "#{indexController}")
    private IndexController indexController;


    public EnfermeriaController() {
    }

    @PostConstruct
    private void init() {
         setRendAgenda(false);
        if (getIndexController().getUsuarioActual().getTipoUsuario().getCodigo().compareTo("2") == 0) {
            setEnfermeroActual(getIndexController().getUsuarioActual());
            setHaySesionEnfermero(true);
        } else if(getIndexController().getUsuarioActual().getIdPerfil().getNombrePerfil().matches("Enfermera")){
            setEnfermeroActual(getIndexController().getUsuarioActual());
            setHaySesionEnfermero(true);
        }else{
            setEnfermeroActual(null);
            setHaySesionEnfermero(false);
            imprimirMensaje("Error", "No eres de enfermeria para ver el contenido", FacesMessage.SEVERITY_ERROR);
        }
        if (enfermeroActual != null) {
            getEnfermeroActual().getNombreCompleto();
        } else {
            evenModel = null;
        }
    }

   public void loadEvents() {
       if (enfermeroActual != null){
           evenModel = new LazyAgendaModel(enfermeroActual.getIdUsuario(), turnosfacade, citasFacade, "enfermeriaTurnos");
            setRendAgenda(true);
       }
        
        
    }



    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        if (event.getTitle() != null) {
            String[] vector = event.getTitle().split(" - ");
            idTurno = vector[0];
            seleccionarCita(Integer.parseInt(idTurno));
            if (citCita != null) {
                RequestContext.getCurrentInstance().execute("PF('eventDialog').show()");
            }
        }
    }

    private void seleccionarCita(Integer id) {
        CtCitas cita = citasFacade.findCitasByTurno(id);
        setCitCita(cita);
        idTurno = cita.getIdTurno().getContador().toString();
        if (cita != null) {
            rend = true;
            if (cita.getAtendida()) {
                setRendBtnSignosVitales(true);
                setRendBtnAtendida(false);
            } else if (cita.getIdTurno().getEstado().equals("en_espera")||cita.getIdTurno().getEstado().equals("asignado")) {
                setRendBtnAtendida(true);
                setRendBtnSignosVitales(true);
            } else {
                setRendBtnSignosVitales(false);
                setRendBtnAtendida(false);
            }
        } else {
            rend = false;
            setRendBtnSignosVitales(false);
            setRendBtnAtendida(false);
        }
        RequestContext.getCurrentInstance().update("formAgendaEnfermeria:pdialog");

    }

    public void openHistoriaClinica() {
        if(citCita.getIdTurno().getEstado().matches("en_espera")){
            imprimirMensaje("Error", "Registro de Signos Vitales ya creado", FacesMessage.SEVERITY_ERROR);
            return;
        }
        
        
        RequestContext.getCurrentInstance().execute("window.parent.loadTab('Signos Vitales','historiaclinica/historias_signosvitales.xhtml','idCita;" + citCita.getIdCita().toString() + "')");
    }

     
    public LazyAgendaModel getEvenModel() {
        return evenModel;
    }

    public void setEvenModel(LazyAgendaModel evenModel) {
        this.evenModel = evenModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public CnUsuarios getEnfermeroActual() {
        return enfermeroActual;
    }

    public void setEnfermeroActual(CnUsuarios enfermeroActual) {
        this.enfermeroActual = enfermeroActual;
    }
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

   

    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }

    public CtCitas getCitCita() {
        return citCita;
    }

    public void setCitCita(CtCitas citCita) {
        this.citCita = citCita;
    }

    public CtTurnos getCtTurnos() {
        return ctTurnos;
    }

    public void setCtTurnos(CtTurnos ctTurnos) {
        this.ctTurnos = ctTurnos;
    }

    public boolean isHaySesionEnfermero() {
        return haySesionEnfermeria;
    }

    public void setHaySesionEnfermero(boolean haySesionEnfermeria) {
        this.haySesionEnfermeria = haySesionEnfermeria;
    }
    
    public boolean isRendBtnAtendida() {
        return rendBtnAtendida;
    }

    public void setRendBtnAtendida(boolean rendBtnAtendida) {
        this.rendBtnAtendida = rendBtnAtendida;
    }

    public String getMinTime() {
        return minTime;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public boolean isRendAgenda() {
        return rendAgenda;
    }

    public void setRendAgenda(boolean rendAgenda) {
        this.rendAgenda = rendAgenda;
    }

    public boolean isRendBtnSignosVitales() {
        return rendBtnSignosVitales;
    }

    public void setRendBtnSignosVitales(boolean rendBtnSignosVitales) {
        this.rendBtnSignosVitales = rendBtnSignosVitales;
    }

   

    public String getIdTurno() {
        return idTurno;
    }

}

