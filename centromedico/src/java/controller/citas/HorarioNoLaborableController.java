
package controller.citas;

import util.MetodosGenerales;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import jpa.modelo.CnDiasNoLaborales;
import jpa.facade.CnDiasNoLaboralesFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "horarioNoLaborableController")
@SessionScoped
public class HorarioNoLaborableController extends MetodosGenerales implements Serializable {

    private String display = "none";
    private Date fechaNoHabil;
    private final int todas = 0;
    private List<CnDiasNoLaborales> listaDiasNoHabiles;

    @EJB
    CnDiasNoLaboralesFacade cnDiasNoLaboralesFacade;



    public HorarioNoLaborableController() {
    }

    public Date getFechaNoHabil() {
        return fechaNoHabil;
    }

   

    public void validarDiaSeleccionado(SelectEvent selectEvent) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
      
        if (cnDiasNoLaboralesFacade.FindDiaNoLaboral( fechaNoHabil) != null) {
            setDisplay("none");
            Date aux = fechaNoHabil;
            setFechaNoHabil(null);
            RequestContext.getCurrentInstance().update("idFormDiasNoLaborales");
            imprimirMensaje("Error", format.format(aux) + " ya se encuentra adicionada", FacesMessage.SEVERITY_ERROR);
           
        } else {
            setDisplay("block");
            RequestContext.getCurrentInstance().update("idFormDiasNoLaborales:idButton");
        }

    }

    public void insertarDia() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        setDisplay("none");
        CnDiasNoLaborales diasNoLaborales = new CnDiasNoLaborales(1, fechaNoHabil);
        cnDiasNoLaboralesFacade.create(diasNoLaborales);
        Date aux = fechaNoHabil;
        cargarDiasNoHabiles();
        imprimirMensaje("Correcto", format.format(aux) + " se ha adicionado", FacesMessage.SEVERITY_INFO);
    }

    public void eliminarDia(ActionEvent actionEvent) {
        Date fecha = (Date) actionEvent.getComponent().getAttributes().get("fecha");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        CnDiasNoLaborales diaNoLaboral = cnDiasNoLaboralesFacade.FindDiaNoLaboral(fecha);
        cnDiasNoLaboralesFacade.remove(diaNoLaboral);
        cargarDiasNoHabiles();
        imprimirMensaje("Correcto", format.format(fecha) + " se ha eliminado", FacesMessage.SEVERITY_INFO);
    }

    public void cargarDiasNoHabiles() {
        setFechaNoHabil(null);
        setListaDiasNoHabiles(cnDiasNoLaboralesFacade.diasNoLaborables());
        setDisplay("none");
        RequestContext.getCurrentInstance().update("idFormDiasNoLaborales");
    }

    public void setFechaNoHabil(Date fechaNoHabil) {
        this.fechaNoHabil = fechaNoHabil;
    }
 
    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public List<CnDiasNoLaborales> getListaDiasNoHabiles() {
        return listaDiasNoHabiles;
    }

    public void setListaDiasNoHabiles(List<CnDiasNoLaborales> listaDiasNoHabiles) {
        this.listaDiasNoHabiles = listaDiasNoHabiles;
    }

    public int getTodas() {
        return todas;
    }

}

