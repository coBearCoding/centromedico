
package controller.citas;

import util.MetodosGenerales;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import jpa.modelo.CnHorario;
import jpa.facade.CnHorarioFacade;
import org.primefaces.context.RequestContext;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "horarioController")
@SessionScoped
public class HorarioController extends MetodosGenerales implements Serializable {


    private String descripcion;
    private List<CnHorario> cnHorarios;
    private List<SelectItem> listahorarios;
    private CnHorario horarioseleccionado;
    private boolean renderizar = true;

    @EJB
    CnHorarioFacade cnHorarioFacade;

    @ManagedProperty(value = "#{detalleHorarioController}")
    private DetalleHorarioController detalleHorarioController;

 
    public HorarioController() {
    }

    @PostConstruct
    public void init() {
        cargarItems();
    }

    private void cargarItems() {
        setListaCnHorario(cnHorarioFacade.findAll());
        listahorarios = new ArrayList<>();
        for (CnHorario cnHorario : cnHorarios) {
            listahorarios.add(new SelectItem(cnHorario.getIdHorario(), cnHorario.getDescripcion()));
        }
        setListaHorarios(listahorarios);
    }

    private boolean turnoExiste() {
        boolean ban = false;
        setHorarioseleccionado(cnHorarioFacade.buscarPorDescripcion(descripcion));
        if (getHorarioseleccionado() != null) {
            ban = true;
        }
        return ban;
    }

    public void validarNombreHorario(ActionEvent actionEvent) {
        if (!descripcion.isEmpty()) {
            if (turnoExiste()) {
                imprimirMensaje("Información", "Ya existe un horario con ese nombre", FacesMessage.SEVERITY_ERROR);
                RequestContext.getCurrentInstance().update("formcnhorario");
            } else {
                setRenderizar(false);
                detalleHorarioController.setListaDetalleHorario(new ArrayList());
                detalleHorarioController.setRenderizarAddItems(true);
                detalleHorarioController.setTablaEditable(true);
                detalleHorarioController.setDisplay("block");
                detalleHorarioController.setIndex(0);
                RequestContext.getCurrentInstance().update("formcnhorario");                
            }
        } else {
            imprimirMensaje("Información", "Es necesario registrar el nombre del horario", FacesMessage.SEVERITY_ERROR);
            detalleHorarioController.setDisplay("none");
        }
    }

    public void guardarHorario() {
            CnHorario cnHorario = new CnHorario();
            cnHorario.setDescripcion(descripcion);
            cnHorarioFacade.create(cnHorario);
            horarioseleccionado = cnHorario;
            detalleHorarioController.setHorarioseleccionado(horarioseleccionado);
    }

    public void validar() {
        if (turnoExiste()) {
            setDescripcion(getHorarioseleccionado().getDescripcion());
            detalleHorarioController.setHorarioseleccionado(horarioseleccionado);
            detalleHorarioController.cargarListadoHorario();
        } else {
            setDescripcion(null);
            detalleHorarioController.setListaDetalleHorario(null);
        }

    }

    public void validarHorario(CnHorario horario) {
        if (horario != null) {
            setDescripcion(horario.getDescripcion());
            detalleHorarioController.setHorarioseleccionado(horario);
            detalleHorarioController.cargarListadoHorario();
            detalleHorarioController.setDisplay("none");
            setRenderizar(false);
        } else {
            detalleHorarioController.setDisplay("none");
            setDescripcion(null);
            setRenderizar(true);
            detalleHorarioController.setListaDetalleHorario(null);
        }
    }
  
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public List<CnHorario> getListaCnHorario() {
        return cnHorarios;
    }

    public void setListaCnHorario(List<CnHorario> listaCnHorario) {
        this.cnHorarios = listaCnHorario;
    }

    public List<SelectItem> getListaHorarios() {
        return listahorarios;
    }

    public void setListaHorarios(List<SelectItem> listaHorario) {
        this.listahorarios = listaHorario;
    }

    public CnHorario getHorarioseleccionado() {
        return horarioseleccionado;
    }

    public void setHorarioseleccionado(CnHorario horarioseleccionado) {
        this.horarioseleccionado = horarioseleccionado;
    }


    public DetalleHorarioController getDetalleHorarioController() {
        return detalleHorarioController;
    }
  

    public void setDetalleHorarioController(DetalleHorarioController detalleHorarioController) {
        this.detalleHorarioController = detalleHorarioController;
    }

    public boolean isRenderizar() {
        return renderizar;
    }

    public void setRenderizar(boolean renderizar) {
        this.renderizar = renderizar;
    }

}

