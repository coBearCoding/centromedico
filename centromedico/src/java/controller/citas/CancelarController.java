
package controller.citas;

import util.LazyCitasDataModel;
import util.LazyPacienteDataModel;
import util.LazyMedicoDataModel;
import util.MetodosGenerales;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.IndexController;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnPacientesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;


@ManagedBean(name = "cancelarController")
@SessionScoped
public class CancelarController extends MetodosGenerales implements Serializable {

    private String ppseleccionada;
    private String descripcion;
    private int motivo;
    private CnUsuarios medicoSeleccionado;
    private CnPacientes pacienteSeleccionado;
    private CtCitas citaSeleccionada;
    private LazyDataModel<CtCitas> listCitas;    
    private LazyDataModel<CnUsuarios> listMedicos;
    private LazyDataModel<CnPacientes> listPacientes;
    private List<SelectItem> listEspecialidades;

    @EJB
    CtCitasFacade citasFacade;
    @EJB
    CnClasificacionesFacade clasificacionesFacade;
    @EJB
    CnPacientesFacade pacientesFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    CtTurnosFacade turnosFacade;

    @ManagedProperty(value = "#{indexController}")
    private IndexController indexController;

    

    public CancelarController() {
    }

    @PostConstruct
    private void init() {        
        setListPacientes(new LazyPacienteDataModel(pacientesFacade));
        setListMedicos(new LazyMedicoDataModel(usuariosFacade));
        cargarEspecialidadesMedicos();
        setListCitas(new LazyCitasDataModel(citasFacade, "nulo", 0));
    }
    //carga de especialidades de los medicos
    private void cargarEspecialidadesMedicos() {
        setListEspecialidades((List<SelectItem>) new ArrayList());
        List<CnClasificaciones> lista = usuariosFacade.findEspecialidades();
        for (CnClasificaciones especialidad : lista) {
            if (especialidad != null) {
                getListEspecialidades().add(new SelectItem(especialidad.getId(), especialidad.getDescripcion()));
            }
        }
    }
    //busqueda de la cita seleccionada
    public void findCita(ActionEvent actionEvent) {
        int id = (int) actionEvent.getComponent().getAttributes().get("id_cita");
        citaSeleccionada = citasFacade.findCitaById(id);
    }
    
    //obtener medico o paciente seleccionado
    public void asignarSeleccionado(ActionEvent actionEvent) {
        ppseleccionada = (String) actionEvent.getComponent().getAttributes().get("seleccionado");
    }
    //carga de lista del medico o paciente seleccionado
    public void cargarListaCitas() {
        switch (getPpseleccionada()) {
            case "paciente":
                setMedicoSeleccionado(null);
                LazyDataModel<CtCitas>  lista = new LazyCitasDataModel(citasFacade, ppseleccionada, getPacienteSeleccionado().getIdPaciente());
                setListCitas(lista);
                break;
            case "medico":
                setPacienteSeleccionado(null);
                setListCitas(new LazyCitasDataModel(citasFacade, ppseleccionada, getMedicoSeleccionado().getIdUsuario()));
                break;
        }
        RequestContext.getCurrentInstance().update("formTablaCitas");
    }
    //cancelar cita seleccionada
    public void cancelarCita(ActionEvent actionEvent) {
        if (!citaSeleccionada.getCancelada() && !citaSeleccionada.getAtendida()) {
            try {
                UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
                
                transaction.begin();
                
                citaSeleccionada.setCancelada(true);
                CnClasificaciones clasificaciones = clasificacionesFacade.find(motivo);
                citaSeleccionada.setDescCancelacion(descripcion);
                citaSeleccionada.setMotivoCancelacion(clasificaciones);
                citaSeleccionada.setFechaCancelacion(citasFacade.FechaServidorddmmyyhhmmTimestamp());
                citaSeleccionada.setIdUsuarioCancelacion(getIndexController().getUsuarioActual().getIdUsuario());
                citasFacade.edit(citaSeleccionada);
                
                CtTurnos turno = citaSeleccionada.getIdTurno();
                turno.setEstado("disponible");
                turnosFacade.edit(turno);

                transaction.commit();
                imprimirMensaje("Correcto", "Cita " + citaSeleccionada.getIdTurno().getContador().toString() + " cancelada", FacesMessage.SEVERITY_INFO);

                setListCitas(listCitas);
            
            } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
                Logger.getLogger(CancelarController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            imprimirMensaje("Error", "Cita " + citaSeleccionada.getIdCita() + " ya se encuentra atendida o cancelada", FacesMessage.SEVERITY_ERROR);
        }
        descripcion = null;

    }

    public String getPpseleccionada() {
        return ppseleccionada;
    }

    public void setPpseleccionada(String ppseleccionada) {
        this.ppseleccionada = ppseleccionada;
    }

    public LazyDataModel<CtCitas> getListCitas() {
        return listCitas;
    }

    public void setListCitas(LazyDataModel<CtCitas> listCitas) {
        this.listCitas = listCitas;
    }
    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public CnUsuarios getMedicoSeleccionado() {
        return medicoSeleccionado;
    }

    public void setMedicoSeleccionado(CnUsuarios medicoSeleccionado) {
        this.medicoSeleccionado = medicoSeleccionado;
    }

    public LazyDataModel<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(LazyDataModel<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public LazyDataModel<CnPacientes> getListPacientes() {
        return listPacientes;
    }

    public void setListPacientes(LazyDataModel<CnPacientes> listPacientes) {
        this.listPacientes = listPacientes;
    }

    public List<SelectItem> getListEspecialidades() {   
        return listEspecialidades;
    }
    public void setListEspecialidades(List<SelectItem> listEspecialidades) {
        this.listEspecialidades = listEspecialidades;
    }

    public int getMotivo() {
        return motivo;
    }

    public void setMotivo(int motivo) {
        this.motivo = motivo;
    }

    public CtCitas getCitaSeleccionada() {
        return citaSeleccionada;
    }

    public void setCitaSeleccionada(CtCitas citaSeleccionada) {
        this.citaSeleccionada = citaSeleccionada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

    

}
