
package controller.citas;

import util.MetodosGenerales;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import jpa.modelo.CnHorario;
import jpa.modelo.CnItemsHorario;
import jpa.facade.CnItemsHorarioFacade;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "detalleHorarioController")
@SessionScoped
public class DetalleHorarioController extends MetodosGenerales implements Serializable {

    private Date horaIni;
    private Date horaFin;
    private Short[] selectedDays;
    private String display = "none";
    private List<CnItemsHorario> listaDetalleHorario;
    private CnHorario horarioseleccionado;
    private int index;
    private boolean renderizar = false;
    private boolean renderizarAddItems = true;
    private boolean tablaEditable = true;

    @EJB
    private CnItemsHorarioFacade cnItemsHorarioFacade;

    public DetalleHorarioController() {
    }

    @PostConstruct
    public void init() {
        listaDetalleHorario = new ArrayList();
        setIndex(0);
    }

    public void addItemHorario() {

            if (selectedDays.length != 0) {

                if (horaIni != null && horaFin != null && horaFin.after(horaIni)) {
                    for (Short selectedDay : selectedDays) {
                        CnItemsHorario h = new CnItemsHorario();
                        h.setIdItemHorario(getIndex());
                        h.setIdHorario(horarioseleccionado);
                        h.setDia(selectedDay);
                        h.setNombredia(asignarDia(selectedDay));
                        h.setHoraInicio(horaIni);
                        h.setHoraFinal(horaFin);
                        listaDetalleHorario.add(h);
                        setIndex(getIndex() + 1);
                    }
                    setListaDetalleHorario(listaDetalleHorario);
                    setRenderizar(true);
                }else{
                    imprimirMensaje("Error", "Ingrese la hora inicial y final", FacesMessage.SEVERITY_ERROR);
                }
            } else {
                imprimirMensaje("Error", "Eliga al menos un dia laboral", FacesMessage.SEVERITY_ERROR);
                if (listaDetalleHorario.size() > 0) {
                    setRenderizar(true);
                } else {
                    setRenderizar(false);
                }
            }
    }
    
    public void removeItem(ActionEvent actionEvent){
        int id_item = (int) actionEvent.getComponent().getAttributes().get("id_item");
        listaDetalleHorario.remove(id_item);
        
    }

    public void guardarItems() {
        if (horarioseleccionado != null) {
            if (listaDetalleHorario.size() > 0) {
                for (CnItemsHorario cnItemsHorario : listaDetalleHorario) {
                    cnItemsHorario.setIdItemHorario(null);
                    cnItemsHorario.setIdHorario(horarioseleccionado);
                    cnItemsHorarioFacade.create(cnItemsHorario);
                }
                imprimirMensaje("Correcto", "Se ha creado el horario satisfactoriamente", FacesMessage.SEVERITY_INFO);
                setRenderizarAddItems(false);
                setTablaEditable(false);
                setRenderizar(false);

            } else {
                imprimirMensaje("Error", "Eliga al menos un dia laboral", FacesMessage.SEVERITY_ERROR);
            }
        } else {
            imprimirMensaje("Error", "Es necesario que un horario este creado", FacesMessage.SEVERITY_ERROR);
        }
    }

    public void cargarListadoHorario() {
        setListaDetalleHorario(cnItemsHorarioFacade.findHorarioByIdCnTurno(horarioseleccionado.getIdHorario()));
    }

    private String asignarDia(Short dia) {
        String nombreDia = "";
        switch (dia) {
            case 0:
                nombreDia = "Domingo";
                break;
            case 1:
                nombreDia = "Lunes";
                break;
            case 2:
                nombreDia = "Martes";
                break;
            case 3:
                nombreDia = "Miercoles";
                break;
            case 4:
                nombreDia = "Jueves";
                break;
            case 5:
                nombreDia = "Viernes";
                break;
            case 6:
                nombreDia = "Sabado";
                break;
        }
        return nombreDia;
    }

    public CnHorario getHorarioseleccionado() {
        return horarioseleccionado;
    }

    public void setHorarioseleccionado(CnHorario horarioseleccionado) {
        this.horarioseleccionado = horarioseleccionado;
    }

    public Date getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(Date horaIni) {
        this.horaIni = horaIni;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public Short[] getSelectedDays() {
        return selectedDays;
    }

    public void setSelectedDays(Short[] selectedDays) {
        this.selectedDays = selectedDays;
    }

    public List<CnItemsHorario> getListaDetalleHorario() {
        return listaDetalleHorario;
    }

    public void setListaDetalleHorario(List<CnItemsHorario> listaDetalleHorario) {
        this.listaDetalleHorario = listaDetalleHorario;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public boolean isRenderizar() {
        return renderizar;
    }

    public void setRenderizar(boolean renderizar) {
        this.renderizar = renderizar;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isRenderizarAddItems() {
        return renderizarAddItems;
    }

    public void setRenderizarAddItems(boolean renderizarAddItems) {
        this.renderizarAddItems = renderizarAddItems;
    }

    public boolean isTablaEditable() {
        return tablaEditable;
    }

    public void setTablaEditable(boolean tablaEditable) {
        this.tablaEditable = tablaEditable;
    }

}

