
package controller.citas;

import beans.utilidades.SesionTfU;
import beans.utilidades.SesionTrU;
import jpa.modelo.CtCitas;
import jpa.facade.CtCitasFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import util.CitaReporte;
import util.LazyPacienteDataModel;
import util.LazyMedicoDataModel;
import util.MetodosGenerales;
import java.text.SimpleDateFormat;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import controller.seguridad.IndexController;
import jpa.modelo.CnEmpresa;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.facade.CnEmpresaFacade;
import jpa.facade.CnPacientesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.HcSesionesTrFacade;
import jpa.facade.HcTfSesionesFacade;
import jpa.modelo.HcSesionesTr;
import jpa.modelo.HcTfSesiones;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author kenlly
 */


@ManagedBean(name = "reporteCitMedicaController")
@ViewScoped
public class ReporteCitMedicaController extends MetodosGenerales implements Serializable {

    private String tipoReporte;
    private String identificacionPaciente;
    private Date fechaInicial;
    private Date fechaFinal;
    private CnPacientes pacienteSeleccionado;
    private CnPacientes paciente;
    private CnUsuarios medicoSeleccionado;

    private List<CnUsuarios> listMedicos;
    private List<CnPacientes> listPacientes;

    private List<SelectItem> listaReportes;

    private LazyDataModel<CnUsuarios> medicos;
    private LazyDataModel<CnPacientes> pacientes;
    private boolean renBtnFiltrar;
    private boolean renBtnReporte;
    private String displayBusquedaPaciente = "none";
    private LazyDataModel<CnPacientes> listPacientesBusqueda;

    @EJB
    CnPacientesFacade pacientesFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;    
    @EJB
    CtCitasFacade citasFacade;
    @EJB
    CnEmpresaFacade cnEmpresaFacade;  
    @EJB
    HcTfSesionesFacade sesionesTfFacade;    
    @EJB
    HcSesionesTrFacade sesionesTrFacade;

    public ReporteCitMedicaController() {
    }

    @PostConstruct
    public void init() {

        listPacientes = new ArrayList();
        listMedicos = new ArrayList();
        setListPacientesBusqueda(new LazyPacienteDataModel(pacientesFacade));
        listaReportes = new ArrayList();

        setRenBtnFiltrar(true);
        setRenBtnReporte(true);
        crearMenuReportes();
        setPacientes(new LazyPacienteDataModel(pacientesFacade));
        setMedicos(new LazyMedicoDataModel(usuariosFacade));
        IndexController indexController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{indexController}", IndexController.class);

    }

    private void crearMenuReportes() {

        listaReportes.add(new SelectItem(1, "Citas Asignadas"));
        listaReportes.add(new SelectItem(2, "Citas Canceladas"));
        listaReportes.add(new SelectItem(3, "Historial Citas"));
        listaReportes.add(new SelectItem(4, "Historial Sesiones Terapia Fisica"));
        listaReportes.add(new SelectItem(5, "Historial Sesiones Terapia Respiratoria"));

    }

    public void habilitarFuncionalidadReporte() {
        setIdentificacionPaciente(null);
        setPaciente(null);

        if (tipoReporte.equals("3")) {
            setRenBtnFiltrar(false);
            setRenBtnReporte(false);
            setDisplayBusquedaPaciente("block");

        } else {
            setRenBtnFiltrar(true);
            setRenBtnReporte(true);
            setDisplayBusquedaPaciente("none");
        }

    }

    public void findPaciente() {
        if (!identificacionPaciente.isEmpty()) {
            paciente = pacientesFacade.buscarPorIdentificacion(getIdentificacionPaciente());
            if (paciente == null) {
                setRenBtnReporte(false);
                imprimirMensaje("Error", "No se encontro el paciente", FacesMessage.SEVERITY_ERROR);
            } else {
                setRenBtnReporte(true);
            }
        } else {
            setPacienteSeleccionado(null);
            setRenBtnReporte(false);
        }
    }

    public void actualizarPaciente() {
        if (paciente != null) {
            setIdentificacionPaciente(paciente.getIdentificacion());
            setRenBtnReporte(true);
        }
    }

    private List<CitaReporte> generarCitaList(List<CtCitas> citas) {
        List<CitaReporte> listaCitaReporte = new ArrayList();
        if (citas != null) {
            for (CtCitas cita : citas) {
                CitaReporte citaReporte = new CitaReporte();
                List<CnEmpresa> listaempresa = cnEmpresaFacade.findAll();
                citaReporte.setEmpresa(listaempresa.get(0).getRazonSocial());
                citaReporte.setEmpresaDireccion(listaempresa.get(0).getDireccion());
                citaReporte.setEmpresaTelefono(listaempresa.get(0).getTelefono1());
                citaReporte.setIdCita(cita.getIdCita());
                citaReporte.setNoTurno(cita.getIdTurno().getContador().toString());
                citaReporte.setFecha(cita.getIdTurno().getFecha());
                citaReporte.setHora(cita.getIdTurno().getHoraIni());
                citaReporte.setFechaimpresion(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(usuariosFacade.FechaServidorddmmyyhhmmTimestamp()));
                citaReporte.setMedicoPN(cita.getIdMedico().nombreCompleto());
                citaReporte.setPacientePN(cita.getIdPaciente().nombreCompleto());
                citaReporte.setSubsecuente(cita.getSubsecuente() == true ? "SI" : "NO");
                citaReporte.setPacienteTipoDoc(cita.getIdPaciente().getTipoIdentificacion().getObservacion());
                citaReporte.setPacienteNumDoc(cita.getIdPaciente().getIdentificacion());
                
                if (cita.getCancelada()) {
                    citaReporte.setCancelada(true);
                    citaReporte.setFechaCancelacion(cita.getFechaCancelacion());
                    citaReporte.setMotivoCancelacion(cita.getMotivoCancelacion().getDescripcion());
                } else {
                    citaReporte.setCancelada(false);
                    citaReporte.setFechaCancelacion(null);
                    citaReporte.setMotivoCancelacion("");
                }
                citaReporte.setAtendida(cita.getAtendida());
                listaCitaReporte.add(citaReporte);

            }
        }
        return listaCitaReporte;
    }
    
    private List<SesionTfU> generarCitasAuxiliarTf(List<HcTfSesiones> citas) {
        List<SesionTfU> listaCitasU = new ArrayList();
        if (citas != null) {
            for (HcTfSesiones cita : citas) {
                SesionTfU sesionTfU = new SesionTfU();
                List<CnEmpresa> listaempresa = cnEmpresaFacade.findAll();
                //solo existira una empresa => intranet
                sesionTfU.setEmpresa(listaempresa.get(0).getRazonSocial());
                sesionTfU.setEmpresaDireccion(listaempresa.get(0).getDireccion());
                sesionTfU.setEmpresaTelefono(listaempresa.get(0).getTelefono1());
                sesionTfU.setObservaciones(listaempresa.get(0).getObservaciones());
                sesionTfU.setFecha(cita.getFechaReg());
                //sesionTfU.setHora(cita.getIdTurno().getHoraIni());
                sesionTfU.setFechaimpresion(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(usuariosFacade.FechaServidorddmmyyhhmmTimestamp()));
                // citaU.setConsultorio(cita.getIdTurno().getIdConsultorio().getNomConsultorio());
                sesionTfU.setIdPaciente(cita.getIdPaciente().getIdPaciente());
                sesionTfU.setPrestadorPN(cita.getIdMedico().nombreCompleto());
                sesionTfU.setPrestadorEspecialidad(cita.getIdMedico().getEspecialidad().getDescripcion());
                sesionTfU.setIdPrestador(cita.getIdMedico().getIdUsuario());
                sesionTfU.setPacientePN(cita.getIdPaciente().nombreCompleto());
                sesionTfU.setFechaRegistro(cita.getFechaReg());
                //sesionTfU.setSubsecuente(cita.getSubsecuente() == true ? "SI" : "NO");
                //citaU.setPacienteTipoDoc(cita.getIdPaciente().getTipoIdentificacion().getDescripcion());
                sesionTfU.setPacienteTipoDoc(cita.getIdPaciente().getTipoIdentificacion().getObservacion());
                sesionTfU.setPacienteNumDoc(cita.getIdPaciente().getIdentificacion());

                sesionTfU.setAtendida(cita.getRealizado());
                listaCitasU.add(sesionTfU);

            }
        }
        return listaCitasU;
    }
    
    private List<SesionTrU> generarCitasAuxiliarTr(List<HcSesionesTr> citas) {
        List<SesionTrU> listaCitasU = new ArrayList();
        if (citas != null) {
            for (HcSesionesTr cita : citas) {
                SesionTrU sesionTrU = new SesionTrU();
                List<CnEmpresa> listaempresa = cnEmpresaFacade.findAll();
                //solo existira una empresa => intranet
                sesionTrU.setEmpresa(listaempresa.get(0).getRazonSocial());
                sesionTrU.setEmpresaDireccion(listaempresa.get(0).getDireccion());
                sesionTrU.setEmpresaTelefono(listaempresa.get(0).getTelefono1());
                sesionTrU.setObservaciones(listaempresa.get(0).getObservaciones());
                sesionTrU.setFecha(cita.getFechaReg());
                //sesionTfU.setHora(cita.getIdTurno().getHoraIni());
                sesionTrU.setFechaimpresion(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(usuariosFacade.FechaServidorddmmyyhhmmTimestamp()));
                // citaU.setConsultorio(cita.getIdTurno().getIdConsultorio().getNomConsultorio());
                sesionTrU.setIdPaciente(cita.getIdPaciente().getIdPaciente());
                sesionTrU.setPrestadorPN(cita.getIdMedico().nombreCompleto());
                sesionTrU.setPrestadorEspecialidad(cita.getIdMedico().getEspecialidad().getDescripcion());
                sesionTrU.setIdPrestador(cita.getIdMedico().getIdUsuario());
                sesionTrU.setPacientePN(cita.getIdPaciente().nombreCompleto());
                sesionTrU.setFechaRegistro(cita.getFechaReg());
                //sesionTfU.setSubsecuente(cita.getSubsecuente() == true ? "SI" : "NO");
                //citaU.setPacienteTipoDoc(cita.getIdPaciente().getTipoIdentificacion().getDescripcion());
                sesionTrU.setPacienteTipoDoc(cita.getIdPaciente().getTipoIdentificacion().getObservacion());
                sesionTrU.setPacienteNumDoc(cita.getIdPaciente().getIdentificacion());

                sesionTrU.setAtendida(cita.getRealizado());
                listaCitasU.add(sesionTrU);

            }
        }
        return listaCitasU;
    }


    public void generarReporte(ActionEvent actionEvent) throws IOException, JRException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoEmpresa = servletContext.getRealPath("/recursos/img/logo-cmamsa-black.png");
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("logoEmpresa", logoEmpresa);

        List<CtCitas> lista;
        List<HcTfSesiones> listaTf;
        List<HcSesionesTr> listaTr;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        switch (tipoReporte) {
            case "1":
                parametros.put("title", "INFORME CITAS ASIGNADAS");
                if (fechaInicial != null || fechaFinal != null || listPacientes.size() > 0 || listMedicos.size() > 0) {
                    lista = citasFacade.findCitasIntervaloParametros(fechaInicial, fechaFinal, listPacientes, listMedicos, 1);
                } else {
                    lista = citasFacade.findCitasIntervaloParametros(fechaInicial, fechaFinal, listPacientes, listMedicos, 0);
                }
                citasReport(lista, parametros, "/citamedica/reportes/citasAsignadas.jasper");
                break;

            case "2":
                parametros.put("title", "INFORME CITAS CANCELADAS");
                if (fechaInicial != null || fechaFinal != null || listPacientes.size() > 0 || listMedicos.size() > 0) {
                    lista = citasFacade.findCitasCanceladasParametrizable(fechaInicial, fechaFinal, listPacientes, listMedicos, 1);
                } else {
                    lista = citasFacade.findCitasCanceladasParametrizable(fechaInicial, fechaFinal, listPacientes, listMedicos, 0);
                }
                citasReport(lista, parametros, "/citamedica/reportes/citasCanceladas.jasper");
                break;

            case "3":
                parametros.put("title", "HISTORIAL DE CITAS");

                String periodo;
                if (fechaInicial == null && fechaFinal == null) {
                    periodo = "TODAS";
                } else if (fechaInicial != null && fechaFinal == null) {
                    periodo = "Desde " + dateFormat.format(fechaInicial);
                } else if (fechaInicial == null && fechaFinal != null) {
                    periodo = "Hasta " + dateFormat.format(fechaFinal);
                } else {
                    periodo = dateFormat.format(fechaInicial) + " - " + dateFormat.format(fechaFinal);
                }
                lista = citasFacade.findHistoriaClinica(paciente, fechaInicial, fechaFinal);
                parametros.put("periodo", periodo);
                if (!lista.isEmpty()) {
                    String paciente = lista.get(0).getIdPaciente().getPrimerNombre() + " " + lista.get(0).getIdPaciente().getSegundoNombre() + " " + lista.get(0).getIdPaciente().getPrimerApellido() + " " + lista.get(0).getIdPaciente().getSegundoApellido();
                    parametros.put("paciente", paciente);
                    citasReport(lista, parametros, "/citamedica/reportes/historialcitas.jasper");
                } else {
                    imprimirMensaje("Error", "No se consiguieron citas en el rango de fechas", FacesMessage.SEVERITY_ERROR);
                }
                break;

            case "4":
                
                parametros.put("title", "HISTORIAL DE SESIONES DE TERAPIA FISICA");
                
                if (fechaInicial == null && fechaFinal == null) {
                    periodo = "TODAS";
                } else if (fechaInicial != null && fechaFinal == null) {
                    periodo = "Desde " + dateFormat.format(fechaInicial);
                } else if (fechaInicial == null && fechaFinal != null) {
                    periodo = "Hasta " + dateFormat.format(fechaFinal);
                } else {
                    periodo = dateFormat.format(fechaInicial) + " - " + dateFormat.format(fechaFinal);
                }
                
                parametros.put("periodo", periodo);
                
                if (fechaInicial != null || fechaFinal != null || listMedicos.size() > 0 || listPacientes.size() > 0) {
                    listaTf = sesionesTfFacade.findCantidadSesiones(fechaInicial, fechaFinal, listPacientes, listMedicos, 1);
                } else {
                    listaTf = sesionesTfFacade.findCantidadSesiones(fechaInicial, fechaFinal, listPacientes, listMedicos, 0);
                }

                if (!listaTf.isEmpty()) {
                    parametros.put("totalsesiones", listaTf.size());
                    citasReportTf(listaTf, parametros, "/citamedica/reportes/historialSesionesTf.jasper");
                } else {
                    imprimirMensaje("Error", "No se consiguieron citas en el rango de fechas", FacesMessage.SEVERITY_ERROR);
                }
                break;

            case "5":
                parametros.put("title", "HISTORIAL DE SESIONES DE TERAPIA RESPIRATORIA");

                if (fechaInicial == null && fechaFinal == null) {
                    periodo = "TODAS";
                } else if (fechaInicial != null && fechaFinal == null) {
                    periodo = "Desde " + dateFormat.format(fechaInicial);
                } else if (fechaInicial == null && fechaFinal != null) {
                    periodo = "Hasta " + dateFormat.format(fechaFinal);
                } else {
                    periodo = dateFormat.format(fechaInicial) + " - " + dateFormat.format(fechaFinal);
                }
                
                parametros.put("periodo", periodo);
                
                if (fechaInicial != null || fechaFinal != null || listMedicos.size() > 0 || listPacientes.size() > 0) {
                    listaTr = sesionesTrFacade.findCantidadSesiones(fechaInicial, fechaFinal, listPacientes, listMedicos, 1);
                } else {
                    listaTr = sesionesTrFacade.findCantidadSesiones(fechaInicial, fechaFinal, listPacientes, listMedicos, 0);
                }
                
                if (!listaTr.isEmpty()) {
                    parametros.put("totalsesiones", listaTr.size());
                    citasReportTr(listaTr, parametros, "/citamedica/reportes/historialSesionesTf.jasper");
                } else {
                    imprimirMensaje("Error", "No se consiguieron citas en el rango de fechas", FacesMessage.SEVERITY_ERROR);
                }
                break;

        }

    }
    

    private void citasReport(List<CtCitas> citas, Map<String, Object> parametros, String path) throws IOException, JRException {
        List<CitaReporte> citasReporte = generarCitaList(citas);

        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(citasReporte);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            String ruta = servletContext.getRealPath(path);
            JasperPrint jasperPrint = JasperFillManager.fillReport(ruta, parametros, beanCollectionDataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();
        }
    }
    
    private void citasReportTf(List<HcTfSesiones> citas, Map<String, Object> parametros, String path) throws IOException, JRException {
        List<SesionTfU> citasU = generarCitasAuxiliarTf(citas);

        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(citasU);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            String ruta = servletContext.getRealPath(path);
            JasperPrint jasperPrint = JasperFillManager.fillReport(ruta, parametros, beanCollectionDataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();
        }
    }
     private void citasReportTr(List<HcSesionesTr> citas, Map<String, Object> parametros, String path) throws IOException, JRException {
        List<SesionTrU> citasU = generarCitasAuxiliarTr(citas);

        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(citasU);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse httpServletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            String ruta = servletContext.getRealPath(path);
            JasperPrint jasperPrint = JasperFillManager.fillReport(ruta, parametros, beanCollectionDataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
            FacesContext.getCurrentInstance().responseComplete();
        }
    }
     
    public void addMedicoFiltro() {
        listMedicos.add(medicoSeleccionado);
    }
     
    public void addPacienteFiltro() {
        listPacientes.add(pacienteSeleccionado);
        RequestContext.getCurrentInstance().update("formfiltrado");

    }

    public void limpiarFiltros() {

        listPacientes.clear();
        listMedicos.clear();
    }


    public String getTipoReporte() {
        return tipoReporte;
    }

    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public List<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(List<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public List<CnPacientes> getListPacientes() {
        return listPacientes;
    }

    public void setListPacientes(List<CnPacientes> listPacientes) {
        this.listPacientes = listPacientes;
    }

    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public CnUsuarios getMedicoSeleccionado() {
        return medicoSeleccionado;
    }

    public void setMedicoSeleccionado(CnUsuarios medicoSeleccionado) {
        this.medicoSeleccionado = medicoSeleccionado;
    }

    public LazyDataModel<CnUsuarios> getMedicos() {
        return medicos;
    }

    public void setMedicos(LazyDataModel<CnUsuarios> medicos) {
        this.medicos = medicos;
    }

    public LazyDataModel<CnPacientes> getPacientes() {
        return pacientes;
    }

    public void setPacientes(LazyDataModel<CnPacientes> pacientes) {
        this.pacientes = pacientes;
    }

    public List<SelectItem> getListaReportes() {
        return listaReportes;
    }

    public void setListaReportes(List<SelectItem> listaReportes) {
        this.listaReportes = listaReportes;
    }

    public boolean isRenBtnFiltrar() {
        return renBtnFiltrar;
    }

    public void setRenBtnFiltrar(boolean renBtnFiltrar) {
        this.renBtnFiltrar = renBtnFiltrar;
    }

    public String getDisplayBusquedaPaciente() {
        return displayBusquedaPaciente;
    }

    public void setDisplayBusquedaPaciente(String displayBusquedaPaciente) {
        this.displayBusquedaPaciente = displayBusquedaPaciente;
    }

    public String getIdentificacionPaciente() {
        return identificacionPaciente;
    }

    public void setIdentificacionPaciente(String identificacionPaciente) {
        this.identificacionPaciente = identificacionPaciente;
    }

    public boolean isRenBtnReporte() {
        return renBtnReporte;
    }

    public void setRenBtnReporte(boolean renBtnReporte) {
        this.renBtnReporte = renBtnReporte;
    }

    public CnPacientes getPaciente() {
        return paciente;
    }

    public void setPaciente(CnPacientes paciente) {
        this.paciente = paciente;
    }

    public LazyDataModel<CnPacientes> getListPacientesBusqueda() {
        return listPacientesBusqueda;
    }

    public void setListPacientesBusqueda(LazyDataModel<CnPacientes> listPacientesBusqueda) {
        this.listPacientesBusqueda = listPacientesBusqueda;
    }

}
