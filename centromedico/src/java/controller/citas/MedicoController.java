
package controller.citas;

import util.LazyAgendaModel;
import util.MetodosGenerales;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import controller.seguridad.IndexController;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author kenlly
 */
@ManagedBean(name = "medicoController")
@SessionScoped
public class MedicoController extends MetodosGenerales implements Serializable {

    private LazyAgendaModel evenModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private String nombreCompleto;
    private boolean rend = false;
    private boolean haySesionMedico;
    private boolean rendBtnHistoriClinica = false;
    private boolean rendBtnAtendida = false;
    private boolean rendAgenda;
    private String idTurno;
    private String minTime;
    private String maxTime;
    private String noTurno;

    private CnUsuarios medicoActual;
    private CtCitas citCita;
    private CtTurnos ctTurnos;
    
    @EJB
    CtCitasFacade citasFacade;

    @EJB
    CtTurnosFacade turnosfacade;

   

    @ManagedProperty(value = "#{indexController}")
    private IndexController indexController;

    public MedicoController() {
    }

    @PostConstruct
    private void init() {
        setRendAgenda(false);
        if (getIndexController().getUsuarioActual().getTipoUsuario().getCodigo().compareTo("2") == 0) {
            setMedicoActual(getIndexController().getUsuarioActual());
            setHaySesionMedico(true);
        } else {
            setMedicoActual(null);
            setHaySesionMedico(false);
            imprimirMensaje("Error", "Solo medico puede ver el contenido", FacesMessage.SEVERITY_ERROR);
        }
        if (medicoActual != null) {
            getMedicoActual().getNombreCompleto();

        } else {
            evenModel = null;
        }
    }

    public void loadEvents() {
        if (medicoActual != null){
            Object[] horas = turnosfacade.MinDateMaxDate(medicoActual.getIdUsuario());
            
            if (horas[0] != null) {
                setMinTime(establerLimitesAgenda((Date) horas[0]));
                Date aux = (Date) horas[1];
                if (aux.getMinutes() > 0) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(aux);
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    aux = calendar.getTime();
                }
                setMaxTime(establerLimitesAgenda(aux));
                evenModel = new LazyAgendaModel(medicoActual.getIdUsuario(), turnosfacade, citasFacade, "agendaMedico");
                setRendAgenda(true);
            } else {
                evenModel = null;
                setRendAgenda(false);
                if (actualizarDesdeHistorias) {
                    imprimirMensaje("Informacion", "No tiene agenda", FacesMessage.SEVERITY_WARN);
                }
            }
        }        
        
    }

    private String establerLimitesAgenda(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("h:mm");
        return format.format(date);
    }

  
    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        if (event.getTitle() != null) {
            String[] vector = event.getTitle().split(" - ");
            idTurno = vector[0];
            seleccionarCita(Integer.parseInt(idTurno));
            if (citCita != null) {
                RequestContext.getCurrentInstance().execute("PF('eventDialog').show()");
            }
        }
    }

    private void seleccionarCita(Integer id) {
        CtCitas cita = citasFacade.findCitasByTurno(id);
        setCitCita(cita);
        noTurno = cita.getIdTurno().getContador().toString();
        if (cita != null) {
            rend = true;
            if (cita.getAtendida()) {
                setRendBtnHistoriClinica(true);
                setRendBtnAtendida(false);
            } else if (cita.getIdTurno().getEstado().equals("en_espera")||cita.getIdTurno().getEstado().equals("asignado")) {
                setRendBtnAtendida(true);
                setRendBtnHistoriClinica(true);
            } else {
                setRendBtnHistoriClinica(false);
                setRendBtnAtendida(false);
            }
        } else {
            rend = false;
            setRendBtnHistoriClinica(false);
            setRendBtnAtendida(false);
        }
        RequestContext.getCurrentInstance().update("formAgendaMedico:pdialog");

    }

    public void openHistoriaClinica() {
        if(citCita.getAtendida()){
            imprimirMensaje("Error", "Registro de ficha médica ya creado", FacesMessage.SEVERITY_ERROR);
            return;
        }
        RequestContext.getCurrentInstance().execute("window.parent.loadTab('Historias Clinicas','historiaclinica/historias.xhtml','idCita;" + citCita.getIdCita().toString() + "')");
    }

    boolean actualizarDesdeHistorias;

       
    public LazyAgendaModel getEvenModel() {
        return evenModel;
    }

    public void setEvenModel(LazyAgendaModel evenModel) {
        this.evenModel = evenModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public CnUsuarios getMedicoActual() {
        return medicoActual;
    }

    public void setMedicoActual(CnUsuarios medicoActual) {
        this.medicoActual = medicoActual;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }

    public CtCitas getCitCita() {
        return citCita;
    }

    public void setCitCita(CtCitas citCita) {
        this.citCita = citCita;
    }

    public CtTurnos getCtTurnos() {
        return ctTurnos;
    }

    public void setCtTurnos(CtTurnos ctTurnos) {
        this.ctTurnos = ctTurnos;
    }

    public boolean isHaySesionMedico() {
        return haySesionMedico;
    }

    public void setHaySesionMedico(boolean haySesionMedico) {
        this.haySesionMedico = haySesionMedico;
    }
    
    public boolean isRendBtnAtendida() {
        return rendBtnAtendida;
    }

    public void setRendBtnAtendida(boolean rendBtnAtendida) {
        this.rendBtnAtendida = rendBtnAtendida;
    }

    public String getMinTime() {
        return minTime;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public boolean isRendAgenda() {
        return rendAgenda;
    }

    public void setRendAgenda(boolean rendAgenda) {
        this.rendAgenda = rendAgenda;
    }

    public boolean isRendBtnHistoriClinica() {
        return rendBtnHistoriClinica;
    }

    public void setRendBtnHistoriClinica(boolean rendBtnHistoriClinica) {
        this.rendBtnHistoriClinica = rendBtnHistoriClinica;
    }

    public String getIdTurno() {
        return idTurno;
    }

    public String getNoTurno() {
        return noTurno;
    }

    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

}

