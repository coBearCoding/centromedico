
package controller.citas;

import util.LazyAgendaModel;
import util.LazyMedicoDataModel;
import util.MetodosGenerales;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.IndexController;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.ScheduleEvent;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "recepcionController")
@SessionScoped
public class RecepcionController extends MetodosGenerales implements Serializable {

    public RecepcionController() {
    }
 
    private LazyAgendaModel evenModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private String nombreCompleto;
    private boolean rendered;    
    private boolean renderAgenda;
    private String identificacion;
    private LazyDataModel<CnUsuarios> listMedicos;
    private String display = "none";
    private List<SelectItem> listEspecialidades;
    private boolean hayMedicoSeleccionado;
    private String idTurno;
    private CnUsuarios medicoActual;
    private CtCitas citCita;
    private String noTurno;
    
    private int motivoCancelacion;
    private String descripcionCancelacion;


    private String minTime;
    private String maxTime;

    @EJB
    CtCitasFacade citasFacade;

    @EJB
    CtTurnosFacade turnosfacade;

    @EJB
    CnUsuariosFacade usuariosFacade;

    @EJB
    CnClasificacionesFacade clasificacionesFacade;

    

    @PostConstruct
    private void init() {
        setRenderAgenda(false);
        IndexController indexController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{indexController}", IndexController.class);

        if (getMedicoActual() != null) {
            loadEvents();

        } else {
            setEvenModel(null);
        }
        listMedicos = new LazyMedicoDataModel(usuariosFacade);
        
        cargarEspecialidadesMedicos();
    }

    
    private void cargarEspecialidadesMedicos() {
        listEspecialidades = new ArrayList();
        List<CnClasificaciones> lista = usuariosFacade.findEspecialidades();
        for (CnClasificaciones especialidad : lista) {
            if (especialidad != null) {
                listEspecialidades.add(new SelectItem(especialidad.getId(), especialidad.getDescripcion()));
            }
        }
    }

    public void functionDisplay() {
        if (medicoActual != null) {
            setDisplay("block");
        } else {
            setDisplay("none");
        }
    }

    public void validarIdentificacion() {
        setRenderAgenda(false);
        CnUsuarios medico = usuariosFacade.buscarPorIdentificacion(identificacion);
        if (medico != null) {
            medicoActual = medico;
            identificacion = medicoActual.getIdentificacion();
            setRenderAgenda(true);
            loadEvents();
            setHayMedicoSeleccionado(true);
            setNombreCompleto(medicoActual.getNombreCompleto());
        } else {
            setHayMedicoSeleccionado(false);
            setNombreCompleto(null);
            setDisplay("none");
            if (!identificacion.isEmpty()) {
                imprimirMensaje("Error", "No se encontro medico", FacesMessage.SEVERITY_ERROR);
            }
            setMedicoActual(null);
            setIdentificacion(null);
        }
    }

    public void findmedico() {
        setRenderAgenda(false);
        if (!identificacion.isEmpty()) {
            try {
                medicoActual = usuariosFacade.buscarPorIdentificacion(identificacion);
                if (medicoActual == null) {
                    setHayMedicoSeleccionado(false);
                    imprimirMensaje("Error", "No se encontro el medico", FacesMessage.SEVERITY_ERROR);
                    setNombreCompleto(null);
                } else {
                    setMedicoActual(medicoActual);
                    setRenderAgenda(true);
                    loadEvents();
                    setHayMedicoSeleccionado(true);
                    setNombreCompleto(medicoActual.getNombreCompleto());
                }
                functionDisplay();
            } catch (Exception e) {
                imprimirMensaje("Error", "Ingrese una identificacion de medico valido", FacesMessage.SEVERITY_ERROR);
                setHayMedicoSeleccionado(false);
                setNombreCompleto(null);
                setDisplay("none");
            }
        } else {
            setMedicoActual(null);
            functionDisplay();
            setHayMedicoSeleccionado(false);
            setNombreCompleto(null);
        }
    }

    public void cargarMedico() {
        if (medicoActual != null) {
            setIdentificacion(medicoActual.getIdentificacion());
            setHayMedicoSeleccionado(true);
            medicoActual.getNombreCompleto();
        }
        functionDisplay();
    }

    private String establerLimitesAgenda(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("ha");
        return format.format(date);
    }

    public void loadEvents() {
        setRenderAgenda(false);
        if (medicoActual != null) {
            Object[] horas = turnosfacade.MinDateMaxDate(medicoActual.getIdUsuario());
            if (horas[0] != null) {
                setMinTime(establerLimitesAgenda((Date) horas[0]));
                Date aux = (Date) horas[1];
                if (aux.getMinutes() > 0) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(aux);
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    aux = calendar.getTime();
                }
                setMaxTime(establerLimitesAgenda(aux));
                setRenderAgenda(true);
                setEvenModel(new LazyAgendaModel(getMedicoActual().getIdUsuario(),turnosfacade, citasFacade, "recepcionCitas"));
            } else {
                imprimirMensaje("Información", "El medico no tiene agenda", FacesMessage.SEVERITY_WARN);
            }
        }
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        if (event.getTitle() != null) {
            String[] vector = event.getTitle().split(" - ");
            idTurno = vector[0];
            seleccionarCita(Integer.parseInt(idTurno));
            if (citCita != null) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.update("formRecepcion:pdialog");
                context.execute("PF('eventDialog').show()");
            }
        }
    }

    private void seleccionarCita(Integer id) {
        CtCitas cita = citasFacade.findCitasByTurno(id);
        setCitCita(cita);
        if (cita != null) {
            rendered = true;
            noTurno = cita.getIdTurno().getContador().toString();
        } else {
            rendered = false;
        }
    }

    public void cancelarCita(ActionEvent actionEvent) {
        if (!citCita.getCancelada() && !citCita.getAtendida()) {
            
            try {
                UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
                
                transaction.begin();
                
                citCita.setCancelada(true);
                CnClasificaciones clasificaciones = clasificacionesFacade.find(getMotivoCancelacion());
                citCita.setDescCancelacion(getDescripcionCancelacion());
                citCita.setMotivoCancelacion(clasificaciones);
                citCita.setFechaCancelacion(citasFacade.FechaServidorddmmyyhhmmTimestamp());
                
                citasFacade.edit(citCita);
                
                CtTurnos turno = citCita.getIdTurno();
                turno.setEstado("disponible");
                turno.setContador(turno.getContador() - 1);
                turnosfacade.edit(turno);
                

                transaction.commit();
                
                RequestContext.getCurrentInstance().update("formRecepcion:eventDetails");
                RequestContext.getCurrentInstance().execute("PF('dlgcancelar').hide()");
                RequestContext.getCurrentInstance().execute("PF('eventDialog').hide()");
                loadEvents();
                RequestContext.getCurrentInstance().update("formRecepcion");
                imprimirMensaje("Correcto", "Cita " + citCita.getIdTurno().getContador() + " cancelada", FacesMessage.SEVERITY_INFO);
            } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
                Logger.getLogger(RecepcionController.class.getName()).log(Level.SEVERE, null, ex);
                imprimirMensaje("Error", "Cita " + citCita.getIdTurno().getContador() + " no se pudo cancelar o ya se encuentra cancelada o atendida", FacesMessage.SEVERITY_ERROR);
            }

        } else {
            imprimirMensaje("Error", "Cita " + citCita.getIdTurno().getContador() + " ya se encuentra cancelada o atendida", FacesMessage.SEVERITY_ERROR);
        }
        setDescripcionCancelacion(null);
    }

   
       
    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public CnUsuarios getMedicoActual() {
        return medicoActual;
    }

    public void setMedicoActual(CnUsuarios medicoActual) {
        this.medicoActual = medicoActual;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public boolean isRendered() {
        return rendered;
    }

    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    

    public CtCitas getCitCita() {
        return citCita;
    }

    public void setCitCita(CtCitas citCita) {
        this.citCita = citCita;
    }

    public LazyDataModel<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(LazyDataModel<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public List<SelectItem> getListEspecialidades() {
        return listEspecialidades;
    }

    public void setListEspecialidades(List<SelectItem> listEspecialidades) {
        this.listEspecialidades = listEspecialidades;
    }

    public boolean isHayMedicoSeleccionado() {
        return hayMedicoSeleccionado;
    }

    public void setHayMedicoSeleccionado(boolean hayMedicoSeleccionado) {
        this.hayMedicoSeleccionado = hayMedicoSeleccionado;
    }

    public int getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(int motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public String getDescripcionCancelacion() {
        return descripcionCancelacion;
    }

    public void setDescripcionCancelacion(String descripcionCancelacion) {
        this.descripcionCancelacion = descripcionCancelacion;
    }

    public String getMinTime() {
        return minTime;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public boolean isRenderAgenda() {
        return renderAgenda;
    }

    public void setRenderAgenda(boolean renderAgenda) {
        this.renderAgenda = renderAgenda;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }


    public String getIdTurno() {
        return idTurno;
    }

    public LazyAgendaModel getEvenModel() {
        return evenModel;
    }

    public void setEvenModel(LazyAgendaModel evenModel) {
        this.evenModel = evenModel;
    }

    public String getNoTurno() {
        return noTurno;
    }

    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

}
