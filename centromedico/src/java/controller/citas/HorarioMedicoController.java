
package controller.citas;

import util.LazyAgendaModel;
import util.LazyMedicoDataModel;
import util.MetodosGenerales;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import controller.seguridad.IndexController;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnConsultorios;
import jpa.modelo.CnHorario;
import jpa.modelo.CnItemsHorario;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtTurnos;
import jpa.facade.CnConsultoriosFacade;
import jpa.facade.CnDiasNoLaboralesFacade;
import jpa.facade.CnHorarioFacade;
import jpa.facade.CnItemsHorarioFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
/**
 *
 * @author kenlly
 */
@ManagedBean(name = "horarioMedicoController")
@SessionScoped
public class HorarioMedicoController extends MetodosGenerales implements Serializable {

    @ManagedProperty(value = "#{horarioController}")
    private HorarioController horarioController;

    @EJB
    CnItemsHorarioFacade cnItemsHorarioFacade;
    @EJB
    CtTurnosFacade ctTurnosFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    CnConsultoriosFacade consultorioFacade;
    @EJB
    CnHorarioFacade cnHorarioFacade;
    @EJB
    CnDiasNoLaboralesFacade diasNoLaboralesFacade;

    private CnUsuarios medicoSeleccionado;
    private CnUsuarios medico;
    private List<CnItemsHorario> lunes;
    private List<CnItemsHorario> martes;
    private List<CnItemsHorario> miercoles;
    private List<CnItemsHorario> jueves;
    private List<CnItemsHorario> viernes;
    private List<CnItemsHorario> sabado;
    private List<CnItemsHorario> domingo;

    private List<SelectItem> listaConsultorios;
    private int id_consultorio;
    private int concurrencia = 1;
    private String identificacionMedico = "";
    private Date fechaInicial;
    private Date fechaFinal;
    private int duracion;
    private Date horaInicial;
    private LazyDataModel<CnUsuarios> listMedicos;
    private String id_medico;
    private String nombreCompleto;
    private String display = "none";
    private String displayBtnEliminarAgenda = "none";
    private String displayHoraInicial = "block";
    private String accion = "simple"; 
    private List listaActualizar;
    private List<SelectItem> listEspecialidades;
    private boolean hayMedicoSeleccionado;

    private List<Short> diasLaborales;
    private int id_horario;
    private List<SelectItem> listahorarios;
    private boolean rendBtnEliminarHorario = false;
    private LazyAgendaModel evenModel = new LazyAgendaModel(1,ctTurnosFacade, null, accion);

    @ManagedProperty(value = "#{indexController}")
    private IndexController indexController;
    
    public HorarioMedicoController() {
    }

    @PostConstruct
    private void init() {
        setDuracion(20);
        setListMedicos(new LazyMedicoDataModel(usuariosFacade));
        cargarEspecialidadesMedicos();
        cargarListaConsultorios();
        cargarDetalleHorario();
        String aux = "formCrearAgenda:";
        listaActualizar = new ArrayList();
        listaActualizar.add(aux.concat("calendarioTurnos"));
    }

    public void validarIdentificacion() {
        displayBtnEliminarAgenda = "none";
        setFechaInicial(null);
        setFechaFinal(null);
        setId_horario(0);
        setDuracion(20);
        setConcurrencia(1);
        medico = usuariosFacade.buscarPorIdentificacion(identificacionMedico);
        if (medico != null) {
            medicoSeleccionado = medico;
            identificacionMedico = medicoSeleccionado.getIdentificacion();
            loadEvents();

            RequestContext.getCurrentInstance().update("formAsignarMedico");
        } else {
            medicoSeleccionado = null;
            RequestContext.getCurrentInstance().update("formAsignarMedico");
            RequestContext.getCurrentInstance().execute("PF('dlgfindMedico').show();");
        }
        functionDisplay();
    }
    
    private void cargarEspecialidadesMedicos() {
        setListEspecialidades((List<SelectItem>) new ArrayList());
        List<CnClasificaciones> lista = usuariosFacade.findEspecialidades();
        for (CnClasificaciones especialidad : lista) {
            if (especialidad != null) {
                getListEspecialidades().add(new SelectItem(especialidad.getId(), especialidad.getDescripcion()));
            }
        }
    }

    public void functionDisplay() {
        displayBtnEliminarAgenda = "none";
        setFechaInicial(null);
        setFechaFinal(null);
        setId_horario(0);
        setDuracion(20);
        setConcurrencia(1);
        if (getMedicoSeleccionado() != null) {
            setIdentificacionMedico(medicoSeleccionado.getIdentificacion());
            setDisplay("block");
        } else {
            setDisplay("none");
        }
        if (listaConsultorios != null) {
            listaConsultorios.clear();
        }
        RequestContext.getCurrentInstance().update("formCrearAgenda");
    }

    public void findmedico() {
        if (!id_medico.isEmpty()) {
            try {
                setMedicoSeleccionado(usuariosFacade.find(Integer.parseInt(getId_medico())));
                if (getMedicoSeleccionado() == null) {
                    setHayMedicoSeleccionado(false);
                    imprimirMensaje("Error", "No se encontro el medico", FacesMessage.SEVERITY_ERROR);
                    setNombreCompleto(null);
                } else {
                    loadEvents();
                    setMedicoSeleccionado(getMedicoSeleccionado());

                    setHayMedicoSeleccionado(true);
                    setNombreCompleto(getMedicoSeleccionado().getNombreCompleto());
                }
                functionDisplay();
            } catch (Exception e) {
                imprimirMensaje("Error", "Ingrese identificacion de medico valido", FacesMessage.SEVERITY_ERROR);
                setHayMedicoSeleccionado(false);
                setNombreCompleto(null);
                setDisplay("none");
            }
        } else {
            setMedicoSeleccionado(null);
            functionDisplay();
            setHayMedicoSeleccionado(false);
            setNombreCompleto(null);
        }
    }

    
    private void construirArrayList(List<CnConsultorios> listaconsultorios) {
        listaConsultorios = new ArrayList<>();
        for (CnConsultorios consultorio : listaconsultorios) {
            listaConsultorios.add(new SelectItem(consultorio.getIdConsultorio(), consultorio.getNomConsultorio()));
        }
    }

    public  List<SelectItem> cargarListaConsultorios() {
            List<CnConsultorios> listaconsultorios = consultorioFacade.findAll();
            construirArrayList(listaconsultorios);
            if (listaconsultorios.isEmpty()) {
                imprimirMensaje("Error", "No hay consultorios disponibles", FacesMessage.SEVERITY_WARN);
            }
       return listaConsultorios;
      
    }

    public void cargarDetalleHorario() {
        List<CnHorario> cnHorarios = cnHorarioFacade.findAll();
        listahorarios = new ArrayList();
        for (CnHorario cnHorario : cnHorarios) {
            listahorarios.add(new SelectItem(cnHorario.getIdHorario(), cnHorario.getDescripcion()));
        }
        setListahorarios(listahorarios);
    }
  
    public void generarHorario() throws ParseException {
        
        if (getMedicoSeleccionado() == null) {
            imprimirMensaje("Error", "Es necesario elegir un medico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (getId_consultorio() == 0) {
            imprimirMensaje("Error", "Falta elegir consultorio", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (duracion < 0) {
            duracion = 20;
            imprimirMensaje("Error", "El valor de la duracion de la cita no es valido", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (concurrencia < 1) {
            concurrencia = 1;
            imprimirMensaje("Error", "El valor mimino para la concurrencia es 1", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaInicial == null) {
            imprimirMensaje("Error", "Falta elegir fecha inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal == null) {
            fechaFinal = fechaInicial;
        }

        if (getId_horario() == 0) {
            imprimirMensaje("Error", "Falta elegir el horario", FacesMessage.SEVERITY_ERROR);
            return;

        }
        
       
        if (!ctTurnosFacade.buscarTurnosParametrizado(medicoSeleccionado.getIdUsuario(), fechaInicial, fechaFinal, id_consultorio, id_horario).isEmpty()) {
            imprimirMensaje("Error", "Se encontro un horario dentro de esas especificaciones", FacesMessage.SEVERITY_ERROR);
            return;
        }
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = ctTurnosFacade.FechaServidorddmmyyhhmmTimestamp();
        Date fechaActual = formatter.parse(formatter.format(fecha));
        if (fechaInicial.before(fechaActual) || fechaFinal.before(fechaActual)) {
            imprimirMensaje("Error", "Las fechas incial y final no deben corresponder a una fecha anterior a la actual", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal.before(fechaInicial)) {
            imprimirMensaje("Error", "La fecha final corresponde a una fecha anterior a la inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        boolean bandera = false;
        diasLaborales = cnItemsHorarioFacade.findSelectedDays(getId_horario());
        crearHorario();
        long oneDayMilSec = 86400000;
        long startHour = 0;
        long endHour = 0;
        long timeCita = duracion * 60000;
        long startDateMilSec = fechaInicial.getTime();
        long endDateMilSec = fechaFinal.getTime();
        Date aux;
        Date aux2;
        Integer contador = 1;

        for (long d = startDateMilSec; d <= endDateMilSec; d = d + oneDayMilSec) {
            aux = new Date(d);
            if (diasNoLaboralesFacade.FindDiaNoLaboral(aux) == null) {
                if (esdialaboral(aux.getDay())) {
                    Date aux3 = aux;
                    String dia = String.valueOf(aux3.getDay());
                    List<CnItemsHorario> auxiliar = findHorario(Short.parseShort(dia));
                    contador = 1;
                    for (CnItemsHorario h : auxiliar) {
                        startHour = generarMilisegundos(aux3, h.getHoraInicio());
                        endHour = generarMilisegundos(aux3, h.getHoraFinal());
                        for (long e = startHour; e < endHour; e = e + timeCita) {
                            if (e + timeCita > endHour) {                                
                                break;
                            }
                            for (int j = 0; j < concurrencia; j++) {

                                aux2 = new Date(e);

                                CtTurnos turno = new CtTurnos();
                                turno.setConcurrencia(1);
                                turno.setFecha(aux);
                                turno.setHoraIni(new Date(e));
                                turno.setHoraFin(new Date(e + timeCita));
                                turno.setIdMedico(medicoSeleccionado);
                                CnConsultorios consultorio = new CnConsultorios();
                                consultorio.setIdConsultorio(getId_consultorio());
                                turno.setIdConsultorio(consultorio);
                                CnHorario horario = new CnHorario(id_horario);
                                turno.setIdHorario(horario);
                                turno.setFechaCreacion(ctTurnosFacade.FechaServidorddmmyyhhmmTimestamp());
                                turno.setEstado("disponible");
                                turno.setContador(contador);
                                turno.setUsuarioCreacion(getIndexController().getUsuarioActual().getIdUsuario());
                                ctTurnosFacade.create(turno);
                                
                                contador++;
                            }
                        }
                    }
                    bandera = true;
                }

            }
        }
        if (bandera) {
            displayBtnEliminarAgenda = "block";
            loadEvents();
            RequestContext.getCurrentInstance().update(listaActualizar);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "La agenda fue creada"));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La agenda no fue creada. Probablemente el periodo elegido corresponde a un dia no laboral para el horario"));
        }
    }

    public void crearTurno() throws ParseException {
        if (id_consultorio == 0) {
            imprimirMensaje("Error", "Falta elegir consultorio", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (duracion <= 0) {
            duracion = 20;
            imprimirMensaje("Error", "El valor de la duracion de la cita no es valido", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (concurrencia < 1) {
            concurrencia = 1;
            imprimirMensaje("Error", "El valor mimino para la concurrencia es 1", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (horaInicial == null) {
            imprimirMensaje("Error", "Ingrese la hora inicial de la cita", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaInicial == null) {
            imprimirMensaje("Error", "Falta elegir fecha inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal == null && fechaInicial != null) {
            fechaFinal = fechaInicial;
        }
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = ctTurnosFacade.FechaServidorddmmyyhhmmTimestamp();
        Date fechaActual = formatter.parse(formatter.format(fecha));
        if (fechaInicial.before(fechaActual) || fechaFinal.before(fechaActual)) {
            imprimirMensaje("Error", "Las fechas incial y final no deben corresponder a una fecha anterior a la actual", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal.before(fechaInicial)) {
            imprimirMensaje("Error", "La fecha final corresponde a una fecha anterior a la inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        Calendar f_inicial = Calendar.getInstance();
        f_inicial.setTime(fechaInicial);
        Calendar f_final = Calendar.getInstance();
        f_final.setTime(fechaFinal);
        long timeCita = duracion * 60000;
        boolean bandera = false;
        while (!f_inicial.after(f_final)) {
            if (diasNoLaboralesFacade.FindDiaNoLaboral(f_inicial.getTime()) == null) {
                Date aux = f_inicial.getTime();
                String dia = String.valueOf(aux.getDay());
                for (int j = 0; j < concurrencia; j++) {
                    CtTurnos turno = new CtTurnos();
                    turno.setConcurrencia(1);
                    turno.setFecha(aux);
                    turno.setHoraIni(horaInicial);
                    turno.setHoraFin(new Date(horaInicial.getTime() + timeCita));
                    turno.setIdMedico(medicoSeleccionado);
                    CnConsultorios consultorio = consultorioFacade.find(id_consultorio);
                    consultorio.setIdConsultorio(getId_consultorio());
                    turno.setIdConsultorio(consultorio);
                    turno.setFechaCreacion(ctTurnosFacade.FechaServidorddmmyyhhmmTimestamp());
                    turno.setEstado("disponible");
                    turno.setContador(1);
                    turno.setUsuarioCreacion(getIndexController().getUsuarioActual().getIdUsuario());
                    ctTurnosFacade.create(turno);
                }
                bandera = true;
            }
            f_inicial.add(Calendar.DATE, 1);
        }
        if (bandera) {
            loadEvents();
            limpiar();
            List<String> componentes = new ArrayList();
            componentes.add("formCrearAgenda:calendarioTurnos");
            RequestContext.getCurrentInstance().update(listaActualizar);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "La agenda fue creada"));
            RequestContext.getCurrentInstance().update(componentes);

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se crearon los turnos es posible que el rango de fecha corresponda a dias no laborales"));
        }
    }

    public void accionControlador() throws ParseException {
        switch (accion) {
            case "simple":
                crearTurno();
                break;
            case "agenda":
                generarHorario();
                break;
        }

    }

    private List<CnItemsHorario> findHorario(Short dia) {
        List<CnItemsHorario> h = null;
        switch (dia) {
            case 0:
                h = domingo;
                break;
            case 1:
                h = lunes;
                break;
            case 2:
                h = martes;
                break;
            case 3:
                h = miercoles;
                break;
            case 4:
                h = jueves;
                break;
            case 5:
                h = viernes;
                break;
            case 6:
                h = sabado;
                break;
        }
        return h;
    }

    private void crearHorario() {
        for (Short d : diasLaborales) {
            switch (d) {
                case 0:
                    domingo = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 1:
                    lunes = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 2:
                    martes = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 3:
                    miercoles = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 4:
                    jueves = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 5:
                    viernes = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
                case 6:
                    sabado = cnItemsHorarioFacade.findDateByDay(getId_horario(), d);
                    break;
            }
        }

    }

    private long generarMilisegundos(Date fecha, Date hora) {
        fecha.setHours(hora.getHours());
        fecha.setMinutes(hora.getMinutes());
        Long milisegundos = fecha.getTime();
        return milisegundos;
    }

    private void limpiar() {
        this.concurrencia = 1;
        setDuracion(20);
        this.horaInicial = null;
        this.fechaFinal = null;
        this.fechaInicial = null;

        setId_consultorio(0);
        setId_horario(0);
    }

    private boolean esdialaboral(int dia) {
        boolean retorno = false;
        for (Short d : diasLaborales) {
            if (d == dia) {
                retorno = true;
                break;
            }
        }

        return retorno;
    }

    public void eliminarAgenda() throws ParseException {

        if (fechaInicial == null) {
            imprimirMensaje("Error", "Se necesita la fecha inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal == null) {
            imprimirMensaje("Error", "Se necesita la fecha final", FacesMessage.SEVERITY_ERROR);
            return;
        }
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = ctTurnosFacade.FechaServidorddmmyyhhmmTimestamp();
        Date fechaActual = formatter.parse(formatter.format(fecha));
        if (fechaInicial.before(fechaActual) || fechaFinal.before(fechaActual)) {
            imprimirMensaje("Error", "Las fechas incial y final no deben corresponder a una fecha anterior a la actual", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaFinal.before(fechaInicial)) {
            imprimirMensaje("Error", "La fecha final corresponde a una fecha anterior a la inicial", FacesMessage.SEVERITY_ERROR);
            return;
        }
        List estados = new ArrayList();
        estados.add("asignado");
        estados.add("en_espera");
        estados.add("atendido");
        try {
            int totalTurnos = ctTurnosFacade.ValidarEliminarAgenda(medicoSeleccionado.getIdUsuario(), fechaInicial, fechaFinal, id_horario);
            if (totalTurnos > 0) {
                int result = ctTurnosFacade.EliminarAgenda(medicoSeleccionado.getIdUsuario(), fechaInicial, fechaFinal, id_horario, estados);
                if (result == totalTurnos) {
                    RequestContext.getCurrentInstance().update(listaActualizar);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "Agenda eliminada"));

                } else if (result > 0) {

                    ctTurnosFacade.actualizarTurno(medicoSeleccionado.getIdUsuario(), fechaInicial, fechaFinal, id_horario);
                    imprimirMensaje("Informacion", "Se eliminaron unicamente los turnos que nunca han sido asignados a una cita", FacesMessage.SEVERITY_WARN);
                } else {
                    result = ctTurnosFacade.actualizarTurno(medicoSeleccionado.getIdUsuario(), fechaInicial, fechaFinal, id_horario);
                    if (result == 0) {
                        imprimirMensaje("Error", "No encontraron turnos a elminar", FacesMessage.SEVERITY_ERROR);
                        return;
                    }
                    imprimirMensaje("Informacion", "Los turnos disponibles relacionados con una cita cancelada no lo estaran", FacesMessage.SEVERITY_INFO);
                }
                loadEvents();
            } else {

                imprimirMensaje("Error", "No se econtraron turnos a eliminiar", FacesMessage.SEVERITY_ERROR);
            }
        } catch (Exception e) {
            imprimirMensaje("Error", "Agenda no eliminada", FacesMessage.SEVERITY_ERROR);
        }

    }

    public void seleccionarHorario() {
        if (id_horario == 0) {
            displayHoraInicial = "block";
            accion = "simple";
            horarioController.validarHorario(null);
            setRendBtnEliminarHorario(false);
        } else {
            displayHoraInicial = "none";
            accion = "agenda";
            horarioController.validarHorario(cnHorarioFacade.find(id_horario));
            setRendBtnEliminarHorario(true);
        }
    }

    public void saveHorarioAndItems() {
        horarioController.guardarHorario();
        horarioController.getDetalleHorarioController().guardarItems();
        cargarDetalleHorario();
        RequestContext.getCurrentInstance().update("formCrearAgenda:idHorario");
    }

    public void limpiarFormHorario() {
        if (id_horario == 0) {
            horarioController.setDescripcion(null);
            horarioController.getDetalleHorarioController().setHorarioseleccionado(null);
            horarioController.getDetalleHorarioController().setHoraFin(null);
            horarioController.getDetalleHorarioController().setHoraIni(null);
            horarioController.getDetalleHorarioController().setListaDetalleHorario(new ArrayList());
            horarioController.getDetalleHorarioController().setIndex(0);
            horarioController.validarHorario(null);
        }
    }

    public void eliminarHorario() {
        try {
            if (ctTurnosFacade.ValidarEliminarHorario(id_horario)) {
                CnHorario horario = cnHorarioFacade.find(id_horario);
                int totalItems = horario.getCnItemsHorarioList().size();
                int elementosEliminados = cnItemsHorarioFacade.eliminarByIdHorario(id_horario);
                RequestContext.getCurrentInstance().execute("PF('dlghorario').hide()");
                if (totalItems == 0 || totalItems == elementosEliminados) {
                    horario = cnHorarioFacade.find(id_horario);
                    cnHorarioFacade.remove(horario);
                    cargarDetalleHorario();
                    imprimirMensaje("Correcto", "Horario eliminado", FacesMessage.SEVERITY_INFO);
                    RequestContext.getCurrentInstance().update("formCrearAgenda:idHorario");
                } else {
                    imprimirMensaje("Error", "El horario no fue eliminado", FacesMessage.SEVERITY_ERROR);
                }
                setId_horario(0);

            } else {
                imprimirMensaje("Error", "El horario esta siendo usado", FacesMessage.SEVERITY_ERROR);
            }
        } catch (Exception e) {
            imprimirMensaje("Error", "El horario esta siendo usado", FacesMessage.SEVERITY_ERROR);
        }
    }

    public void loadEvents() {
        evenModel = new LazyAgendaModel(medicoSeleccionado.getIdUsuario(), ctTurnosFacade, null, "generarHorario");
        RequestContext.getCurrentInstance().update("formCrearAgenda:calendarioTurnos");
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

   

    public int getConcurrencia() {
        return concurrencia;
    }

    public void setConcurrencia(int concurrencia) {
        this.concurrencia = concurrencia;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public LazyDataModel<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(LazyDataModel<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public String getId_medico() {
        return id_medico;
    }

    public void setId_medico(String id_medico) {
        this.id_medico = id_medico;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public List<SelectItem> getListEspecialidades() {
        return listEspecialidades;
    }

    public void setListEspecialidades(List<SelectItem> listEspecialidades) {
        this.listEspecialidades = listEspecialidades;
    }

    public boolean isHayMedicoSeleccionado() {
        return hayMedicoSeleccionado;
    }

    public void setHayMedicoSeleccionado(boolean hayMedicoSeleccionado) {
        this.hayMedicoSeleccionado = hayMedicoSeleccionado;
    }

    public CnUsuarios getMedicoSeleccionado() {
        return medicoSeleccionado;
    }

    public void setMedicoSeleccionado(CnUsuarios medicoSeleccionado) {
        this.medicoSeleccionado = medicoSeleccionado;
    }

    public List<SelectItem> getListaConsultorios() {
        return listaConsultorios;
    }

    public void setListaConsultorios(List<SelectItem> listaConsultorios) {
        this.listaConsultorios = listaConsultorios;
    }

    public int getId_consultorio() {
        return id_consultorio;
    }

    public void setId_consultorio(int id_consultorio) {
        this.id_consultorio = id_consultorio;
    }

    

    public int getId_horario() {
        return id_horario;
    }

    public void setId_horario(int id_horario) {
        this.id_horario = id_horario;
    }

    public List<SelectItem> getListahorarios() {
        return listahorarios;
    }

    public void setListahorarios(List<SelectItem> listahorarios) {
        this.listahorarios = listahorarios;
    }

    public LazyAgendaModel getEvenModel() {
        return evenModel;
    }

    public void setEvenModel(LazyAgendaModel evenModel) {
        this.evenModel = evenModel;
    }

    public HorarioController getHorarioController() {
        return horarioController;
    }

    public void setHorarioController(HorarioController horarioController) {
        this.horarioController = horarioController;
    }

    

    public String getIdentificacionMedico() {
        return identificacionMedico;
    }

    public void setIdentificacionMedico(String identificacionMedico) {
        this.identificacionMedico = identificacionMedico;
    }
    public String getDisplayBtnEliminarAgenda() {
        return displayBtnEliminarAgenda;
    }

    public Date getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(Date horaInicial) {
        this.horaInicial = horaInicial;
    }

    public boolean isRendBtnEliminarHorario() {
        return rendBtnEliminarHorario;
    }

    public void setRendBtnEliminarHorario(boolean rendBtnEliminarHorario) {
        this.rendBtnEliminarHorario = rendBtnEliminarHorario;
    }

    public String getDisplayHoraInicial() {
        return displayHoraInicial;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

    

}

