
package controller.citas;

import util.LazyAgendaModel;
import util.LazyPacienteDataModel;
import util.LazyMedicoDataModel;
import jpa.modelo.CtCitas;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtTurnos;
import jpa.facade.CtCitasFacade;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CtTurnosFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import util.MetodosGenerales;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.IndexController;
import jpa.facade.CnCie10Facade;
import jpa.modelo.CnDiasNoLaborales;
import jpa.facade.CnDiasNoLaboralesFacade;
import jpa.facade.CnPacientesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.HcArchivosFacade;
import jpa.facade.HcTfSesionesFacade;
import jpa.modelo.HcArchivos;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author kenlly
 */
@ManagedBean(name = "citaController")
@SessionScoped
public class CitaController extends MetodosGenerales implements Serializable {

    private String tipoIdentificacion;
    private String identificacion;
    private CnPacientes pacienteSeleccionado;
    private String displayPaciente = "none";
    private boolean hayPacienteSeleccionado;
    private LazyDataModel<CnPacientes> listPacientes;
    private String idTurno;
    private String noTurno;
    private CnUsuarios medicoSeleccionado;
    private String id_medico;
    private String nombreCompleto;
    private String displayMedico = "none";
    private List<SelectItem> listEspecialidades;
    private boolean hayMedicoSeleccionado;
    private LazyDataModel<CnUsuarios> listMedicos;
    private boolean esFisioterapia = false;
    private boolean sesionesPendientes = false;

    private CtTurnos turnoSeleccionado;
    private CtCitas citaSeleccionada;

    private boolean estado;
    private boolean subsecuente;
    private int motivoConsultas;
    private int motivoCancelacion;
    private String descripcionCancelacion;

    private LazyAgendaModel evenModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private boolean rend = false;
    private boolean rendBtnElegir = false;
    private boolean renderizarbotones = false;
    private List<CtCitas> listaCitas;

    private List<SelectItem> listaTipoCitas = null;
    private CtCitas citaSelecionada;
    private List<SelectItem> listaMotivo;

    //variables para subida de orden de fisioterapia
    private UploadedFile file;
    private boolean EstaSubido = false;
    private Long idArchivoFt;

    IndexController indexController;
    @EJB
    CtCitasFacade citasFacade;
    @EJB
    CtTurnosFacade turnosFacade;
    @EJB
    CnClasificacionesFacade clasificacionesFacade;
    @EJB
    CnPacientesFacade pacientesFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    CnDiasNoLaboralesFacade noLaborablesFacade;
    
    @EJB
    CnCie10Facade Cncie10Facade;

    @EJB
    HcArchivosFacade hcArchivosFacade;

    @EJB
    HcTfSesionesFacade sesionesTfFacade;
    
    public CitaController() {
    }

    @PostConstruct
    private void init() {

        setListPacientes(new LazyPacienteDataModel(pacientesFacade));
        listaMotivo = new ArrayList();
        setListMedicos(new LazyMedicoDataModel(usuariosFacade));
        cargarEspecialidadesMedicos();
        listaCitas = new ArrayList();
        indexController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{indexController}", IndexController.class);

    }
  //buscar paciente por identificacion
    public void findPaciente() {
        if (getIdentificacion() != null) {
            if (!identificacion.isEmpty()) {

                pacienteSeleccionado = pacientesFacade.buscarPorIdentificacion(getIdentificacion());
                if (pacienteSeleccionado == null) {
                    imprimirMensaje("Error", "No se encontro el paciente", FacesMessage.SEVERITY_ERROR);
                    setHayPacienteSeleccionado(false);
                    setDisplayPaciente("none");
                } else {
                    pacienteSeleccionado.setEdad(calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
                    setDisplayPaciente("block");
                    setPacienteSeleccionado(pacienteSeleccionado);
                    setHayPacienteSeleccionado(true);
                    RequestContext.getCurrentInstance().update("cargar");
                    try {
                    } catch (Exception e) {
                    }

                }
            } else {
                setPacienteSeleccionado(null);
                setDisplayPaciente("none");
                setHayPacienteSeleccionado(false);
            }
        } else {
            setPacienteSeleccionado(null);
            setDisplayPaciente("none");
            setHayPacienteSeleccionado(false);
        }

    }
//ir a tab de crear paciente
    public void crearPaciente() {
        RequestContext.getCurrentInstance().execute("window.parent.loadTab('Paciente','configuraciones/pacientes.xhtml','-')");
    }
//cargar datos del paciente seleccionado
    public void cargarPaciente() {
        if (pacienteSeleccionado != null) {
            setHayPacienteSeleccionado(true);
            setIdentificacion(pacienteSeleccionado.getIdentificacion());
            if (pacienteSeleccionado.getFechaNacimiento() != null) {
                pacienteSeleccionado.setEdad(calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            }
            setDisplayPaciente("block");
            setTipoIdentificacion(String.valueOf(pacienteSeleccionado.getTipoIdentificacion().getId()));
            try {
            } catch (Exception e) {
            }

        }
    }
//cargar las especialidades existentes
    private void cargarEspecialidadesMedicos() {
        setListEspecialidades((List<SelectItem>) new ArrayList());
        List<CnClasificaciones> lista = usuariosFacade.findEspecialidades();
        for (CnClasificaciones especialidad : lista) {
            if (especialidad != null) {
                getListEspecialidades().add(new SelectItem(especialidad.getId(), especialidad.getDescripcion()));
            }
        }
    }
//display de objetos
    public void functionDisplay() {
        if (getMedicoSeleccionado() != null) {
            setDisplayMedico("block");
        } else {
            setDisplayMedico("none");
        }
    }
//buscar medico por identificacion
    public void findmedico() {

        if (id_medico == null) {
            setEvenModel(null);
            return;
        }
        if (!id_medico.isEmpty()) {
            try {
                setMedicoSeleccionado(usuariosFacade.buscarPorIdentificacion(getId_medico()));
                if (getMedicoSeleccionado() == null) {

                    setHayMedicoSeleccionado(false);
                    imprimirMensaje("Error", "No se encontro el medico", FacesMessage.SEVERITY_ERROR);
                    setNombreCompleto(null);
                } else {
                    setMedicoSeleccionado(getMedicoSeleccionado());

                    setHayMedicoSeleccionado(true);
                    setNombreCompleto(getMedicoSeleccionado().getNombreCompleto());
                }
                functionDisplay();
                loadEvents();
            } catch (Exception e) {
                imprimirMensaje("Error", "Ingrese identificacion de medico valido", FacesMessage.SEVERITY_ERROR);
                setHayMedicoSeleccionado(false);
                setNombreCompleto(null);
                setDisplayMedico("none");
            }
        } else {
            setMedicoSeleccionado(null);
            functionDisplay();
            setHayMedicoSeleccionado(false);
            setNombreCompleto(null);
            setEvenModel(null);
        }
    }
//cargar datos medico seleccionado
    public void cargarMedico(ActionEvent actionEvent) {
        if (getMedicoSeleccionado() != null) {
            setId_medico(getMedicoSeleccionado().getIdentificacion());           
             if (getMedicoSeleccionado().getEspecialidad().getDescripcion().equals("FISIOTERAPIA") && sesionesTfFacade.tieneSesionesPendientes(pacienteSeleccionado)) {                
                setEsFisioterapia(true);
            } else {
                setEsFisioterapia(false);
            }
            setHayMedicoSeleccionado(true);
            setNombreCompleto(getMedicoSeleccionado().getNombreCompleto());
        }
        loadEvents();
        functionDisplay();
    }
//cargar citas previas del medico
    public void cargarCitas() {
        setListaCitas(citasFacade.findCitas());
    }
//agendar la cita
    public void guardarCita() throws ParseException {
        try {
            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            if (pacienteSeleccionado == null) {
                imprimirMensaje("Error", "Necesita ingresar el paciente", FacesMessage.SEVERITY_ERROR);
                return;
            }

            if (getMedicoSeleccionado() == null) {
                imprimirMensaje("Error", "Necesita ingresar el medico", FacesMessage.SEVERITY_ERROR);
                return;
            }

            if (turnoSeleccionado == null) {
                imprimirMensaje("Error", "Necesita elegir el turno de la cita", FacesMessage.SEVERITY_ERROR);
                return;
            }

            if (esFisioterapia && sesionesTfFacade.tieneSesionesPendientes(pacienteSeleccionado)) {
                if (file == null) {
                    imprimirMensaje("Error", "Para terapia fisica es obligatorio subir una orden medica", FacesMessage.SEVERITY_ERROR);
                    return;
                }
            }
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha = turnosFacade.FechaServidorddmmyyhhmmTimestamp();
            Date fechaActual = formatter.parse(formatter.format(fecha));
            if (turnoSeleccionado.getFecha().before(fechaActual)) {
                imprimirMensaje("Error", "El turno seleccionado esta programado para una fecha previa", FacesMessage.SEVERITY_ERROR);
                return;
            }

            CnDiasNoLaborales noLabora = noLaborablesFacade.FindDiaNoLaboral(turnoSeleccionado.getFecha());
            if (noLabora != null) {
                imprimirMensaje("Error", "El dia del turno seleccionado no está disponible", FacesMessage.SEVERITY_ERROR);
                return;
            }

            if (motivoConsultas == 0) {
                imprimirMensaje("Error", "Especifique el motivo de consulta", FacesMessage.SEVERITY_ERROR);
                return;
            }
            List<Integer> listaIdTurno = new ArrayList();
            listaIdTurno.add(turnoSeleccionado.getIdTurno());
            int totalTurnoDisponible = turnosFacade.totalTurnosDisponibles(listaIdTurno);
            if (totalTurnoDisponible != 1) {
                loadEvents();
                imprimirMensaje("Error", "El turno ya se encuentra asignado", FacesMessage.SEVERITY_ERROR);
                return;
            }

            if (pacienteSeleccionado != null) {
                CtCitas nuevaCita = new CtCitas();
                nuevaCita.setIdPaciente(pacienteSeleccionado);
                nuevaCita.setIdMedico(getMedicoSeleccionado());
                nuevaCita.setIdTurno(turnoSeleccionado);
                CnClasificaciones clasificaciones;
                if (motivoConsultas != 0) {
                    clasificaciones = clasificacionesFacade.find(motivoConsultas);
                    nuevaCita.setTipoCita(clasificaciones);
                }
                nuevaCita.setFechaRegistro(citasFacade.FechaServidorddmmyyhhmmTimestamp());
                nuevaCita.setFechaHoraRegistro(citasFacade.FechaServidorddmmyyhhmmTimestamp());
                nuevaCita.setIdUsuarioRegistro(indexController.getUsuarioActual().getIdUsuario());
                nuevaCita.setSubsecuente(subsecuente);
                nuevaCita.setAtendida(false);
                nuevaCita.setCancelada(false);
                nuevaCita.setTieneRegAsociado(false);
                if(esFisioterapia){
                    nuevaCita.setIdArchivoTf(idArchivoFt);
                    nuevaCita.setEsFisioterpia(true);
                }
                citasFacade.create(nuevaCita);

                turnoSeleccionado.setEstado("asignado");
                turnosFacade.edit(turnoSeleccionado);
                
                transaction.commit();
                
                imprimirMensaje("Correcto", "La cita ha sido creada.", FacesMessage.SEVERITY_INFO);
                loadEvents();
                setEsFisioterapia(false);
                setFile(null);
                setEstaSubido(false);

            } else {
                imprimirMensaje("Error", "Es necesario seleccionar el paciente", FacesMessage.SEVERITY_ERROR);
                return;
            }
            limpiarCampos(1);
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(CitaController.class.getName()).log(Level.SEVERE, null, ex);
            imprimirMensaje("Error", "No se pudo crear cita", FacesMessage.SEVERITY_ERROR);
        }

    }

  
//limpiar los campos
    public void limpiarCampos(int va) {
        if (va == 1) {
            setPacienteSeleccionado(null);
            setMedicoSeleccionado(null);
            setListaTipoCitas(null);
            motivoConsultas = 0;
        }
        motivoCancelacion = 0;
        descripcionCancelacion = null;
        setTurnoSeleccionado(null);

    }
//obtener datos de la cita seleccionada
    public void seleccionarCita(ActionEvent actionEvent) {
        int id_cita = (int) actionEvent.getComponent().getAttributes().get("id_cita");
        citaSelecionada = citasFacade.find(id_cita);
    }

  //cancelar cita  
    public void cancelarCita(ActionEvent actionEvent) {
        if (!citaSelecionada.getCancelada() && !citaSeleccionada.getAtendida()) {

            try {
                UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

                transaction.begin();

                citaSelecionada.setCancelada(true);
                CnClasificaciones clasificaciones = clasificacionesFacade.find(motivoCancelacion);
                citaSelecionada.setDescCancelacion(descripcionCancelacion);
                citaSelecionada.setMotivoCancelacion(clasificaciones);
                Date aux = citasFacade.FechaServidorddmmyyhhmmTimestamp();
                citaSelecionada.setFechaCancelacion(aux);
                citaSeleccionada.setIdUsuarioCancelacion(indexController.getUsuarioActual().getIdUsuario());
                citasFacade.edit(citaSelecionada);
                CtTurnos turno = citaSelecionada.getIdTurno();
                turno.setEstado("disponible");
                turnosFacade.edit(turno);

                transaction.commit();
                imprimirMensaje("Correcto", "Cita " + citaSelecionada.getIdCita() + " cancelada", FacesMessage.SEVERITY_INFO);
              
                listaCitas.remove(citaSelecionada);
                setListaCitas(listaCitas);

            } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
                Logger.getLogger(CancelarController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            imprimirMensaje("Error", "Cita " + citaSelecionada.getIdCita() + " ya se encuentra cancelada, atendida o facturada.", FacesMessage.SEVERITY_ERROR);
        }
        descripcionCancelacion = null;

    }

    private void seleccionarCita(Integer id) {
        citaSeleccionada = citasFacade.findCitasByTurno(id);

        if (getCitaSeleccionada() != null) {
            setCitaSeleccionada(citaSeleccionada);
            setRend(true);
            setRenderizarbotones(true);
            setRendBtnElegir(false);
            noTurno = citaSeleccionada.getIdTurno().getContador().toString();
        } else {
            setCitaSeleccionada(null);
            seleccionarTurno(id);
            setRenderizarbotones(false);
            setRend(false);
            setRendBtnElegir(true);
        }
        CtTurnos turno = turnosFacade.findById(id);
        if (!turno.getEstado().equals("no_disponible")) {
         
            RequestContext.getCurrentInstance().update("formCalendarioCitas:eventDetails");
            RequestContext.getCurrentInstance().execute("PF('eventDialog').show()");

        }
    }

    private void seleccionarTurno(Integer id) {
        CtTurnos turno = turnosFacade.buscarPorId(id);
        noTurno = turno.getContador().toString();
        setTurnoSeleccionado(turno);

    }

    public void loadEvents() {
        setEvenModel(new LazyAgendaModel(medicoSeleccionado.getIdUsuario(), turnosFacade, citasFacade, "cita"));
    }

    public void onEventSelect(SelectEvent selectEvent) {
        setEvent((ScheduleEvent) selectEvent.getObject());
        if (event.getTitle() != null) {
            String[] vector = event.getTitle().split(" - ");
            idTurno = vector[0];
            seleccionarCita(Integer.parseInt(idTurno));
        }
    }
    
     public void manageFileTf(FileUploadEvent event) {

        try {
            file = event.getFile();
            SimpleDateFormat dt = new SimpleDateFormat("yyyyymmddhhmmss");
            String nomarch = "TF_" + pacienteSeleccionado.getIdPaciente() + "_" + dt.format(Cncie10Facade.FechaServidorddmmyyhhmmTimestamp())//diferenciar el usuario actual
                    + file.getFileName().substring(file.getFileName().lastIndexOf("."), file.getFileName().length());//colocar extension
            String path = indexController.getUrlFileTf();
            //revisar si sube bien el archivo. cambio aumento de nomarch en la siguiente linea
            String descriparchivo = "orden para fisioterapia del paciente " + pacienteSeleccionado.getNombreCompleto();
            if (uploadArchivo(file, path, nomarch)) {
                //guardar en base de datos
                try {
                    HcArchivos a = new HcArchivos();
                    a.setDescripcion(descriparchivo);
                    a.setUrlFile(path + nomarch);
                    a.setFechaUpload(hcArchivosFacade.FechaServidorddmmyyhhmmTimestamp());
                    a.setIdPaciente(pacienteSeleccionado);
                    a.setUsuarioUpload(indexController.getUsuarioActual().getIdUsuario());
                    hcArchivosFacade.create(a);
                    idArchivoFt = hcArchivosFacade.getIdArchivoByNombreArchivo(path + nomarch);
                    //
                    //listaArchivo = hcArchivosFacade.getHcArchivosByPaciente(pacienteSeleccionado);
                    imprimirMensaje("Información", "El archivo se guardo", FacesMessage.SEVERITY_INFO);
                    descriparchivo = "";
                    setEstaSubido(true);
                    RequestContext.getCurrentInstance().update("subirOrdenTf");
                } catch (Exception e) {
                    imprimirMensaje("Error", "El archivo no se guardo", FacesMessage.SEVERITY_WARN);
                }

            }

        } catch (Exception ex) {
            System.out.println("Error 20 en " + this.getClass().getName() + ":" + ex.toString());
        }

    }


    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<CtCitas> getListaCitas() {
        return listaCitas;
    }

    public void setListaCitas(List<CtCitas> listaCitas) {
        this.listaCitas = listaCitas;
    }

  
    public CtCitas getCitaSelecionada() {
        return citaSelecionada;
    }

    public void setCitaSelecionada(CtCitas citaSelecionada) {
        this.citaSelecionada = citaSelecionada;
    }

    public int getMotivoConsultas() {
        return motivoConsultas;
    }

    public void setMotivoConsultas(int motivoConsulta) {
        this.motivoConsultas = motivoConsulta;
    }

    public int getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(int motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public String getDescripcionCancelacion() {
        return descripcionCancelacion;
    }

    public void setDescripcionCancelacion(String descripcionCancelacion) {
        this.descripcionCancelacion = descripcionCancelacion;
    }

    public List<SelectItem> getListaTipoCitas() {
        return listaTipoCitas;
    }

    public void setListaTipoCitas(List<SelectItem> listaTipoCitas) {
        this.listaTipoCitas = listaTipoCitas;
    }

   

    public LazyDataModel<CnPacientes> getListPacientes() {
        return listPacientes;
    }

    public void setListPacientes(LazyDataModel<CnPacientes> listPacientes) {
        this.listPacientes = listPacientes;
    }

    public CnUsuarios getPrestadrSeleccionado() {
        return getMedicoSeleccionado();
    }

    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public boolean isHayPacienteSeleccionado() {
        return hayPacienteSeleccionado;
    }

    public void setHayPacienteSeleccionado(boolean hayPacienteSeleccionado) {
        this.hayPacienteSeleccionado = hayPacienteSeleccionado;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getDisplayPaciente() {
        return displayPaciente;
    }

    public void setDisplayPaciente(String displayPaciente) {
        this.displayPaciente = displayPaciente;
    }

    public String getDisplayMedico() {
        return displayMedico;
    }

    public void setDisplayMedico(String displayMedico) {
        this.displayMedico = displayMedico;
    }

    public boolean isHayMedicoSeleccionado() {
        return hayMedicoSeleccionado;
    }

    public void setHayMedicoSeleccionado(boolean hayMedicoSeleccionado) {
        this.hayMedicoSeleccionado = hayMedicoSeleccionado;
    }

    public LazyDataModel<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(LazyDataModel<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public String getId_medico() {
        return id_medico;
    }

    public void setId_medico(String id_medico) {
        this.id_medico = id_medico;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public List<SelectItem> getListEspecialidades() {
        return listEspecialidades;
    }

    public void setListEspecialidades(List<SelectItem> listEspecialidades) {
        this.listEspecialidades = listEspecialidades;
    }

    public CnUsuarios getMedicoSeleccionado() {
        return medicoSeleccionado;
    }

    public void setMedicoSeleccionado(CnUsuarios medicoSeleccionado) {
        this.medicoSeleccionado = medicoSeleccionado;
    }

    public CtTurnos getTurnoSeleccionado() {
        return turnoSeleccionado;
    }

    public void setTurnoSeleccionado(CtTurnos turnoSeleccionado) {
        this.turnoSeleccionado = turnoSeleccionado;
    }

    public LazyAgendaModel getEvenModel() {
        return evenModel;
    }

    public void setEvenModel(LazyAgendaModel evenModel) {
        this.evenModel = evenModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }


    public boolean isRendBtnElegir() {
        return rendBtnElegir;
    }

    public void setRendBtnElegir(boolean rendBtnElegir) {
        this.rendBtnElegir = rendBtnElegir;
    }

    public CtCitas getCitaSeleccionada() {
        return citaSeleccionada;
    }

    public void setCitaSeleccionada(CtCitas citaSeleccionada) {
        this.citaSeleccionada = citaSeleccionada;
    }

    public boolean isRenderizarbotones() {
        return renderizarbotones;
    }

    public void setRenderizarbotones(boolean renderizarbotones) {
        this.renderizarbotones = renderizarbotones;
    }

    public String getIdTurno() {
        return idTurno;
    }

    public String getNoTurno() {
        return noTurno;
    }

 
    public CtCitasFacade getCitasFacade() {
        return citasFacade;
    }

    public void setCitasFacade(CtCitasFacade citasFacade) {
        this.citasFacade = citasFacade;
    }

    public CtTurnosFacade getTurnosFacade() {
        return turnosFacade;
    }

    public void setTurnosFacade(CtTurnosFacade turnosFacade) {
        this.turnosFacade = turnosFacade;
    }

    public CnClasificacionesFacade getClasificacionesFacade() {
        return clasificacionesFacade;
    }

    public void setClasificacionesFacade(CnClasificacionesFacade clasificacionesFacade) {
        this.clasificacionesFacade = clasificacionesFacade;
    }

    public CnPacientesFacade getPacientesFacade() {
        return pacientesFacade;
    }

    public void setPacientesFacade(CnPacientesFacade pacientesFacade) {
        this.pacientesFacade = pacientesFacade;
    }

    public CnUsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }

    public void setUsuariosFacade(CnUsuariosFacade usuariosFacade) {
        this.usuariosFacade = usuariosFacade;
    }

    public List<SelectItem> getListaMotivo() {
        if (listaMotivo != null) {
            listaMotivo.clear();
        }
        List<SelectItem> listaRetorno = new ArrayList<>();
        List<CnClasificaciones> listaCategorias = clasificacionesFacade.buscarPorMaestro("MotivoConsulta");
        for (CnClasificaciones clasificacion : listaCategorias) {
            listaRetorno.add(new SelectItem(clasificacion.getId(), clasificacion.getCodigo() + " - " + clasificacion.getDescripcion()));
        }
        for (SelectItem servicio : listaRetorno) {
            if (servicio.getValue().toString().equals("475")) {
                listaMotivo.add(servicio);
            }
        }
        for (SelectItem servicio : listaRetorno) {
            if (!servicio.getValue().toString().equals("475")) {
                listaMotivo.add(servicio);
            }
        }
        return listaMotivo;
    }

    public void setListaMotivo(List<SelectItem> listaMotivoConsulta) {
        this.listaMotivo = listaMotivoConsulta;
    }

    public boolean isSubsecuente() {
        return subsecuente;
    }

    public void setSubsecuente(boolean subsecuente) {
        this.subsecuente = subsecuente;
    }

    public boolean isEsFisioterapia() {
        return esFisioterapia;
}

    public void setEsFisioterapia(boolean esFisioterapia) {
        this.esFisioterapia = esFisioterapia;
    }

    public boolean isEstaSubido() {
        return EstaSubido;
    }

    public void setEstaSubido(boolean EstaSubido) {
        this.EstaSubido = EstaSubido;
    }
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public boolean isSesionesPendientes() {
        return sesionesPendientes;
    }

    public void setSesionesPendientes(boolean sesionesPendientes) {
        this.sesionesPendientes = sesionesPendientes;
    }
}
