
package controller.citas;

import util.CategoriaMaestro;
import util.MetodosGenerales;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtCitasFacade;
/**
 *
 * @author kenlly
 */
@Named(value = "informeExcelController")
@ViewScoped
public class InformeExcelController extends MetodosGenerales implements java.io.Serializable {

    @EJB
    private CnUsuariosFacade medicoFacade;
    @EJB
    private CtCitasFacade citaFacade;
    @EJB
    private CnClasificacionesFacade clasificacionesFacade;

    private String medico;
    private Date fechaDesde;
    private Date fechaHasta;
    private String motivoConsulta;
    private String estado;
    private String estadoPaciente;
    private boolean renderBuscar;
    private Boolean customExporter;
    private List<CnUsuarios> listMedicos;
    private List<CtCitas> listaCitas;
    private List<CnClasificaciones> listaMotivoConsulta;

    public InformeExcelController() {
        medico = "";
        motivoConsulta = "";
        estadoPaciente = "1";
        estado = "";
        customExporter=false;

    }

    @PostConstruct
    public void init() {
        listMedicos = medicoFacade.findMedicos();
        listaCitas = new ArrayList<>();
        listaMotivoConsulta = clasificacionesFacade.buscarPorMaestro(CategoriaMaestro.MotivoConsulta.toString());
    }

    public void limpiar() {
        listaCitas.clear();
        renderBuscar = false;
    }

    public void consultar() {
        listaCitas.clear();
        if (medico == null) {
            imprimirMensaje("Error al consultar", "Debe Ingresar Medico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaDesde == null || fechaHasta == null) {
            imprimirMensaje("Error al consultar", "Debe seleccionar rango de fechas", FacesMessage.SEVERITY_ERROR);
            return;
        }

        String filtro = "";
        if (medico != null) {
            if (!medico.equals("")) {
                filtro += " AND c.idMedico.idUsuario =" + medico;
            }
        }
        if (fechaDesde != null && fechaHasta != null) {
            filtro += " AND c.fechaRegistro BETWEEN :param1 and :param2 ";
        }
        if (motivoConsulta != null) {
            if (!motivoConsulta.equals("")) {
                filtro += " AND c.tipoCita.id = " + motivoConsulta + "";
            }
        }
        if (estado != null) {
            if (!estado.equals("")) {
                switch (estado) {
                    case "1"://Asignada
                        filtro += " AND c.atendida=false and c.cancelada=false ";
                        break;
                    case "2"://Atendida
                        filtro += " AND c.atendida=TRUE ";
                        break;
                    case "3"://Cancelada
                        filtro += " AND c.cancelada=TRUE";
                        break;
                   
                }
            }
        }
        listaCitas = citaFacade.getInformeFiltro(filtro, fechaDesde, fechaHasta);
        if (!listaCitas.isEmpty()) {
            renderBuscar = true;
        } else {
            imprimirMensaje("No hay registros", "No se encontró registros", FacesMessage.SEVERITY_INFO);
            renderBuscar = false;
        }

    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(List<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public boolean isRenderBuscar() {
        return renderBuscar;
    }

    public void setRenderBuscar(boolean renderBuscar) {
        this.renderBuscar = renderBuscar;
    }

    public List<CtCitas> getListaCitas() {
        return listaCitas;
    }

    public void setListaCitas(List<CtCitas> listaCitas) {
        this.listaCitas = listaCitas;
    }

    public List<CnClasificaciones> getListaMotivoConsulta() {
        return listaMotivoConsulta;
    }

    public void setListaMotivoConsulta(List<CnClasificaciones> listaMotivoConsulta) {
        this.listaMotivoConsulta = listaMotivoConsulta;
    }

    public String getEstadoPaciente() {
        return estadoPaciente;
    }

    public void setEstadoPaciente(String estadoPaciente) {
        this.estadoPaciente = estadoPaciente;
    }

    public Boolean getCustomExporter() {
        return customExporter;
    }

    public void setCustomExporter(Boolean customExporter) {
        this.customExporter = customExporter;
    }

}

