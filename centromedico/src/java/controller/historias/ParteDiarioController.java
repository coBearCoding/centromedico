
package controller.historias;

import util.MetodosGenerales;
import util.RegistroReporte;
import util.XlsxBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletResponse;
import controller.seguridad.IndexController;
import jpa.modelo.CnUsuarios;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.HcRegistroFacade;
/**
 *
 * @author kenlly
 */
@Named(value = "parteDiarioController")
@ViewScoped
public class ParteDiarioController extends MetodosGenerales implements java.io.Serializable {

    @EJB
    private CnUsuariosFacade medicoFacade;
    @EJB
    private HcRegistroFacade registroFacade;
    @EJB
    CnClasificacionesFacade clasificacionesFacade;
   
    private IndexController login = new IndexController();

    private CnUsuarios medicoactual = new CnUsuarios();
    
    private String medico;
    private Date fechaDesde;
    private String fechaFormat;
    
    private List<CnUsuarios> listMedicos;
    private List<RegistroReporte> listaRegistro;
    
    SimpleDateFormat dateformatter = new SimpleDateFormat ("dd/MM/yyyy");
   
     // Path of a file 
    private String path; 
    //static File file = new File(FILEPATH); 
  
    // Method which write the bytes into a file 
    private void writeByte(byte[] bytes) 
    { 
        try { 
            
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            
            
            
            String fecha = getfecha(fechaFormat);
            String full_path = "RDACAA-" + medicoFacade.find(Integer.parseInt(medico)).getPrimerNombre()+medicoFacade.find(Integer.parseInt(medico)).getPrimerApellido() + "-" +fecha + ".xlsx";
            File file = new File(full_path);
            
            response.reset();
            response.setHeader("Content-Type", "application/xlsx");
            //reemplazar
            //if (file.exists()) {
              //  file.delete();
            //}
            // Initialize a pointer 
            // in file using OutputStream 
            OutputStream 
                os 
                = new FileOutputStream(file); 
  
            // Starts writing the bytes in it 
            os.write(bytes); 
             // Close the file 
            os.close(); 
            
            
             InputStream in = new FileInputStream(file);

            // Gets MIME type of the file
            String mimeType = new MimetypesFileTypeMap().getContentType(full_path);

            if (mimeType == null) {
                // Set to binary type if MIME mapping not found
                mimeType = "application/octet-stream";
            }
            System.out.println("MIME type: " + mimeType);

            // Modifies response
            response.setContentType(mimeType);
            response.setContentLength((int) file.length());

            // Forces download
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
            response.setHeader(headerKey, headerValue);

            // obtains response's output stream
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = in.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            in.close();
            outStream.close();

            System.out.println("File downloaded at client successfully");
            
            if (file.exists()) {
                file.delete();
            }
            
           // FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo generado exitosamente", "El archivo se encuentra en la siguiente ruta:" + full_path);
         
           // PrimeFaces.current().dialog().showMessageDynamic(message);
           // System.out.println("Successfully"
             //                  + " byte inserted"); 
  
           
        } 
  
        catch (Exception e) { 
            System.out.println("Exception: " + e); 
        } 
    } 

    public ParteDiarioController() {
        medico = "";
       
    }

    @PostConstruct
    public void init() {
        listMedicos = medicoFacade.findMedicos();

        if (System.getProperty("os.name").matches(".*Windows.*")){
            path ="C:\\temp\\"; 
        }else{
            path="/tmp/"; 
        } 
    }

    public void limpiar() {
        
    }
    
    public String getfecha(String f){
        String[] parts = f.split("/");
        String dia = parts[0]; 
        String mes = parts[1]; 
        String anio = parts[2]; 
        
        String fechacompleta = dia + mes + anio;
        return fechacompleta;
    }
    
    public List<RegistroReporte> listaResultado(){
        List<RegistroReporte> lista = new ArrayList<>();
        
        fechaFormat = dateformatter.format(fechaDesde);
        
        
        String sql = "select * from f_partediario("+ Integer.parseInt(medico) +",'"+ fechaFormat +"')" ;
        lista = registroFacade.findBySqlNativo(sql);
        
        return lista;
    }

    public void generarParteDiario(ActionEvent actionEvent){
        
        if(fechaDesde == null){
            imprimirMensaje("Error", "El campo fecha es obligatorio", FacesMessage.SEVERITY_ERROR);
            return;
        }
        
        if(actionEvent != null ){
            medico = String.valueOf(actionEvent.getComponent().getAttributes().get("medico"));
        }        
        medicoactual = medicoFacade.find(Integer.parseInt(medico));
        listaRegistro = listaResultado();
        
        if(listaRegistro.isEmpty()){
            imprimirMensaje("Info", "No existen datos para generar Parte Diario", FacesMessage.SEVERITY_INFO);
            return;
        }
        
       byte[] report = new XlsxBuilder().
        startSheet("Parte Diario").                            // start with sheet
        startRow().                                          // then go row by row
        setRowTitleHeight().                                 // set row style, add borders and so on 
        addTitleTextColumn("CENTRO MUNICIPAL DE ASISTENCIA MÉDICA SANTA ANA").                    // add columns
        startRow().                                          // then go row by row
        setRowTitleHeight().                                 // set row style, add borders and so on 
        addTitleTextColumn("REGISTRO DIARIO AUTOMATIZADO DE CONSULTAS Y ATENCIONES AMBULATORIAS (RDACAA)").                    // add columns
        startRow().                                          // another row
        setRowTitleHeight().                                 // row styling
        addBoldTextLeftAlignedColumn("DATOS DEL PROFESIONAL:").
        startRow().                                          // another row
        setRowTitleHeight().                                 // row styling
        //setRowThinBottomBorder().
        addBoldTextLeftAlignedColumn("Nombres y Apellidos:").
        addTextLeftAlignedColumn(medicoactual.getNombreCompleto()).
        addBoldTextLeftAlignedColumn("Sexo").
        addTextLeftAlignedColumn(medicoactual.getGenero() == null ? "" : medicoactual.getGenero().getCodigo()).
        addBoldTextLeftAlignedColumn("Fecha de Nacimiento").
        addTextLeftAlignedColumn(medicoactual.getFechanacimiento() == null ? "" : dateformatter.format(medicoactual.getFechanacimiento())).
        addBoldTextLeftAlignedColumn("Formación Profesional").
        addTextLeftAlignedColumn(medicoactual.getFormacionprofesional()).
        addBoldTextLeftAlignedColumn("Especialidad/Subespecialidad").
        addTextLeftAlignedColumn(medicoactual.getEspecialidad() == null ? "" : medicoactual.getEspecialidad().getDescripcion()).
        startRow().                                          // another row
        setRowTitleHeight().                                 // row styling
        addBoldTextLeftAlignedColumn("Nacionalidad:").
        addTextLeftAlignedColumn(medicoactual.getNacionalidad() == null ? "" : clasificacionesFacade.find(medicoactual.getNacionalidad()).getCodigo()).
        addBoldTextLeftAlignedColumn("Autoidentificación:").
        addTextLeftAlignedColumn(medicoactual.getAutoidentificacion() == null ? "" : clasificacionesFacade.find(medicoactual.getAutoidentificacion()).getCodigo()).
        addBoldTextLeftAlignedColumn("C.C. ó Pasaporte").
        addTextLeftAlignedColumn(medicoactual.getIdentificacion()).
        addBoldTextLeftAlignedColumn("Código MSP").
        addTextLeftAlignedColumn(medicoactual.getCodmsp()).
        startRow().                                          // another row
        setRowTitleHeight().                                 // row styling
        setRowThinBottomBorder().
        addBoldTextLeftAlignedColumn("Fecha:").
        addTextLeftAlignedColumn(fechaFormat).
        startRow().                                          // empty row
        startRow().                                          // header row
        setRowTitleHeight().
        setRowThickTopBorder().
        setRowThickBottomBorder().
        addBoldTextCenterAlignedColumn("Apellidos y Nombres").
        addBoldTextCenterAlignedColumn("Nº. de cédula de Ciudadanía ó Nº. de Pasaporte ó Nº. de Historia Clínica").
        addBoldTextCenterAlignedColumn("Sexo").
        addBoldTextCenterAlignedColumn("Orientación sexual").
        addBoldTextCenterAlignedColumn("Identidad de Género").
        addBoldTextCenterAlignedColumn("Fecha de Nacimiento").
        addBoldTextCenterAlignedColumn("Nº. de cédula de ciudadanía del representante").
        addBoldTextCenterAlignedColumn("Nacionalidad").
        addBoldTextCenterAlignedColumn("Auto identificación étnica").
        addBoldTextCenterAlignedColumn("Nacionalidades o Pueblos").
        addBoldTextCenterAlignedColumn("Aporta ó es Afiliado al:").
        addBoldTextCenterAlignedColumn("Grupos Prioritarios").
        addBoldTextCenterAlignedColumn("Semana de gestación").
        addBoldTextCenterAlignedColumn("Provincia").
        addBoldTextCenterAlignedColumn("Cantón").
        addBoldTextCenterAlignedColumn("Parroquia").
        addBoldTextCenterAlignedColumn("Barrio, Sector, Recinto, Comunidad").
        addBoldTextCenterAlignedColumn("Descripción").
        addBoldTextCenterAlignedColumn("Código C.I.E. - 10").
        addBoldTextCenterAlignedColumn("Tipo de Atención").
        addTextLeftAlignedColumnList(listaRegistro).
        startRow().
        setRowThinTopBorder().
        startRow().
        //startRow().                                          // footer row
        //addTextLeftAlignedColumn("This is just a footer  'Lorem ipsum dolor...'").
        setColumnSize(0, "XXXXXXXXXXXXXXXXXXXXXXXXXX".length()).          // setting up column sizes at the end of the sheet
        setAutoSizeColumn(1).
        setAutoSizeColumn(2).
        setAutoSizeColumn(3).
        setAutoSizeColumn(4).
        setAutoSizeColumn(5).
        setAutoSizeColumn(6).
        setAutoSizeColumn(7).
        setAutoSizeColumn(8).
        setAutoSizeColumn(9).
        setAutoSizeColumn(10).
        setAutoSizeColumn(11).
        setAutoSizeColumn(12).
        setAutoSizeColumn(13).
        setAutoSizeColumn(14).
        setAutoSizeColumn(15).
        setAutoSizeColumn(16).
        setAutoSizeColumn(17).
        setAutoSizeColumn(18).
        setAutoSizeColumn(19).
        setAutoSizeColumn(20).
        build();
// now deal with byte array (e.g. write to the file or database)
       writeByte(report);
   }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public List<RegistroReporte> getListaRegistro() {
        return listaRegistro;
    }

    public void setListaRegistro(List<RegistroReporte> listaRegistro) {
        this.listaRegistro = listaRegistro;
    }

   

    public List<CnUsuarios> getListMedicos() {
        return listMedicos;
    }

    public void setListMedicos(List<CnUsuarios> listMedicos) {
        this.listMedicos = listMedicos;
    }

    public String getFechaFormat() {
        return fechaFormat;
    }

    public void setFechaFormat(String fechaFormat) {
        this.fechaFormat = fechaFormat;
    }

   

}

