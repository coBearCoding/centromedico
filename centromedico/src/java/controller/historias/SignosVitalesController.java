package controller.historias;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import util.MetodosGenerales;
import util.NodoArbolHistorial;
import util.TipoNodoEnum;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.GenericoController;
import controller.seguridad.IndexController;
import jpa.modelo.CnEmpresa;
import jpa.modelo.CnPacientes;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.modelo.HcCamposReg;
import jpa.modelo.HcDetalle;
import jpa.modelo.HcRegistro;
import jpa.modelo.HcTipoReg;
import jpa.facade.CnEmpresaFacade;
import jpa.facade.CnPacientesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import jpa.facade.HcCamposRegFacade;
import jpa.facade.HcDetalleFacade;
import jpa.facade.HcDetalleTrFacade;
import jpa.facade.HcRegistroFacade;
import jpa.facade.HcSesionesTrFacade;
import jpa.facade.HcTipoRegFacade;
import jpa.modelo.HcTfTratamientos;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@ManagedBean(name = "signosvitalesController")
@SessionScoped
public class SignosVitalesController extends MetodosGenerales implements Serializable {

  
    @EJB
    HcRegistroFacade registroFacade;
    @EJB
    CtTurnosFacade turnosFacade;
    @EJB
    CtCitasFacade citasFacade;
    @EJB
    HcTipoRegFacade tipoRegistroFacade;
    @EJB
    HcCamposRegFacade camposRegFacade;
    @EJB
    CnPacientesFacade pacientesFacade;   
    @EJB
    CnUsuariosFacade usuariosFacade; 
    @EJB
    CnEmpresaFacade empresaFacade;
    @EJB
    HcDetalleFacade detalleFacade;
    @EJB
    HcDetalleTrFacade detalleTrFacade;
    @EJB
    HcSesionesTrFacade sesionesTrFacade;
   
    private HcTipoReg tipoRegistroActual;
    private CnPacientes pacienteSeleccionadoTabla;
    private CnPacientes pacienteSeleccionado;
    private CnPacientes pacienteTemporal;
    private HcRegistro registroEncontrado = null;
    private CnEmpresa empresa;
    private CtCitas citaActual = null;
    
    private DatosHistoriaClinica datosFichamedica = new DatosHistoriaClinica();
    private IndexController indexController;
    private GenericoController genericoController;
         
    private Date parametroFechaInicial = null;
    private Date parametroFechaFinal = null;
    private Date fechaRegistro;
    private Date fechaSistema;

    private String[] totalHistoriasClinicas;
    private String tipoHistoriaClinica = "92";    
    private String url = "";
    private String nombreTipoHistoriaClinica = "";
    private String identificacionPaciente = "";
    private String tipoIdentificacionPaciente = "";
    private String nombrePaciente = "Paciente";
    private String generoPaciente = "";
    private String edadPaciente = "";
    private String idMedico = "";
    private String especialidadMedico = "";
    private String turnoCita = "";
    private String noTurno;
    
    int posicion = 0;
    
    private boolean isPacienteSelected = false;
    private boolean isRegistroEdited = false;
    private boolean btnDisabled = true;
    private boolean btnEditarRendered = true;
    private boolean cargandoDesdeTab = false;
    
    private TreeNode treeNodeRaiz;
    private TreeNode[] treeNodesSeleccionado;
    
    private List<HcTipoReg> listaTipoHistoriaClinica;
    private List<HcTipoReg> listaTipoHistoriaClinicaReporte;
    private List<DatosHistoriaClinica> listaDatosReporte;    

    private final SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat formatoDateHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private final SimpleDateFormat formatoLongDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);//Thu Apr 30 19:00:00 COT 2015
   
    
    public SignosVitalesController() {
        genericoController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{genericoController}", GenericoController.class);

    }

    @PostConstruct
    public void init() {
        listaTipoHistoriaClinica = tipoRegistroFacade.buscarTipoRegistroSignosVitales();
        listaTipoHistoriaClinicaReporte = tipoRegistroFacade.buscarTipoRegistroEnfermeria();
        empresa = empresaFacade.findAll().get(0);
    }

    private String calculoIMC(float peso, float talla) {

        //Infrapeso	IMC menos de 18.5
        //Peso Normal	IMC de 18.5 a 24.9
        //Sobrepeso	IMC de 25 a 29.9
        //Obesidad	IMC 30 o mayor
        //Obesidad Mórbida	IMC 40 o mayor
        //        
        float metros = talla / 100;
        float imcCalculado = peso / (metros * metros);

        String textoImc = "";
        if (imcCalculado < 18.5) {
            textoImc = "Infrapeso";
        } else if (imcCalculado >= 18.5 && imcCalculado < 25) {
            textoImc = "Peso Normal";
        } else if (imcCalculado >= 25 && imcCalculado < 30) {
            textoImc = "Sobrepeso";
        } else if (imcCalculado >= 30 && imcCalculado < 40) {
            textoImc = "Obesidad";
        } else if (imcCalculado >= 40) {
            textoImc = "Obesidad Mórbida";
        }
        double imcCalculadoD = (double) ((int) (imcCalculado * 100) / 100.0);
        return String.valueOf(imcCalculadoD) + ", " + textoImc;
    }

    public void calcularIMC() {
        try {
            float peso = Float.parseFloat(datosFichamedica.getDato0().toString());
            float talla = Float.parseFloat(datosFichamedica.getDato1().toString());
            datosFichamedica.setDato2(calculoIMC(peso, talla));

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

   
    public void cargarDesdeTab(String id) {
        cargandoDesdeTab = true;
        turnoCita = "";
        citaActual = null;
        limpiar();
        String[] splitId = id.split(";");
        if (splitId[0].compareTo("idCita") == 0) {
            citaActual = citasFacade.find(Integer.parseInt(splitId[1]));
            pacienteTemporal = pacientesFacade.find(citaActual.getIdPaciente().getIdPaciente());
            if (pacienteTemporal == null) {
                imprimirMensaje("Error", "No se encontró datos del paciente", FacesMessage.SEVERITY_ERROR);
                cargandoDesdeTab = false;
                RequestContext.getCurrentInstance().update("IdFormFacturacion");
                RequestContext.getCurrentInstance().update("IdMensajeFacturacion");
                return;
            }
            pacienteSeleccionadoTabla = pacienteTemporal;
            cargarPaciente();
            turnoCita = citaActual.getIdTurno().getIdTurno().toString();
            noTurno = citaActual.getIdTurno().getContador().toString();
        }
        RequestContext.getCurrentInstance().update("IdMensajeFacturacion");
        cargandoDesdeTab = false;
    }

    private void cargarHistoriasClinicas() {
        totalHistoriasClinicas = new String[listaTipoHistoriaClinicaReporte.size()];
        for (int i = 0; i < listaTipoHistoriaClinicaReporte.size(); i++) {
            totalHistoriasClinicas[i] = listaTipoHistoriaClinicaReporte.get(i).getIdTipoReg().toString();
        }
    }

    public void cargarHistorialCompleto() {
        btnDisabled = true;
      
        cargarHistoriasClinicas();
        cargarHistorial();
    }

    public void cargarHistorial() {
        

        treeNodeRaiz = new DefaultTreeNode(new NodoArbolHistorial(TipoNodoEnum.RAIZ_HISTORIAL, null, -1, -1, null, null), null);
        NodoArbolHistorial nodoSeleccionTodoNada = new NodoArbolHistorial(TipoNodoEnum.NOVALUE, null, -1, -1, null, "Selección Todo/Ninguno");

        TreeNode nodoInicial = new DefaultTreeNode(nodoSeleccionTodoNada, treeNodeRaiz);
        nodoInicial.setExpanded(true);

        treeNodesSeleccionado = null;
        btnDisabled = true;
        boolean exitenRegistros = false;
        boolean usarFiltroFechas = false;

        if (pacienteSeleccionado == null) {
            imprimirMensaje("Error", "No hay un paciente seleccionado", FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().update("IdFormHistorias:IdPanelHistorico");
            return;
        }
        if ((parametroFechaInicial == null && parametroFechaFinal != null) || (parametroFechaInicial != null && parametroFechaFinal == null)) {
            imprimirMensaje("Error", "Debe especificar fecha inicial y fecha final al tiempo ", FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().update("IdFormHistorias:IdPanelHistorico");
            return;
        }
        if (parametroFechaInicial != null && parametroFechaFinal != null) {
            usarFiltroFechas = true;
        }

        List<HcRegistro> listaRegistrosPaciente;
        
        if (usarFiltroFechas) {
            listaRegistrosPaciente = registroFacade.buscarOrdenado(pacienteSeleccionado.getIdPaciente(), parametroFechaInicial, parametroFechaFinal);
        } else {
            listaRegistrosPaciente = registroFacade.buscarOrdenadoPorFecha(pacienteSeleccionado.getIdPaciente());
        }

        NodoArbolHistorial nodHisNuevo;
        NodoArbolHistorial nodHisFecha;
        NodoArbolHistorial nodHisComparar;
        TreeNode treeNodeFecha = null;
        TreeNode treeNodeCreado;
        for (HcRegistro registro : listaRegistrosPaciente) {
            if (estaEnLista(registro.getIdTipoReg().getIdTipoReg().toString())) {
                exitenRegistros = true;
                nodHisNuevo = new NodoArbolHistorial(
                        TipoNodoEnum.REGISTRO_HISTORIAL,
                        registro.getFechaReg(),
                        registro.getIdRegistro(),
                        registro.getIdTipoReg().getIdTipoReg(),
                        registro.getIdTipoReg().getNombre(),
                        registro.getIdMedico().nombreCompleto());
                if (treeNodeFecha == null) {
                    nodHisFecha = new NodoArbolHistorial(TipoNodoEnum.FECHA_HISTORIAL, registro.getFechaReg(), -1, -1, null, null);
                    treeNodeFecha = new DefaultTreeNode(nodHisFecha, nodoInicial);
                    treeNodeFecha.setExpanded(true);
                    treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                } else {
                    nodHisComparar = (NodoArbolHistorial) treeNodeFecha.getData();
                    if (nodHisNuevo.getStrFecha().compareTo(nodHisComparar.getStrFecha()) == 0) {
                        treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                    } else {
                        nodHisFecha = new NodoArbolHistorial(TipoNodoEnum.FECHA_HISTORIAL, registro.getFechaReg(), -1, -1, null, null);
                        treeNodeFecha = new DefaultTreeNode(nodHisFecha, nodoInicial);
                        treeNodeFecha.setExpanded(true);
                        treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                    }
                }
            }
        }
        if (!exitenRegistros) {
            nodHisNuevo = new NodoArbolHistorial(TipoNodoEnum.NOVALUE, null, -1, -1, "", "NO SE ENCONTRARON REGISTROS");
            treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeRaiz);
        }

       
    }

    private boolean estaEnLista(String idBuscado) {
        
        for (String idTipRegCliSelHis : totalHistoriasClinicas) {
            if (idBuscado.compareTo(idTipRegCliSelHis) == 0) {
                return true;
            }
        }
        return false;
    }

    public void seleccionarRegistroHistorial() {
        btnDisabled = true;
        boolean igualTipoRegistro = true;
        int tipoRegistroEncontrado = -1;

        if (treeNodesSeleccionado != null) {
            int contadorRegistrosSeleccionadosHistorial = 0;
            for (TreeNode nodo : treeNodesSeleccionado) {
                NodoArbolHistorial nodoSeleccionado = (NodoArbolHistorial) nodo.getData();
                if (nodoSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                    if (tipoRegistroEncontrado == -1) {
                        tipoRegistroEncontrado = nodoSeleccionado.getIdTipoRegistro();
                    } else if (tipoRegistroEncontrado != nodoSeleccionado.getIdTipoRegistro()) {
                        igualTipoRegistro = false;
                    }
                    contadorRegistrosSeleccionadosHistorial++;
                }
            }
            if (contadorRegistrosSeleccionadosHistorial == 0) {
                btnDisabled = true;
            } else if (contadorRegistrosSeleccionadosHistorial == 1) {
                btnDisabled = false;
            } else
            {
                if (igualTipoRegistro) {
                    btnDisabled = true;
                } else {
                    btnDisabled = true;
                }
            }
        }
    }

    public void cargarHistoriaClinica() {
       
        if (treeNodesSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        NodoArbolHistorial nodoSeleccionado = null;
        for (TreeNode nodo : treeNodesSeleccionado) {
            nodoSeleccionado = (NodoArbolHistorial) nodo.getData();
            if (nodoSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                break;
            }
        }
        if (nodoSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }

        tipoHistoriaClinica = "";
        tipoHistoriaClinica = String.valueOf(nodoSeleccionado.getIdTipoRegistro());
        limpiar();
        
        registroEncontrado = registroFacade.find(nodoSeleccionado.getIdRegistro());
        if (registroEncontrado.getIdMedico() != null) {
            idMedico = registroEncontrado.getIdMedico().getIdUsuario().toString();
            especialidadMedico = registroEncontrado.getIdMedico().getEspecialidad().getDescripcion();
        } else {
            idMedico = "";
            especialidadMedico = "";
        }
        System.out.println("Medico: " + idMedico);
        fechaRegistro = registroEncontrado.getFechaReg();
        fechaSistema = registroEncontrado.getFechaSis();

        
        Set<HcDetalle> setDetalles = registroEncontrado.getHcDetalleList();
        List<HcDetalle> listaDetalles= new ArrayList<>(setDetalles) ; 
        
        for (HcDetalle detalle : listaDetalles) {
            HcCamposReg campo = camposRegFacade.find(detalle.getHcDetallePK().getIdCampo());
            if (campo != null) {
                if (campo.getTabla() != null && campo.getTabla().length() != 0) {
                    switch (campo.getTabla()) {
                        case "date":
                            try {
                                Date f = formatoDateHora.parse(detalle.getValor());
                                datosFichamedica.setValor(campo.getPosicion(), f);
                            } catch (ParseException ex) {
                                datosFichamedica.setValor(campo.getPosicion(), "");
                            }
                            break;
                        default:
                            datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                            break;
                    }
                } else {
                    datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                }
            } else {
                System.out.println("Error: no se encontro hc_campos_reg.id_campo= " + detalle.getHcDetallePK().getIdCampo());
            }

        }
        isRegistroEdited = true;
        mostrarHistoriaClinica();
        RequestContext.getCurrentInstance().execute("PF('dlgHistorico').hide();");
        RequestContext.getCurrentInstance().update("IdFormHistorias");
    }

  
    private void cargarDatosReporte(HcRegistro registroSeleccionado) {
        listaDatosReporte = new ArrayList<>();
        DatosHistoriaClinica datosReporte = new DatosHistoriaClinica();
        Set<HcDetalle> setCamposHistoria = registroSeleccionado.getHcDetalleList();
        List<HcDetalle> listaCamposHistoria= new ArrayList<>(setCamposHistoria) ; 
       
        List<HcCamposReg> listaCamposSignosVitales = camposRegFacade.buscarPorTipoRegistro(registroSeleccionado.getIdTipoReg().getIdTipoReg());
        for (HcCamposReg campoPorTipoRegistro : listaCamposSignosVitales) {
            datosReporte.setValor(campoPorTipoRegistro.getPosicion(), "<b>" + campoPorTipoRegistro.getNombrePdf() + " </b>");
        }
      
        datosReporte.setValor(51, "<b>HISTORIA No: </b>" + registroSeleccionado.getIdPaciente().getIdentificacion());        
        datosReporte.setValor(53, "<b>NOMBRE: </b>" + pacienteSeleccionado.nombreCompleto());
        datosReporte.setValor(54, "<b>FECHA ATENCION: </b> " + formatoDateHora.format(registroSeleccionado.getFechaReg()));      

        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            datosReporte.setValor(56, "<b>EDAD: </b> " + calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>" + formatoDate.format(pacienteSeleccionado.getFechaNacimiento()));
        } else {
            datosReporte.setValor(56, "<b>EDAD: </b> ");
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>");
        }
        if (pacienteSeleccionado.getSexo()!= null) {
            datosReporte.setValor(57, "<b>SEXO: </b> " + pacienteSeleccionado.getSexo().getDescripcion());
        } else {
            datosReporte.setValor(57, "<b>SEXO: </b> ");
        }

        datosReporte.setValor(59, "<b>DIRECCION: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getDireccion()));
        datosReporte.setValor(60, "<b>TELEFONO: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getTelefono()));

        if (pacienteSeleccionado.getTipoIdentificacion() != null) {          
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        } else {            
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        }
        
        if (pacienteSeleccionado.getEstadoCivil() != null) {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> " + pacienteSeleccionado.getEstadoCivil().getDescripcion());
        } else {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> ");
        }
        
        if (pacienteSeleccionado.getCanton()!= null) {
            datosReporte.setValor(67, "<b>CANTON: </b>" + pacienteSeleccionado.getCanton().getDescripcion());//TELEFONO EMPRESA                
        } else {
            datosReporte.setValor(67, "<b>CANTON: </b>");//TELEFONO EMPRESA                
        }


        if (registroSeleccionado.getIdMedico() != null) {         
            datosReporte.setValor(84, registroSeleccionado.getIdMedico().nombreCompleto());
            if (registroSeleccionado.getIdMedico().getEspecialidad() != null) {            
                datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> " + registroSeleccionado.getIdMedico().getEspecialidad().getDescripcion());//PARA FIRMA  NOMBRE MEDICO
            }           
            datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> Reg. prof. " + registroSeleccionado.getIdMedico().getRegistroProfesional());//NOMBRE MEDICO    
        }
        datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
        datosReporte.setValor(76, "<b>SIGNOS VITALES</b>");              
       

        for (HcDetalle campoDeRegistroEncontrado : listaCamposHistoria) {
            datosReporte.setValor(campoDeRegistroEncontrado.getHcCamposReg().getPosicion(), "<b>" + campoDeRegistroEncontrado.getHcCamposReg().getNombrePdf() + " </b>" + campoDeRegistroEncontrado.getValor());
        }
        
        listaDatosReporte.add(datosReporte);
    }
    
     private void cargarDatosReporteTerapia(HcRegistro registroSeleccionado) {
        listaDatosReporte = new ArrayList<>();
        DatosHistoriaClinica datosReporte = new DatosHistoriaClinica();
        
        datosReporte.setValor(0, detalleTrFacade.valorTratamiento("TR001", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(1, detalleTrFacade.valorTratamiento("TR002", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(2, detalleTrFacade.valorTratamiento("TR003", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(3, detalleTrFacade.valorTratamiento("TR004", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(4, detalleTrFacade.valorTratamiento("TR005", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(5, detalleTrFacade.valorTratamiento("TR006", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(6, detalleTrFacade.valorTratamiento("TR007", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(7, detalleTrFacade.valorTratamiento("TR008", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(8, detalleTrFacade.valorTratamiento("TR009", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(9, detalleTrFacade.valorTratamiento("TR010", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(10, detalleTrFacade.valorTratamiento("TR011", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(11, detalleTrFacade.valorTratamiento("TR012", registroSeleccionado.getIdRegistro()));
        datosReporte.setValor(34, "<b>NÚMERO TERAPIAS: </b>" + sesionesTrFacade.numSesiones(registroSeleccionado.getIdRegistro()));
       
        
        datosReporte.setValor(49, pacienteSeleccionado.nombreCompleto());
        datosReporte.setValor(51, "<b>HISTORIA No: </b>" + registroSeleccionado.getIdPaciente().getIdentificacion());
        
        datosReporte.setValor(53, "<b>NOMBRE: </b>" + pacienteSeleccionado.nombreCompleto());      

        datosReporte.setValor(54, "<b>FECHA ATENCION: </b> " + formatoDateHora.format(registroSeleccionado.getFechaReg()));
       
        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            datosReporte.setValor(56, "<b>EDAD: </b> " + calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>" + formatoDate.format(pacienteSeleccionado.getFechaNacimiento()));
        } else {
            datosReporte.setValor(56, "<b>EDAD: </b> ");
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>");
        }
        if (pacienteSeleccionado.getSexo() != null) {
            datosReporte.setValor(57, "<b>SEXO: </b> " + pacienteSeleccionado.getSexo().getDescripcion());
        } else {
            datosReporte.setValor(57, "<b>SEXO: </b> ");
        }
        datosReporte.setValor(59, "<b>DIRECCION: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getDireccion()));
        datosReporte.setValor(60, "<b>TELEFONO: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getTelefono()));

        if (pacienteSeleccionado.getTipoIdentificacion() != null) {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        } else {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        }
       
        if (pacienteSeleccionado.getEstadoCivil() != null) {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> " + pacienteSeleccionado.getEstadoCivil().getDescripcion());
        } else {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> ");
        }
       
        if (pacienteSeleccionado.getCanton() != null) {
            datosReporte.setValor(67, "<b>CANTÓN: </b>" + pacienteSeleccionado.getCanton().getDescripcion());
        } else {
            datosReporte.setValor(67, "<b>CANTÓN: </b>");//TELEFONO EMPRESA                
        }

        if (registroSeleccionado.getIdMedico() != null) {
          
            datosReporte.setValor(84, registroSeleccionado.getIdMedico().nombreCompleto());
            if (registroSeleccionado.getIdMedico().getEspecialidad() != null) {          
                datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> " + registroSeleccionado.getIdMedico().getEspecialidad().getDescripcion());//PARA FIRMA  NOMBRE MEDICO
            }
          
            datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> Reg. prof. " + registroSeleccionado.getIdMedico().getRegistroProfesional());//NOMBRE MEDICO

        }

        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 96) {
            datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(76, "<b>FÓRMULA</b>");
            datosReporte.setValor(77, "<b>DOSIS / ML</b>");
            datosReporte.setValor(78, "<b>DOSIS / GOTAS</b>");
            datosReporte.setValor(79, "<b>Solución salina al 9%</b>");
            datosReporte.setValor(80, "<b>Combivent ampolla 2 ml</b>");
            datosReporte.setValor(81, "<b>Dexametasona ampolla 4 mg - 1 ml</b>");
            datosReporte.setValor(82, "<b>Ventolin frasco / gotero solución</b>");
            datosReporte.setValor(86, "<b>Fluimucil ampolla</b>");
            datosReporte.setValor(87, "<b>Ambroxol frasco / gotero</b>");
        }
       
        listaDatosReporte.add(datosReporte);
    }


    public void generarReporte() throws JRException, IOException {
        
        if (treeNodesSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        NodoArbolHistorial nodoSeleccionado = null;
        for (TreeNode nodo : treeNodesSeleccionado) {
            nodoSeleccionado = (NodoArbolHistorial) nodo.getData();
            if (nodoSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                break;
            }
        }
        if (nodoSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        HcRegistro registroSeleccionado = registroFacade.find(nodoSeleccionado.getIdRegistro());
     
        if (registroSeleccionado.getIdTipoReg().getUrlPagina().startsWith("creaOrden")){
            cargarDatosReporteTerapia(registroSeleccionado);
        }else {
            cargarDatosReporte(registroSeleccionado);
        }
       
        JRBeanCollectionDataSource beanCollectionDataSource;
        HttpServletResponse httpServletResponse;
        String rutaReporte = "";
        rutaReporte = rutaReporte + "historiaclinica/reportes/";
        rutaReporte = rutaReporte + registroSeleccionado.getIdTipoReg().getUrlPagina();
        rutaReporte = rutaReporte.substring(0, rutaReporte.length() - 6);
        rutaReporte = rutaReporte + ".jasper";

        List<JasperPrint> list = new ArrayList<JasperPrint>();

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String urlImagenes = request.getRequestURL().toString();
        urlImagenes = urlImagenes.substring(0, urlImagenes.indexOf("historias_signosvitales.xhtml")) + "img/";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext2 = (ServletContext) facesContext.getExternalContext().getContext();
        String logoEmpresa = servletContext2.getRealPath("/recursos/img/logo-cmamsa-black.png");
        HashMap mapParams = new HashMap();
        mapParams.put("urlBaseImagenes", urlImagenes);
        mapParams.put("logoEmpresa", logoEmpresa);
        beanCollectionDataSource = new JRBeanCollectionDataSource(listaDatosReporte);
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), mapParams, beanCollectionDataSource);
            list.add(jasperPrint);

            if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 54) {
                rutaReporte = "historiaclinica/reportes/s_paraclinicossolicitados.jasper";
                beanCollectionDataSource = new JRBeanCollectionDataSource(listaDatosReporte);
                httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.setContentType("application/pdf");
                servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), new HashMap(), beanCollectionDataSource);
                list.add(jasperPrint);
                rutaReporte = "historiaclinica/reportes/s_valoracionespecialista.jasper";
                beanCollectionDataSource = new JRBeanCollectionDataSource(listaDatosReporte);
                httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.setContentType("application/pdf");
                servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), new HashMap(), beanCollectionDataSource);
                list.add(jasperPrint);

            }

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, list);
            exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, servletOutputStream);
            exporter.exportReport();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception ex) {
            System.out.println("ERRR " + ex.getLocalizedMessage());
            ex.printStackTrace();
            System.out.println("Error generando reporte " + ex.getMessage());
        }
    }

    public void limpiar() {
        
        isRegistroEdited = false;
        registroEncontrado = null;
        datosFichamedica.limpiar();
        idMedico = null;
        especialidadMedico = null;
        fechaRegistro = citasFacade.FechaServidorddmmyyhhmmTimestamp();
        fechaSistema = citasFacade.FechaServidorddmmyyhhmmTimestamp();
        datosFichamedica = new DatosHistoriaClinica();
        for (int i = 0; i <= 745; i++) {
            datosFichamedica.setValor(i, "");
        }
    }

    public void btnLimpiar() {
        limpiar();
        isRegistroEdited = false;
    }

    public void cambiaTipoHistoriaClinica() {
        isRegistroEdited = false;   
        limpiar();
        mostrarHistoriaClinica();
       
    }

   

    private void mostrarHistoriaClinica() {
        tipoHistoriaClinica = "92";
        if (tipoHistoriaClinica != null && tipoHistoriaClinica.length() != 0) {
            tipoRegistroActual = tipoRegistroFacade.find(Integer.parseInt(tipoHistoriaClinica));
            
            url = tipoRegistroActual.getUrlPagina();
            nombreTipoHistoriaClinica = tipoRegistroActual.getNombre();
        } else {
            tipoRegistroActual = null;
            url = "";
            nombreTipoHistoriaClinica = "";
        }
    }

    public void cambiaMedico() {
        if (idMedico != null && idMedico.length() != 0) {
            especialidadMedico = usuariosFacade.find(Integer.parseInt(idMedico)).getEspecialidad().getDescripcion();
        } else {
            especialidadMedico = "";
        }
    }
    
    public void actualizarRegistro() {
        List<HcDetalle> listaDetalle = new ArrayList<>();
        HcDetalle nuevoDetalle;
        HcCamposReg campoResgistro;

       
        if (!isRegistroEdited) {
            imprimirMensaje("Error", "No se ha cargado un registro para poder modificarlo", FacesMessage.SEVERITY_ERROR);
            return;
        }
        
        if (idMedico != null && idMedico.length() != 0) {
        } else {
        }
        if (tipoRegistroActual != null) {
            registroEncontrado.setIdTipoReg(tipoRegistroActual);
        } else {
            imprimirMensaje("Error", "No se puede determinar el tipo de registro clinico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaRegistro == null) {
            fechaRegistro = fechaSistema;
        }
        registroEncontrado.setIdPaciente(pacienteSeleccionado);

        for (int i = 0; i < tipoRegistroActual.getCantCampos(); i++) {
        if (datosFichamedica.getValor(i) != null && datosFichamedica.getValor(i).toString().length() != 0) {
            campoResgistro = camposRegFacade.buscarPorTipoRegistroYPosicion(tipoRegistroActual.getIdTipoReg(), i);
            if (campoResgistro != null) {
                nuevoDetalle = new HcDetalle(registroEncontrado.getIdRegistro(), campoResgistro.getIdCampo());               
                nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
               
                listaDetalle.add(nuevoDetalle);
                
                HcRegistro hc = registroFacade.find(Integer.valueOf(nuevoDetalle.getHcDetallePK().getIdRegistro()));                
                hc.setIdUsuarioEditar(indexController.getUsuarioActual().getIdUsuario());
                hc.setFechaEditar(detalleFacade.FechaServidorddmmyyhhmmTimestamp());
                detalleFacade.edit(nuevoDetalle);
                registroFacade.edit(hc);
                
                }
            } else {
                System.out.println("No encontro en tabla hc_campos_registro el valor: id_tipo_reg = " + tipoRegistroActual.getIdTipoReg());
            }
        }

        imprimirMensaje("Correcto", "Registro actualizado.", FacesMessage.SEVERITY_INFO);
        limpiar();
        RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
    }

    public void guardarRegistro() {
        
        try {
           
            UserTransaction transaction = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            HcRegistro nuevoRegistro = new HcRegistro();
            List<HcDetalle> listaDetalle = new ArrayList<>();
            HcDetalle nuevoDetalle;
            HcCamposReg campoResgistro;

            nuevoRegistro.setIdRegistro(registroFacade.siguienteid());
            
            idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
            if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                nuevoRegistro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
            } else {
                imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            if (tipoRegistroActual != null) {
                nuevoRegistro.setIdTipoReg(tipoRegistroActual);
            } else {
                imprimirMensaje("Error", "No se puede determinar el tipo de registro clinico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            if (fechaRegistro == null) {
                fechaRegistro = fechaSistema;
            }
            

            nuevoRegistro.setFechaReg(fechaRegistro);
            nuevoRegistro.setFechaSis(fechaSistema);
            nuevoRegistro.setIdPaciente(pacienteSeleccionado);
            nuevoRegistro.setFolio(registroFacade.buscarMaximoFolio(nuevoRegistro.getIdPaciente().getIdPaciente()) + 1);
            registroFacade.create(nuevoRegistro);

            if (validarNoVacio(turnoCita)) {
                CtTurnos turno = turnosFacade.find(Integer.parseInt(turnoCita));
                turno.setEstado("en_espera");
                turnosFacade.edit(turno);
            }

            System.out.println(tipoRegistroActual.getCantCampos());
            for (int i = 0; i < tipoRegistroActual.getCantCampos(); i++) {
                if (datosFichamedica.getValor(i) != null && datosFichamedica.getValor(i).toString().length() != 0) {
                    campoResgistro = camposRegFacade.buscarPorTipoRegistroYPosicion(tipoRegistroActual.getIdTipoReg(), i);
                    if (campoResgistro != null) {
                        nuevoDetalle = new HcDetalle(nuevoRegistro.getIdRegistro(), campoResgistro.getIdCampo());                        
                        nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
                        listaDetalle.add(nuevoDetalle);
                        detalleFacade.create(nuevoDetalle);
                    } else {
                        System.out.println("No encontro en tabla hc_campos_registro el valor: id_tipo_reg=" + tipoRegistroActual.getIdTipoReg() + " posicion " + i);
                    }
                }
            }

            transaction.commit();

            imprimirMensaje("Correcto", "Nuevo registro almacenado.", FacesMessage.SEVERITY_INFO);
            limpiar();
            turnoCita = "";
            tipoHistoriaClinica = "";


            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
            RequestContext.getCurrentInstance().update("IdFormHistorias");
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(SignosVitalesController.class.getName()).log(Level.SEVERE, null, ex);
        
        }
    }

    public void validarIdentificacion() {
        pacienteTemporal = pacientesFacade.buscarPorIdentificacion(identificacionPaciente);

        if (pacienteTemporal != null) {
            tipoHistoriaClinica = "";
            url = "";
            nombreTipoHistoriaClinica = "";
            pacienteSeleccionadoTabla = pacienteTemporal;
            isPacienteSelected = true;
            cargarPaciente();

        } else {
            RequestContext.getCurrentInstance().execute("PF('dlgSeleccionarPaciente').show();");
        }
    }

    public void editarPaciente() {
        if (pacienteSeleccionado != null) {
            RequestContext.getCurrentInstance().execute("window.parent.cargarPaciente('Pacientes','configuraciones/pacientes.xhtml','" + pacienteSeleccionado.getIdPaciente() + "')");
        } else {
            isPacienteSelected = false;
            imprimirMensaje("Error", "Se debe seleccionar un paciente de la tabla", FacesMessage.SEVERITY_ERROR);
        }
    }


    public void cargarPaciente() {
        if (pacienteSeleccionadoTabla != null) {

            turnoCita = "";
            pacienteSeleccionado = pacientesFacade.find(pacienteSeleccionadoTabla.getIdPaciente());
           
            url = "";
            identificacionPaciente = "";
            nombrePaciente = "Paciente";
            nombreTipoHistoriaClinica = "";
            isPacienteSelected = true;
            identificacionPaciente = pacienteSeleccionado.getIdentificacion();
            if (pacienteSeleccionado.getTipoIdentificacion() != null) {
                tipoIdentificacionPaciente = pacienteSeleccionado.getTipoIdentificacion().getObservacion();
            } else {
                tipoIdentificacionPaciente = "";
            }
            nombrePaciente = pacienteSeleccionado.nombreCompleto();
           
            if (pacienteSeleccionado.getSexo() != null) {
                generoPaciente = pacienteSeleccionado.getSexo().getObservacion();
            } else {
                generoPaciente = "";
            }
            if (pacienteSeleccionado.getFechaNacimiento() != null) {
                edadPaciente = calcularEdad(pacienteSeleccionado.getFechaNacimiento());
            } else {
                edadPaciente = "";
            }
            tipoHistoriaClinica = "";
            cambiaTipoHistoriaClinica();
            if (!cargandoDesdeTab) {
                RequestContext.getCurrentInstance().execute("PF('dlgSeleccionarPaciente').hide();");
            }

            try {
                
            } catch (Exception e) {
            }

        } else {
            isPacienteSelected = false;
            imprimirMensaje("Error", "Se debe seleccionar un paciente de la tabla", FacesMessage.SEVERITY_ERROR);
        }
    }


    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public String getIdentificacionPaciente() {
        return identificacionPaciente;
    }

    public void setIdentificacionPaciente(String identificacionPaciente) {
        this.identificacionPaciente = identificacionPaciente;
    }

    public String getUrlPagina() {
        return url;
    }

    public void setUrlPagina(String url) {
        this.url = url;
    }

    public String getTipoRegistroClinico() {
        return tipoHistoriaClinica;
    }

    public void setTipoRegistroClinico(String tipoHistoriaClinica) {
        this.tipoHistoriaClinica = tipoHistoriaClinica;
    }
    
    public boolean isHayPacienteSeleccionado() {
        return isPacienteSelected;
    }

    public void setHayPacienteSeleccionado(boolean isPacienteSelected) {
        this.isPacienteSelected = isPacienteSelected;
    }


    public String getTipoIdentificacion() {
        return tipoIdentificacionPaciente;
    }

    public void setTipoIdentificacion(String tipoIdentificacionPaciente) {
        this.tipoIdentificacionPaciente = tipoIdentificacionPaciente;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getGeneroPaciente() {
        return generoPaciente;
    }

    public void setGeneroPaciente(String generoPaciente) {
        this.generoPaciente = generoPaciente;
    }

   

    public String getEdadPaciente() {
        return edadPaciente;
    }

    public void setEdadPaciente(String edadPaciente) {
        this.edadPaciente = edadPaciente;
    }



    public String getNombreTipoRegistroClinico() {
        return nombreTipoHistoriaClinica;
    }

    public void setNombreTipoRegistroClinico(String nombreTipoHistoriaClinica) {
        this.nombreTipoHistoriaClinica = nombreTipoHistoriaClinica;
    }

    public DatosHistoriaClinica getDatosFichamedica() {
        return datosFichamedica;
    }

    public void setDatosFichamedica(DatosHistoriaClinica datosFichamedica) {
        this.datosFichamedica = datosFichamedica;
    }

    

    public String getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(String idMedico) {
        this.idMedico = idMedico;
    }

    public String getEspecialidadMedico() {
        return especialidadMedico;
    }

    public void setEspecialidadMedico(String especialidadMedico) {
        this.especialidadMedico = especialidadMedico;
    }

    public Date getFechaReg() {
        return fechaRegistro;
    }

    public void setFechaReg(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaSis() {
        return fechaSistema;
    }

    public void setFechaSis(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public String[] getRegCliSelHistorial() {
        return totalHistoriasClinicas;
    }

    public void setRegCliSelHistorial(String[] totalHistoriasClinicas) {
        this.totalHistoriasClinicas = totalHistoriasClinicas;
    }

    public TreeNode getTreeNodeRaiz() {
        return treeNodeRaiz;
    }

    public void setTreeNodeRaiz(TreeNode treeNodeRaiz) {
        this.treeNodeRaiz = treeNodeRaiz;
    }

    public TreeNode[] getTreeNodesSeleccionados() {
        return treeNodesSeleccionado;
    }

    public void setTreeNodesSeleccionados(TreeNode[] treeNodesSeleccionado) {
        this.treeNodesSeleccionado = treeNodesSeleccionado;
    }

    public boolean isModificandoRegCli() {
        return isRegistroEdited;
    }

    public void setModificandoRegCli(boolean isRegistroEdited) {
        this.isRegistroEdited = isRegistroEdited;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

    public boolean isBtnHistorialDisabled() {
        return btnDisabled;
    }

    public void setBtnHistorialDisabled(boolean btnDisabled) {
        this.btnDisabled = btnDisabled;
    }

    public CnPacientes getPacienteSeleccionadoTabla() {
        return pacienteSeleccionadoTabla;
    }

    public void setPacienteSeleccionadoTabla(CnPacientes pacienteSeleccionadoTabla) {
        this.pacienteSeleccionadoTabla = pacienteSeleccionadoTabla;
    }


    public Date getFiltroFechaInicial() {
        return parametroFechaInicial;
    }

    public void setFiltroFechaInicial(Date parametroFechaInicial) {
        this.parametroFechaInicial = parametroFechaInicial;
    }

    public Date getFiltroFechaFinal() {
        return parametroFechaFinal;
    }

    public void setFiltroFechaFinal(Date parametroFechaFinal) {
        this.parametroFechaFinal = parametroFechaFinal;
    }

    public boolean isBtnEditarRendered() {
        return btnEditarRendered;
    }

    public void setBtnEditarRendered(boolean btnEditarRendered) {
        this.btnEditarRendered = btnEditarRendered;
    }


    public String getTurnoCita() {
        return turnoCita;
    }

    public void setTurnoCita(String turnoCita) {
        this.turnoCita = turnoCita;
    }

    public String getNoTurno() {
        return noTurno;
    }

    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

  
    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }


    public HcRegistroFacade getRegistroFacade() {
        return registroFacade;
    }

    public void setRegistroFacade(HcRegistroFacade registroFacade) {
        this.registroFacade = registroFacade;
    }

    public CtTurnosFacade getTurnosFacade() {
        return turnosFacade;
    }

    public void setTurnosFacade(CtTurnosFacade turnosFacade) {
        this.turnosFacade = turnosFacade;
    }

    public CtCitasFacade getCitasFacade() {
        return citasFacade;
    }

    public void setCitasFacade(CtCitasFacade citasFacade) {
        this.citasFacade = citasFacade;
    }

    public HcTipoRegFacade getTipoRegCliFacade() {
        return tipoRegistroFacade;
    }

    public void setTipoRegCliFacade(HcTipoRegFacade tipoRegistroFacade) {
        this.tipoRegistroFacade = tipoRegistroFacade;
    }

    public HcCamposRegFacade getCamposRegFacade() {
        return camposRegFacade;
    }

    public void setCamposRegFacade(HcCamposRegFacade camposRegFacade) {
        this.camposRegFacade = camposRegFacade;
    }

    public CnPacientesFacade getPacientesFacade() {
        return pacientesFacade;
    }

    public void setPacientesFacade(CnPacientesFacade pacientesFacade) {
        this.pacientesFacade = pacientesFacade;
    }


    public CnUsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }

    public void setUsuariosFacade(CnUsuariosFacade usuariosFacade) {
        this.usuariosFacade = usuariosFacade;
    }


    public CnEmpresaFacade getEmpresaFacade() {
        return empresaFacade;
    }

    public void setEmpresaFacade(CnEmpresaFacade empresaFacade) {
        this.empresaFacade = empresaFacade;
    }

    public HcTipoReg getTipoRegistroClinicoActual() {
        return tipoRegistroActual;
    }

    public void setTipoRegistroClinicoActual(HcTipoReg tipoRegistroActual) {
        this.tipoRegistroActual = tipoRegistroActual;
    }

    public CnPacientes getPacienteTmp() {
        return pacienteTemporal;
    }

    public void setPacienteTmp(CnPacientes pacienteTemporal) {
        this.pacienteTemporal = pacienteTemporal;
    }


    public List<HcTipoReg> getListaTipoRegistroClinico() {
        return listaTipoHistoriaClinica;
    }

    public void setListaTipoRegistroClinico(List<HcTipoReg> listaTipoHistoriaClinica) {
        this.listaTipoHistoriaClinica = listaTipoHistoriaClinica;
    }

    public List<HcTipoReg> getListaTipoHistoriaClinicaReporte() {
        return listaTipoHistoriaClinicaReporte;
    }

    public void setListaTipoHistoriaClinicaReporte(List<HcTipoReg> listaTipoHistoriaClinicaReporte) {
        this.listaTipoHistoriaClinicaReporte = listaTipoHistoriaClinicaReporte;
    }

    public HcRegistro getRegistroEncontrado() {
        return registroEncontrado;
    }

    public void setRegistroEncontrado(HcRegistro registroEncontrado) {
        this.registroEncontrado = registroEncontrado;
    }

    public List<DatosHistoriaClinica> getListaRegistrosParaPdf() {
        return listaDatosReporte;
    }

    public void setListaRegistrosParaPdf(List<DatosHistoriaClinica> listaDatosReporte) {
        this.listaDatosReporte = listaDatosReporte;
    }

    public CnEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(CnEmpresa empresa) {
        this.empresa = empresa;
    }

    public boolean isCargandoDesdeTab() {
        return cargandoDesdeTab;
    }

    public void setCargandoDesdeTab(boolean cargandoDesdeTab) {
        this.cargandoDesdeTab = cargandoDesdeTab;
    }

    public CtCitas getCitaActual() {
        return citaActual;
    }

    public void setCitaActual(CtCitas citaActual) {
        this.citaActual = citaActual;
    }

   
}


