package controller.historias;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import util.MetodosGenerales;
import util.NodoArbolHistorial;
import util.TipoNodoEnum;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import controller.seguridad.GenericoController;
import controller.seguridad.IndexController;
import java.util.Map;
import javax.faces.bean.ManagedProperty;
import jpa.modelo.CnClasificaciones;
import jpa.modelo.CnEmpresa;
import jpa.modelo.CnHistoriaCamposPredefinidos;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;
import jpa.modelo.CnCie10;
import jpa.modelo.HcArchivos;
import jpa.modelo.HcCamposReg;
import jpa.modelo.HcDetalle;
import jpa.modelo.HcRegistro;
import jpa.modelo.HcTipoReg;
import jpa.facade.CnClasificacionesFacade;
import jpa.facade.CnEmpresaFacade;
import jpa.facade.CnHistoriaCamposPredefinidosFacade;
import jpa.facade.CnPacientesFacade;
import jpa.facade.CnUsuariosFacade;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import jpa.facade.CnCie10Facade;
import jpa.facade.HcArchivosFacade;
import jpa.facade.HcCamposRegFacade;
import jpa.facade.HcDetalleFacade;
import jpa.facade.HcDetalleTrFacade;
import jpa.facade.HcRegistroFacade;
import jpa.facade.HcSesionesTrFacade;
import jpa.facade.HcTfSesionesFacade;
import jpa.facade.HcTfSesionesHistorialFacade;
import jpa.facade.HcTfTratamientosFacade;
import jpa.facade.HcTipoRegFacade;
import jpa.modelo.HcDetalleTr;
import jpa.modelo.HcSesionesTr;
import jpa.modelo.HcTfSesiones;
import jpa.modelo.HcTfSesionesHistorial;
import jpa.modelo.HcTfTratamientos;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "historiaClinicaController")
@SessionScoped
public class HistoriaClinicaController extends MetodosGenerales implements Serializable {

    @EJB
    HcRegistroFacade registroFacade;
    @EJB
    CtTurnosFacade turnosFacade;
    @EJB
    CtCitasFacade citasFacade;
    @EJB
    HcTipoRegFacade tipoRegCliFacade;
    @EJB
    HcCamposRegFacade camposRegFacade;
    @EJB
    CnPacientesFacade pacientesFacade;
    @EJB
    CnClasificacionesFacade maestrosClasificacionesFacade;
    @EJB
    CnUsuariosFacade usuariosFacade;
    @EJB
    CnClasificacionesFacade clasificacionesFacade;
    @EJB
    CnEmpresaFacade empresaFacade;
    @EJB
    CnCie10Facade Cncie10Facade;
    @EJB
    HcArchivosFacade hcArchivosFacade;
    @EJB
    HcDetalleFacade detalleFacade;
    @EJB
    HcDetalleTrFacade detalleTrFacade;
    @EJB
    CnHistoriaCamposPredefinidosFacade cnHistoriaCamposPredefinidosFacade;
    @EJB
    HcTfSesionesFacade SesionesTfFacade;
    @EJB
    HcTfSesionesHistorialFacade SesionesHistorialTfFacade;
    @EJB
    HcSesionesTrFacade sesionesTrFacade;
    @EJB
    HcTfTratamientosFacade tratamientosTfFacade;
    
    private List<CnHistoriaCamposPredefinidos> listaCamposPredefinidos;

    private HcTipoReg tipoRegistroClinicoActual;

    private List<CnPacientes> listPacientes;
    private List<CnPacientes> listPacientesFiltro;
    private CnPacientes pacienteSeleccionadoTabla;
    private CnPacientes pacienteSeleccionado;
    private CnPacientes pacienteTemporal;
    private CnClasificaciones clasificacionBuscada;
    private List<CnHistoriaCamposPredefinidos> listaTextos;
       
    private Date filtroFechaInicial = null;
    private Date filtroFechaFinal = null;

    public UploadedFile archivos;
    private String urlFile = "";
    private String detalleTextoPredef = "";
    private String descriparchivo = "";
    private String idTextoPredef = "";
    private String idMaestroTextoPredef = "";
    private String nombreTextoPredefinido = "";
    private String tipoHistoriaClinica = "";
    private boolean estaSubido = false;//valida si la orden de terapia fisica esta subida

    private String[] registrosHistoriaClinica;
    private String urlPagina = "";
    private String nombreTipoHistoriaClinica = "";
    private String identificacionPaciente = "";
    private String tipoIdentificacionPaciente = "";
    private String nombrePaciente = "Paciente";
    private String generoPaciente = "";
    private String edadPaciente = "";
    private StreamedContent ordenFt = null;
    private HcArchivos file = null;
    private String idEditorSeleccionado = "";
    private int posicionTxtPredefinidos = 0;
    private String idMedico = "";
    private String especialidadMedico = "";

     //variables terapia respiratoria
    private float ssml = 0;
    private float ssg = 0;
    private float dxml = 0;
    private float dxg = 0;
    private float cbml = 0;
    private float cbg = 0;
    private float vnml = 0;
    private float vng = 0;
    private float flml = 0;
    private float flg = 0;
    private float abml = 0;
    private float abg = 0;
    private List<CnClasificaciones> listaAgentesFisicos;
    private List<CnClasificaciones> listaEjerciciosTerapeuticos;
    private List<CnClasificaciones> AgentesFisicosSeleccionados;
    private List<CnClasificaciones> EjerciciosTerapeuticosSeleccionados;
    private boolean respirador = false;
    private short cantidadTr = 0;
    private String obsTf = "";
    private List<HcSesionesTr> listaTrNoPagos;
    private HcSesionesTr pagoSeleccionado;
    private List<HcSesionesTr> listaTrAtencion;
    private HcSesionesTr atencionSeleccionada;    
    private Map<String, Float> mapValues;
    //fin variables terapia respiratoria
    
    private int tipomovimiento = 0;
    private Integer idfichamedica;

    private DatosHistoriaClinica datosFichamedica = new DatosHistoriaClinica();
    private Date fechaRegistro;
    private Date fechaSistema;
    private TreeNode treeNodeRaiz;
    
    private TreeNode[] treeSeleccionado;
    private List<HcTipoReg> listaTipoRegistroClinico;
    private List<HcTipoReg> listaTipoRegistroClinicoAll;
    private boolean hayPacienteSeleccionado = false;
    private boolean modificandoHistoriaClinica = false;
    private boolean btnHistorialDisabled = true;
    private HcRegistro registroEncontrado = null;
    private List<DatosHistoriaClinica> listaHistoriaReporte;

    
    private CnEmpresa empresa;
    @Inject
    private IndexController indexController;
    private boolean btnEditarRendered = true;

    private String turnoCita = "";
    private boolean cargandoDesdeTab = false;
    private CtCitas citaActual = null;


    private final SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
    private final SimpleDateFormat formatoDateHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private final SimpleDateFormat formatoLongDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);//Thu Apr 30 19:00:00 COT 2015
    

    private String idcie10;
    private String codigo;
    private String descripcion;
    private String presentacion;
    private String observacion;
    private String noTurno;
    private String notas;
    
    private List<HcArchivos> listaArchivo;
    private HcArchivos archivoSeleccionado;
    private StreamedContent fileDownload;
    private int edadAnios;
    int posicion = 0;
    int tipo = 0;


    private GenericoController genericoController;

    
    private String valorDefecto1;
    private String valorDefecto2;
    private String valorDefecto3;
    
    private List<SelectItem> listaValoresDefecto;
    
    ////////////////////////varibales de tratamientos terapia fisica
    private boolean af1 = false;
    private boolean af2 = false;
    private boolean af3 = false;
    private boolean af4 = false;
    private boolean af5 = false;
    private boolean af6 = false;
    private boolean af7 = false;
    private boolean af8 = false;
    private boolean af9 = false;
    private boolean af10 = false;
    private boolean af11 = false;
    private boolean et1 = false;
    private boolean et2 = false;
    private boolean et3 = false;
    private boolean et4 = false;
    private boolean et5 = false;
    private boolean et6 = false;
    private boolean et7 = false;
    private boolean et8 = false;
    private boolean et9 = false;
    private boolean et10 = false;
    private boolean et11 = false;
    private boolean et12 = false;
    private boolean et13 = false;
    private boolean et14 = false;
    private boolean et15 = false;
    private boolean et16 = false;
    private boolean et17 = false;
    private boolean et18 = false;
    private boolean et19 = false;
    private boolean et20 = false;
    private boolean et21 = false;
    private boolean et22 = false;
    private List<HcTfSesiones> listaTfAtencion;
    private HcTfSesiones atencionTfSeleccionada;
    private HcTfSesionesHistorial sesionesHistorialTf;
    private int cantSesiones = 0;
    private boolean modificaAtencionTf;
    private Map<Integer, String> mapValuesTf;
    private Set<Map.Entry<Integer, String>> tratValTf;
    private int sesionSeleccionadaTf = 0;
    private long idTratameinto = 0;

    HcCamposReg campoSeleccionado;
    private String nombreFormulario;
    private CnHistoriaCamposPredefinidos textoSeleccionado;
    

    public HistoriaClinicaController() {
        genericoController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{genericoController}", GenericoController.class);
        listaValoresDefecto = new ArrayList();
        listaValoresDefecto.add(new SelectItem("No Refiere", "No Refiere"));
        listaValoresDefecto.add(new SelectItem("Normal", "Normal"));

        listaTextos = new ArrayList();
        campoSeleccionado = new HcCamposReg();
    }

    public void cargarTextoPredefinido(int posicion, String formulario) {
        try {
            this.nombreFormulario = formulario;
            campoSeleccionado = camposRegFacade.buscarPorTipoRegistroYPosicion(tipoRegistroClinicoActual.getIdTipoReg(), posicion);
            listaTextos = cnHistoriaCamposPredefinidosFacade.getCamposDefinidosXCampo(campoSeleccionado.getIdCampo());
            RequestContext.getCurrentInstance().update("dialogTexto");
            RequestContext.getCurrentInstance().update("optexto");
            RequestContext.getCurrentInstance().update(nombreFormulario);
            RequestContext.getCurrentInstance().execute("PF('dlgTexto').show();");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void textoPredefinidoSeleccionado(SelectEvent event) {
        textoSeleccionado = (CnHistoriaCamposPredefinidos) event.getObject();
        datosFichamedica.setValor(campoSeleccionado.getPosicion(), textoSeleccionado.getValor());
        RequestContext.getCurrentInstance().update(nombreFormulario);
        RequestContext.getCurrentInstance().execute("PF('dlgTexto').hide();");

    }

    public void cambiarValorDefecto(int tipo) {
        if (tipoRegistroClinicoActual == null) {
            return;
        }
        switch (tipo) {
            case 1:
                if (tipoRegistroClinicoActual.getIdTipoReg() == 93) {
                    datosFichamedica.setDato2(valorDefecto1);
                    datosFichamedica.setDato3(valorDefecto1);
                    datosFichamedica.setDato4(valorDefecto1);
                    datosFichamedica.setDato5(valorDefecto1);
                    datosFichamedica.setDato6(valorDefecto1);
                    datosFichamedica.setDato7(valorDefecto1);
                    datosFichamedica.setDato8(valorDefecto1);
                    datosFichamedica.setDato9(valorDefecto1);
                    datosFichamedica.setDato10(valorDefecto1);
                    datosFichamedica.setDato11(valorDefecto1);
                    datosFichamedica.setDato12(valorDefecto1);
                    datosFichamedica.setDato13(valorDefecto1);
                    datosFichamedica.setDato14(valorDefecto1);
                    datosFichamedica.setDato15(valorDefecto1);
                    datosFichamedica.setDato16(valorDefecto1);
                }

                break;
            case 2:
                if (tipoRegistroClinicoActual.getIdTipoReg() == 93) {
                    datosFichamedica.setDato17(valorDefecto2);
                    datosFichamedica.setDato18(valorDefecto2);
                    datosFichamedica.setDato19(valorDefecto2);
                    datosFichamedica.setDato20(valorDefecto2);
                    datosFichamedica.setDato21(valorDefecto2);
                    datosFichamedica.setDato22(valorDefecto2);
                    datosFichamedica.setDato23(valorDefecto2);
                    datosFichamedica.setDato24(valorDefecto2);
                    datosFichamedica.setDato25(valorDefecto2);
                    datosFichamedica.setDato26(valorDefecto2);
                    datosFichamedica.setDato27(valorDefecto2);
                    datosFichamedica.setDato28(valorDefecto2);
                }

                break;
            case 3:
                if (tipoRegistroClinicoActual.getIdTipoReg() == 93) {
                    datosFichamedica.setDato29(valorDefecto3);
                    datosFichamedica.setDato30(valorDefecto3);
                    datosFichamedica.setDato31(valorDefecto3);
                    datosFichamedica.setDato32(valorDefecto3);
                    datosFichamedica.setDato33(valorDefecto3);
                    datosFichamedica.setDato34(valorDefecto3);
                    datosFichamedica.setDato35(valorDefecto3);
                    datosFichamedica.setDato36(valorDefecto3);
                    datosFichamedica.setDato37(valorDefecto3);
                    datosFichamedica.setDato38(valorDefecto3);
                    datosFichamedica.setDato39(valorDefecto3);
                    datosFichamedica.setDato40(valorDefecto3);
                    datosFichamedica.setDato41(valorDefecto3);
                    datosFichamedica.setDato42(valorDefecto3);
                    datosFichamedica.setDato43(valorDefecto3);
                    datosFichamedica.setDato44(valorDefecto3);
                    datosFichamedica.setDato45(valorDefecto3);
                    datosFichamedica.setDato46(valorDefecto3);
                }

                break;
        }
    }

    @PostConstruct
    public void init() {
        if (getIndexController().getUsuarioActual().getIdPerfil().getIdPerfil() == 3) {
            listaTipoRegistroClinico = tipoRegCliFacade.buscarTiposRegistroGeneral();
        }else if(getIndexController().getUsuarioActual().getIdPerfil().getIdPerfil() == 2){
            listaTipoRegistroClinico = tipoRegCliFacade.buscarTiposRegistroTerapia();
        }else if(getIndexController().getUsuarioActual().getIdPerfil().getIdPerfil() == 6){
            listaTipoRegistroClinico = tipoRegCliFacade.buscarTiposRegistroPediatria();
        }else{
            listaTipoRegistroClinico = tipoRegCliFacade.buscarTiposRegstroActivos();
        }
        listaTipoRegistroClinicoAll = tipoRegCliFacade.buscarTiposRegstroActivosAll();

        seleccionaTodosRegCliHis();
        empresa = empresaFacade.findAll().get(0);

        codigo = "";
        descripcion = "";
        presentacion = "";
        observacion = "";

    }
    
    //metodos terapia respiratoria
    public void initPagosTr() {
        listaTrNoPagos = sesionesTrFacade.buscarRegistrosNoPagos();
    }

    public void initAtencionTr() {
        listaTrAtencion = sesionesTrFacade.buscarRegistrosNoAtendidos();

    }

    public void initAtencionTf() {
        listaTfAtencion = SesionesTfFacade.buscarRegistrosNoAtendidos(pacienteSeleccionado);
        if(listaTfAtencion.size() > 0){
            listaTfAtencion = SesionesTfFacade.buscarRegistrosNoAtendidosAll(listaTfAtencion.get(0).getIdTratamiento().intValue(), pacienteSeleccionado);
        }        
        modificaAtencionTf = SesionesTfFacade.tieneSesionesPendientes(pacienteSeleccionado);
        idTratameinto = SesionesTfFacade.buscaIdTratamiento(pacienteSeleccionado);

    }

    public void registraPago() {
        if (pagoSeleccionado == null) {
            imprimirMensaje("Error", "No se ha seleccionado ningúna sesion", FacesMessage.SEVERITY_ERROR);
            return;
        }
        int res = sesionesTrFacade.realizaPago(pagoSeleccionado.getId());
        if (res == 1) {
            imprimirMensaje("Exito", "Se ha realizado el pago con exito", FacesMessage.SEVERITY_INFO);
        }
        System.out.println(res);
        initPagosTr();
        RequestContext.getCurrentInstance().update("IdFormPagos");
    }

    public void registraAtencion() {
        if (atencionSeleccionada == null) {
            imprimirMensaje("Error", "No se ha seleccionado ninguna terapia", FacesMessage.SEVERITY_ERROR);
            return;
        }
        //int res = sesionesTrFacade.realizaAtencion(atencionSeleccionada.getId());
        atencionSeleccionada.setRealizado(true);
        atencionSeleccionada.setFechaAtencion(fechaSistema);
        sesionesTrFacade.edit(atencionSeleccionada);
        
        
      //  if (res == 1) {
            imprimirMensaje("Exito", "Se ha realizado la atencion con exito", FacesMessage.SEVERITY_INFO);
      //  }
       // System.out.println(res);
        initAtencionTr();
        RequestContext.getCurrentInstance().update("IdFormPagos");
    }

    public void consultaTratamientos(int id) {
        List<HcDetalleTr> tratamientos = detalleTrFacade.tratamientosById(id);
        CnClasificaciones codigo;
        mapValues = new HashMap<>();
        for (HcDetalleTr trat : tratamientos) {
            codigo = clasificacionesFacade.getTramientoByMaestro(trat.getIdCampo());
            for (int i = 0; i <= tratamientos.size(); i++) {
                //HcDetalleTr aux = tratamientos.get(i);
                String tt = trat.getIdCampo().toString();
                String cod = codigo.getCodigo().toString();
                if (tt.equals(cod)) {
                    mapValues.put(codigo.getDescripcion(), trat.getValor());
                }
            }
        }
        getMyHashMapEntryList();
        RequestContext.getCurrentInstance().update("tratamientosSoluciones");
    }

    public List<Map.Entry<String, Float>> getMyHashMapEntryList() {
        try {
            Set<Map.Entry<String, Float>> tratVal = mapValues.entrySet();
            return new ArrayList<Map.Entry<String, Float>>(tratVal);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    //fin metodos terapia respiratoria
    //metodos terapia fisica
     public void consultaTratamientosTf() {
        //atencionTfSeleccionada = atencionTf;
        //atencionTfobservacion = atencionTf;
        List<HcTfTratamientos> tratamientos = tratamientosTfFacade.tratamientosById(atencionTfSeleccionada.getIdTratamiento().intValue());
        CnClasificaciones codigo;
        mapValuesTf = new HashMap<>();
        for (HcTfTratamientos trat : tratamientos) {
            if (trat.getActivo()) {
                codigo = clasificacionesFacade.buscarPorIdTf(trat.getCodTratamientos().getId());
                mapValuesTf.put(codigo.getId(), "(" + codigo.getMaestro() + ") " + codigo.getDescripcion());
            }
        }
        getMyHashMapEntryListTf();
        
        //RequestContext.getCurrentInstance().update("tratamientosSoluciones");
        RequestContext.getCurrentInstance().update("IdFormRegistroClinico:IdVisualizaTratamientos");
    }

    public List<Map.Entry<Integer, String>> getMyHashMapEntryListTf() {
        
        try {
            Set<Map.Entry<Integer, String>> tratValTf = mapValuesTf.entrySet();
            return new ArrayList<Map.Entry<Integer, String>>(tratValTf);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    
    public void realizaSesion() {
        try {
            if (atencionTfSeleccionada == null) {
                imprimirMensaje("Error", "No se ha seleccionado ningúna sesion", FacesMessage.SEVERITY_ERROR);
                return;
            }
            //int res = SesionesTfFacade.finalizaSesion(atencionTfSeleccionada.getId());
            
            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            
            transaction.begin();
            
            atencionTfSeleccionada.setRealizado(true);
            atencionTfSeleccionada.setFechaSesion(fechaSistema);
            atencionTfSeleccionada.setNotas(notas);
            SesionesTfFacade.edit(atencionTfSeleccionada);
            
            HcRegistro registro = new HcRegistro();
            
            registro.setIdRegistro(registroFacade.siguienteid());
            if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                registro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
            } else {
                imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            registro.setIdTipoReg(tipoRegistroClinicoActual);
            registro.setFechaReg(fechaRegistro);
            registro.setFechaSis(fechaSistema);
            registro.setIdPaciente(pacienteSeleccionado);
            registro.setIdtratamiento(atencionTfSeleccionada.getIdTratamiento().intValue());
            registro.setSesion(atencionTfSeleccionada.getNumSesion());
            registroFacade.create(registro);
            
            List<HcTfTratamientos> listaTratamientos = tratamientosTfFacade.buscarTratamientosActivosById(atencionTfSeleccionada.getIdTratamiento());
            
            for(HcTfTratamientos t : listaTratamientos){
                HcTfSesionesHistorial sesionhistorial = new HcTfSesionesHistorial();
                sesionhistorial.setNumSesion(atencionTfSeleccionada.getNumSesion());
                sesionhistorial.setIdTratamiento(atencionTfSeleccionada.getIdTratamiento());
                sesionhistorial.setCodTratamientos(t.getCodTratamientos().getId());
                SesionesHistorialTfFacade.create(sesionhistorial);
            }
            
            transaction.commit();
            
            // if (res == 1) {
            imprimirMensaje("Exito", "Se ha finalizado la sesion", FacesMessage.SEVERITY_INFO);
            notas = "";
            //  }
            //  System.out.println(res);
            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
        } catch (NamingException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotSupportedException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SystemException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HeuristicMixedException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HeuristicRollbackException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalStateException ex) {
            Logger.getLogger(HistoriaClinicaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void limpiarTf() {
        af1 = false;
        af2 = false;
        af3 = false;
        af4 = false;
        af5 = false;
        af6 = false;
        af7 = false;
        af8 = false;
        af9 = false;
        af10 = false;
        af11 = false;
        et1 = false;
        et2 = false;
        et3 = false;
        et4 = false;
        et5 = false;
        et6 = false;
        et7 = false;
        et8 = false;
        et9 = false;
        et10 = false;
        et11 = false;
        et12 = false;
        et13 = false;
        et14 = false;
        et15 = false;
        et16 = false;
        et17 = false;
        et18 = false;
        et19 = false;
        et20 = false;
        et21 = false;
        observacion = "";
        obsTf = "";
        cantSesiones = 0;
    }

    public void limpiarTr() {
        ssml = 0;
        ssg = 0;
        cbml = 0;
        cbg = 0;
        dxml = 0;
        dxg = 0;
        vnml = 0;
        vng = 0;
        flml = 0;
        flg = 0;
        abml = 0;
        abg = 0;
        cantidadTr = 0;
        respirador = false;
    }

    //fin metodos terapia fisica

   
    public void motivoConsulta() {
        if (citaActual != null && tipoHistoriaClinica.matches("93") && !modificandoHistoriaClinica) {
            datosFichamedica.setValor(0, citaActual.getTipoCita().getDescripcion());
        }
    }

    private void valoresPorDefecto() {
        datosFichamedica = new DatosHistoriaClinica();
        for (int i = 0; i <= 745; i++) {
            datosFichamedica.setValor(i, "");
        }
        if (tipoRegistroClinicoActual != null) {
            try {
                for (HcCamposReg hc : tipoRegistroClinicoActual.getHcCamposRegList()) {
                    if (hc.getValorDefault() != null) {
                        datosFichamedica.setValor(hc.getPosicion(), hc.getValorDefault());
                    }
                }
            } catch (Exception e) {

            }
        }

        try {
            listaCamposPredefinidos = new ArrayList<>();
            listaCamposPredefinidos = cnHistoriaCamposPredefinidosFacade.getCamposDefinidosXHistoriaClinica(tipoRegistroClinicoActual.getIdTipoReg());
            for (CnHistoriaCamposPredefinidos campos : listaCamposPredefinidos) {
                if (campos.getDefaultValor()) {
                    datosFichamedica.setValor(campos.getIdCampo().getPosicion(), campos.getValor());
                }
            }

        } catch (Exception e) {
        }
    }

   

    public void cargarDialogoAgregarEnfermedad() {

        codigo = "";
        descripcion = "";
        presentacion = "";

        RequestContext.getCurrentInstance().update("IdFormRegistroClinico:IdPanelAgregarEnfermedad");
        RequestContext.getCurrentInstance().execute("PF('dialogoAgregarEnfermedad').show();");
    }

    public void agregarEnfermedad() {
        if (validacionCampoVacio(codigo, "Codigo")) {
            return;
        }
        if (validacionCampoVacio(descripcion, "Descripcion")) {
            return;
        }
        if (validacionCampoVacio(presentacion, "Clase")) {
            return;
        }

        System.out.println("Id Medicamento " + idcie10);

        CnCie10 enfermedad = Cncie10Facade.getEnfermedadById(idcie10);

        System.out.println("Enfermedad " + enfermedad.getDescripcion());

        getDatosFormulario().setDato1(codigo + "-" + descripcion);
        idcie10 = "";
        codigo = "";
        descripcion = "";
        presentacion = "";

        RequestContext.getCurrentInstance().update("dato1");
        RequestContext.getCurrentInstance().update("IdPanelAgregarEnfermedad");


    }

    public void cerrarFormEnfermedad() {
        idcie10 = "";
        RequestContext.getCurrentInstance().execute("PF('dialogoAgregarEnfermedad').hide();");
    }

    public void cambiarEnfermedad() {
        System.out.println("Codigo Cie10 " + idcie10);
        CnCie10 enfermedad = Cncie10Facade.getEnfermedadById(idcie10);
        codigo = enfermedad.getCodigo();
        descripcion = enfermedad.getDescripcion();

        presentacion = enfermedad.getClase();

        System.out.println("Enfermedad " + enfermedad.getDescripcion());
    }

    public void cargarDesdeTab(String id) {              
        cargandoDesdeTab = true;
        turnoCita = "";
        citaActual = null;
        limpiarFormulario();
        String[] splitId = id.split(";");
        if (splitId[0].compareTo("idCita") == 0) {
            citaActual = citasFacade.find(Integer.parseInt(splitId[1]));
            pacienteTemporal = pacientesFacade.find(citaActual.getIdPaciente().getIdPaciente());
            if (pacienteTemporal == null) {
                imprimirMensaje("Error", "No se encontró el paciente correspondiente a la cita", FacesMessage.SEVERITY_ERROR);
                cargandoDesdeTab = false;
                RequestContext.getCurrentInstance().update("IdFormFacturacion");
                RequestContext.getCurrentInstance().update("IdMensajeFacturacion");
                return;
            }
            pacienteSeleccionadoTabla = pacienteTemporal;
            cargarPaciente();
            turnoCita = citaActual.getIdTurno().getIdTurno().toString();
            noTurno = citaActual.getIdTurno().getContador().toString();
        }
        RequestContext.getCurrentInstance().update("IdMensajeFacturacion");
        cargandoDesdeTab = false;
    }

    private void seleccionaTodosRegCliHis() {
        registrosHistoriaClinica = new String[listaTipoRegistroClinico.size()];
        for (int i = 0; i < listaTipoRegistroClinico.size(); i++) {
            registrosHistoriaClinica[i] = listaTipoRegistroClinico.get(i).getIdTipoReg().toString();
        }
    }

    public void cargarHistorialCompleto() {
        btnHistorialDisabled = true;
        seleccionaTodosRegCliHis();
        cargarHistorial();
    }

    public void cargarHistorial() {
        

        treeNodeRaiz = new DefaultTreeNode(new NodoArbolHistorial(TipoNodoEnum.RAIZ_HISTORIAL, null, -1, -1, null, null), null);
        NodoArbolHistorial nodoSeleccionTodoNada = new NodoArbolHistorial(TipoNodoEnum.NOVALUE, null, -1, -1, null, "Selección Todo/Ninguno");

        TreeNode nodoInicial = new DefaultTreeNode(nodoSeleccionTodoNada, treeNodeRaiz);
        nodoInicial.setExpanded(true);

        treeSeleccionado = null;
        btnHistorialDisabled = true;
        boolean exitenRegistros = false;
        boolean usarFiltroFechas = false;
        boolean usarFiltroAutorizacion = false;

        if (pacienteSeleccionado == null) {
            imprimirMensaje("Error", "No hay un paciente seleccionado", FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().update("IdFormHistorias:IdPanelHistorico");
            return;
        }
        if ((filtroFechaInicial == null && filtroFechaFinal != null) || (filtroFechaInicial != null && filtroFechaFinal == null)) {
            imprimirMensaje("Error", "Debe especificar fecha inicial y fecha final al tiempo ", FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().update("IdFormHistorias:IdPanelHistorico");
            return;
        }
        if (filtroFechaInicial != null && filtroFechaFinal != null) {
            usarFiltroFechas = true;
        }

        List<HcRegistro> listaRegistrosPaciente;
        if (usarFiltroFechas) {
            listaRegistrosPaciente = registroFacade.buscarOrdenado(pacienteSeleccionado.getIdPaciente(), filtroFechaInicial, filtroFechaFinal);
        } else {
            listaRegistrosPaciente = registroFacade.buscarOrdenadoPorFecha(pacienteSeleccionado.getIdPaciente());
        }

        NodoArbolHistorial nodHisNuevo;
        NodoArbolHistorial nodHisFecha;
        NodoArbolHistorial nodHisComparar;
        TreeNode treeNodeFecha = null;
        TreeNode treeNodeCreado;
        for (HcRegistro registro : listaRegistrosPaciente) {
            if (estaEnListaTipRegHis(registro.getIdTipoReg().getIdTipoReg().toString())) {
                exitenRegistros = true;
                nodHisNuevo = new NodoArbolHistorial(
                        TipoNodoEnum.REGISTRO_HISTORIAL,
                        registro.getFechaReg(),
                        registro.getIdRegistro(),
                        registro.getIdTipoReg().getIdTipoReg(),
                        registro.getIdTipoReg().getNombre(),
                        registro.getIdMedico().nombreCompleto());
                if (treeNodeFecha == null) {
                    nodHisFecha = new NodoArbolHistorial(TipoNodoEnum.FECHA_HISTORIAL, registro.getFechaReg(), -1, -1, null, null);
                    treeNodeFecha = new DefaultTreeNode(nodHisFecha, nodoInicial);
                    treeNodeFecha.setExpanded(true);
                    treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                } else {
                    nodHisComparar = (NodoArbolHistorial) treeNodeFecha.getData();
                    if (nodHisNuevo.getStrFecha().compareTo(nodHisComparar.getStrFecha()) == 0) {
                        treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                    } else {//la fecha ha cambiado, se crea un nodo 'FECHA_HISTORIAL' y se le agrega el nodo REGISTRO_HISTORIAL
                        nodHisFecha = new NodoArbolHistorial(TipoNodoEnum.FECHA_HISTORIAL, registro.getFechaReg(), -1, -1, null, null);
                        treeNodeFecha = new DefaultTreeNode(nodHisFecha, nodoInicial);
                        treeNodeFecha.setExpanded(true);
                        treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeFecha);
                    }
                }
            }
        }
        if (!exitenRegistros) {
            nodHisNuevo = new NodoArbolHistorial(TipoNodoEnum.NOVALUE, null, -1, -1, "", "NO SE ENCONTRARON REGISTROS");
            treeNodeCreado = new DefaultTreeNode(nodHisNuevo, treeNodeRaiz);
        }

        RequestContext.getCurrentInstance().update("formHistorico:IdPanelHistorico");
    }

    private boolean estaEnListaTipRegHis(String idBuscado) {
        for (String idTipRegCliSelHis : registrosHistoriaClinica) {
            if (idBuscado.compareTo(idTipRegCliSelHis) == 0) {
                return true;
            }
        }
        return false;
    }

    public void seleccionaNodoArbol() {
        btnHistorialDisabled = true;
        boolean igualTipoRegistro = true;
        int tipoRegistroEncontrado = -1;

        if (treeSeleccionado != null) {
            int contadorRegistrosSeleccionadosHistorial = 0;
            for (TreeNode nodo : treeSeleccionado) {
                NodoArbolHistorial nodArbolHisSeleccionado = (NodoArbolHistorial) nodo.getData();
                if (nodArbolHisSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                    if (tipoRegistroEncontrado == -1) {
                        tipoRegistroEncontrado = nodArbolHisSeleccionado.getIdTipoRegistro();
                    } else if (tipoRegistroEncontrado != nodArbolHisSeleccionado.getIdTipoRegistro()) {
                        igualTipoRegistro = false;
                    }
                    contadorRegistrosSeleccionadosHistorial++;
                }
            }
            if (contadorRegistrosSeleccionadosHistorial == 0) {
                btnHistorialDisabled = true;
            } else if (contadorRegistrosSeleccionadosHistorial == 1) {
                btnHistorialDisabled = false;
            } else
            {
                if (igualTipoRegistro) {
                    btnHistorialDisabled = true;
                } else {
                    btnHistorialDisabled = true;
                }
            }
        }
    }

    public void cargarRegistroClinico() {
        if (treeSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        NodoArbolHistorial nodArbolHisSeleccionado = null;
        for (TreeNode nodo : treeSeleccionado) {
            nodArbolHisSeleccionado = (NodoArbolHistorial) nodo.getData();
            if (nodArbolHisSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                break;
            }
        }
        if (nodArbolHisSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }

        tipoHistoriaClinica = "";
        tipoHistoriaClinica = String.valueOf(nodArbolHisSeleccionado.getIdTipoRegistro());
        limpiarFormulario();
        registroEncontrado = registroFacade.find(nodArbolHisSeleccionado.getIdRegistro());
        if (registroEncontrado.getIdMedico() != null) {
            idMedico = registroEncontrado.getIdMedico().getIdUsuario().toString();
            especialidadMedico = registroEncontrado.getIdMedico().getEspecialidad().getDescripcion();
        } else {
            idMedico = "";
            especialidadMedico = "";
        }
        fechaRegistro = registroEncontrado.getFechaReg();
        fechaSistema = registroEncontrado.getFechaSis();

        Set<HcDetalle> setDetalles = registroEncontrado.getHcDetalleList();
        List<HcDetalle> listaDetalles = new ArrayList<>(setDetalles);

        for (HcDetalle detalle : listaDetalles) {
            HcCamposReg campo = camposRegFacade.find(detalle.getHcDetallePK().getIdCampo());
            if (campo != null) {
                if (campo.getTabla() != null && campo.getTabla().length() != 0) {
                    switch (campo.getTabla()) {
                        case "date":
                            try {
                                
                                Date f = formatoDate.parse(detalle.getValor());
                                datosFichamedica.setValor(campo.getPosicion(), f);
                            } catch (ParseException ex) {
                                datosFichamedica.setValor(campo.getPosicion(), "");
                            }
                            break;
                        default:
                            datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                            break;
                    }
                } else {
                    datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                }
            } else {
                System.out.println("Error: no se encontro hc_campos_reg.id_campo= " + detalle.getHcDetallePK().getIdCampo());
            }

        }
        modificandoHistoriaClinica = true;
        mostrarFormularioRegistroClinico();
        RequestContext.getCurrentInstance().execute("PF('dlgHistorico').hide();");
        RequestContext.getCurrentInstance().update("IdFormHistorias");
    }

    public void cargarRecetaxRegistroClinico() {

        HcRegistro receta = registroFacade.buscarFiltradoPorFichaMedicaPadre(registroEncontrado.getIdRegistro());
        if (receta != null) {
            tipoHistoriaClinica = "";
            tipoHistoriaClinica = "6";
            limpiarFormulario();
            registroEncontrado = registroFacade.find(receta.getIdRegistro());
            if (registroEncontrado.getIdMedico() != null) {
                idMedico = registroEncontrado.getIdMedico().getIdUsuario().toString();
                especialidadMedico = registroEncontrado.getIdMedico().getEspecialidad().getDescripcion();
            } else {
                idMedico = "";
                especialidadMedico = "";
            }
            System.out.println("Medico: " + idMedico);
            fechaRegistro = registroEncontrado.getFechaReg();
            fechaSistema = registroEncontrado.getFechaSis();

            Set<HcDetalle> setDetalles = registroEncontrado.getHcDetalleList();
            List<HcDetalle> listaDetalles = new ArrayList<>(setDetalles);

            for (HcDetalle detalle : listaDetalles) {
                HcCamposReg campo = camposRegFacade.find(detalle.getHcDetallePK().getIdCampo());
                if (campo != null) {
                    if (campo.getTabla() != null && campo.getTabla().length() != 0) {
                        switch (campo.getTabla()) {
                            case "date":
                                try {
                                    Date f = formatoDateHora.parse(detalle.getValor());
                                    datosFichamedica.setValor(campo.getPosicion(), f);
                                } catch (ParseException ex) {
                                    datosFichamedica.setValor(campo.getPosicion(), "");
                                }
                                break;
                            default:
                                datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                                break;
                        }
                    } else {
                        datosFichamedica.setValor(campo.getPosicion(), detalle.getValor());
                    }
                } else {
                    System.out.println("Error: no se encontro hc_campos_reg.id_campo= " + detalle.getHcDetallePK().getIdCampo());
                }
            }
            modificandoHistoriaClinica = true;
            mostrarFormularioRegistroClinico();
            RequestContext.getCurrentInstance().execute("refresh();");
            RequestContext.getCurrentInstance().update("IdFormHistorias");
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se ha encontrado receta médica");
            PrimeFaces.current().dialog().showMessageDynamic(message);

        }

    }


    private void cargarDatosReporte(HcRegistro registroSeleccionado) {
        listaHistoriaReporte = new ArrayList<>();
        DatosHistoriaClinica datosReporte = new DatosHistoriaClinica();
        Set<HcDetalle> setCamposReporte = registroSeleccionado.getHcDetalleList();
        List<HcDetalle> listaCamposReporte = new ArrayList<>(setCamposReporte);
        
        List<HcCamposReg> listaCamposTipoReporte = camposRegFacade.buscarPorTipoRegistro(registroSeleccionado.getIdTipoReg().getIdTipoReg());
        for (HcCamposReg campoPorTipoRegistro : listaCamposTipoReporte) {
            datosReporte.setValor(campoPorTipoRegistro.getPosicion(), "<b>" + campoPorTipoRegistro.getNombrePdf() + " </b>");
        }
       
        datosReporte.setValor(49, pacienteSeleccionado.nombreCompleto());
        datosReporte.setValor(51, "<b>HISTORIA No: </b>" + registroSeleccionado.getIdPaciente().getIdentificacion());
        
        datosReporte.setValor(53, "<b>NOMBRE: </b>" + pacienteSeleccionado.nombreCompleto());      

        datosReporte.setValor(54, "<b>FECHA ATENCION: </b> " + formatoDateHora.format(registroSeleccionado.getFechaReg()));
       
        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            datosReporte.setValor(56, "<b>EDAD: </b> " + calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>" + formatoDate.format(pacienteSeleccionado.getFechaNacimiento()));
        } else {
            datosReporte.setValor(56, "<b>EDAD: </b> ");
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>");
        }
        if (pacienteSeleccionado.getSexo() != null) {
            datosReporte.setValor(57, "<b>SEXO: </b> " + pacienteSeleccionado.getSexo().getDescripcion());
        } else {
            datosReporte.setValor(57, "<b>SEXO: </b> ");
        }
        datosReporte.setValor(59, "<b>DIRECCION: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getDireccion()));
        datosReporte.setValor(60, "<b>TELEFONO: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getTelefono()));

        if (pacienteSeleccionado.getTipoIdentificacion() != null) {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        } else {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        }
       
        if (pacienteSeleccionado.getEstadoCivil() != null) {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> " + pacienteSeleccionado.getEstadoCivil().getDescripcion());
        } else {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> ");
        }
       
        if (pacienteSeleccionado.getCanton() != null) {
            datosReporte.setValor(67, "<b>CANTÓN: </b>" + pacienteSeleccionado.getCanton().getDescripcion());
        } else {
            datosReporte.setValor(67, "<b>CANTÓN: </b>");//TELEFONO EMPRESA                
        }

        if (registroSeleccionado.getIdMedico() != null) {
          
            datosReporte.setValor(84, registroSeleccionado.getIdMedico().nombreCompleto());
            if (registroSeleccionado.getIdMedico().getEspecialidad() != null) {          
                datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> " + registroSeleccionado.getIdMedico().getEspecialidad().getDescripcion());//PARA FIRMA  NOMBRE MEDICO
            }
          
            datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> Reg. prof. " + registroSeleccionado.getIdMedico().getRegistroProfesional());//NOMBRE MEDICO

        }

        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 92) {
            datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(76, "<b>SIGNOS VITALES</b>");
        }
        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 94) {

            datosReporte.setValor(75, "<b>Certifico haber atendido al paciente:</b>");
            datosReporte.setValor(76, "<b>Cuyo diagnóstico es:</b>");
            datosReporte.setValor(77, "<b>Debiendo guardar reposo:</b>");         
            datosReporte.setValor(80, "<b>Extiendo el presente certificado para ser presentado en:</b>");
        }
        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 6) {

            datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(76, "<b>RECETA</b>");
        }


        for (HcDetalle campoDeRegistroEncontrado : listaCamposReporte) {
            datosReporte.setValor(campoDeRegistroEncontrado.getHcCamposReg().getPosicion(), "<b>" + campoDeRegistroEncontrado.getHcCamposReg().getNombrePdf() + " </b>" + campoDeRegistroEncontrado.getValor());
         
        }
        listaHistoriaReporte.add(datosReporte);
    }

    private void cargarDatosReporteTerapia(HcRegistro registroSeleccionado) {
        listaHistoriaReporte = new ArrayList<>();
        DatosHistoriaClinica datosReporte = new DatosHistoriaClinica();
        int i = 1;
        int j = 12;
        
        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 91) {         
        
            List<HcTfSesionesHistorial> listaCamposTipoReporte = SesionesHistorialTfFacade.buscarTratamientosActivosById(registroSeleccionado.getIdtratamiento(), registroSeleccionado.getSesion());

            for (HcTfSesionesHistorial campoDeRegistroEncontrado : listaCamposTipoReporte) {
                if(clasificacionesFacade.find(campoDeRegistroEncontrado.getCodTratamientos()).getMaestro().matches("Agente Fisico")){
                    datosReporte.setValor(i, clasificacionesFacade.find(campoDeRegistroEncontrado.getCodTratamientos()).getDescripcion());
                    i++;
                }else if(clasificacionesFacade.find(campoDeRegistroEncontrado.getCodTratamientos()).getMaestro().matches("Ejercicio Terapeutico")){
                    datosReporte.setValor(j, clasificacionesFacade.find(campoDeRegistroEncontrado.getCodTratamientos()).getDescripcion());
                    j++;
                }

            }
            
            datosReporte.setValor(0, SesionesTfFacade.tratamientosById(registroSeleccionado.getIdtratamiento()).getObservacion());
            
            if (listaCamposTipoReporte.get(0).getNumSesion() == 0){
                datosReporte.setValor(34, "<b>NÚMERO SESIONES: </b>" + SesionesTfFacade.numSesiones(registroSeleccionado.getIdtratamiento()));
            }else{
                datosReporte.setValor(34, "<b>NÚMERO SESIONES: </b>" + listaCamposTipoReporte.get(0).getNumSesion() + "/" + SesionesTfFacade.numSesiones(registroSeleccionado.getIdtratamiento()));
            }
            
        }
        
        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 96) {
            datosReporte.setValor(0, detalleTrFacade.valorTratamiento("TR001", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(1, detalleTrFacade.valorTratamiento("TR002", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(2, detalleTrFacade.valorTratamiento("TR003", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(3, detalleTrFacade.valorTratamiento("TR004", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(4, detalleTrFacade.valorTratamiento("TR005", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(5, detalleTrFacade.valorTratamiento("TR006", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(6, detalleTrFacade.valorTratamiento("TR007", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(7, detalleTrFacade.valorTratamiento("TR008", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(8, detalleTrFacade.valorTratamiento("TR009", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(9, detalleTrFacade.valorTratamiento("TR010", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(10, detalleTrFacade.valorTratamiento("TR011", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(11, detalleTrFacade.valorTratamiento("TR012", registroSeleccionado.getIdRegistro()));
            datosReporte.setValor(34, "<b>NÚMERO TERAPIAS: </b>" + sesionesTrFacade.numSesiones(registroSeleccionado.getIdRegistro()));
        }
        
        datosReporte.setValor(49, pacienteSeleccionado.nombreCompleto());
        datosReporte.setValor(51, "<b>HISTORIA No: </b>" + registroSeleccionado.getIdPaciente().getIdentificacion());
        
        datosReporte.setValor(53, "<b>NOMBRE: </b>" + pacienteSeleccionado.nombreCompleto());      

        datosReporte.setValor(54, "<b>FECHA ATENCION: </b> " + formatoDateHora.format(registroSeleccionado.getFechaReg()));
       
        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            datosReporte.setValor(56, "<b>EDAD: </b> " + calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>" + formatoDate.format(pacienteSeleccionado.getFechaNacimiento()));
        } else {
            datosReporte.setValor(56, "<b>EDAD: </b> ");
            datosReporte.setValor(55, "<b>FECHA NACIMIENTO: </b>");
        }
        if (pacienteSeleccionado.getSexo() != null) {
            datosReporte.setValor(57, "<b>SEXO: </b> " + pacienteSeleccionado.getSexo().getDescripcion());
        } else {
            datosReporte.setValor(57, "<b>SEXO: </b> ");
        }
        datosReporte.setValor(59, "<b>DIRECCION: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getDireccion()));
        datosReporte.setValor(60, "<b>TELEFONO: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getTelefono()));

        if (pacienteSeleccionado.getTipoIdentificacion() != null) {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        } else {
       
            datosReporte.setValor(61, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        }
       
        if (pacienteSeleccionado.getEstadoCivil() != null) {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> " + pacienteSeleccionado.getEstadoCivil().getDescripcion());
        } else {
            datosReporte.setValor(65, "<b>ESTADO CIVIL: </b> ");
        }
       
        if (pacienteSeleccionado.getCanton() != null) {
            datosReporte.setValor(67, "<b>CANTÓN: </b>" + pacienteSeleccionado.getCanton().getDescripcion());
        } else {
            datosReporte.setValor(67, "<b>CANTÓN: </b>");//TELEFONO EMPRESA                
        }

        if (registroSeleccionado.getIdMedico() != null) {
          
            datosReporte.setValor(84, registroSeleccionado.getIdMedico().nombreCompleto());
            if (registroSeleccionado.getIdMedico().getEspecialidad() != null) {          
                datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> " + registroSeleccionado.getIdMedico().getEspecialidad().getDescripcion());//PARA FIRMA  NOMBRE MEDICO
            }
          
            datosReporte.setValor(84, datosReporte.getValor(84) + " <br/> Reg. prof. " + registroSeleccionado.getIdMedico().getRegistroProfesional());//NOMBRE MEDICO

        }

        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 91) {
            datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(76, "<b>TRATAMIENTOS</b>");
            datosReporte.setValor(77, "<b>AGENTE FÍSICO</b>");
            datosReporte.setValor(78, "<b>MECANORAPIA Y EJERCICIO TERAPÉUTICO</b>");
            datosReporte.setValor(79, "<b>PRECAUCIÓN Y/O INDICACIONES ESPECIALES:</b>");
        }
        
        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 96) {
            datosReporte.setValor(75, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(76, "<b>FÓRMULA</b>");
            datosReporte.setValor(77, "<b>DOSIS / ML</b>");
            datosReporte.setValor(78, "<b>DOSIS / GOTAS</b>");
            datosReporte.setValor(79, "<b>Solución salina al 9%</b>");
            datosReporte.setValor(80, "<b>Combivent ampolla 2 ml</b>");
            datosReporte.setValor(81, "<b>Dexametasona ampolla 4 mg - 1 ml</b>");
            datosReporte.setValor(82, "<b>Ventolin frasco / gotero solución</b>");
            datosReporte.setValor(86, "<b>Fluimucil ampolla</b>");
            datosReporte.setValor(87, "<b>Ambroxol frasco / gotero</b>");
        }
       


       
        listaHistoriaReporte.add(datosReporte);
    }

   
    private void cargarDatosFichaMedica(HcRegistro registroSeleccionado) {


        listaHistoriaReporte = new ArrayList<>();
        DatosHistoriaClinica datosReporte = new DatosHistoriaClinica();
        Set<HcDetalle> setCamposReporte = registroSeleccionado.getHcDetalleList();
        List<HcDetalle> listaCamposReporte = new ArrayList<>(setCamposReporte);
       
        List<HcCamposReg> listaCamposTipoReporte = camposRegFacade.buscarPorTipoRegistro(registroSeleccionado.getIdTipoReg().getIdTipoReg());
        for (HcCamposReg campoPorTipoRegistro : listaCamposTipoReporte) {
            try {
                datosReporte.setValor(campoPorTipoRegistro.getPosicion(), "<b>" + campoPorTipoRegistro.getNombrePdf() + " </b>");
            } catch (Exception e) {
            }

        }
        
     
        datosReporte.setValor(206, "<b>FOLIO: </b>" + registroSeleccionado.getFolio());
     
        datosReporte.setValor(207, "<b>HISTORIA No: </b>" + registroSeleccionado.getIdPaciente().getIdentificacion());
        
        datosReporte.setValor(208, "<b>NOMBRE: </b>" + pacienteSeleccionado.nombreCompleto());   

      
        datosReporte.setValor(210, "<b>FECHA ATENCION: </b> " + formatoDate.format(registroSeleccionado.getFechaReg()) + "<b> HORA: </b> " + formatoHora.format(registroSeleccionado.getFechaReg()));

       
        if (pacienteSeleccionado.getFechaNacimiento() != null) {
            datosReporte.setValor(211, "<b>EDAD: </b> " + calcularEdad(pacienteSeleccionado.getFechaNacimiento()));
            datosReporte.setValor(212, "<b>FECHA NACIMIENTO: </b>" + formatoDate.format(pacienteSeleccionado.getFechaNacimiento()));
        } else {
            datosReporte.setValor(211, "<b>EDAD: </b> ");
            datosReporte.setValor(212, "<b>FECHA NACIMIENTO: </b>");
        }
      
        if (pacienteSeleccionado.getSexo() != null) {
            datosReporte.setValor(213, "<b>SEXO: </b> " + pacienteSeleccionado.getSexo().getDescripcion());
        } else {
            datosReporte.setValor(213, "<b>SEXO: </b> ");
        }
        if (pacienteSeleccionado.getDireccion() != null) {
            datosReporte.setValor(265, "<b>DIRECCION: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getDireccion()));
        }
     
        datosReporte.setValor(216, "<b>TELEFONO: </b> " + obtenerCadenaNoNula(pacienteSeleccionado.getTelefono()));

        if (pacienteSeleccionado.getTipoIdentificacion() != null) {
         
            datosReporte.setValor(218, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        } else {

            datosReporte.setValor(218, "<b>IDENTIFICACION: </b> " + pacienteSeleccionado.getIdentificacion());
        }

        if (pacienteSeleccionado.getEstadoCivil() != null) {
            datosReporte.setValor(223, "<b>ESTADO CIVIL: </b> " + pacienteSeleccionado.getEstadoCivil().getDescripcion());
        } else {
            datosReporte.setValor(223, "<b>ESTADO CIVIL: </b> ");
        }

        
        
        if (pacienteSeleccionado.getCanton() != null) {
            datosReporte.setValor(225, "<b>CANTON: </b>" + pacienteSeleccionado.getCanton().getDescripcion());
        } else {
            datosReporte.setValor(225, "<b>CANTON: </b>");//TELEFONO EMPRESA                
        }

          if (registroSeleccionado.getIdMedico() != null) {
            datosReporte.setValor(235, registroSeleccionado.getIdMedico().nombreCompleto());//PARA FIRMA NOMBRE MEDICO
            if (registroSeleccionado.getIdMedico().getEspecialidad() != null) {

                datosReporte.setValor(235, datosReporte.getValor(235) + " <br/> " + registroSeleccionado.getIdMedico().getEspecialidad().getDescripcion());//PARA FIRMA  NOMBRE MEDICO
            }
            datosReporte.setValor(235, datosReporte.getValor(235) + " <br/> Reg. prof. " + registroSeleccionado.getIdMedico().getRegistroProfesional());//NOMBRE MEDICO

        }

   

        if (registroSeleccionado.getIdTipoReg().getIdTipoReg() == 93) {
            datosReporte.setValor(103, "<b>DATOS PACIENTE</b>");
            datosReporte.setValor(104, "<b>DATOS CONSULTA</b>");
            datosReporte.setValor(105, "<b>ANTECEDENTES PERSONALES</b>");
            datosReporte.setValor(106, "<b>ANTECEDENTES FAMILIARES</b>");
            datosReporte.setValor(107, "<b>ANTECEDENTES VACUNACIÓN</b>");
            datosReporte.setValor(108, "<b>EVOLUCIÓN</b>");
            datosReporte.setValor(109, "<b>EXAMEN FÍSICO</b>");
            datosReporte.setValor(110, "<b>EXAMEN DE LABORATORIO</b>");
            datosReporte.setValor(111, "<b>EXAMEN DE GABINETE</b>");
            datosReporte.setValor(112, "<b>DIAGNÓSTICO</b>");
            datosReporte.setValor(191, "<b>TRATAMIENTO</b>");
        }

        for (HcDetalle campoDeRegistroEncontrado : listaCamposReporte) { 
                datosReporte.setValor(campoDeRegistroEncontrado.getHcCamposReg().getPosicion(), "<b>" + campoDeRegistroEncontrado.getHcCamposReg().getNombrePdf() + " </b>" + campoDeRegistroEncontrado.getValor());

        }

        listaHistoriaReporte.add(datosReporte);
    }

     public void guardarTerapiasTf() {
        try {

            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            HcRegistro registro = new HcRegistro();
            HcTfSesiones nuevoRegistro =  new HcTfSesiones();
            HcTfTratamientos nuevosTratamientos = new HcTfTratamientos();

            //long idReg = tratamientosTfFacade.siguienteid();

            Map<String, Boolean> valsTf = new HashMap<>();

            valsTf.put("AF1", af1);
            valsTf.put("AF2", af2);
            valsTf.put("AF3", af3);
            valsTf.put("AF4", af4);
            valsTf.put("AF5", af5);
            valsTf.put("AF6", af6);
            valsTf.put("AF7", af7);
            valsTf.put("AF8", af8);
            valsTf.put("AF9", af9);
            valsTf.put("AF10", af10);
            valsTf.put("AF11", af11);
            valsTf.put("ET1", et1);
            valsTf.put("ET2", et2);
            valsTf.put("ET3", et3);
            valsTf.put("ET4", et4);
            valsTf.put("ET5", et5);
            valsTf.put("ET6", et6);
            valsTf.put("ET7", et7);
            valsTf.put("ET8", et8);
            valsTf.put("ET9", et9);
            valsTf.put("ET10", et10);
            valsTf.put("ET11", et11);
            valsTf.put("ET12", et12);
            valsTf.put("ET13", et13);
            valsTf.put("ET14", et14);
            valsTf.put("ET15", et15);
            valsTf.put("ET16", et16);
            valsTf.put("ET17", et17);
            valsTf.put("ET18", et18);
            valsTf.put("ET19", et19);
            valsTf.put("ET20", et20);
            valsTf.put("ET21", et21);
            
            if(cantSesiones <= 0){
                imprimirMensaje("Error", "Cantidad de sesiones debe ser mayor a cero", FacesMessage.SEVERITY_ERROR);
                return; 
            }
            
            registro.setIdRegistro(registroFacade.siguienteid());
            idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
            if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                registro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
            } else {
                imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            registro.setIdTipoReg(tipoRegistroClinicoActual);
            registro.setFechaReg(fechaRegistro);
            registro.setFechaSis(fechaSistema);
            registro.setIdPaciente(pacienteSeleccionado);
            registro.setFolio(registroFacade.buscarMaximoFolio(registro.getIdPaciente().getIdPaciente()) + 1);
            registro.setIdtratamiento(registro.getIdRegistro());
            registro.setSesion(0);
            if (validarNoVacio(turnoCita)) {
                List<CtCitas> listaCitas = (List<CtCitas>) turnosFacade.find(Integer.parseInt(turnoCita)).getCtCitasList();
                for (CtCitas cita : listaCitas) {
                    if (cita.getCancelada() == false) {
                        registro.setIdCita(cita.getIdCita());
                        break;
                    }
                }
            }
            registroFacade.create(registro);

            for (int i = 1; i <= cantSesiones; i++) {
                nuevoRegistro = new HcTfSesiones();
                nuevoRegistro.setIdTratamiento(registro.getIdRegistro().longValue());
                nuevoRegistro.setNumSesion(i);
                idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
                if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                    nuevoRegistro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
                } else {
                    imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                    return;
                }

                if (fechaRegistro == null) {
                    fechaRegistro = fechaSistema;
                }

                nuevoRegistro.setFechaReg(fechaRegistro);
                nuevoRegistro.setIdPaciente(pacienteSeleccionado);
                nuevoRegistro.setRealizado(false);
                nuevoRegistro.setObservacion(obsTf);

                SesionesTfFacade.create(nuevoRegistro);

            }

            for (Map.Entry<String, Boolean> val : valsTf.entrySet()) {
                nuevosTratamientos = new HcTfTratamientos();
                nuevosTratamientos.setIdTratamiento(nuevoRegistro.getIdTratamiento());
                nuevosTratamientos.setActivo(val.getValue());
                CnClasificaciones trat = clasificacionesFacade.getTramientoByMaestro(val.getKey());;
                nuevosTratamientos.setCodTratamientos(trat);
                tratamientosTfFacade.create(nuevosTratamientos);
                
                if(val.getValue()){
                    HcTfSesionesHistorial sesionhistorial = new HcTfSesionesHistorial();
                    sesionhistorial.setNumSesion(0);
                    sesionhistorial.setIdTratamiento(nuevoRegistro.getIdTratamiento());
                    sesionhistorial.setCodTratamientos(trat.getId());
                    SesionesHistorialTfFacade.create(sesionhistorial);
                }

            }

            transaction.commit();

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "Nuevo registro almacenado");

            PrimeFaces.current().dialog().showMessageDynamic(message);
            limpiarTf();
            //valoresPorDefecto();

            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
            RequestContext.getCurrentInstance().update("IdFormHistorias");

        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(HistoriaClinicaController.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "No se pudo almacenar registro");
        }
    }

    public void cargaTerapiasTf() {
        List<HcTfTratamientos> listaTratamientos = tratamientosTfFacade.getTratamientosById(idTratameinto);
        Map<String, Boolean> valsTf = new HashMap<>();
        if (!listaTratamientos.isEmpty()) {
            for (HcTfTratamientos valor : listaTratamientos) {
                CnClasificaciones key = clasificacionesFacade.buscarPorIdTf(valor.getCodTratamientos().getId());
                valsTf.put(key.getCodigo(), valor.getActivo());

            }
            af1 = valsTf.get("AF1");
            af2 = valsTf.get("AF2");
            af3 = valsTf.get("AF3");
            af4 = valsTf.get("AF4");
            af5 = valsTf.get("AF5");
            af6 = valsTf.get("AF6");
            af7 = valsTf.get("AF7");
            af8 = valsTf.get("AF8");
            af9 = valsTf.get("AF9");
            af10 = valsTf.get("AF10");
            af11 = valsTf.get("AF11");
            et1 = valsTf.get("ET1");
            et2 = valsTf.get("ET2");
            et3 = valsTf.get("ET3");
            et4 = valsTf.get("ET4");
            et5 = valsTf.get("ET5");
            et6 = valsTf.get("ET6");
            et7 = valsTf.get("ET7");
            et8 = valsTf.get("ET8");
            et9 = valsTf.get("ET9");
            et10 = valsTf.get("ET10");
            et11 = valsTf.get("ET11");
            et12 = valsTf.get("ET12");
            et13 = valsTf.get("ET13");
            et14 = valsTf.get("ET14");
            et15 = valsTf.get("ET15");
            et16 = valsTf.get("ET16");
            et17 = valsTf.get("ET17");
            et18 = valsTf.get("ET18");
            et19 = valsTf.get("ET19");
            et20 = valsTf.get("ET20");
            et21 = valsTf.get("ET21");

            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
        }else{
           FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Paciente no tiene tratamientos");

          PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }
    

    public void actualizarTerapias() {
        try {

            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            HcTfTratamientos actTratamientos;
            List<HcTfTratamientos> listaTratamientos = tratamientosTfFacade.getTratamientosById(idTratameinto);

            Map<String, Boolean> valsTf = new HashMap<>();

            valsTf.put("AF1", af1);
            valsTf.put("AF2", af2);
            valsTf.put("AF3", af3);
            valsTf.put("AF4", af4);
            valsTf.put("AF5", af5);
            valsTf.put("AF6", af6);
            valsTf.put("AF7", af7);
            valsTf.put("AF8", af8);
            valsTf.put("AF9", af9);
            valsTf.put("AF10", af10);
            valsTf.put("AF11", af11);
            valsTf.put("ET1", et1);
            valsTf.put("ET2", et2);
            valsTf.put("ET3", et3);
            valsTf.put("ET4", et4);
            valsTf.put("ET5", et5);
            valsTf.put("ET6", et6);
            valsTf.put("ET7", et7);
            valsTf.put("ET8", et8);
            valsTf.put("ET9", et9);
            valsTf.put("ET10", et10);
            valsTf.put("ET11", et11);
            valsTf.put("ET12", et12);
            valsTf.put("ET13", et13);
            valsTf.put("ET14", et14);
            valsTf.put("ET15", et15);
            valsTf.put("ET16", et16);
            valsTf.put("ET17", et17);
            valsTf.put("ET18", et18);
            valsTf.put("ET19", et19);
            valsTf.put("ET20", et20);
            valsTf.put("ET21", et21);

            //for (Map.Entry<String, Boolean> val : valsTf.entrySet()) {
            for (HcTfTratamientos trat : listaTratamientos) {
                actTratamientos = tratamientosTfFacade.find(trat.getId());//new HcTfTratamientos();
                CnClasificaciones codTrat = clasificacionesFacade.buscarPorIdTf(trat.getCodTratamientos().getId());
                actTratamientos.setActivo(valsTf.get(codTrat.getCodigo()));
                //CnClasificaciones trat = clasificacionesFacade.getTramientoByMaestro(val.getKey());;
                //actTratamientos.setCodTratamientos(trat);
                tratamientosTfFacade.edit(actTratamientos);
                //tratamientosTfFacade.create(actTratamientos);
            }

            transaction.commit();

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "Tratamientos Actualizados");

            PrimeFaces.current().dialog().showMessageDynamic(message);
            limpiarTf();
            //valoresPorDefecto();

            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
            RequestContext.getCurrentInstance().update("IdFormHistorias");

        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(HistoriaClinicaController.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "No se pudo actualizar tratamientos");
        }
    }


    public void generarReporte() throws JRException, IOException {
        
        if (treeSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        NodoArbolHistorial nodArbolHisSeleccionado = null;
        for (TreeNode nodo : treeSeleccionado) {
            nodArbolHisSeleccionado = (NodoArbolHistorial) nodo.getData();
            if (nodArbolHisSeleccionado.getTipoDeNodo() == TipoNodoEnum.REGISTRO_HISTORIAL) {
                break;
            }
        }
        if (nodArbolHisSeleccionado == null) {
            imprimirMensaje("Error", "Se debe seleccionar un registro del histórico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        HcRegistro registroSeleccionado = registroFacade.find(nodArbolHisSeleccionado.getIdRegistro());
      
       if (registroSeleccionado.getIdTipoReg().getUrlPagina().startsWith("g_")) {
            cargarDatosFichaMedica(registroSeleccionado);
        } else if (registroSeleccionado.getIdTipoReg().getUrlPagina().startsWith("terapia") || registroSeleccionado.getIdTipoReg().getUrlPagina().startsWith("creaOrden")){
            cargarDatosReporteTerapia(registroSeleccionado);
        }else {
            cargarDatosReporte(registroSeleccionado);
        }
       
        JRBeanCollectionDataSource beanCollectionDataSource;
        HttpServletResponse httpServletResponse;
        String rutaReporte = "";
        rutaReporte = rutaReporte + "historiaclinica/reportes/";
        rutaReporte = rutaReporte + registroSeleccionado.getIdTipoReg().getUrlPagina();
        rutaReporte = rutaReporte.substring(0, rutaReporte.length() - 6);
        rutaReporte = rutaReporte + ".jasper";

        String fecha_registro = formatoDateHora.format(registroSeleccionado.getFechaReg());


        List<JasperPrint> list = new ArrayList<JasperPrint>();

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String urlImagenes = request.getRequestURL().toString();
        urlImagenes = urlImagenes.substring(0, urlImagenes.indexOf("historias.xhtml")) + "img/";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext2 = (ServletContext) facesContext.getExternalContext().getContext();
        String logoEmpresa = servletContext2.getRealPath("/recursos/img/logo-cmamsa-black.png");
        HashMap mapParams = new HashMap();
        mapParams.put("urlBaseImagenes", urlImagenes);
        mapParams.put("logoEmpresa", logoEmpresa);
        beanCollectionDataSource = new JRBeanCollectionDataSource(listaHistoriaReporte);
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), mapParams, beanCollectionDataSource);
            list.add(jasperPrint);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, list);
            exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, servletOutputStream);
            exporter.exportReport();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception ex) {
            System.out.println("ERRR " + ex.getLocalizedMessage());
            ex.printStackTrace();
            System.out.println("Error generando reporte " + ex.getMessage());
        }
    }

    public void generarReporteReceta() throws JRException, IOException {

        HcRegistro registroSeleccionado = registroFacade.find(idfichamedica);
        cargarDatosReporte(registroSeleccionado);
       
       
        JRBeanCollectionDataSource beanCollectionDataSource;
        HttpServletResponse httpServletResponse;
        String rutaReporte = "";
        rutaReporte = rutaReporte + "historiaclinica/reportes/";
        rutaReporte = rutaReporte + registroSeleccionado.getIdTipoReg().getUrlPagina();
        rutaReporte = rutaReporte.substring(0, rutaReporte.length() - 6);
        rutaReporte = rutaReporte + ".jasper";

        String fecha_registro = formatoDateHora.format(registroSeleccionado.getFechaReg());

       
        List<JasperPrint> list = new ArrayList<JasperPrint>();

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String urlImagenes = request.getRequestURL().toString();
        urlImagenes = urlImagenes.substring(0, urlImagenes.indexOf("recetario.xhtml")) + "img/";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext2 = (ServletContext) facesContext.getExternalContext().getContext();
        String logoEmpresa = servletContext2.getRealPath("/recursos/img/logo-cmamsa-black.png");
        HashMap mapParams = new HashMap();
        mapParams.put("urlBaseImagenes", urlImagenes);
        mapParams.put("logoEmpresa", logoEmpresa);
        beanCollectionDataSource = new JRBeanCollectionDataSource(listaHistoriaReporte);
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), mapParams, beanCollectionDataSource);
            list.add(jasperPrint);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, list);
            exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, servletOutputStream);
            exporter.exportReport();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception ex) {
            System.out.println("ERRR " + ex.getLocalizedMessage());
            ex.printStackTrace();
            System.out.println("Error generando reporte " + ex.getMessage());
        }
    }

    public void generarReporteCertificado() throws JRException, IOException {

        HcRegistro registroSeleccionado = registroFacade.find(idfichamedica);
        cargarDatosReporte(registroSeleccionado);
        
        JRBeanCollectionDataSource beanCollectionDataSource;
        HttpServletResponse httpServletResponse;
        String rutaReporte = "";
        rutaReporte = rutaReporte + "historiaclinica/reportes/";
        rutaReporte = rutaReporte + registroSeleccionado.getIdTipoReg().getUrlPagina();
        rutaReporte = rutaReporte.substring(0, rutaReporte.length() - 6);
        rutaReporte = rutaReporte + ".jasper";

        String fecha_registro = formatoDateHora.format(registroSeleccionado.getFechaReg());

        System.out.println("Reporte --> " + rutaReporte);
        System.out.println("....................................");

        List<JasperPrint> list = new ArrayList<JasperPrint>();

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String urlImagenes = request.getRequestURL().toString();
        urlImagenes = urlImagenes.substring(0, urlImagenes.indexOf("certificadomedico.xhtml")) + "img/";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext2 = (ServletContext) facesContext.getExternalContext().getContext();
        String logoEmpresa = servletContext2.getRealPath("/recursos/img/logo-cmamsa-black.png");
        HashMap mapParams = new HashMap();
        mapParams.put("urlBaseImagenes", urlImagenes);
        mapParams.put("logoEmpresa", logoEmpresa);
        beanCollectionDataSource = new JRBeanCollectionDataSource(listaHistoriaReporte);
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        try (ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream()) {
            httpServletResponse.setContentType("application/pdf");
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            JasperPrint jasperPrint = JasperFillManager.fillReport(servletContext.getRealPath(rutaReporte), mapParams, beanCollectionDataSource);
            list.add(jasperPrint);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, list);
            exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, servletOutputStream);
            exporter.exportReport();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception ex) {
            System.out.println("ERRR " + ex.getLocalizedMessage());
            ex.printStackTrace();
            System.out.println("Error generando reporte " + ex.getMessage());
        }
    }

    public void limpiarFormulario() {
      

        modificandoHistoriaClinica = false;
        registroEncontrado = null;
        datosFichamedica.limpiar();
        datosFichamedica.setValor(0, "");
        tipo = 0;
        idMedico = null;
        especialidadMedico = null;
        fechaRegistro = citasFacade.FechaServidorddmmyyhhmmTimestamp();
        fechaSistema = citasFacade.FechaServidorddmmyyhhmmTimestamp();
        if (indexController.getUsuarioActual().getTipoUsuario().getCodigo().equals("2")) {
            idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
            especialidadMedico = indexController.getUsuarioActual().getEspecialidad().getDescripcion();
         
        }

        datosFichamedica = new DatosHistoriaClinica();
        for (int i = 0; i <= 745; i++) {
            datosFichamedica.setValor(i, "");
        }

    }

    public void btnLimpiarFormulario() {
        limpiarFormulario();
        valoresPorDefecto();
        modificandoHistoriaClinica = false;
    }

    public void cambiaTipoRegistroClinico() {
        modificandoHistoriaClinica = false;
        limpiarFormulario();
        valoresPorDefecto();
        mostrarFormularioRegistroClinico();

    }

    public void manageFile(FileUploadEvent event) {
        if (!"".equals(descriparchivo)) {

            try {

                UploadedFile file = event.getFile();
                SimpleDateFormat dt = new SimpleDateFormat("yyyyymmddhhmmss");
                String nomarch = "HC_" + pacienteSeleccionado.getIdPaciente() + "_" + dt.format(Cncie10Facade.FechaServidorddmmyyhhmmTimestamp())//diferenciar el usuario actual
                        + file.getFileName().substring(file.getFileName().lastIndexOf("."), file.getFileName().length());//colocar extension
                String path = indexController.getUrlFile();
                
                if (uploadArchivo(file, path, nomarch)) {
                
                    try {
                        HcArchivos a = new HcArchivos();
                        a.setDescripcion(descriparchivo);
                        a.setUrlFile(path + nomarch);
                        a.setFechaUpload(hcArchivosFacade.FechaServidorddmmyyhhmmTimestamp());
                        a.setIdPaciente(pacienteSeleccionado);
                        a.setUsuarioUpload(indexController.getUsuarioActual().getIdUsuario());
                        hcArchivosFacade.create(a);
                       
                        listaArchivo = hcArchivosFacade.getHcArchivosByPaciente(pacienteSeleccionado);
                        imprimirMensaje("Información", "El archivo se guardo", FacesMessage.SEVERITY_INFO);
                        descriparchivo = "";

                    } catch (Exception e) {
                        imprimirMensaje("Error", "El archivo no se guardo", FacesMessage.SEVERITY_WARN);

                    }

                }

            } catch (Exception ex) {
                System.out.println("Error 20 en " + this.getClass().getName() + ":" + ex.toString());
            }

        } else {
            imprimirMensaje("Error", "Agregar una descripcion por favor y seleccionar de nuevo el archivo", FacesMessage.SEVERITY_WARN);

        }

    }

    public void downloadArchivo() throws FileNotFoundException, MagicParseException, MagicMatchNotFoundException, MagicException {
        if (archivoSeleccionado != null) {
            File f = new File(archivoSeleccionado.getUrlFile());
            InputStream stream = new FileInputStream(f);
            fileDownload = new DefaultStreamedContent(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", f.getName());
        }
    }

   
    private void mostrarFormularioRegistroClinico() {
        if (tipoHistoriaClinica != null && tipoHistoriaClinica.length() != 0) {
            tipoRegistroClinicoActual = tipoRegCliFacade.find(Integer.parseInt(tipoHistoriaClinica));

            if (tipoRegistroClinicoActual.getIdTipoReg() == 92) {
                urlPagina = "signosvitales-view.xhtml";
            } else {
                urlPagina = tipoRegistroClinicoActual.getUrlPagina();
            }

            nombreTipoHistoriaClinica = tipoRegistroClinicoActual.getNombre();
        } else {
            tipoRegistroClinicoActual = null;
            urlPagina = "";
            nombreTipoHistoriaClinica = "";
        }
    }

   
    public void actualizarRegistro() {
        List<HcDetalle> listaDetalle = new ArrayList<>();
        HcDetalle nuevoDetalle;
        HcCamposReg campoResgistro;

        if (!modificandoHistoriaClinica) {
            imprimirMensaje("Error", "No se ha cargado un registro para poder modificarlo", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (idMedico != null && idMedico.length() != 0) {
        } else {
        }
        if (tipoRegistroClinicoActual != null) {
            registroEncontrado.setIdTipoReg(tipoRegistroClinicoActual);
        } else {
            imprimirMensaje("Error", "No se puede determinar el tipo de registro clinico", FacesMessage.SEVERITY_ERROR);
            return;
        }
        if (fechaRegistro == null) {
            fechaRegistro = fechaSistema;
        }
        registroEncontrado.setIdPaciente(pacienteSeleccionado);

        for (int i = 0; i < tipoRegistroClinicoActual.getCantCampos(); i++) {

            if (datosFichamedica.getValor(i) != null && datosFichamedica.getValor(i).toString().length() != 0) {
                campoResgistro = camposRegFacade.buscarPorTipoRegistroYPosicion(tipoRegistroClinicoActual.getIdTipoReg(), i);
                if (campoResgistro != null) {
                    nuevoDetalle = new HcDetalle(registroEncontrado.getIdRegistro(), campoResgistro.getIdCampo());
                    if (campoResgistro.getTabla() == null || campoResgistro.getTabla().length() == 0) {
                        nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
                    } else {
                        switch (campoResgistro.getTabla()) {                            
                            case "date":
                                try {
                                    Date f = formatoLongDate.parse(datosFichamedica.getValor(i).toString());
                                    nuevoDetalle.setValor(formatoDateHora.format(f));
                                } catch (ParseException ex) {
                                    nuevoDetalle.setValor("Error: " + datosFichamedica.getValor(i).toString());
                                }
                                break;                          
                            
                            default:
                                nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
                                break;
                        }
                    }
                    listaDetalle.add(nuevoDetalle);
                    HcRegistro hc = registroFacade.find(Integer.valueOf(nuevoDetalle.getHcDetallePK().getIdRegistro()));
                    hc.setIdUsuarioEditar(indexController.getUsuarioActual().getIdUsuario());
                    hc.setFechaEditar(hcArchivosFacade.FechaServidorddmmyyhhmmTimestamp());
                    detalleFacade.edit(nuevoDetalle);
                    registroFacade.edit(hc);
                }
            } else {
                System.out.println("No encontro en tabla hc_campos_registro el valor: id_tipo_reg = " + tipoRegistroClinicoActual.getIdTipoReg());
            }
        }

        imprimirMensaje("Correcto", "Registro actualizado.", FacesMessage.SEVERITY_INFO);
        limpiarFormulario();
        valoresPorDefecto();
        RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
    }

   

    public void guardarRegistro() {

        try {
            
            System.out.println(datosFichamedica.getDato0());

            System.out.println("Iniciando el guardado del registro");

            if (tipoRegistroClinicoActual.getIdTipoReg() == 94) {
                try {
                    if ((formatoLongDate.parse(datosFichamedica.getValor(1).toString()).compareTo(formatoLongDate.parse(datosFichamedica.getValor(2).toString())) == 1)) {
                        imprimirMensaje("Error", "Rango de fecha incorrecto", FacesMessage.SEVERITY_ERROR);
                        return;

                    }
                } catch (ParseException ex) {
                    Logger.getLogger(HistoriaClinicaController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            HcRegistro nuevoRegistro = new HcRegistro();
            List<HcDetalle> listaDetalle = new ArrayList<>();
            HcDetalle nuevoDetalle;
            HcCamposReg campoResgistro;

            nuevoRegistro.setIdRegistro(registroFacade.siguienteid());

            idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
            if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                nuevoRegistro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
            } else {
                imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            if (tipoRegistroClinicoActual != null) {
                nuevoRegistro.setIdTipoReg(tipoRegistroClinicoActual);
            } else {
                imprimirMensaje("Error", "No se puede determinar el tipo de registro clinico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            if (fechaRegistro == null) {
                fechaRegistro = fechaSistema;
            }

            nuevoRegistro.setFechaReg(fechaRegistro);
            nuevoRegistro.setFechaSis(fechaSistema);
            nuevoRegistro.setIdPaciente(pacienteSeleccionado);
            nuevoRegistro.setFolio(registroFacade.buscarMaximoFolio(nuevoRegistro.getIdPaciente().getIdPaciente()) + 1);
            if (idfichamedica != null) {
                if (tipoRegistroClinicoActual.getIdTipoReg() == 6) {
                    nuevoRegistro.setIdfichamedica(idfichamedica);
                    idfichamedica = null;
                }
            }

            registroFacade.create(nuevoRegistro);

            if (validarNoVacio(turnoCita)) {
                List<CtCitas> listaCitas = (List<CtCitas>) turnosFacade.find(Integer.parseInt(turnoCita)).getCtCitasList();
                CtCitas citaAtendida = null;
                for (CtCitas cita : listaCitas) {
                    if (cita.getCancelada() == false) {
                        nuevoRegistro.setIdCita(cita.getIdCita());
                        citaAtendida = cita;
                        break;
                    }
                }
                if (citaAtendida != null) {
                    citaAtendida.setAtendida(true);
                    citasFacade.edit(citaAtendida);
                }
            }
            if (tipoRegistroClinicoActual.getCantCampos() != null) {
                System.out.println(tipoRegistroClinicoActual.getCantCampos());
                for (int i = 0; i < tipoRegistroClinicoActual.getCantCampos(); i++) {
                    if (datosFichamedica.getValor(i) != null && datosFichamedica.getValor(i).toString().length() != 0) {
                        campoResgistro = camposRegFacade.buscarPorTipoRegistroYPosicion(tipoRegistroClinicoActual.getIdTipoReg(), i);
                        System.out.println("CAMPO: " + campoResgistro);
                        if (campoResgistro != null) {
                            nuevoDetalle = new HcDetalle(nuevoRegistro.getIdRegistro(), campoResgistro.getIdCampo());
                            if (campoResgistro.getTabla() == null || campoResgistro.getTabla().length() == 0) {
                                nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
                            } else {
                                switch (campoResgistro.getTabla()) {                                    
                                    case "date":
                                        try {
                                            Date f = formatoLongDate.parse(datosFichamedica.getValor(i).toString());
                                            nuevoDetalle.setValor(formatoDate.format(f));
                                        } catch (ParseException ex) {
                                            nuevoDetalle.setValor("Error: " + datosFichamedica.getValor(i).toString());
                                        }
                                        break;
                                    
                                    case "date2":
                                        try {
                                            Date f = formatoLongDate.parse(datosFichamedica.getValor(i).toString());
                                            if (tipoRegistroClinicoActual.getIdTipoReg() == 55) {
                                                Calendar calendar = Calendar.getInstance();
                                                calendar.setTime(f);
                                                calendar.add(Calendar.DAY_OF_YEAR, 1);
                                                nuevoDetalle.setValor(formatoDate.format(calendar.getTime()));
                                            } else {
                                                nuevoDetalle.setValor(formatoDate.format(f));
                                            }

                                        } catch (ParseException ex) {
                                            nuevoDetalle.setValor("Error: " + datosFichamedica.getValor(i).toString());
                                        }
                                        break;
                                    default:
                                        nuevoDetalle.setValor(datosFichamedica.getValor(i).toString());
                                        break;
                                }
                            }

                            listaDetalle.add(nuevoDetalle);
                            detalleFacade.create(nuevoDetalle);
                        } else {
                            System.out.println("No encontro en tabla hc_campos_registro el valor: id_tipo_reg=" + tipoRegistroClinicoActual.getIdTipoReg() + " posicion " + i);
                        }
                    }
                }
            }
            transaction.commit();

            if (tipoRegistroClinicoActual.getIdTipoReg() == 93) {
                if(indexController.getUsuarioActual().getIdPerfil().getIdPerfil() == 2){
                    idfichamedica = nuevoRegistro.getIdRegistro();
                    tipoHistoriaClinica = "91";
                    tipoRegistroClinicoActual = tipoRegCliFacade.find(Integer.parseInt(tipoHistoriaClinica));
                    urlPagina = tipoRegistroClinicoActual.getUrlPagina();
                    nombreTipoHistoriaClinica = tipoRegistroClinicoActual.getNombre();
                }else{
                    idfichamedica = nuevoRegistro.getIdRegistro();
                    tipoHistoriaClinica = "6";
                    tipoRegistroClinicoActual = tipoRegCliFacade.find(Integer.parseInt(tipoHistoriaClinica));
                    urlPagina = tipoRegistroClinicoActual.getUrlPagina();
                    nombreTipoHistoriaClinica = tipoRegistroClinicoActual.getNombre();
                }               
               
            } else if (tipoRegistroClinicoActual.getIdTipoReg() == 6) {
                idfichamedica = nuevoRegistro.getIdRegistro();
                tipoHistoriaClinica = "94";
                tipoRegistroClinicoActual = tipoRegCliFacade.find(Integer.parseInt(tipoHistoriaClinica));
                urlPagina = tipoRegistroClinicoActual.getUrlPagina();
                nombreTipoHistoriaClinica = tipoRegistroClinicoActual.getNombre();
            } else if (tipoRegistroClinicoActual.getIdTipoReg() == 94) {
                idfichamedica = nuevoRegistro.getIdRegistro();
                turnoCita = "";
                tipoHistoriaClinica = "";
            } else {
                turnoCita = "";
                tipoHistoriaClinica = "";
            }

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "Nuevo registro almacenado");

            PrimeFaces.current().dialog().showMessageDynamic(message);
            limpiarFormulario();
            valoresPorDefecto();

            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
            RequestContext.getCurrentInstance().update("IdFormHistorias");

            if (tipoRegistroClinicoActual.getIdTipoReg() == 6 || tipoRegistroClinicoActual.getIdTipoReg() == 94) {
                RequestContext.getCurrentInstance().execute("PF('printButton').jq.click();");

            }
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(HistoriaClinicaController.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "No se pudo almacenar registro");
        }
    }
    
    public void renderReceta(){
        tipoHistoriaClinica = "6";
        tipoRegistroClinicoActual = tipoRegCliFacade.find(Integer.parseInt(tipoHistoriaClinica));
        urlPagina = tipoRegistroClinicoActual.getUrlPagina();
        nombreTipoHistoriaClinica = tipoRegistroClinicoActual.getNombre();
        RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
        RequestContext.getCurrentInstance().update("IdFormHistorias");
    }

    public void validarIdentificacion() {
        pacienteTemporal = pacientesFacade.buscarPorIdentificacion(identificacionPaciente);

        if (pacienteTemporal != null) {
            tipoHistoriaClinica = "";
            urlPagina = "";
            nombreTipoHistoriaClinica = "";
            pacienteSeleccionadoTabla = pacienteTemporal;
            hayPacienteSeleccionado = true;
            cargarPaciente();

        } else {
            RequestContext.getCurrentInstance().execute("PF('dlgSeleccionarPaciente').show();");
        }
    }

    public void editarPaciente() {
        if (pacienteSeleccionado != null) {
            RequestContext.getCurrentInstance().execute("window.parent.cargarPaciente('Pacientes','configuraciones/pacientes.xhtml','" + pacienteSeleccionado.getIdPaciente() + "')");
        } else {
            hayPacienteSeleccionado = false;
            imprimirMensaje("Error", "Se debe seleccionar un paciente de la tabla", FacesMessage.SEVERITY_ERROR);
        }
    }

    public void cargarPaciente() {
        if (pacienteSeleccionadoTabla != null) {

            turnoCita = "";
            pacienteSeleccionado = pacientesFacade.find(pacienteSeleccionadoTabla.getIdPaciente());
            listaArchivo = hcArchivosFacade.getHcArchivosByPaciente(pacienteSeleccionado);
            urlPagina = "";
            identificacionPaciente = "";
            nombrePaciente = "Paciente";
            nombreTipoHistoriaClinica = "";
            hayPacienteSeleccionado = true;
            identificacionPaciente = pacienteSeleccionado.getIdentificacion();
            if (pacienteSeleccionado.getTipoIdentificacion() != null) {
                tipoIdentificacionPaciente = pacienteSeleccionado.getTipoIdentificacion().getObservacion();
            } else {
                tipoIdentificacionPaciente = "";
            }
            nombrePaciente = pacienteSeleccionado.nombreCompleto();

            if (pacienteSeleccionado.getSexo() != null) {
                generoPaciente = pacienteSeleccionado.getSexo().getObservacion();
            } else {
                generoPaciente = "";
            }
            if (pacienteSeleccionado.getFechaNacimiento() != null) {
                edadPaciente = calcularEdad(pacienteSeleccionado.getFechaNacimiento());
            } else {
                edadPaciente = "";
            }

            tipoHistoriaClinica = "";
            cambiaTipoRegistroClinico();
            if (!cargandoDesdeTab) {
                RequestContext.getCurrentInstance().execute("PF('dlgSeleccionarPaciente').hide();");
            }

            try {
            } catch (Exception e) {
            }

        } else {
            hayPacienteSeleccionado = false;
            imprimirMensaje("Error", "Se debe seleccionar un paciente de la tabla", FacesMessage.SEVERITY_ERROR);
        }
    }

    public void cargarTerapias() {
        listaAgentesFisicos = maestrosClasificacionesFacade.buscarPorMaestro("Agente Fisico");
        listaEjerciciosTerapeuticos = maestrosClasificacionesFacade.buscarPorMaestro("Ejercicio Terapeutico");
    }
   
    public void creaTerapiaResp() {
        try {
            //guardar un nuevo registro clinico
            tipoHistoriaClinica = "96";
            tipoRegistroClinicoActual.setIdTipoReg(Integer.parseInt(tipoHistoriaClinica));
            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");

            transaction.begin();

            HcRegistro registro = new HcRegistro();
            HcSesionesTr nuevoRegistro = new HcSesionesTr();
            List<CnClasificaciones> listaClasificaciones = new ArrayList<>();
            HcDetalleTr nuevoDetalle;
            HcCamposReg campoResgistro;

            int idReg = sesionesTrFacade.siguienteid();

            Map<String, Float> valsTr = new HashMap<>();

            if (ssml != 0) {
                valsTr.put("TR001", ssml);
            }
            if (ssg != 0) {
                valsTr.put("TR002", ssg);
            }
            if (cbml != 0) {
                valsTr.put("TR003", cbml);
            }
            if (cbg != 0) {
                valsTr.put("TR004", cbg);
            }
            if (dxml != 0) {
                valsTr.put("TR005", dxml);
            }
            if (dxg != 0) {
                valsTr.put("TR006", dxg);
            }
            if (vnml != 0) {
                valsTr.put("TR007", vnml);
            }
            if (vng != 0) {
                valsTr.put("TR008", vng);
            }
            if (flml != 0) {
                valsTr.put("TR009", flml);
            }
            if (flg != 0) {
                valsTr.put("TR010", flg);
            }
            if (abml != 0) {
                valsTr.put("TR011", abml);
            }
            if (abg != 0) {
                valsTr.put("TR012", abg);
            }
            
            if (cantidadTr <= 0){
                imprimirMensaje("Error", "Debe ingresa cantidad de terapias a realizar", FacesMessage.SEVERITY_ERROR);
                return;
            }
            
            registro.setIdRegistro(registroFacade.siguienteid());
            idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
            if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                registro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
            } else {
                imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                return;
            }
            registro.setIdTipoReg(tipoRegistroClinicoActual);
            registro.setFechaReg(fechaRegistro);
            registro.setFechaSis(fechaSistema);
            registro.setIdPaciente(pacienteSeleccionado);
            registro.setFolio(registroFacade.buscarMaximoFolio(registro.getIdPaciente().getIdPaciente()) + 1);
            if (validarNoVacio(turnoCita)) {
                List<CtCitas> listaCitas = (List<CtCitas>) turnosFacade.find(Integer.parseInt(turnoCita)).getCtCitasList();
                for (CtCitas cita : listaCitas) {
                    if (cita.getCancelada() == false) {
                        registro.setIdCita(cita.getIdCita());
                        break;
                    }
                }
            }
            registroFacade.create(registro);
            
            for (int i = 1; i <= cantidadTr; i++) {
                nuevoRegistro = new HcSesionesTr();
                nuevoRegistro.setIdRegistro(registro.getIdRegistro());
                nuevoRegistro.setNumSesion(i);
                idMedico = indexController.getUsuarioActual().getIdUsuario().toString();
                if (idMedico != null && idMedico.length() != 0) {//validacion de campos obligatorios
                    nuevoRegistro.setIdMedico(usuariosFacade.find(Integer.parseInt(idMedico)));
                } else {
                    imprimirMensaje("Error", "Debe seleccionar un médico", FacesMessage.SEVERITY_ERROR);
                    return;
                }
                if (tipoRegistroClinicoActual != null) {
                    nuevoRegistro.setIdTipoReg(tipoRegistroClinicoActual);
                } else {
                    imprimirMensaje("Error", "No se puede determinar el tipo de registro clinico", FacesMessage.SEVERITY_ERROR);
                    return;
                }
                if (fechaRegistro == null) {
                    fechaRegistro = fechaSistema;
                }

                nuevoRegistro.setFechaReg(fechaRegistro);
                nuevoRegistro.setIdPaciente(pacienteSeleccionado);
                nuevoRegistro.setPagado(false);
                nuevoRegistro.setRealizado(false);
                nuevoRegistro.setRespirador(respirador);

                sesionesTrFacade.create(nuevoRegistro);

            }

            for (Map.Entry<String, Float> val : valsTr.entrySet()) {
                nuevoDetalle = new HcDetalleTr();
                nuevoDetalle.setIdCampo(val.getKey());
                nuevoDetalle.setValor(val.getValue());
                nuevoDetalle.setIdRegistro(nuevoRegistro.getIdRegistro());
                detalleTrFacade.create(nuevoDetalle);

            }

            transaction.commit();

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "Nuevo registro almacenado");

            PrimeFaces.current().dialog().showMessageDynamic(message);
            limpiarTr();

            RequestContext.getCurrentInstance().update("IdFormRegistroClinico");
            RequestContext.getCurrentInstance().update("IdFormHistorias");

            if (tipoRegistroClinicoActual.getIdTipoReg() == 6 || tipoRegistroClinicoActual.getIdTipoReg() == 94) {
                RequestContext.getCurrentInstance().execute("PF('printButton').jq.click();");

            }
        } catch (NamingException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(HistoriaClinicaController.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "No se pudo almacenar registro");
        }
    }
    
    public List<CnPacientes> getListPacientes() {
        return listPacientes;
    }

    public void setListPacientes(List<CnPacientes> listPacientes) {
        this.listPacientes = listPacientes;
    }

    public List<CnPacientes> getListPacientesFiltro() {
        return listPacientesFiltro;
    }

    public void setListPacientesFiltro(List<CnPacientes> listPacientesFiltro) {
        this.listPacientesFiltro = listPacientesFiltro;
    }

    public CnPacientes getPacienteSeleccionado() {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(CnPacientes pacienteSeleccionado) {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public String getIdentificacionPaciente() {
        return identificacionPaciente;
    }

    public void setIdentificacionPaciente(String identificacionPaciente) {
        this.identificacionPaciente = identificacionPaciente;
    }

    public String getUrlPagina() {
        return urlPagina;
    }

    public void setUrlPagina(String urlPagina) {
        this.urlPagina = urlPagina;
    }

    public String getTipoRegistroClinico() {
        return tipoHistoriaClinica;
    }

    public void setTipoRegistroClinico(String tipoHistoriaClinica) {
        this.tipoHistoriaClinica = tipoHistoriaClinica;
    }

    public boolean isHayPacienteSeleccionado() {
        return hayPacienteSeleccionado;
    }

    public void setHayPacienteSeleccionado(boolean hayPacienteSeleccionado) {
        this.hayPacienteSeleccionado = hayPacienteSeleccionado;
    }

  

    public String getTipoIdentificacion() {
        return tipoIdentificacionPaciente;
    }

    public void setTipoIdentificacion(String tipoIdentificacionPaciente) {
        this.tipoIdentificacionPaciente = tipoIdentificacionPaciente;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getSexoPaciente() {
        return generoPaciente;
    }

    public void setGeneroPaciente(String generoPaciente) {
        this.generoPaciente = generoPaciente;
    }

    public String getEdadPaciente() {
        return edadPaciente;
    }

    public void setEdadPaciente(String edadPaciente) {
        this.edadPaciente = edadPaciente;
    }

    public String geturlFile() {
        return urlFile;
    }

    public void seturlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public String getNombreTipoRegistroClinico() {
        return nombreTipoHistoriaClinica;
    }

    public void setNombreTipoRegistroClinico(String nombreTipoHistoriaClinica) {
        this.nombreTipoHistoriaClinica = nombreTipoHistoriaClinica;
    }

    public String getDetalleTextoPredef() {
        return detalleTextoPredef;
    }

    public void setDetalleTextoPredef(String detalleTextoPredef) {
        this.detalleTextoPredef = detalleTextoPredef;
    }

    public String getIdTextoPredef() {
        return idTextoPredef;
    }

    public void setIdTextoPredef(String idTextoPredef) {
        this.idTextoPredef = idTextoPredef;
    }

    public String getIdMaestroTextoPredef() {
        return idMaestroTextoPredef;
    }

    public void setIdMaestroTextoPredef(String idMaestroTextoPredef) {
        this.idMaestroTextoPredef = idMaestroTextoPredef;
    }

    public String getNombreTextoPredefinido() {
        return nombreTextoPredefinido;
    }

    public void setNombreTextoPredefinido(String nombreTextoPredefinido) {
        this.nombreTextoPredefinido = nombreTextoPredefinido;
    }

    public DatosHistoriaClinica getDatosFormulario() {
        return datosFichamedica;
    }

    public boolean ValidarDato(Object _dato, String _dato2) {
        return _dato.toString().compareTo(_dato2) == 0;
    }

    public void setDatosFormulario(DatosHistoriaClinica datosFichamedica) {
        this.datosFichamedica = datosFichamedica;
    }

    public String getIdEditorSeleccionado() {
        return idEditorSeleccionado;
    }

    public void setIdEditorSeleccionado(String idEditorSeleccionado) {
        this.idEditorSeleccionado = idEditorSeleccionado;
    }

    public String getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(String idMedico) {
        this.idMedico = idMedico;
    }

    public String getEspecialidadMedico() {
        return especialidadMedico;
    }

    public void setEspecialidadMedico(String especialidadMedico) {
        this.especialidadMedico = especialidadMedico;
    }

    public Date getFechaReg() {
        return fechaRegistro;
    }

    public void setFechaReg(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaSis() {
        return fechaSistema;
    }

    public void setFechaSis(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public String[] getRegCliSelHistorial() {
        return registrosHistoriaClinica;
    }

    public void setRegCliSelHistorial(String[] registrosHistoriaClinica) {
        this.registrosHistoriaClinica = registrosHistoriaClinica;
    }

    public TreeNode getTreeNodeRaiz() {
        return treeNodeRaiz;
    }

    public void setTreeNodeRaiz(TreeNode treeNodeRaiz) {
        this.treeNodeRaiz = treeNodeRaiz;
    }

    public TreeNode[] getTreeNodesSeleccionados() {
        return treeSeleccionado;
    }

    public void setTreeNodesSeleccionados(TreeNode[] treeSeleccionado) {
        this.treeSeleccionado = treeSeleccionado;
    }

    public boolean isModificandoRegCli() {
        return modificandoHistoriaClinica;
    }

    public void setModificandoRegCli(boolean modificandoHistoriaClinica) {
        this.modificandoHistoriaClinica = modificandoHistoriaClinica;
    }

    public IndexController getIndexController() {
        return indexController;
    }

    public void setIndexController(IndexController indexController) {
        this.indexController = indexController;
    }

    public boolean isBtnHistorialDisabled() {
        return btnHistorialDisabled;
    }

    public void setBtnHistorialDisabled(boolean btnHistorialDisabled) {
        this.btnHistorialDisabled = btnHistorialDisabled;
    }

    public CnPacientes getPacienteSeleccionadoTabla() {
        return pacienteSeleccionadoTabla;
    }

    public void setPacienteSeleccionadoTabla(CnPacientes pacienteSeleccionadoTabla) {
        this.pacienteSeleccionadoTabla = pacienteSeleccionadoTabla;
    }

  

    public Date getFiltroFechaInicial() {
        return filtroFechaInicial;
    }

    public void setFiltroFechaInicial(Date filtroFechaInicial) {
        this.filtroFechaInicial = filtroFechaInicial;
    }

    public Date getFiltroFechaFinal() {
        return filtroFechaFinal;
    }

    public void setFiltroFechaFinal(Date filtroFechaFinal) {
        this.filtroFechaFinal = filtroFechaFinal;
    }

    public boolean isBtnEditarRendered() {
        return btnEditarRendered;
    }

    public void setBtnEditarRendered(boolean btnEditarRendered) {
        this.btnEditarRendered = btnEditarRendered;
    }

   

    public String getTurnoCita() {
        return turnoCita;
    }

    public void setTurnoCita(String turnoCita) {
        this.turnoCita = turnoCita;
    }

  

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getIdcie10() {
        return idcie10;
    }

    public void setIdcie10(String idcie10) {
        this.idcie10 = idcie10;
    }


    public List<HcArchivos> getListaArchivo() {
        return listaArchivo;
    }

    public HcArchivos getArchivoSeleccionado() {
        return archivoSeleccionado;
    }

    public void setArchivoSeleccionado(HcArchivos archivoSeleccionado) {
        this.archivoSeleccionado = archivoSeleccionado;
    }

    public StreamedContent getFileDownload() {
        return fileDownload;
    }

    public String getDescriparchivo() {
        return descriparchivo;
    }

    public void setDescriparchivo(String descriparchivo) {
        this.descriparchivo = descriparchivo;
    }


    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }


    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public HcRegistroFacade getRegistroFacade() {
        return registroFacade;
    }

    public void setRegistroFacade(HcRegistroFacade registroFacade) {
        this.registroFacade = registroFacade;
    }

    public CtTurnosFacade getTurnosFacade() {
        return turnosFacade;
    }

    public void setTurnosFacade(CtTurnosFacade turnosFacade) {
        this.turnosFacade = turnosFacade;
    }

    public CtCitasFacade getCitasFacade() {
        return citasFacade;
    }

    public void setCitasFacade(CtCitasFacade citasFacade) {
        this.citasFacade = citasFacade;
    }

    public HcTipoRegFacade getTipoRegCliFacade() {
        return tipoRegCliFacade;
    }

    public void setTipoRegCliFacade(HcTipoRegFacade tipoRegCliFacade) {
        this.tipoRegCliFacade = tipoRegCliFacade;
    }

    public HcCamposRegFacade getCamposRegFacade() {
        return camposRegFacade;
    }

    public void setCamposRegFacade(HcCamposRegFacade camposRegFacade) {
        this.camposRegFacade = camposRegFacade;
    }

    public CnPacientesFacade getPacientesFacade() {
        return pacientesFacade;
    }

    public void setPacientesFacade(CnPacientesFacade pacientesFacade) {
        this.pacientesFacade = pacientesFacade;
    }

    public CnClasificacionesFacade getMaestrosClasificacionesFacade() {
        return maestrosClasificacionesFacade;
    }

    public void setMaestrosClasificacionesFacade(CnClasificacionesFacade maestrosClasificacionesFacade) {
        this.maestrosClasificacionesFacade = maestrosClasificacionesFacade;
    }

    public CnUsuariosFacade getUsuariosFacade() {
        return usuariosFacade;
    }

    public void setUsuariosFacade(CnUsuariosFacade usuariosFacade) {
        this.usuariosFacade = usuariosFacade;
    }

    public CnClasificacionesFacade getClasificacionesFacade() {
        return clasificacionesFacade;
    }

    public void setClasificacionesFacade(CnClasificacionesFacade clasificacionesFacade) {
        this.clasificacionesFacade = clasificacionesFacade;
    }

    public CnEmpresaFacade getEmpresaFacade() {
        return empresaFacade;
    }

    public void setEmpresaFacade(CnEmpresaFacade empresaFacade) {
        this.empresaFacade = empresaFacade;
    }

    public HcArchivosFacade getHcArchivosFacade() {
        return hcArchivosFacade;
    }

    public void setHcArchivosFacade(HcArchivosFacade hcArchivosFacade) {
        this.hcArchivosFacade = hcArchivosFacade;
    }

    public HcTipoReg getTipoRegistroClinicoActual() {
        return tipoRegistroClinicoActual;
    }

    public void setTipoRegistroClinicoActual(HcTipoReg tipoRegistroClinicoActual) {
        this.tipoRegistroClinicoActual = tipoRegistroClinicoActual;
    }

    public CnPacientes getPacienteTmp() {
        return pacienteTemporal;
    }

    public void setPacienteTmp(CnPacientes pacienteTemporal) {
        this.pacienteTemporal = pacienteTemporal;
    }

    public CnClasificaciones getClasificacionBuscada() {
        return clasificacionBuscada;
    }

    public void setClasificacionBuscada(CnClasificaciones clasificacionBuscada) {
        this.clasificacionBuscada = clasificacionBuscada;
    }

    public UploadedFile getArchivos() {
        return archivos;
    }

    public void setArchivos(UploadedFile archivos) {
        this.archivos = archivos;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public int getPosListaTxtPredef() {
        return posicionTxtPredefinidos;
    }

    public void setPosListaTxtPredef(int posicionTxtPredefinidos) {
        this.posicionTxtPredefinidos = posicionTxtPredefinidos;
    }

    public List<HcTipoReg> getListaTipoRegistroClinico() {
        return listaTipoRegistroClinico;
    }

    public void setListaTipoRegistroClinico(List<HcTipoReg> listaTipoRegistroClinico) {
        this.listaTipoRegistroClinico = listaTipoRegistroClinico;
    }

    public List<HcTipoReg> getListaTipoRegistroClinicoAll() {
        return listaTipoRegistroClinicoAll;
    }

    public void setListaTipoRegistroClinicoAll(List<HcTipoReg> listaTipoRegistroClinicoAll) {
        this.listaTipoRegistroClinicoAll = listaTipoRegistroClinicoAll;
    }

    public HcRegistro getRegistroEncontrado() {
        return registroEncontrado;
    }

    public void setRegistroEncontrado(HcRegistro registroEncontrado) {
        this.registroEncontrado = registroEncontrado;
    }

    public List<DatosHistoriaClinica> getListaRegistrosParaPdf() {
        return listaHistoriaReporte;
    }

    public void setListaRegistrosParaPdf(List<DatosHistoriaClinica> listaHistoriaReporte) {
        this.listaHistoriaReporte = listaHistoriaReporte;
    }

    public CnEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(CnEmpresa empresa) {
        this.empresa = empresa;
    }

    public boolean isCargandoDesdeTab() {
        return cargandoDesdeTab;
    }

    public void setCargandoDesdeTab(boolean cargandoDesdeTab) {
        this.cargandoDesdeTab = cargandoDesdeTab;
    }

    public CtCitas getCitaActual() {
        return citaActual;
    }

    public void setCitaActual(CtCitas citaActual) {
        this.citaActual = citaActual;
    }


    public int getEdadAnios() {
        edadAnios = this.calcularEdadInt(this.getPacienteSeleccionado().getFechaNacimiento());
        return edadAnios;
    }

    public void setEdadAnios(int edadAnios) {
        this.edadAnios = edadAnios;
    }

    public String getValorDefecto1() {
        return valorDefecto1;
    }

    public void setValorDefecto1(String valorDefecto1) {
        this.valorDefecto1 = valorDefecto1;
    }

    public String getValorDefecto2() {
        return valorDefecto2;
    }

    public void setValorDefecto2(String valorDefecto2) {
        this.valorDefecto2 = valorDefecto2;
    }

    public String getValorDefecto3() {
        return valorDefecto3;
    }

    public void setValorDefecto3(String valorDefecto3) {
        this.valorDefecto3 = valorDefecto3;
    }

    public List<SelectItem> getListaValoresDefecto() {
        return listaValoresDefecto;
    }

    public void setListaValoresDefecto(List<SelectItem> listaValoresDefecto) {
        this.listaValoresDefecto = listaValoresDefecto;
    }

    public List<CnHistoriaCamposPredefinidos> getListaTextos() {
        return listaTextos;
    }

    public void setListaTextos(List<CnHistoriaCamposPredefinidos> listaTextos) {
        this.listaTextos = listaTextos;
    }

    public HcCamposReg getCampoSeleccionado() {
        return campoSeleccionado;
    }

    public void setCampoSeleccionado(HcCamposReg campoSeleccionado) {
        this.campoSeleccionado = campoSeleccionado;
    }

    public CnHistoriaCamposPredefinidos getTextoSeleccionado() {
        return textoSeleccionado;
    }

    public void setTextoSeleccionado(CnHistoriaCamposPredefinidos textoSeleccionado) {
        this.textoSeleccionado = textoSeleccionado;
    }

    public String getNombreFormulario() {
        return nombreFormulario;
    }

    public void setNombreFormulario(String nombreFormulario) {
        this.nombreFormulario = nombreFormulario;
    }



    public int getTipomovimiento() {
        return tipomovimiento;
    }

    public void setTipomovimiento(int tipomovimiento) {
        this.tipomovimiento = tipomovimiento;
    }

    public String getNoTurno() {
        return noTurno;
    }

    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

    public boolean isEstaSubido() {
        return estaSubido;
    }

    public void setEstaSubido(boolean estaSubido) {
        this.estaSubido = estaSubido;
    }

    public List<CnClasificaciones> getListaEjerciciosTerapeuticos() {
        return listaEjerciciosTerapeuticos;
    }

    public void setListaEjerciciosTerapeuticos(List<CnClasificaciones> listaEjerciciosTerapeuticos) {
        this.listaEjerciciosTerapeuticos = listaEjerciciosTerapeuticos;
    }
   
    public List<CnClasificaciones> getEjerciciosTerapeuticosSeleccionados() {
        return EjerciciosTerapeuticosSeleccionados;
    }

    public void setEjerciciosTerapeuticosSeleccionados(List<CnClasificaciones> EjerciciosTerapeuticosSeleccionados) {
        this.EjerciciosTerapeuticosSeleccionados = EjerciciosTerapeuticosSeleccionados;
    }

    public StreamedContent getOrdenFt() {
        return ordenFt;
    }

    public void setOrdenFt(StreamedContent ordenFt) {
        this.ordenFt = ordenFt;
    }

    public int getCantSesiones() {
        return cantSesiones;
    }

    public void setCantSesiones(int cantSesiones) {
        this.cantSesiones = cantSesiones;
    }

    public boolean isRespirador() {
        return respirador;
    }

    public void setRespirador(boolean respirador) {
        this.respirador = respirador;
    }

    public short getCantidadTr() {
        return cantidadTr;
    }

    public void setCantidadTr(short cantidadTr) {
        this.cantidadTr = cantidadTr;
    }
    
    public float getSsml() {
        return ssml;
}

    public void setSsml(float ssml) {
        this.ssml = ssml;
    }

    public float getSsg() {
        return ssg;
    }

    public void setSsg(float ssg) {
        this.ssg = ssg;
    }

    public float getDxg() {
        return dxg;
    }

    public void setDxg(float dxg) {
        this.dxg = dxg;
    }

    public float getCbml() {
        return cbml;
    }

    public void setCbml(float cbml) {
        this.cbml = cbml;
    }

    public float getCbg() {
        return cbg;
    }

    public void setCbg(float cbg) {
        this.cbg = cbg;
    }

    public float getVnml() {
        return vnml;
    }

    public void setVnml(float vnml) {
        this.vnml = vnml;
    }

    public float getVng() {
        return vng;
    }

    public void setVng(float vng) {
        this.vng = vng;
    }

    public float getFlml() {
        return flml;
    }

    public void setFlml(float flml) {
        this.flml = flml;
    }

    public float getFlg() {
        return flg;
    }

    public void setFlg(float flg) {
        this.flg = flg;
    }

    public float getAbml() {
        return abml;
    }

    public void setAbml(float abml) {
        this.abml = abml;
    }

    public float getAbg() {
        return abg;
    }

    public void setAbg(float abg) {
        this.abg = abg;
    }

    public float getDxml() {
        return dxml;
    }

    public void setDxml(float dxml) {
        this.dxml = dxml;
    }

    public boolean isAf1() {
        return af1;
    }

    public void setAf1(boolean af1) {
        this.af1 = af1;
    }

    public boolean isAf2() {
        return af2;
    }

    public void setAf2(boolean af2) {
        this.af2 = af2;
    }

    public boolean isAf3() {
        return af3;
    }

    public void setAf3(boolean af3) {
        this.af3 = af3;
    }

    public boolean isAf4() {
        return af4;
    }

    public void setAf4(boolean af4) {
        this.af4 = af4;
    }

    public boolean isAf5() {
        return af5;
    }

    public void setAf5(boolean af5) {
        this.af5 = af5;
    }

    public boolean isAf6() {
        return af6;
    }

    public void setAf6(boolean af6) {
        this.af6 = af6;
    }

    public boolean isAf7() {
        return af7;
    }

    public void setAf7(boolean af7) {
        this.af7 = af7;
    }

    public boolean isAf8() {
        return af8;
    }

    public void setAf8(boolean af8) {
        this.af8 = af8;
    }

    public boolean isAf9() {
        return af9;
    }

    public void setAf9(boolean af9) {
        this.af9 = af9;
    }

    public boolean isAf10() {
        return af10;
    }

    public void setAf10(boolean af10) {
        this.af10 = af10;
    }

    public boolean isAf11() {
        return af11;
    }

    public void setAf11(boolean af11) {
        this.af11 = af11;
    }

    public boolean isEt1() {
        return et1;
    }

    public void setEt1(boolean et1) {
        this.et1 = et1;
    }

    public boolean isEt2() {
        return et2;
    }

    public void setEt2(boolean et2) {
        this.et2 = et2;
    }

    public boolean isEt3() {
        return et3;
    }

    public void setEt3(boolean et3) {
        this.et3 = et3;
    }

    public boolean isEt4() {
        return et4;
    }

    public void setEt4(boolean et4) {
        this.et4 = et4;
    }

    public boolean isEt5() {
        return et5;
    }

    public void setEt5(boolean et5) {
        this.et5 = et5;
    }

    public boolean isEt6() {
        return et6;
    }

    public void setEt6(boolean et6) {
        this.et6 = et6;
    }

    public boolean isEt7() {
        return et7;
    }

    public void setEt7(boolean et7) {
        this.et7 = et7;
    }

    public boolean isEt8() {
        return et8;
    }

    public void setEt8(boolean et8) {
        this.et8 = et8;
    }

    public boolean isEt9() {
        return et9;
    }

    public void setEt9(boolean et9) {
        this.et9 = et9;
    }

    public boolean isEt10() {
        return et10;
    }

    public void setEt10(boolean et10) {
        this.et10 = et10;
    }

    public boolean isEt11() {
        return et11;
    }

    public void setEt11(boolean et11) {
        this.et11 = et11;
    }

    public boolean isEt12() {
        return et12;
    }

    public void setEt12(boolean et12) {
        this.et12 = et12;
    }

    public boolean isEt13() {
        return et13;
    }

    public void setEt13(boolean et13) {
        this.et13 = et13;
    }

    public boolean isEt14() {
        return et14;
    }

    public void setEt14(boolean et14) {
        this.et14 = et14;
    }

    public boolean isEt15() {
        return et15;
    }

    public void setEt15(boolean et15) {
        this.et15 = et15;
    }

    public boolean isEt16() {
        return et16;
    }

    public void setEt16(boolean et16) {
        this.et16 = et16;
    }

    public boolean isEt17() {
        return et17;
    }

    public void setEt17(boolean et17) {
        this.et17 = et17;
    }

    public boolean isEt18() {
        return et18;
    }

    public void setEt18(boolean et18) {
        this.et18 = et18;
    }

    public boolean isEt19() {
        return et19;
    }

    public void setEt19(boolean et19) {
        this.et19 = et19;
    }

    public boolean isEt20() {
        return et20;
    }

    public void setEt20(boolean et20) {
        this.et20 = et20;
    }

    public boolean isEt21() {
        return et21;
    }

    public void setEt21(boolean et21) {
        this.et21 = et21;
    }

    public boolean isEt22() {
        return et22;
    }

    public void setEt22(boolean et22) {
        this.et22 = et22;
    }

    public String getObsTf() {
        return obsTf;
    }

    public void setObsTf(String obsTf) {
        this.obsTf = obsTf;
    }

    public List<HcSesionesTr> getListaTrNoPagos() {
        return listaTrNoPagos;
    }

    public void setListaTrNoPagos(List<HcSesionesTr> listaTrNoPagos) {
        this.listaTrNoPagos = listaTrNoPagos;
    }

    public HcSesionesTr getPagoSeleccionado() {
        return pagoSeleccionado;
    }

    public void setPagoSeleccionado(HcSesionesTr pagoSeleccionado) {
        this.pagoSeleccionado = pagoSeleccionado;
    }

    public List<HcSesionesTr> getListaTrAtencion() {
        return listaTrAtencion;
    }

    public void setListaTrAtencion(List<HcSesionesTr> listaTrAtencion) {
        this.listaTrAtencion = listaTrAtencion;
    }

    public HcSesionesTr getAtencionSeleccionada() {
        return atencionSeleccionada;
    }

    public void setAtencionSeleccionada(HcSesionesTr atencionSeleccionada) {
        this.atencionSeleccionada = atencionSeleccionada;
    }

    public Map<String, Float> getMapValues() {
        return mapValues;
    }

    public void setMapValues(Map<String, Float> mapValues) {
        this.mapValues = mapValues;
    }

    public List<HcTfSesiones> getListaTfAtencion() {
        return listaTfAtencion;
    }

    public void setListaTfAtencion(List<HcTfSesiones> listaTfAtencion) {
        this.listaTfAtencion = listaTfAtencion;
    }

    public HcTfSesiones getAtencionTfSeleccionada() {
        return atencionTfSeleccionada;
    }

    public void setAtencionTfSeleccionada(HcTfSesiones atencionTfSeleccionada) {
        this.atencionTfSeleccionada = atencionTfSeleccionada;
    }

    public boolean isModificaAtencionTf() {
        return modificaAtencionTf;
    }

    public void setModificaAtencionTf(boolean modificaAtencionTf) {
        this.modificaAtencionTf = modificaAtencionTf;
    }

    public Map<Integer, String> getMapValuesTf() {
        return mapValuesTf;
    }

    public void setMapValuesTf(Map<Integer, String> mapValuesTf) {
        this.mapValuesTf = mapValuesTf;
    }

    public int getSesionSeleccionadaTf() {
        return sesionSeleccionadaTf;
    }

    public void setSesionSeleccionadaTf(int sesionSeleccionadaTf) {
        this.sesionSeleccionadaTf = sesionSeleccionadaTf;
    }

    public Set<Map.Entry<Integer, String>> getTratValTf() {
        return tratValTf;
    }

    public void setTratValTf(Set<Map.Entry<Integer, String>> tratValTf) {
        this.tratValTf = tratValTf;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }


}
