/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.facade;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.HcTfSesiones;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.HcTfTratamientos;

/**
 *
 * @author dante
 */
@Stateless
public class HcTfSesionesFacade extends AbstractFacade<HcTfSesiones> {

    public HcTfSesionesFacade() {
        super(HcTfSesiones.class);
    }

    public List<HcTfSesiones> buscarSesionesPendientes(int idPaciente) {
        try {
            String hql = "SELECT u FROM HcTfSesiones u WHERE u.idPaciente=" + idPaciente + " and u.atencion=false ORDER BY u.idSesion";
            Query query = getEntityManager().createQuery(hql);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public int getTramientoLastId(int idPaciente) {
        try {
            String hql = "SELECT max(u.idTratamiento) FROM hc_tr_sesiones u WHERE u.idPaciente=" + idPaciente + " ";
            Query query = getEntityManager().createQuery(hql);
            return (Integer) query.getSingleResult();
        } catch (Exception e) {
            return -1;
        }
    }

    public Integer siguienteid() {

        int numeracion = 0;
        Integer consulta;
        try {
            consulta = (Integer) getEntityManager().createNativeQuery("SELECT MAX(id_registro) FROM hc_tr_sesiones").getSingleResult();
            if (consulta == null) {
                numeracion = 1;
            } else {
                numeracion = consulta;
                numeracion = numeracion + 1;
            }

        } catch (Exception e) {
            throw e;
        }

        return numeracion;
    }
    
    public HcTfSesiones tratamientosById(int id){
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfSesiones t WHERE t.idTratamiento = ?1").setParameter(1, id);
            return (HcTfSesiones) query.getResultList().get(0);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
    public Long numSesiones(int id) {
        return (Long) getEntityManager().createNativeQuery("SELECT count(*) FROM hc_tf_sesiones where id_tratamiento = " + id + "").getSingleResult();
           
    }

    public List<HcTfSesiones> buscarRegistrosNoAtendidos(CnPacientes paciente) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfSesiones t WHERE t.realizado = false and t.idPaciente = ?1");            
            query.setParameter(1, paciente);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTfSesiones> buscarRegistrosNoAtendidosAll(int id, CnPacientes paciente) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfSesiones t WHERE t.idTratamiento = ?1 and t.idPaciente = ?2");
            query.setParameter(1, id);
            query.setParameter(2, paciente);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean tieneSesionesPendientes(CnPacientes paciente) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfSesiones t WHERE t.realizado = false and t.idPaciente = ?1");
            query.setParameter(1, paciente);
            if (query.getResultList().isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public long buscaIdTratamiento(CnPacientes paciente) {
        try {
            Query query = getEntityManager().createQuery("SELECT DISTINCT(t.idTratamiento) FROM HcTfSesiones t WHERE t.realizado = false and t.idPaciente = ?1");
            query.setParameter(1, paciente);
            return (Long) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }

    }

    public int finalizaSesion(Long id) {
        try {
            Query query = getEntityManager().createQuery("UPDATE HcTfSesiones t SET t.realizado = true WHERE t.id = ?1");
            query.setParameter(1, id);
            int resultado = query.executeUpdate();
            return resultado;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public List<HcTfSesiones> findCantidadSesiones(Date fechaInicial, Date fechaFinal, List<CnPacientes> pacientes, List<CnUsuarios> prestadores, int ban) {
        String consulta = "SELECT c FROM HcTfSesiones c WHERE c.realizado = true";
        if (ban == 1) {
            int aux = 1;
            if (fechaInicial != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                consulta = consulta.concat("  c.fechaSesion >= ?1");
                aux = 1;

            }
            if (fechaFinal != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat("  c.fechaSesion <= ?2");
            }
            if (pacientes.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idPaciente IN ?3");
            }
            if (prestadores.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idMedico IN ?4");
            }
            
        }
        consulta = consulta.concat(" ORDER BY c.fechaReg");
        try {
            Query query = getEntityManager().createQuery(consulta);
            if (ban == 1) {
                if (fechaInicial != null) {
                    query.setParameter(1, fechaInicial);
                }
                if (fechaFinal != null) {
                    query.setParameter(2, fechaFinal);
                }
                if (pacientes.size() > 0) {
                    query.setParameter(3, pacientes);
                }
                if (prestadores.size() > 0) {
                    query.setParameter(4, prestadores);
                }
               
            }
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
            }

}
