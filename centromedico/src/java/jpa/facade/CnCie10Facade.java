
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import jpa.modelo.CnCie10;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnCie10Facade extends AbstractFacade<CnCie10> {

    public CnCie10Facade() {
        super(CnCie10.class);
    }

    public CnCie10 getEnfermedadById(String codigo) {
        try {
            String hql = "SELECT c FROM CnCie10 c WHERE c.codigo = :codigo ";
            return (CnCie10) getEntityManager().createQuery(hql).setParameter("codigo", codigo).getResultList().get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<CnCie10> buscarOrdenado() {
        try {
            String hql = "SELECT m FROM CnCie10 m ORDER BY m.codigo ASC";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

   
}
