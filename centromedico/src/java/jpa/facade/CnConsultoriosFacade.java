
package jpa.facade;

import javax.ejb.Stateless;
import jpa.modelo.CnConsultorios;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnConsultoriosFacade extends AbstractFacade<CnConsultorios> {

    public CnConsultoriosFacade() {
        super(CnConsultorios.class);
    }
   
}
