
package jpa.facade;

import jpa.modelo.CnImagenes;
import javax.ejb.Stateless;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnImagenesFacade extends AbstractFacade<CnImagenes> {

    public CnImagenesFacade() {
        super(CnImagenes.class);
    }

}
