/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.facade;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.HcTfSesiones;
import jpa.modelo.HcTfTratamientos;

/**
 *
 * @author dante
 */
@Stateless
public class HcTfTratamientosFacade extends AbstractFacade<HcTfTratamientos>{

    public HcTfTratamientosFacade() {
        super(HcTfTratamientos.class);
    }
    
    public List<HcTfTratamientos> tratamientosById(int id){
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfTratamientos t WHERE t.idTratamiento = ?1").setParameter(1, id);
            return query.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public List<HcTfTratamientos> buscarTratamientosActivosById(long id){
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfTratamientos t WHERE t.idTratamiento = ?1 and t.activo = true").setParameter(1, id);
            return query.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
    public List<HcTfSesiones> buscarSesionesPendientes(int idPaciente) {
        try {
            String hql = "SELECT u FROM HcTfSesiones u WHERE u.idPaciente="+idPaciente+" and u.atencion=false ORDER BY u.idSesion";
            Query query = getEntityManager().createQuery(hql);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public int getTramientoLastId(int idPaciente) {
        try {
            String hql = "SELECT max(u.idTratamiento) FROM HcTfSesiones u WHERE u.idPaciente="+idPaciente+" ";
            Query query = getEntityManager().createQuery(hql);
            return (Integer)query.getSingleResult();
        } catch (Exception e) {
            return -1;
        }
    }
    
    public List<HcTfTratamientos> getTratamientosById(long idTratamiento){
        try{
        String hql = "SELECT u FROM HcTfTratamientos u WHERE u.idTratamiento = ?1";
        Query query = getEntityManager().createQuery(hql).setParameter(1, idTratamiento);
        return query.getResultList();
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    public Long siguienteid() {
       
        long numeracion = 0;
         Long consulta;
        try {
             consulta = (Long) getEntityManager().createNativeQuery("SELECT MAX(id_tratamiento) FROM hc_tf_tratamientos").getSingleResult();
             if(consulta == null){
                numeracion = 1;
            }else{
                 numeracion = consulta;
                numeracion = numeracion + 1;
            }
            
        } catch (Exception e) {
            throw e;
        }
        

        return numeracion;
    }
    
    
}
