
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import jpa.modelo.CnGruposprioritarios;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnGruposPrioritariosFacade extends AbstractFacade<CnGruposprioritarios> {

    public CnGruposPrioritariosFacade() {
        super(CnGruposprioritarios.class);
    }

           
    public List<CnGruposprioritarios> getGruposByIdPaciente(Integer codigo) {    
        try {
            String hql = "SELECT c FROM CnGruposprioritarios c WHERE c.cnGruposprioritariosPK.idpaciente = :idpaciente ";
            return (List<CnGruposprioritarios>) getEntityManager().createQuery(hql).setParameter("idpaciente", codigo).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public CnGruposprioritarios getGruposById(Integer grupo, Integer paciente) {    
        try {
            String hql = "SELECT c FROM CnGruposprioritarios c WHERE c.cnGruposprioritariosPK.idgruposprioritarios = :idgruposprioritarios and c.cnGruposprioritariosPK.idpaciente = :idpaciente ";
            return (CnGruposprioritarios) getEntityManager().createQuery(hql).setParameter("idgruposprioritarios", grupo).setParameter("idpaciente", paciente).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
   
}
