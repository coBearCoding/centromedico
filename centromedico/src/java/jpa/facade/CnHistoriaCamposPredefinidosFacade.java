
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CnHistoriaCamposPredefinidos;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnHistoriaCamposPredefinidosFacade  extends AbstractFacade<CnHistoriaCamposPredefinidos>{

    public CnHistoriaCamposPredefinidosFacade() {
        super(CnHistoriaCamposPredefinidos.class);
    }

    public List<CnHistoriaCamposPredefinidos> getCamposDefinidosXHistoriaClinica(int idHistoriaClinica){
        String hql = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.idCampo.idTipoReg.idTipoReg=:idTipoReg";
        Query query = getEntityManager().createQuery(hql).setParameter("idTipoReg", idHistoriaClinica);
        return query.getResultList();
    }
    public List<CnHistoriaCamposPredefinidos> getCamposDefinidosXHistoriaClinicaXCampo(int idHistoriaClinica, int idCampo){
        String hql = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.idCampo.idTipoReg.idTipoReg=:idTipoReg and c.idCampo.idCampo=:idCampo";
        Query query = getEntityManager().createQuery(hql).setParameter("idTipoReg", idHistoriaClinica).setParameter("idCampo",idCampo);
        return query.getResultList();
    }
    
    public List<CnHistoriaCamposPredefinidos> getCamposDefinidosXCampo(int idCampo){
        String hql = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.idCampo.idCampo=:idCampo";
        Query query = getEntityManager().createQuery(hql).setParameter("idCampo",idCampo);
        return query.getResultList();
    }
    
   
}
