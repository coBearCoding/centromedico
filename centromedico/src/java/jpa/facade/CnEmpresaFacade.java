
package jpa.facade;

import javax.ejb.Stateless;
import jpa.modelo.CnEmpresa;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnEmpresaFacade extends AbstractFacade<CnEmpresa> {

    public CnEmpresaFacade() {
        super(CnEmpresa.class);
    }
    
}
