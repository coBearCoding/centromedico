
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CnPacientes;
import jpa.modelo.HcArchivos;

/**
 *
 * @author kenlly
 */
@Stateless
public class HcArchivosFacade extends AbstractFacade<HcArchivos> {

    public HcArchivosFacade() {
        super(HcArchivos.class);
    }

    public List<HcArchivos> getHcArchivosByPaciente(CnPacientes paciente) {
        Query query = getEntityManager().createQuery("SELECT a FROM HcArchivos a WHERE a.idPaciente = ?1 ORDER BY a.fechaUpload DESC");
        query.setParameter(1, paciente);
        return query.getResultList();
    }
    
    public HcArchivos getHcArchivoFt(CnPacientes paciente) {
        Query query = getEntityManager().createQuery("SELECT a FROM HcArchivos a WHERE a.idArchivo = ?1 ORDER BY a.fechaUpload DESC");
        query.setParameter(1, paciente);
        return (HcArchivos)query.getSingleResult();
}

    public Long getIdArchivoByNombreArchivo(String nombreArchivo) {
        Query query = getEntityManager().createQuery("SELECT a.idArchivo FROM HcArchivos a WHERE a.urlFile = ?1");
        query.setParameter(1, nombreArchivo);
        return (Long) query.getSingleResult();
    }
   

}
