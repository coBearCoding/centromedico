
package jpa.facade;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CnDiasNoLaborales;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnDiasNoLaboralesFacade extends AbstractFacade<CnDiasNoLaborales> {

    public CnDiasNoLaboralesFacade() {
        super(CnDiasNoLaborales.class);
    }

    public List<CnDiasNoLaborales> diasNoLaborables() {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CnDiasNoLaborales c WHERE c.cnDiasNoLaboralesPK.fechaNoLaboral >= CURRENT_DATE ORDER BY c.cnDiasNoLaboralesPK.fechaNoLaboral");            
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public CnDiasNoLaborales FindDiaNoLaboral(Date fecha) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CnDiasNoLaborales c WHERE c.cnDiasNoLaboralesPK.fechaNoLaboral = ?1");            
            query.setParameter(1, fecha);
            return (CnDiasNoLaborales) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

  

}
