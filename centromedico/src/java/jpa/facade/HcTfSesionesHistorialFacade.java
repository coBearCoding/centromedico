
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.HcTfSesionesHistorial;

/**
 *
 * @author kenlly
 */
@Stateless
public class HcTfSesionesHistorialFacade extends AbstractFacade<HcTfSesionesHistorial> {

    public HcTfSesionesHistorialFacade() {
        super(HcTfSesionesHistorial.class);
    }
    
    public List<HcTfSesionesHistorial> buscarTratamientosActivosById(int id, int sesion){
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcTfSesionesHistorial t WHERE t.idTratamiento = ?1 and t.numSesion = ?2 ").setParameter(1, id).setParameter(2, sesion);
            return query.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
   
}
