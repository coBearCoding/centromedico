
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import jpa.modelo.HcCamposReg;

/**
 *
 * @author kenlly
 */
@Stateless
public class HcCamposRegFacade extends AbstractFacade<HcCamposReg> {

    public HcCamposRegFacade() {
        super(HcCamposReg.class);
    }

    public HcCamposReg buscarPorTipoRegistroYPosicion(int idTipoRegistro, int posicion) {
        try {
            String hql = "SELECT h FROM HcCamposReg h WHERE h.idTipoReg.idTipoReg = :idTipoReg AND h.posicion = :posicion";
            return (HcCamposReg) getEntityManager().createQuery(hql).setParameter("idTipoReg", idTipoRegistro).setParameter("posicion", posicion).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public List<HcCamposReg> buscarPorTipoRegistro(int idTipoRegistro) {
        try {
            String hql = "SELECT h FROM HcCamposReg h WHERE h.idTipoReg.idTipoReg = :idTipoReg";
            return getEntityManager().createQuery(hql).setParameter("idTipoReg", idTipoRegistro).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcCamposReg> buscarPorTipoRegistroPredefinido(int idTipoRegistro) {
        try {
            String hql = "SELECT h FROM HcCamposReg h WHERE h.idTipoReg.idTipoReg = :idTipoReg and h.predefinido = TRUE";
            return getEntityManager().createQuery(hql).setParameter("idTipoReg", idTipoRegistro).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

}
