
package jpa.facade;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.CtCitas;

/**
 *
 * @author kenlly
 */
@Stateless
public class CtCitasFacade extends AbstractFacade<CtCitas> {

    public CtCitasFacade() {
        super(CtCitas.class);
    }


    public CtCitas findCitasByTurno(int id_turno) {
        CtCitas cita;
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idTurno.idTurno = ?1 AND c.cancelada = false");
            query.setParameter(1, id_turno);
            cita = (CtCitas) query.getSingleResult();
        } catch (Exception e) {
            cita = null;
        }
        return cita;
    }

    public List<CtCitas> findCitasByMedicoVigentes(int idmedico, int offset, int limit) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.atendida = false AND c.atendida = false AND c.idTurno.fecha >= CURRENT_DATE ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
            query.setParameter(1, idmedico);
            query.setFirstResult(offset);
            query.setMaxResults(limit);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CtCitas> findCitasCanceladasParametrizable(Date fechaInicial, Date fechaFinal, List<CnPacientes> pacientes, List<CnUsuarios> medicos,  int ban) {
        String consulta = "SELECT c FROM CtCitas c WHERE c.cancelada = true";
        if (ban == 1) {
            if (fechaInicial != null) {
                consulta = consulta.concat(" AND c.idTurno.fecha >= ?1");
            }
            if (fechaFinal != null) {
                consulta = consulta.concat("  AND c.idTurno.fecha <= ?2");
            }
            if (pacientes.size() > 0) {
                consulta = consulta.concat(" AND c.idPaciente IN ?3");
            }
            if (medicos.size() > 0) {
                consulta = consulta.concat(" AND c.idMedico IN ?4");
            }
            
        }
        consulta = consulta.concat("  ORDER BY c.idTurno.fecha, c.idTurno.horaIni, c.idCita");
        try {
            Query query = getEntityManager().createQuery(consulta);
            if (ban == 1) {
                if (fechaInicial != null) {
                    query.setParameter(1, fechaInicial);
                }
                if (fechaFinal != null) {
                    query.setParameter(2, fechaFinal);
                }
                if (pacientes.size() > 0) {
                    query.setParameter(3, pacientes);
                }
                if (medicos.size() > 0) {
                    query.setParameter(4, medicos);
                }
           
            }
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<CtCitas> findCitasByPacienteVigentes(int idpaciente, int offset, int limit) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idPaciente.idPaciente = ?1 AND c.atendida = false AND c.idTurno.fecha >= CURRENT_DATE ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
            query.setParameter(1, idpaciente);
            query.setFirstResult(offset);
            query.setMaxResults(limit);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public int TotalCitasVigentesByPaciente(int idpaciente) {
        try {
            Query query = getEntityManager().createQuery("SELECT COUNT (c.idCita) FROM CtCitas c WHERE c.idPaciente.idPaciente = ?1 AND c.atendida = false AND c.idTurno.fecha >= CURRENT_DATE");
            query.setParameter(1, idpaciente);
            int total = Integer.parseInt(query.getSingleResult().toString());
            return total;
        } catch (Exception e) {
            return 0;
        }
    }

    public int TotalCitasVigentesByMedico(int idmedico) {
        try {
            Query query = getEntityManager().createQuery("SELECT COUNT (c.idCita) FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.atendida = false AND c.idTurno.fecha >= CURRENT_DATE");
            query.setParameter(1, idmedico);
            int total = Integer.parseInt(query.getSingleResult().toString());
            return total;
        } catch (Exception e) {
            return 0;
        }
    }

    public List<CtCitas> findCitas() {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idTurno.fecha >= CURRENT_DATE AND c.atendida = false ORDER BY c.idCita DESC");
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CtCitas> findCitasIntervaloParametros(Date fechaInicial, Date fechaFinal, List<CnPacientes> pacientes, List<CnUsuarios> medicos, int ban) {
        String consulta = "SELECT c FROM CtCitas c";
        if (ban == 1) {
            int aux = 0;
            consulta = consulta.concat("  WHERE");
            if (fechaInicial != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                consulta = consulta.concat("  c.idTurno.fecha >= ?1");
                aux = 1;

            }
            if (fechaFinal != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat("  c.idTurno.fecha <= ?2");
            }
            if (pacientes.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idPaciente IN ?3");
            }
            if (medicos.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idMedico IN ?4");
            }
            
        }
        consulta = consulta.concat(" ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
        try {
            Query query = getEntityManager().createQuery(consulta);
            if (ban == 1) {
                if (fechaInicial != null) {
                    query.setParameter(1, fechaInicial);
                }
                if (fechaFinal != null) {
                    query.setParameter(2, fechaFinal);
                }
                if (pacientes.size() > 0) {
                    query.setParameter(3, pacientes);
                }
                if (medicos.size() > 0) {
                    query.setParameter(4, medicos);
                }
               
            }
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }


    public CtCitas findCitaById(int id) {
        Query query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idCita =?1");
        query.setParameter(1, id);
        return (CtCitas) query.getSingleResult();
    }

    public List<CtCitas> findHistoriaClinica(CnPacientes paciente, Date fechaInicial, Date fechaFinal) {
        try {
            Query query;
            if (fechaInicial == null && fechaFinal == null) {
                query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idPaciente = ?1 ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
            } else if (fechaInicial != null && fechaFinal == null) {
                query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idPaciente = ?1 AND c.idTurno.fecha >= ?2 ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
                query.setParameter(2, fechaInicial);
            } else if (fechaInicial == null && fechaFinal != null) {
                query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idPaciente = ?1 AND c.idTurno.fecha <= ?3 ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
                query.setParameter(3, fechaFinal);
            } else {
                query = getEntityManager().createQuery("SELECT c FROM CtCitas c WHERE c.idPaciente = ?1 AND c.idTurno.fecha >= ?2 AND c.idTurno.fecha <= ?3 ORDER BY c.idTurno.fecha, c.idTurno.horaIni");
                query.setParameter(2, fechaInicial);
                query.setParameter(3, fechaFinal);
            }
            query.setParameter(1, paciente);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CtCitas> getInformeFiltro(String filtro, Date fechaDesde, Date fechaHasta) {
        try {
            String hql = "SELECT c FROM CtCitas c WHERE c.idCita>0 " + filtro;
            if (fechaDesde != null && fechaHasta != null) {
                return getEntityManager().createQuery(hql).setParameter("param1", fechaDesde, TemporalType.TIMESTAMP).setParameter("param2", fechaHasta, TemporalType.TIMESTAMP).getResultList();
            } else {
                return getEntityManager().createQuery(hql).getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
 
}
