/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.facade;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CnPacientes;
import jpa.modelo.CnUsuarios;
import jpa.modelo.HcSesionesTr;

/**
 *
 * @author dante
 */
@Stateless
public class HcSesionesTrFacade extends AbstractFacade<HcSesionesTr> {

    public HcSesionesTrFacade() {
        super(HcSesionesTr.class);
    }

    public Integer siguienteid() {

        int numeracion = 0;
        Integer consulta;
        try {
            consulta = (Integer) getEntityManager().createNativeQuery("SELECT MAX(id_registro) FROM hc_sesiones_tr").getSingleResult();
            if (consulta == null) {
                numeracion = 1;
            } else {
                numeracion = consulta;
                numeracion = numeracion + 1;
            }

        } catch (Exception e) {
            throw e;
        }

        return numeracion;
    }

    public List<HcSesionesTr> buscarRegistrosNoPagos() {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcSesionesTr t WHERE t.pagado = false");
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<HcSesionesTr> buscarRegistrosNoAtendidos() {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcSesionesTr t WHERE t.realizado = false and t.pagado = true ");
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public int realizaPago(Long id) {
        try {
            Query query = getEntityManager().createQuery("UPDATE HcSesionesTr SET pagado = true WHERE id = ?1");
            query.setParameter(1, id);
            int resultado = query.executeUpdate();
            return resultado;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public int realizaAtencion(Long id) {
        try {
            Query query = getEntityManager().createQuery("UPDATE HcSesionesTr SET realizado = true WHERE id = ?1");
            query.setParameter(1, id);
            int resultado = query.executeUpdate();
            return resultado;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    
    public Long numSesiones(int id) {
        return (Long) getEntityManager().createNativeQuery("SELECT count(*) FROM hc_sesiones_tr where id_registro = " + id + "").getSingleResult();
           
    }

    public List<HcSesionesTr> findCantidadSesiones(Date fechaInicial, Date fechaFinal, List<CnPacientes> pacientes, List<CnUsuarios> prestadores, int ban) {
        String consulta = "SELECT c FROM HcSesionesTr c WHERE c.realizado = true";
        if (ban == 1) {
            int aux = 1;
            if (fechaInicial != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
}
                consulta = consulta.concat("  c.fechaAtencion >= ?1");
                aux = 1;

            }
            if (fechaFinal != null) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat("  c.fechaAtencion <= ?2");
            }
            if (pacientes.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idPaciente IN ?3");
            }
            if (prestadores.size() > 0) {
                if (aux == 1) {
                    consulta = consulta.concat("  AND");
                }
                aux = 1;
                consulta = consulta.concat(" c.idMedico IN ?4");
            }
            
        }
        consulta = consulta.concat(" ORDER BY c.fechaReg");
        try {
            Query query = getEntityManager().createQuery(consulta);
            if (ban == 1) {
                if (fechaInicial != null) {
                    query.setParameter(1, fechaInicial);
                }
                if (fechaFinal != null) {
                    query.setParameter(2, fechaFinal);
                }
                if (pacientes.size() > 0) {
                    query.setParameter(3, pacientes);
                }
                if (prestadores.size() > 0) {
                    query.setParameter(4, prestadores);
                }
               
            }
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

}
