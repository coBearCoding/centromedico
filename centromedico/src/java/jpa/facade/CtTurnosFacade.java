
package jpa.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CtTurnos;

/**
 *
 * @author kenlly
 */
@Stateless
public class CtTurnosFacade extends AbstractFacade<CtTurnos> {

    public CtTurnosFacade() {
        super(CtTurnos.class);
    }

    public CtTurnos findById(Integer id_turno) {
        Query query = getEntityManager().createQuery("SELECT t FROM CtTurnos t WHERE t.idTurno = ?1");
        query.setParameter(1, id_turno);
        return (CtTurnos) query.getSingleResult();
    }

    public List<CtTurnos> findTurnosDisponiblesByMedicosLazyNative(String consulta, List id_medicos, int especialidad, Date horaIni, Date horaFin, List<Integer> daysofweek, int offset, int limit) {
        try {
            Query query = getEntityManager().createNativeQuery(consulta.concat(" order by fecha, hora_ini"), CtTurnos.class);
            query.setFirstResult(offset);
            query.setMaxResults(limit);
            return (List<CtTurnos>) query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public int totalTurnosByMedicosLazyNative(String consulta, List id_medicos, int especialidad, Date horaIni, Date horaFin, List<Integer> daysofweek) {
        try {
            Query query = getEntityManager().createNativeQuery(consulta);
            return Integer.parseInt(query.getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }

    public CtTurnos buscarPorId(Integer id_turno) {
        return getEntityManager().createNamedQuery("CtTurnos.findByIdTurno", CtTurnos.class
        ).setParameter("idTurno", id_turno).getSingleResult();

    }

    public int EliminarAgenda(int idMedico, Date fechaInicial, Date fechaFinal, int id_horario, List estados) {
        Query query;
        try {
            if (id_horario != 0) {
                query = getEntityManager().createQuery("DELETE FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1 AND t.fecha >= ?2 AND t.fecha <= ?3 AND t.idHorario.idHorario = ?4 AND t.estado NOT IN ?5  AND t.idTurno NOT IN (SELECT c.idTurno.idTurno FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.idTurno.fecha >= ?2 AND c.idTurno.fecha <= ?3 AND c.idTurno.idHorario.idHorario = ?4)");
                query.setParameter(4, id_horario);
            } else {
                query = getEntityManager().createQuery("DELETE FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1 AND t.fecha >= ?2 AND t.fecha <= ?3 AND t.idHorario IS NULL AND t.estado NOT IN ?5 AND t.idTurno NOT IN (SELECT c.idTurno.idTurno FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.idTurno.fecha >= ?2 AND c.idTurno.fecha <= ?3 AND c.idTurno.idHorario IS NULL)");
            }
            query.setParameter(1, idMedico);
            query.setParameter(2, fechaInicial);
            query.setParameter(3, fechaFinal);
            query.setParameter(5, estados);
            return query.executeUpdate();
        } catch (Exception e) {
            return 0;
        }
    }

    public int ValidarEliminarAgenda(int idMedico, Date fechaInicial, Date fechaFinal, int id_horario) {
        Query query;
        try {
            if (id_horario != 0) {
                query = getEntityManager().createQuery("SELECT COUNT(t.idTurno) FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1 AND t.fecha >= ?2 AND t.fecha <= ?3 AND t.idHorario.idHorario = ?4");
                query.setParameter(4, id_horario);
            } else {
                query = getEntityManager().createQuery("SELECT COUNT(t.idTurno) FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1 AND t.fecha >= ?2 AND t.fecha <= ?3 AND t.idHorario IS NULL");
            }
            query.setParameter(1, idMedico);
            query.setParameter(2, fechaInicial);
            query.setParameter(3, fechaFinal);
            int totalTurnos = Integer.parseInt(query.getSingleResult().toString());
            return totalTurnos;
        } catch (Exception e) {
            return 0;
        }
    }

    public int actualizarTurno(int idMedico, Date fechaInicial, Date fechaFinal, int id_horario) {
        Query query;
        try {
            if (id_horario != 0) {
                query = getEntityManager().createQuery("UPDATE CtTurnos t SET t.idHorario = null, t.estado = 'no_disponible' WHERE t.idTurno IN (SELECT c.idTurno.idTurno FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.idTurno.estado = 'disponible' AND c.idTurno.fecha >= ?2 AND c.idTurno.fecha <= ?3 AND c.idTurno.idHorario.idHorario = ?4 )");
                query.setParameter(4, id_horario);
            } else {
                query = getEntityManager().createQuery("UPDATE CtTurnos t SET t.estado = 'no_disponible' WHERE t.idTurno IN (SELECT c.idTurno.idTurno FROM CtCitas c WHERE c.idMedico.idUsuario = ?1 AND c.idTurno.estado = 'disponible' AND c.idTurno.fecha >= ?2 AND c.idTurno.fecha <= ?3 AND c.idTurno.idHorario IS NULL )");
            }
            query.setParameter(1, idMedico);
            query.setParameter(2, fechaInicial);
            query.setParameter(3, fechaFinal);
            
            return query.executeUpdate();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean ValidarEliminarHorario(int id_horario) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM CtTurnos t WHERE t.idHorario.idHorario = ?1");
            query.setParameter(1, id_horario);
            query.setMaxResults(1);
            CtTurnos turno = (CtTurnos) query.getSingleResult();
            return turno == null;
        } catch (Exception e) {
            return true;
        }
    }

    public List<CtTurnos> buscarTurnosParametrizado(int id_medico, Date fechaInicial, Date fechaFinal, int id_consultorio, int id_horario) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1 AND t.fecha >= ?2 AND t.fecha <= ?3 AND t.idConsultorio.idConsultorio = ?4 AND t.idHorario.idHorario = ?5");
            query.setParameter(1, id_medico);
            query.setParameter(2, fechaInicial);
            query.setParameter(3, fechaFinal);
            query.setParameter(4, id_consultorio);
            query.setParameter(5, id_horario);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CtTurnos> obtenerTurnosLazy(int idMedico, Date start, Date end) {
        try {
          
            Query query = getEntityManager().createQuery("SELECT t FROM CtTurnos t WHERE  t.idMedico.idUsuario = ?1  AND t.fecha >= ?2 AND t.fecha <= ?3  ORDER BY t.fecha, t.horaIni");
            query.setParameter(1, idMedico);
            query.setParameter(2, start);
            query.setParameter(3, end);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }

    }
        
    public Object[] MinDateMaxDate(int idMedico) {
        try {          
            Query query = getEntityManager().createQuery("SELECT MIN(t.horaIni), MAX(t.horaFin) FROM CtTurnos t WHERE t.idMedico.idUsuario = ?1", Object[].class);
            query.setParameter(1, idMedico);
            Object[] o = (Object[]) query.getSingleResult();
            return o;
        } catch (Exception e) {
            return null;
        }
    }

    public int totalTurnosDisponibles(List<Integer> idTurnos) {
        try {
            Query query = getEntityManager().createQuery("SELECT COUNT(t.idTurno) FROM CtTurnos t WHERE t.idTurno IN ?1 AND t.estado = 'disponible'");
            query.setParameter(1, idTurnos);
            return Integer.parseInt(query.getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }
    
     public List<CtTurnos> obtenerTurnosEnfermeriaLazy(Date start, Date end) {
        try {

            Query query = getEntityManager().createQuery("SELECT t FROM CtTurnos t WHERE  t.fecha >= ?1 AND t.fecha <= ?2 and t.estado <> 'disponible' ORDER BY t.fecha, t.horaIni");            
            query.setParameter(1, start);
            query.setParameter(2, end);
            return query.getResultList();
            
        } catch (Exception e) {
            return null;
        }
    }
    
}
