
package jpa.facade;

import jpa.modelo.CnPerfilesUsuario;
import javax.ejb.Stateless;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnPerfilesUsuarioFacade extends AbstractFacade<CnPerfilesUsuario> {

    public CnPerfilesUsuarioFacade() {
        super(CnPerfilesUsuario.class);
    }
    
}
