
package jpa.facade;

import jpa.modelo.CnClasificaciones;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnClasificacionesFacade extends AbstractFacade<CnClasificaciones> {

    public CnClasificacionesFacade() {
        super(CnClasificaciones.class);
    }

    public List<CnClasificaciones> buscarPorMaestro(String maestro) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro = :maestro order by c.codigo ASC";
            return getEntityManager().createQuery(hql).setParameter("maestro", maestro).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public CnClasificaciones buscarPorIdTf(int id) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.id = :id";
            getEntityManager().createQuery(hql).setParameter("id", id).getSingleResult();
            return (CnClasificaciones) getEntityManager().createQuery(hql).setParameter("id",id).getSingleResult();
        } catch (NumberFormatException e) {
            return null;
        }
    }
    
        public CnClasificaciones buscarPorIdTfL(int id) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.id = :id";
            getEntityManager().createQuery(hql).setParameter("id", id).getSingleResult();
            return (CnClasificaciones) getEntityManager().createQuery(hql).setParameter("id",id).getSingleResult();
        } catch (NumberFormatException e) {
            return null;
        }
    }
        
    public List<CnClasificaciones> buscarPorMaestroObservacion(String maestro, String observacion) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro = :maestro AND c.observacion = :observacion order by c.codigo";
            return getEntityManager().createQuery(hql).setParameter("maestro", maestro).setParameter("observacion", observacion).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
   
    public List<CnClasificaciones> buscarCantonPorProvincia(String provincia) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro LIKE 'Canton' AND c.codigo LIKE '" + provincia + "%' ORDER BY c.codigo";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CnClasificaciones> buscarParroquiaPorCanton(String canton) {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro LIKE 'Parroquia' AND c.codigo LIKE '" + canton + "%'  ORDER BY c.codigo";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<CnClasificaciones> buscarTerapiasRespiratorias() {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro LIKE 'Terapia Respiratoria' ORDER BY c.codigo";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
     public List<CnClasificaciones> buscarEjercicioTerapeuticos() {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro LIKE 'Ejercicio Terapeutico' ORDER BY c.codigo";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public CnClasificaciones getTramientoByMaestro(String maestro) {
        try {
            Query query = getEntityManager().createQuery("SELECT u FROM CnClasificaciones u WHERE u.codigo = ?1");
            query.setParameter(1, maestro);
            CnClasificaciones resultado = (CnClasificaciones) query.getSingleResult();
            return resultado;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        /*try {
             CnClasificaciones consulta = (CnClasificaciones) getEntityManager().createNativeQuery("SELECT * FROM cn_clasificaciones where maestro = '" +maestro+"'").getSingleResult();
             return consulta;
        } catch (Exception e) {
            throw e;
        }*/
    }

    public List<CnClasificaciones> buscarTerapiasFisicas() {
        try {
            String hql = "SELECT c FROM CnClasificaciones c WHERE c.maestro IN ('Ejercicio Terapeutico','Agente Fisico') ORDER BY c.codigo";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
