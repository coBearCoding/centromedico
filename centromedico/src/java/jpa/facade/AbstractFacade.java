
package jpa.facade;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author kenlly
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    @PersistenceContext(unitName = "centromedicoPU")
    private EntityManager em;   

    
    protected EntityManager getEntityManager() {
            return em;
    }


    public int consultaNativaConteo(String sql) {
        try {
            return Integer.parseInt(getEntityManager().createNativeQuery(sql).getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }
    
    

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public void create(T entity) {
        
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity);
        if(constraintViolations.size() > 0){
            Iterator<ConstraintViolation<T>> iterator = constraintViolations.iterator();
            while(iterator.hasNext()){
                ConstraintViolation<T> cv = iterator.next();
                System.err.println(cv.getRootBeanClass().getName()+"."+cv.getPropertyPath() + " " +cv.getMessage());
            }
        }else{
            getEntityManager().persist(entity);
        }
    }

    public void edit(T entity) {
       getEntityManager().merge(entity);
    }
    
    
    
    public void refresh(T entity){
        getEntityManager().refresh(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public Date FechaServidorddmmyyhhmmTimestamp() {
        StoredProcedureQuery q = getEntityManager().createStoredProcedureQuery("f_fecha_servidor_ddmmyyhhmm_timestamp");        
        q.execute();
        Object[] o = (Object[]) q.getSingleResult();
        Timestamp tm = null;
        Date fecha = null;        
        for (Object obj:o) {
            tm = (Timestamp) obj;
        }
        fecha =  new java.sql.Date(tm.getTime());
        return fecha;
    }

}
