
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import jpa.modelo.HcTipoReg;

/**
 *
 * @author kenlly
 */
@Stateless
public class HcTipoRegFacade extends AbstractFacade<HcTipoReg> {

    public HcTipoRegFacade() {
        super(HcTipoReg.class);
    }
    
    public List<HcTipoReg> buscarTiposRegstroActivos() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg != 92  ";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTiposRegistroGeneral() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg in (6,93,94,96)  ";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTiposRegistroTerapia() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg in (6,93,94,91)  ";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTiposRegistroPediatria() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg in (96)  ";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public HcTipoReg buscarTiposRegstroActivosTf() {
        try {
            //String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE ORDER BY h.idTipoReg";
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg = 91 ";
            return (HcTipoReg)getEntityManager().createQuery(hql).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public HcTipoReg buscarTiposRegstroActivosTr() {
        try {
            //String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE ORDER BY h.idTipoReg";
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg = 96 ";
            return (HcTipoReg)getEntityManager().createQuery(hql).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTiposRegstroFichaMedica() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg = 93  ";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTiposRegstroActivosAll() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE ORDER BY h.idTipoReg";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTipoRegistroSignosVitales() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg = 92";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<HcTipoReg> buscarTipoRegistroEnfermeria() {
        try {
            String hql = "SELECT h FROM HcTipoReg h WHERE h.activo = TRUE and h.idTipoReg in (92,96)";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

}
