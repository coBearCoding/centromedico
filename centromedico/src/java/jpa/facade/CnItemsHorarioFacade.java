
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.CnItemsHorario;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnItemsHorarioFacade extends AbstractFacade<CnItemsHorario> {

    public CnItemsHorarioFacade() {
        super(CnItemsHorario.class);
    }

    public List<CnItemsHorario> findHorarioByIdCnTurno(int id_cnturno) {
        List<CnItemsHorario> cnHorarios;
        try {            
            Query query = getEntityManager().createQuery("SELECT c FROM CnItemsHorario c WHERE c.idHorario.idHorario = ?1 ");
            query.setParameter(1, id_cnturno);
            cnHorarios = query.getResultList();
        } catch (Exception e) {
            cnHorarios = null;
        }
        return cnHorarios;
    }

    public List<Short> findSelectedDays(int id_horario) {
        List<Short> dias;
        try {
            Query query = getEntityManager().createQuery("SELECT  DISTINCT c.dia FROM  CnItemsHorario c WHERE c.idHorario.idHorario = ?1");
            query.setParameter(1, id_horario);
            dias = query.getResultList();
        } catch (Exception e) {
            dias = null;
        }
        return dias;
    }

    public List<CnItemsHorario> findDateByDay(int id, Short dia) {
        List<CnItemsHorario> horario;
        try {
            Query query = getEntityManager().createQuery("SELECT  c FROM  CnItemsHorario c WHERE c.idHorario.idHorario = ?1 AND c.dia =?2");
            query.setParameter(1, id);
            query.setParameter(2, dia);
            horario = query.getResultList();
        } catch (Exception e) {
            horario = null;
        }
        return horario;
    }

    public int eliminarByIdHorario(int idHorario) {
        try {
            Query query = getEntityManager().createQuery("DELETE FROM CnItemsHorario c WHERE c.idHorario.idHorario = ?1");
            query.setParameter(1, idHorario);
            return query.executeUpdate();
        } catch (Exception e) {
            return 0;
        }
    }
}
