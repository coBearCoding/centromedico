
package jpa.facade;

import javax.ejb.Stateless;
import jpa.modelo.CnHorario;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnHorarioFacade extends AbstractFacade<CnHorario> {

    public CnHorarioFacade() {
        super(CnHorario.class);
    }

    public CnHorario buscarPorDescripcion(String descripcion) {
        try {
            return getEntityManager().createNamedQuery("CnHorario.findByDescripcion", CnHorario.class).setParameter("descripcion", descripcion).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
