
package jpa.facade;

import util.RegistroReporte;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query; 
import jpa.modelo.HcDetalle;
import jpa.modelo.HcRegistro;

/**
 *
 * @author kenlly
 */
@Stateless
public class HcRegistroFacade extends AbstractFacade<HcRegistro> {

    public HcRegistroFacade() {
        super(HcRegistro.class);
    }

    public List<HcRegistro> buscarOrdenadoPorFecha(int idPaciente) {//buscar todos los registros clinicos de un paciente
        try {
            String hql = "SELECT u FROM HcRegistro u WHERE u.idPaciente.idPaciente = :idPaciente ORDER BY u.fechaReg DESC";
            return getEntityManager().createQuery(hql).setParameter("idPaciente", idPaciente).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<HcRegistro> buscarOrdenado(int idPaciente, Date fechaInicial, Date fechaFinal) {//buscar registros clinicos filtrando por fecha inicial y final
        try {
           
            Query query = getEntityManager().createQuery("SELECT u FROM HcRegistro u WHERE u.idPaciente.idPaciente = ?1 AND u.fechaReg >= ?2 AND u.fechaReg <= ?3 ORDER BY u.fechaReg DESC");           
            query.setParameter(1, idPaciente);
            query.setParameter(2, fechaInicial);
            query.setParameter(3, fechaFinal);
            
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public HcRegistro buscarFiltradoPorFichaMedicaPadre(Integer idfichamedica) {//buscar registros clinicos filtrando por ficha medica padre
        try {
            HcRegistro listaResultado = null;
            Query query = getEntityManager().createQuery("SELECT u FROM HcRegistro u WHERE u.idfichamedica = ?1 ");
            query.setParameter(1, idfichamedica);
            listaResultado = (HcRegistro) query.getSingleResult();
            
            return listaResultado;
        } catch (Exception e) {
            return null;
        }
    }

    public int buscarMaximoFolio(Integer idPaciente) {
        try {            
            return Integer.parseInt(getEntityManager().createNativeQuery("SELECT MAX(folio) FROM hc_registro WHERE id_paciente = " + idPaciente).getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }
    
    public Integer siguienteid() {
       
        int numeracion = 0;
         Integer consulta;
        try {
             consulta = (Integer) getEntityManager().createNativeQuery("SELECT MAX(id_registro) FROM hc_registro").getSingleResult();
             if(consulta == null){
                numeracion = 1;
            }else{
                 numeracion = consulta;
                numeracion = numeracion + 1;
            }
            
        } catch (Exception e) {
            throw e;
        }       

        return numeracion;
    }
        
    public List<RegistroReporte> findBySqlNativo(String sql) {
       List<RegistroReporte> lista = new ArrayList<>();
       List<Object> obj = new ArrayList<>();
       RegistroReporte registro = new RegistroReporte();
       try { 
        if (!sql.isEmpty()) {
             Query query = getEntityManager().createNativeQuery(sql);
             obj = query.getResultList();
       
        } 
        
        for (Object ob : obj) {
            registro = new RegistroReporte();
            Object[] subitem = (Object[])  ob;
            registro.setNombre(subitem[2].toString());
            registro.setIdentificacion(subitem[3].toString());
            registro.setSexo(subitem[4] == null ? "" : subitem[4].toString());
            registro.setOrientacionsexual(subitem[5] == null ? "" : subitem[5].toString());
            registro.setIdentidadgenero(subitem[6] == null ? "" : subitem[6].toString());
            registro.setFecha_nacimiento(subitem[7] == null ? "" : subitem[7].toString());
            registro.setIdentificacionresponsable(subitem[8] == null ? "" : subitem[8].toString());
            registro.setNacionalidad(subitem[9] == null ? "" : subitem[9].toString());
            registro.setEtnia(subitem[10] == null ? "" : subitem[10].toString());
            registro.setNacionalidades(subitem[11] == null ? "" : subitem[11].toString());
            registro.setAfiliado(subitem[12] == null ? "" : subitem[12].toString());
            registro.setGruposprioritarios(subitem[13] == null ? "" : subitem[13].toString());
            registro.setSemanagestacion(subitem[14] == null ? "" : subitem[14].toString());
            registro.setProvincia(subitem[15] == null ? "" : subitem[15].toString());
            registro.setCanton(subitem[16] == null ? "" : subitem[16].toString());
            registro.setParroquia(subitem[17] == null ? "" : subitem[17].toString());
            registro.setBarrio(subitem[18] == null ? "" : subitem[18].toString());
            registro.setEnfermedad(subitem[19] == null ? "" : subitem[19].toString());
            registro.setCodigo(subitem[20] == null ? "" : subitem[20].toString());
            registro.setTipoatencion(subitem[21] == null ? "" : subitem[21].toString());
            lista.add(registro);
        }
        return lista;
        } catch (NoResultException  e) {
            return null;
         }
    }
}
