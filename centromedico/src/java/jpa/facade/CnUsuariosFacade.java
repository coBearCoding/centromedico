
package jpa.facade;

import jpa.modelo.CnUsuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import jpa.modelo.CnClasificaciones;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnUsuariosFacade extends AbstractFacade<CnUsuarios> {

    public CnUsuariosFacade() {
        super(CnUsuarios.class);
    }
    
    public List<CnUsuarios> buscarOrdenado() {
        try {
            String hql = "SELECT u FROM CnUsuarios u ORDER BY u.primerNombre, u.segundoNombre , u.primerApellido, u.segundoApellido";
            return getEntityManager().createQuery(hql).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public CnUsuarios buscarPorLoginClave(String login, String clave) {
        try {
            String hql = "SELECT u FROM CnUsuarios u WHERE u.loginUsuario = :loginUsuario AND u.clave = :clave";
            return (CnUsuarios) getEntityManager().createQuery(hql).setParameter("loginUsuario", login).setParameter("clave", clave).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public EntityManager sacarEntityManager() {
        return getEntityManager();
    }

    public CnUsuarios buscarPorIdentificacion(String identificacion) {
        try {
            return getEntityManager().createNamedQuery("CnUsuarios.findByIdentificacion", CnUsuarios.class).setParameter("identificacion", identificacion).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<CnClasificaciones> findEspecialidades() {
        try {
            Query query = getEntityManager().createQuery("SELECT DISTINCT p.especialidad FROM CnUsuarios p WHERE p.tipoUsuario.codigo='2' AND p.estadoCuenta = true");
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CnUsuarios> findMedicos() {
        try {
            Query query = getEntityManager().createQuery("SELECT p FROM CnUsuarios p WHERE p.tipoUsuario.codigo = '2' AND p.visible = true");
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<CnUsuarios> findMedicosLazy(String sql, int limit, int offset, int especialidad, String medico) {
        try {
            Query query = getEntityManager().createQuery(sql);
            if (especialidad != 0) {
                query.setParameter(1, especialidad);
            }
            if (!medico.isEmpty()) {
                query.setParameter(3, medico + '%');
            }
            query.setFirstResult(offset);
            query.setMaxResults(limit);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public EntityManager obtenerEntityManager() {
        return getEntityManager();
    }

    public int totalMedicosLazy(String consulta, int especialidad, String medico) {
        try {
            Query query = getEntityManager().createQuery(consulta);
            if (especialidad != 0) {
                query.setParameter(1, especialidad);
            }
            if (!medico.isEmpty()) {
                query.setParameter(3, medico + '%');

            }
            return Integer.parseInt(query.getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }
    
}
