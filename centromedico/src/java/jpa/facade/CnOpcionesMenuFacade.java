
package jpa.facade;

import jpa.modelo.CnOpcionesMenu;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnOpcionesMenuFacade extends AbstractFacade<CnOpcionesMenu> {

    public CnOpcionesMenuFacade() {
        super(CnOpcionesMenu.class);
    }

}
