/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import jpa.modelo.HcDetalleTr;

/**
 *
 * @author dante
 */
@Stateless
public class HcDetalleTrFacade extends AbstractFacade<HcDetalleTr> {

    public HcDetalleTrFacade() {
        super(HcDetalleTr.class);
    }
    
    public Integer  siguienteid() {
       
        int numeracion = 0;
         Integer consulta;
        try {
             consulta = (Integer) getEntityManager().createNativeQuery("SELECT MAX(id_registro) FROM hc_detalle_tr").getSingleResult();
             if(consulta == null){
                numeracion = 1;
            }else{
                 numeracion = consulta;
                numeracion = numeracion + 1;
            }
            
        } catch (Exception e) {
            throw e;
        }
        

        return numeracion;
    }
    
    public List<HcDetalleTr> tratamientosById(int id){
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcDetalleTr t WHERE t.idRegistro = ?1").setParameter(1, id);
            return query.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
     public Double valorTratamiento(String codigo, int id){
        Double valor = 0.00;
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM HcDetalleTr t WHERE t.idRegistro = ?1 and t.idCampo = ?2").setParameter(1, id).setParameter(2, codigo);
            valor = ((HcDetalleTr) query.getSingleResult()).getValor().doubleValue();
            if (valor == null){
                valor = 0.00;
            }
            return valor;
        } catch (Exception e) {
            System.out.println(e);
            return 0.00;
        }
    }
    
    //public List<HcSes>buscarRegistrosNoPagos
}
