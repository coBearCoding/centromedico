
package jpa.facade;

import java.util.List;
import jpa.modelo.CnPacientes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author kenlly
 */
@Stateless
public class CnPacientesFacade extends AbstractFacade<CnPacientes> {
    
    public CnPacientesFacade() {
        super(CnPacientes.class);
    }
    
    public EntityManager obtenerEntityManager() {
        return getEntityManager();
    }

    public List<CnPacientes> consultaNativaPacientes(String sql) {
        List<CnPacientes> listPacientes = (List<CnPacientes>) getEntityManager().createNativeQuery(sql, CnPacientes.class).getResultList();
        return listPacientes;
    }   
  
    public String findBySqlNativo(String sql) {
      String lista = null;
       try { 
        if (!sql.isEmpty()) {
             Query query = getEntityManager().createNativeQuery(sql);
             lista = query.getSingleResult().toString();
       
        } 
        return lista;
        } catch (NoResultException  e) {
            return null;
         }
    }
    
    public int numeroCedulas(String identificacion) {
        try {
            Query q = getEntityManager().createQuery("SELECT COUNT(p) FROM CnPacientes  p where p.identificacion = ?1");
            q.setParameter(1, identificacion);
            return Integer.parseInt(q.getSingleResult().toString());
        } catch (Exception e) {
            return 0;
        }
    }

    public int numeroTotalRegistros() {
        try {            
            return Integer.parseInt(getEntityManager().createNativeQuery("SELECT COUNT(*) FROM cn_pacientes").getSingleResult().toString());            
        } catch (Exception e) {
            return 0;
        }
    }
    
    public Integer  siguienteid() {
        
        int numeracion = 0;
        Integer consulta;
        try {
            consulta = (Integer) getEntityManager().createNativeQuery("SELECT MAX(id_paciente) FROM cn_pacientes").getSingleResult();
            if(consulta == null){
                numeracion = 1;
            }else{
                numeracion = consulta;
                numeracion = numeracion + 1;
            }
            
        } catch (Exception e) {
            throw e;
        }
        
        return numeracion;
    }
    
    public CnPacientes buscarPorIdentificacion(String identificacion) {
        try {
            return getEntityManager().createNamedQuery("CnPacientes.findByIdentificacion", CnPacientes.class).setParameter("identificacion", identificacion).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public CnPacientes findPacienteByTipIden(int id, String identificacion) {
        CnPacientes paciente;
        try {
            Query query = getEntityManager().createQuery("SELECT p FROM CnPacientes p WHERE p.identificacion=?1 AND p.tipoIdentificacion.id=?2");
            query.setParameter(1, identificacion);
            query.setParameter(2, id);
            paciente = (CnPacientes) query.getSingleResult();
        } catch (Exception e) {
            paciente = null;
        }
        return paciente;
    }


}
