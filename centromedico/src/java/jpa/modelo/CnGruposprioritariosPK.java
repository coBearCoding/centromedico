/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author kenlly
 */
@Embeddable
public class CnGruposprioritariosPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgruposprioritarios")
    private Integer idgruposprioritarios;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idpaciente")
    private int idpaciente;

    public CnGruposprioritariosPK() {
    }

    public CnGruposprioritariosPK(int idgruposprioritarios, int idpaciente) {
        this.idgruposprioritarios = idgruposprioritarios;
        this.idpaciente = idpaciente;
    }

    public Integer getIdgruposprioritarios() {
        return idgruposprioritarios;
    }

    public void setIdgruposprioritarios(Integer idgruposprioritarios) {
        this.idgruposprioritarios = idgruposprioritarios;
    }

    public int getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(int idpaciente) {
        this.idpaciente = idpaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idgruposprioritarios;
        hash += (int) idpaciente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnGruposprioritariosPK)) {
            return false;
        }
        CnGruposprioritariosPK other = (CnGruposprioritariosPK) object;
        if (this.idgruposprioritarios != other.idgruposprioritarios) {
            return false;
        }
        if (this.idpaciente != other.idpaciente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnGruposprioritariosPK[ idgruposprioritarios=" + idgruposprioritarios + ", idpaciente=" + idpaciente + " ]";
    }
    
}
