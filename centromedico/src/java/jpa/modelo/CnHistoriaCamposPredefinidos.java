/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_historia_campos_predefinidos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnHistoriaCamposPredefinidos.findAll", query = "SELECT c FROM CnHistoriaCamposPredefinidos c")
    , @NamedQuery(name = "CnHistoriaCamposPredefinidos.findById", query = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.id = :id")
    , @NamedQuery(name = "CnHistoriaCamposPredefinidos.findByValor", query = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.valor = :valor")
    , @NamedQuery(name = "CnHistoriaCamposPredefinidos.findByDefaultValor", query = "SELECT c FROM CnHistoriaCamposPredefinidos c WHERE c.defaultValor = :defaultValor")})
public class CnHistoriaCamposPredefinidos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "valor")
    private String valor;
    @Column(name = "default_valor")
    private Boolean defaultValor;
    @JoinColumn(name = "id_campo", referencedColumnName = "id_campo")
    @ManyToOne
    private HcCamposReg idCampo;

    public CnHistoriaCamposPredefinidos() {
    }

    public CnHistoriaCamposPredefinidos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Boolean getDefaultValor() {
        return defaultValor;
    }

    public void setDefaultValor(Boolean defaultValor) {
        this.defaultValor = defaultValor;
    }

    public HcCamposReg getIdCampo() {
        return idCampo;
    }

    public void setIdCampo(HcCamposReg idCampo) {
        this.idCampo = idCampo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnHistoriaCamposPredefinidos)) {
            return false;
        }
        CnHistoriaCamposPredefinidos other = (CnHistoriaCamposPredefinidos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnHistoriaCamposPredefinidos[ id=" + id + " ]";
    }
    
}
