/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_opciones_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnOpcionesMenu.findAll", query = "SELECT c FROM CnOpcionesMenu c")
    , @NamedQuery(name = "CnOpcionesMenu.findByIdOpcionMenu", query = "SELECT c FROM CnOpcionesMenu c WHERE c.idOpcionMenu = :idOpcionMenu")
    , @NamedQuery(name = "CnOpcionesMenu.findByNombreOpcion", query = "SELECT c FROM CnOpcionesMenu c WHERE c.nombreOpcion = :nombreOpcion")
    , @NamedQuery(name = "CnOpcionesMenu.findByStyle", query = "SELECT c FROM CnOpcionesMenu c WHERE c.style = :style")
    , @NamedQuery(name = "CnOpcionesMenu.findByUrlOpcion", query = "SELECT c FROM CnOpcionesMenu c WHERE c.urlOpcion = :urlOpcion")
    , @NamedQuery(name = "CnOpcionesMenu.findByOrden", query = "SELECT c FROM CnOpcionesMenu c WHERE c.orden = :orden")})
public class CnOpcionesMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_opcion_menu")
    private Integer idOpcionMenu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre_opcion")
    private String nombreOpcion;
    @Size(max = 100)
    @Column(name = "style")
    private String style;
    @Size(max = 100)
    @Column(name = "url_opcion")
    private String urlOpcion;
    @Column(name = "orden")
    private Integer orden;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "cnOpcionesMenuList")    
    private Set<CnPerfilesUsuario> cnPerfilesUsuarioList;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idOpcionPadre")
    private Set<CnOpcionesMenu> cnOpcionesMenuList;
    @JoinColumn(name = "id_opcion_padre", referencedColumnName = "id_opcion_menu")
    @ManyToOne
    private CnOpcionesMenu idOpcionPadre;

    public CnOpcionesMenu() {
    }

    public CnOpcionesMenu(Integer idOpcionMenu) {
        this.idOpcionMenu = idOpcionMenu;
    }

    public CnOpcionesMenu(Integer idOpcionMenu, String nombreOpcion) {
        this.idOpcionMenu = idOpcionMenu;
        this.nombreOpcion = nombreOpcion;
    }

    public Integer getIdOpcionMenu() {
        return idOpcionMenu;
    }

    public void setIdOpcionMenu(Integer idOpcionMenu) {
        this.idOpcionMenu = idOpcionMenu;
    }

    public String getNombreOpcion() {
        return nombreOpcion;
    }

    public void setNombreOpcion(String nombreOpcion) {
        this.nombreOpcion = nombreOpcion;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getUrlOpcion() {
        return urlOpcion;
    }

    public void setUrlOpcion(String urlOpcion) {
        this.urlOpcion = urlOpcion;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Set<CnPerfilesUsuario> getCnPerfilesUsuarioList() {
        return cnPerfilesUsuarioList;
    }

    public void setCnPerfilesUsuarioList(Set<CnPerfilesUsuario> cnPerfilesUsuarioList) {
        this.cnPerfilesUsuarioList = cnPerfilesUsuarioList;
    }

    public Set<CnOpcionesMenu> getCnOpcionesMenuList() {
        return cnOpcionesMenuList;
    }

    public void setCnOpcionesMenuList(Set<CnOpcionesMenu> cnOpcionesMenuList) {
        this.cnOpcionesMenuList = cnOpcionesMenuList;
    }
    

    public CnOpcionesMenu getIdOpcionPadre() {
        return idOpcionPadre;
    }

    public void setIdOpcionPadre(CnOpcionesMenu idOpcionPadre) {
        this.idOpcionPadre = idOpcionPadre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOpcionMenu != null ? idOpcionMenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnOpcionesMenu)) {
            return false;
        }
        CnOpcionesMenu other = (CnOpcionesMenu) object;
        if ((this.idOpcionMenu == null && other.idOpcionMenu != null) || (this.idOpcionMenu != null && !this.idOpcionMenu.equals(other.idOpcionMenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnOpcionesMenu[ idOpcionMenu=" + idOpcionMenu + " ]";
    }
    
}
