/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_gruposprioritarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnGruposprioritarios.findAll", query = "SELECT c FROM CnGruposprioritarios c"),
    @NamedQuery(name = "CnGruposprioritarios.findByIdgruposprioritarios", query = "SELECT c FROM CnGruposprioritarios c WHERE c.cnGruposprioritariosPK.idgruposprioritarios = :idgruposprioritarios"),
    @NamedQuery(name = "CnGruposprioritarios.findByIdpaciente", query = "SELECT c FROM CnGruposprioritarios c WHERE c.cnGruposprioritariosPK.idpaciente = :idpaciente")})
public class CnGruposprioritarios implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnGruposprioritariosPK cnGruposprioritariosPK;
    @JoinColumn(name = "idpaciente", referencedColumnName = "id_paciente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CnPacientes cnPacientes;

    public CnGruposprioritarios() {
    }

    public CnGruposprioritarios(CnGruposprioritariosPK cnGruposprioritariosPK) {
        this.cnGruposprioritariosPK = cnGruposprioritariosPK;
    }

    public CnGruposprioritarios(int idgruposprioritarios, int idpaciente) {
        this.cnGruposprioritariosPK = new CnGruposprioritariosPK(idgruposprioritarios, idpaciente);
    }

    public CnGruposprioritariosPK getCnGruposprioritariosPK() {
        return cnGruposprioritariosPK;
    }

    public void setCnGruposprioritariosPK(CnGruposprioritariosPK cnGruposprioritariosPK) {
        this.cnGruposprioritariosPK = cnGruposprioritariosPK;
    }

    public CnPacientes getCnPacientes() {
        return cnPacientes;
    }

    public void setCnPacientes(CnPacientes cnPacientes) {
        this.cnPacientes = cnPacientes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnGruposprioritariosPK != null ? cnGruposprioritariosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnGruposprioritarios)) {
            return false;
        }
        CnGruposprioritarios other = (CnGruposprioritarios) object;
        if ((this.cnGruposprioritariosPK == null && other.cnGruposprioritariosPK != null) || (this.cnGruposprioritariosPK != null && !this.cnGruposprioritariosPK.equals(other.cnGruposprioritariosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnGruposprioritarios[ cnGruposprioritariosPK=" + cnGruposprioritariosPK + " ]";
    }
    
}
