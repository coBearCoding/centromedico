/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_horario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnHorario.findAll", query = "SELECT c FROM CnHorario c")
    , @NamedQuery(name = "CnHorario.findByIdHorario", query = "SELECT c FROM CnHorario c WHERE c.idHorario = :idHorario")
    , @NamedQuery(name = "CnHorario.findByCodigo", query = "SELECT c FROM CnHorario c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "CnHorario.findByDescripcion", query = "SELECT c FROM CnHorario c WHERE c.descripcion = :descripcion")})
public class CnHorario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_horario")
    private Integer idHorario;
    @Size(max = 5)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 150)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idHorario")
    private List<CnItemsHorario> cnItemsHorarioList;
    @OneToMany(mappedBy = "idHorario")
    private List<CtTurnos> ctTurnosList;

    public CnHorario() {
    }

    public CnHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }

    public Integer getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<CnItemsHorario> getCnItemsHorarioList() {
        return cnItemsHorarioList;
    }

    public void setCnItemsHorarioList(List<CnItemsHorario> cnItemsHorarioList) {
        this.cnItemsHorarioList = cnItemsHorarioList;
    }

    @XmlTransient
    public List<CtTurnos> getCtTurnosList() {
        return ctTurnosList;
    }

    public void setCtTurnosList(List<CtTurnos> ctTurnosList) {
        this.ctTurnosList = ctTurnosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHorario != null ? idHorario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnHorario)) {
            return false;
        }
        CnHorario other = (CnHorario) object;
        if ((this.idHorario == null && other.idHorario != null) || (this.idHorario != null && !this.idHorario.equals(other.idHorario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnHorario[ idHorario=" + idHorario + " ]";
    }
    
}
