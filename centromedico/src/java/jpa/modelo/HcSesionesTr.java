/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "hc_sesiones_tr")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcSesionesTr.findAll", query = "SELECT h FROM HcSesionesTr h")
    , @NamedQuery(name = "HcSesionesTr.findByIdRegistro", query = "SELECT h FROM HcSesionesTr h WHERE h.idRegistro = :idRegistro")
    , @NamedQuery(name = "HcSesionesTr.findByFechaReg", query = "SELECT h FROM HcSesionesTr h WHERE h.fechaReg = :fechaReg")
    , @NamedQuery(name = "HcSesionesTr.findByIdCita", query = "SELECT h FROM HcSesionesTr h WHERE h.idCita = :idCita")})
public class HcSesionesTr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    //
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Column(name = "fecha_reg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReg;
    @Column(name = "id_cita")
    private Integer idCita;
    @Column(name = "pagado")
    private Boolean pagado;
    @Column(name = "realizado")
    private Boolean realizado;
    @Column(name = "respirador")
    private Boolean respirador;
    @Column(name = "num_sesion")
    private Integer numSesion;
     @Column(name = "fecha_atencion")
    @Temporal(TemporalType.DATE)
    private Date fechaAtencion;
    @JoinColumn(name = "id_tipo_reg", referencedColumnName = "id_tipo_reg")
    @ManyToOne
    private HcTipoReg idTipoReg;
    @JoinColumn(name = "id_medico", referencedColumnName = "id_usuario")
    @ManyToOne
    private CnUsuarios idMedico;
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente")
    @ManyToOne
    private CnPacientes idPaciente;
    
    public HcSesionesTr() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public HcTipoReg getIdTipoReg() {
        return idTipoReg;
    }

    public void setIdTipoReg(HcTipoReg idTipoReg) {
        this.idTipoReg = idTipoReg;
    }

    public CnUsuarios getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(CnUsuarios idMedico) {
        this.idMedico = idMedico;
    }

    public CnPacientes getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(CnPacientes idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Boolean getPagado() {
        return pagado;
    }

    public void setPagado(Boolean pagado) {
        this.pagado = pagado;
    }

    public Boolean getRealizado() {
        return realizado;
    }

    public void setRealizado(Boolean realizado) {
        this.realizado = realizado;
    }

    public Boolean getRespirador() {
        return respirador;
    }

    public void setRespirador(Boolean respirador) {
        this.respirador = respirador;
    }

    public Integer getNumSesion() {
        return numSesion;
    }

    public void setNumSesion(Integer numSesion) {
        this.numSesion = numSesion;
    }

    public Date getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HcSesionesTr)) {
            return false;
        }
        HcSesionesTr other = (HcSesionesTr) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.HcRegistroTr[ idRegistro=" + idRegistro + " ]";
    }
}
