/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_dias_no_laborales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnDiasNoLaborales.findAll", query = "SELECT c FROM CnDiasNoLaborales c")
    , @NamedQuery(name = "CnDiasNoLaborales.findById", query = "SELECT c FROM CnDiasNoLaborales c WHERE c.cnDiasNoLaboralesPK.id = :id")
    , @NamedQuery(name = "CnDiasNoLaborales.findByFechaNoLaboral", query = "SELECT c FROM CnDiasNoLaborales c WHERE c.cnDiasNoLaboralesPK.fechaNoLaboral = :fechaNoLaboral")})
public class CnDiasNoLaborales implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CnDiasNoLaboralesPK cnDiasNoLaboralesPK;

    public CnDiasNoLaborales() {
    }

    public CnDiasNoLaborales(CnDiasNoLaboralesPK cnDiasNoLaboralesPK) {
        this.cnDiasNoLaboralesPK = cnDiasNoLaboralesPK;
    }

    public CnDiasNoLaborales(int id, Date fechaNoLaboral) {
        this.cnDiasNoLaboralesPK = new CnDiasNoLaboralesPK(id, fechaNoLaboral);
    }

    public CnDiasNoLaboralesPK getCnDiasNoLaboralesPK() {
        return cnDiasNoLaboralesPK;
    }

    public void setCnDiasNoLaboralesPK(CnDiasNoLaboralesPK cnDiasNoLaboralesPK) {
        this.cnDiasNoLaboralesPK = cnDiasNoLaboralesPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnDiasNoLaboralesPK != null ? cnDiasNoLaboralesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnDiasNoLaborales)) {
            return false;
        }
        CnDiasNoLaborales other = (CnDiasNoLaborales) object;
        if ((this.cnDiasNoLaboralesPK == null && other.cnDiasNoLaboralesPK != null) || (this.cnDiasNoLaboralesPK != null && !this.cnDiasNoLaboralesPK.equals(other.cnDiasNoLaboralesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnDiasNoLaborales[ cnDiasNoLaboralesPK=" + cnDiasNoLaboralesPK + " ]";
    }
    
}
