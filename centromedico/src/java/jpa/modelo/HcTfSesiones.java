/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dante
 */
@Entity
@Table(name = "hc_tf_sesiones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcTfSesiones.findAll", query = "SELECT h FROM HcTfSesiones h")
    , @NamedQuery(name = "HcTfSesiones.findById", query = "SELECT h FROM HcTfSesiones h WHERE h.id = :id")
    , @NamedQuery(name = "HcTfSesiones.findByIdSesion", query = "SELECT h FROM HcTfSesiones h WHERE h.numSesion = :numSesion")
    , @NamedQuery(name = "HcTfSesiones.findByFechaSesion", query = "SELECT h FROM HcTfSesiones h WHERE h.fechaSesion = :fechaSesion")
    , @NamedQuery(name = "HcTfSesiones.findByIdPaciente", query = "SELECT h FROM HcTfSesiones h WHERE h.idPaciente = :idPaciente")})
public class HcTfSesiones implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    //
    @Basic(optional = false)
    @Column(name = "fecha_reg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReg;
    @Column(name = "realizado")
    private Boolean realizado;
    @Column(name = "num_sesion")
    private Integer numSesion;
    @Column(name = "fecha_sesion")
    @Temporal(TemporalType.DATE)
    private Date fechaSesion;
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "id_tratamiento")
    private Long idTratamiento;
    @Column(name = "notas")
    private String notas;

    @JoinColumn(name = "id_medico", referencedColumnName = "id_usuario")
    @ManyToOne
    private CnUsuarios idMedico;
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente")
    @ManyToOne
    private CnPacientes idPaciente;

    public HcTfSesiones() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Boolean getRealizado() {
        return realizado;
    }

    public void setRealizado(Boolean realizado) {
        this.realizado = realizado;
    }

    public Integer getNumSesion() {
        return numSesion;
    }

    public void setNumSesion(Integer numSesion) {
        this.numSesion = numSesion;
    }

    public Date getFechaSesion() {
        return fechaSesion;
    }

    public void setFechaSesion(Date fechaSesion) {
        this.fechaSesion = fechaSesion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Long idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public CnUsuarios getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(CnUsuarios idMedico) {
        this.idMedico = idMedico;
    }

    public CnPacientes getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(CnPacientes idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    
}
