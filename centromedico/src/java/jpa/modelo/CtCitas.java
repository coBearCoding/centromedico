/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "ct_citas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtCitas.findAll", query = "SELECT c FROM CtCitas c")
    , @NamedQuery(name = "CtCitas.findByIdCita", query = "SELECT c FROM CtCitas c WHERE c.idCita = :idCita")
    , @NamedQuery(name = "CtCitas.findByAtendida", query = "SELECT c FROM CtCitas c WHERE c.atendida = :atendida")
    , @NamedQuery(name = "CtCitas.findByCancelada", query = "SELECT c FROM CtCitas c WHERE c.cancelada = :cancelada")

    , @NamedQuery(name = "CtCitas.findByDescCancelacion", query = "SELECT c FROM CtCitas c WHERE c.descCancelacion = :descCancelacion")
    , @NamedQuery(name = "CtCitas.findByFechaRegistro", query = "SELECT c FROM CtCitas c WHERE c.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "CtCitas.findByFechaCancelacion", query = "SELECT c FROM CtCitas c WHERE c.fechaCancelacion = :fechaCancelacion")
    , @NamedQuery(name = "CtCitas.findBySubsecuente", query = "SELECT c FROM CtCitas c WHERE c.subsecuente = :subsecuente")

    , @NamedQuery(name = "CtCitas.findByTieneRegAsociado", query = "SELECT c FROM CtCitas c WHERE c.tieneRegAsociado = :tieneRegAsociado")
})
public class CtCitas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cita")
    private Integer idCita;
    @Column(name = "atendida")
    private Boolean atendida;
    @Column(name = "cancelada")
    private Boolean cancelada;

    @Size(max = 2147483647)
    @Column(name = "desc_cancelacion")
    private String descCancelacion;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date fechaRegistro;
    @Column(name = "fecha_cancelacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCancelacion;
    @Column(name = "subsecuente")
    private Boolean subsecuente;
    @Column(name = "fecha_hora_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraRegistro;

    @Column(name = "tiene_reg_asociado")
    private Boolean tieneRegAsociado;
    @Column(name = "id_usuario_registro")
    private Integer idUsuarioRegistro;
    @Column(name = "id_usuario_cancelacion")
    private Integer idUsuarioCancelacion;
    @Column(name = "id_archivo_tf")
    private Long idArchivoTf;
    @Column(name = "es_fisioterapia")
    private boolean esFisioterpia;
    @Column(name = "cant_terapias")
    private Integer cantTerapias;

    @JoinColumn(name = "motivo_cancelacion", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones motivoCancelacion;
    @JoinColumn(name = "tipo_cita", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoCita;
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente")
    @ManyToOne(optional = false)
    private CnPacientes idPaciente;
    @JoinColumn(name = "id_medico", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private CnUsuarios idMedico;

    @JoinColumn(name = "id_turno", referencedColumnName = "id_turno")
    @ManyToOne(optional = false)
    private CtTurnos idTurno;

    public CtCitas() {
    }

    public CtCitas(Integer idCita) {
        this.idCita = idCita;
    }

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public Boolean getAtendida() {
        return atendida;
    }

    public void setAtendida(Boolean atendida) {
        this.atendida = atendida;
    }

    public Boolean getCancelada() {
        return cancelada;
    }

    public void setCancelada(Boolean cancelada) {
        this.cancelada = cancelada;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getIdUsuarioCancelacion() {
        return idUsuarioCancelacion;
    }

    public void setIdUsuarioCancelacion(Integer idUsuarioCancelacion) {
        this.idUsuarioCancelacion = idUsuarioCancelacion;
    }

    public String getDescCancelacion() {
        return descCancelacion;
    }

    public void setDescCancelacion(String descCancelacion) {
        this.descCancelacion = descCancelacion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public Boolean getSubsecuente() {
        return subsecuente;
    }

    public void setSubsecuente(Boolean subsecuente) {
        this.subsecuente = subsecuente;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Boolean getTieneRegAsociado() {
        return tieneRegAsociado;
    }

    public void setTieneRegAsociado(Boolean tieneRegAsociado) {
        this.tieneRegAsociado = tieneRegAsociado;
    }

    public CnClasificaciones getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(CnClasificaciones motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public CnClasificaciones getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(CnClasificaciones tipoCita) {
        this.tipoCita = tipoCita;
    }

    public CnPacientes getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(CnPacientes idPaciente) {
        this.idPaciente = idPaciente;
    }

    public CnUsuarios getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(CnUsuarios idMedico) {
        this.idMedico = idMedico;
    }

    public CtTurnos getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(CtTurnos idTurno) {
        this.idTurno = idTurno;
    }

    public Long getIdArchivoTf() {
        return idArchivoTf;
    }

    public void setIdArchivoTf(Long idArchivoTf) {
        this.idArchivoTf = idArchivoTf;
    }

    public boolean isEsFisioterpia() {
        return esFisioterpia;
    }

    public void setEsFisioterpia(boolean esFisioterpia) {
        this.esFisioterpia = esFisioterpia;
    }

    public Integer getCantTerapias() {
        return cantTerapias;
    }

    public void setCantTerapias(Integer cantTerapias) {
        this.cantTerapias = cantTerapias;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCita != null ? idCita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtCitas)) {
            return false;
        }
        CtCitas other = (CtCitas) object;
        if ((this.idCita == null && other.idCita != null) || (this.idCita != null && !this.idCita.equals(other.idCita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CtCitas[ idCita=" + idCita + " ]";
    }

}
