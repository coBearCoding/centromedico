/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "hc_tf_sesiones_historial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcTfSesionesHistorial.findAll", query = "SELECT h FROM HcTfSesionesHistorial h")
    , @NamedQuery(name = "HcTfSesionesHistorial.findById", query = "SELECT h FROM HcTfSesionesHistorial h WHERE h.id = :id")
    , @NamedQuery(name = "HcTfSesionesHistorial.findByIdTratamiento", query = "SELECT h FROM HcTfSesionesHistorial h WHERE h.idTratamiento = :idTratamiento")
    , @NamedQuery(name = "HcTfSesionesHistorial.findByCodTratamientos", query = "SELECT h FROM HcTfSesionesHistorial h WHERE h.codTratamientos = :codTratamientos")})
public class HcTfSesionesHistorial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "num_sesion")
    private Integer numSesion;
    @Column(name = "id_tratamiento")
    private Long idTratamiento;    
    @Column(name = "cod_tratamientos")
    private Integer codTratamientos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumSesion() {
        return numSesion;
    }

    public void setNumSesion(Integer numSesion) {
        this.numSesion = numSesion;
    }

    public Long getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Long idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public Integer getCodTratamientos() {
        return codTratamientos;
    }

    public void setCodTratamientos(Integer codTratamientos) {
        this.codTratamientos = codTratamientos;
    }

   
    
}
