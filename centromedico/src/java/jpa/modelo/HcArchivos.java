
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "hc_archivos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcArchivos.findAll", query = "SELECT h FROM HcArchivos h")
    , @NamedQuery(name = "HcArchivos.findByUrlFile", query = "SELECT h FROM HcArchivos h WHERE h.urlFile = :urlFile")
    , @NamedQuery(name = "HcArchivos.findByFechaUpload", query = "SELECT h FROM HcArchivos h WHERE h.fechaUpload = :fechaUpload")
    , @NamedQuery(name = "HcArchivos.findByDescripcion", query = "SELECT h FROM HcArchivos h WHERE h.descripcion = :descripcion")
    , @NamedQuery(name = "HcArchivos.findByIdArchivo", query = "SELECT h FROM HcArchivos h WHERE h.idArchivo = :idArchivo")})
public class HcArchivos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "url_file")
    private String urlFile;
    @Column(name = "fecha_upload")
    @Temporal(TemporalType.DATE)
    private Date fechaUpload;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "usuario_upload")
    private Integer usuarioUpload;
    @Column(name = "es_fisioterapia")
    private boolean esFisioterapia;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_archivo")
    private Long idArchivo;

    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente")
    @ManyToOne
    private CnPacientes idPaciente;

    public HcArchivos() {
    }

    public HcArchivos(Long idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public Date getFechaUpload() {
        return fechaUpload;
    }

    public void setFechaUpload(Date fechaUpload) {
        this.fechaUpload = fechaUpload;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Long idArchivo) {
        this.idArchivo = idArchivo;
    }

    public CnPacientes getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(CnPacientes idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Integer getUsuarioUpload() {
        return usuarioUpload;
    }

    public void setUsuarioUpload(Integer usuarioUpload) {
        this.usuarioUpload = usuarioUpload;
    }

    public boolean isEsFisioterapia() {
        return esFisioterapia;
    }

    public void setEsFisioterapia(boolean esFisioterapia) {
        this.esFisioterapia = esFisioterapia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArchivo != null ? idArchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HcArchivos)) {
            return false;
        }
        HcArchivos other = (HcArchivos) object;
        if ((this.idArchivo == null && other.idArchivo != null) || (this.idArchivo != null && !this.idArchivo.equals(other.idArchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.HcArchivos[ idArchivo=" + idArchivo + " ]";
    }

}
