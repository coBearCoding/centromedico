/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author kenlly
 */
@Embeddable
public class CnDiasNoLaboralesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_no_laboral")
    @Temporal(TemporalType.DATE)
    private Date fechaNoLaboral;

    public CnDiasNoLaboralesPK() {
    }

    public CnDiasNoLaboralesPK(int id, Date fechaNoLaboral) {
        this.id = id;
        this.fechaNoLaboral = fechaNoLaboral;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaNoLaboral() {
        return fechaNoLaboral;
    }

    public void setFechaNoLaboral(Date fechaNoLaboral) {
        this.fechaNoLaboral = fechaNoLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (fechaNoLaboral != null ? fechaNoLaboral.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnDiasNoLaboralesPK)) {
            return false;
        }
        CnDiasNoLaboralesPK other = (CnDiasNoLaboralesPK) object;
        if (this.id != other.id) {
            return false;
        }
        if ((this.fechaNoLaboral == null && other.fechaNoLaboral != null) || (this.fechaNoLaboral != null && !this.fechaNoLaboral.equals(other.fechaNoLaboral))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnDiasNoLaboralesPK[ id=" + id + ", fechaNoLaboral=" + fechaNoLaboral + " ]";
    }
    
}
