/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_cie10")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnCie10.findAll", query = "SELECT c FROM CnCie10 c"),
    @NamedQuery(name = "CnCie10.findByCodigo", query = "SELECT c FROM CnCie10 c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "CnCie10.findByDescripcion", query = "SELECT c FROM CnCie10 c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CnCie10.findByClase", query = "SELECT c FROM CnCie10 c WHERE c.clase = :clase"),
    @NamedQuery(name = "CnCie10.findByEstado", query = "SELECT c FROM CnCie10 c WHERE c.estado = :estado"),
    @NamedQuery(name = "CnCie10.findBySecuencia", query = "SELECT c FROM CnCie10 c WHERE c.secuencia = :secuencia")})
public class CnCie10 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 2147483647)
    @Column(name = "clase")
    private String clase;
    @Size(max = 2147483647)
    @Column(name = "estado")
    private String estado;
    @Size(max = 2147483647)
    @Column(name = "secuencia")
    private String secuencia;

    public CnCie10() {
    }

    public CnCie10(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnCie10)) {
            return false;
        }
        CnCie10 other = (CnCie10) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnCie10[ codigo=" + codigo + " ]";
    }
    
}
