/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_consultorios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnConsultorios.findAll", query = "SELECT c FROM CnConsultorios c")
    , @NamedQuery(name = "CnConsultorios.findByIdConsultorio", query = "SELECT c FROM CnConsultorios c WHERE c.idConsultorio = :idConsultorio")
    , @NamedQuery(name = "CnConsultorios.findByCodConsultorio", query = "SELECT c FROM CnConsultorios c WHERE c.codConsultorio = :codConsultorio")
    , @NamedQuery(name = "CnConsultorios.findByNomConsultorio", query = "SELECT c FROM CnConsultorios c WHERE c.nomConsultorio = :nomConsultorio")})
public class CnConsultorios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_consultorio")
    private Integer idConsultorio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "cod_consultorio")
    private String codConsultorio;
    @Size(max = 2147483647)
    @Column(name = "nom_consultorio")
    private String nomConsultorio;
    @JoinColumn(name = "cod_especialidad", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones codEspecialidad;   
    @OneToMany(mappedBy = "idConsultorio")
    private List<CtTurnos> ctTurnosList;

    public CnConsultorios() {
    }

    public CnConsultorios(Integer idConsultorio) {
        this.idConsultorio = idConsultorio;
    }

    public CnConsultorios(Integer idConsultorio, String codConsultorio) {
        this.idConsultorio = idConsultorio;
        this.codConsultorio = codConsultorio;
    }

    public Integer getIdConsultorio() {
        return idConsultorio;
    }

    public void setIdConsultorio(Integer idConsultorio) {
        this.idConsultorio = idConsultorio;
    }

    public String getCodConsultorio() {
        return codConsultorio;
    }

    public void setCodConsultorio(String codConsultorio) {
        this.codConsultorio = codConsultorio;
    }

    public String getNomConsultorio() {
        return nomConsultorio;
    }

    public void setNomConsultorio(String nomConsultorio) {
        this.nomConsultorio = nomConsultorio;
    }  

    public CnClasificaciones getCodEspecialidad() {
        return codEspecialidad;
    }

    public void setCodEspecialidad(CnClasificaciones codEspecialidad) {
        this.codEspecialidad = codEspecialidad;
    }

    public List<CtTurnos> getCtTurnosList() {
        return ctTurnosList;
    }

    public void setCtTurnosList(List<CtTurnos> ctTurnosList) {
        this.ctTurnosList = ctTurnosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConsultorio != null ? idConsultorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnConsultorios)) {
            return false;
        }
        CnConsultorios other = (CnConsultorios) object;
        if ((this.idConsultorio == null && other.idConsultorio != null) || (this.idConsultorio != null && !this.idConsultorio.equals(other.idConsultorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnConsultorios[ idConsultorio=" + idConsultorio + " ]";
    }
    
}
