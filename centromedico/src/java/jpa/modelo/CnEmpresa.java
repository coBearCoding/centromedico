/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnEmpresa.findAll", query = "SELECT c FROM CnEmpresa c")
    , @NamedQuery(name = "CnEmpresa.findByCodEmpresa", query = "SELECT c FROM CnEmpresa c WHERE c.codEmpresa = :codEmpresa")
    , @NamedQuery(name = "CnEmpresa.findByNumIdentificacion", query = "SELECT c FROM CnEmpresa c WHERE c.numIdentificacion = :numIdentificacion")
    
    , @NamedQuery(name = "CnEmpresa.findByRazonSocial", query = "SELECT c FROM CnEmpresa c WHERE c.razonSocial = :razonSocial")
    , @NamedQuery(name = "CnEmpresa.findByNombreComercial", query = "SELECT c FROM CnEmpresa c WHERE c.nombreComercial = :nombreComercial")
    
    , @NamedQuery(name = "CnEmpresa.findByDireccion", query = "SELECT c FROM CnEmpresa c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "CnEmpresa.findByTelefono1", query = "SELECT c FROM CnEmpresa c WHERE c.telefono1 = :telefono1")
    , @NamedQuery(name = "CnEmpresa.findByTelefono2", query = "SELECT c FROM CnEmpresa c WHERE c.telefono2 = :telefono2")
    
    , @NamedQuery(name = "CnEmpresa.findByObservaciones", query = "SELECT c FROM CnEmpresa c WHERE c.observaciones = :observaciones")
    
    })
public class CnEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_empresa")
    private Integer codEmpresa;
    @Size(max = 20)
    @Column(name = "num_identificacion")
    private String numIdentificacion;
   
    @Size(max = 200)
    @Column(name = "razon_social")
    private String razonSocial;
   
    @Size(max = 200)
    @Column(name = "nombre_comercial")
    private String nombreComercial;
    @Size(max = 200)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 10)
    @Column(name = "telefono_1")
    private String telefono1;
    @Size(max = 10)
    @Column(name = "telefono_2")
    private String telefono2;
    
    @Size(max = 2147483647)
    @Column(name = "observaciones")
    private String observaciones;
   
   
    @JoinColumn(name = "cod_Provincia", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones codProvincia;
    @JoinColumn(name = "cod_Canton", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones codCanton;
    @JoinColumn(name = "tipo_doc", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoDoc;
    @JoinColumn(name = "tipo_doc_rep_legal", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoDocRepLegal;
    @JoinColumn(name = "logo", referencedColumnName = "id")
    @ManyToOne
    private CnImagenes logo;

    public CnEmpresa() {
    }

    public CnEmpresa(Integer codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public Integer getCodEmpresa() {
        return codEmpresa;
    }

    public void setCodEmpresa(Integer codEmpresa) {
        this.codEmpresa = codEmpresa;
    }

    public String getNumIdentificacion() {
        return numIdentificacion;
    }

    public void setNumIdentificacion(String numIdentificacion) {
        this.numIdentificacion = numIdentificacion;
    }

   

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

  

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    

    public CnClasificaciones getCodProvincia() {
        return codProvincia;
    }

    public void setCodProvincia(CnClasificaciones codProvincia) {
        this.codProvincia = codProvincia;
    }

    public CnClasificaciones getCodCanton() {
        return codCanton;
    }

    public void setCodCanton(CnClasificaciones codCanton) {
        this.codCanton = codCanton;
    }

    public CnClasificaciones getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(CnClasificaciones tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public CnClasificaciones getTipoDocRepLegal() {
        return tipoDocRepLegal;
    }

    public void setTipoDocRepLegal(CnClasificaciones tipoDocRepLegal) {
        this.tipoDocRepLegal = tipoDocRepLegal;
    }

    public CnImagenes getLogo() {
        return logo;
    }

    public void setLogo(CnImagenes logo) {
        this.logo = logo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEmpresa != null ? codEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnEmpresa)) {
            return false;
        }
        CnEmpresa other = (CnEmpresa) object;
        if ((this.codEmpresa == null && other.codEmpresa != null) || (this.codEmpresa != null && !this.codEmpresa.equals(other.codEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnEmpresa[ codEmpresa=" + codEmpresa + " ]";
    }
    
}
