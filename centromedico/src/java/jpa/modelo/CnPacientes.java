/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnPacientes.findAll", query = "SELECT c FROM CnPacientes c")
    , @NamedQuery(name = "CnPacientes.findByIdPaciente", query = "SELECT c FROM CnPacientes c WHERE c.idPaciente = :idPaciente")
    , @NamedQuery(name = "CnPacientes.findByIdentificacion", query = "SELECT c FROM CnPacientes c WHERE c.identificacion = :identificacion")
    , @NamedQuery(name = "CnPacientes.findByPrimerNombre", query = "SELECT c FROM CnPacientes c WHERE c.primerNombre = :primerNombre")
    , @NamedQuery(name = "CnPacientes.findBySegundoNombre", query = "SELECT c FROM CnPacientes c WHERE c.segundoNombre = :segundoNombre")
    , @NamedQuery(name = "CnPacientes.findByPrimerApellido", query = "SELECT c FROM CnPacientes c WHERE c.primerApellido = :primerApellido")
    , @NamedQuery(name = "CnPacientes.findBySegundoApellido", query = "SELECT c FROM CnPacientes c WHERE c.segundoApellido = :segundoApellido")
    , @NamedQuery(name = "CnPacientes.findByFechaNacimiento", query = "SELECT c FROM CnPacientes c WHERE c.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "CnPacientes.findByBarrio", query = "SELECT c FROM CnPacientes c WHERE c.barrio = :barrio")
    , @NamedQuery(name = "CnPacientes.findByDireccion", query = "SELECT c FROM CnPacientes c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "CnPacientes.findByNacionalidad", query = "SELECT c FROM CnPacientes c WHERE c.nacionalidad = :nacionalidad")
    , @NamedQuery(name = "CnPacientes.findByNacionalidades", query = "SELECT c FROM CnPacientes c WHERE c.nacionalidades = :nacionalidades")
    , @NamedQuery(name = "CnPacientes.findByEmail", query = "SELECT c FROM CnPacientes c WHERE c.email = :email")
    , @NamedQuery(name = "CnPacientes.findByTelefono", query = "SELECT c FROM CnPacientes c WHERE c.telefono = :telefono")
    , @NamedQuery(name = "CnPacientes.findByTelefonofamiliar", query = "SELECT c FROM CnPacientes c WHERE c.telefonofamiliar = :telefonofamiliar")
    , @NamedQuery(name = "CnPacientes.findByCelular", query = "SELECT c FROM CnPacientes c WHERE c.celular = :celular")
    , @NamedQuery(name = "CnPacientes.findByResponsable", query = "SELECT c FROM CnPacientes c WHERE c.responsable = :responsable")
    
    , @NamedQuery(name = "CnPacientes.findByIdentificacionresponsable", query = "SELECT c FROM CnPacientes c WHERE c.identificacionresponsable = :identificacionresponsable")
    , @NamedQuery(name = "CnPacientes.findByTelefonoResponsable", query = "SELECT c FROM CnPacientes c WHERE c.telefonoResponsable = :telefonoResponsable")
    , @NamedQuery(name = "CnPacientes.findByAcompanante", query = "SELECT c FROM CnPacientes c WHERE c.acompanante = :acompanante")
    , @NamedQuery(name = "CnPacientes.findByTelefonoAcompanante", query = "SELECT c FROM CnPacientes c WHERE c.telefonoAcompanante = :telefonoAcompanante")
    , @NamedQuery(name = "CnPacientes.findByParentescoA", query = "SELECT c FROM CnPacientes c WHERE c.parentescoA = :parentescoA")
    , @NamedQuery(name = "CnPacientes.findByHistoria", query = "SELECT c FROM CnPacientes c WHERE c.historia = :historia")
    
    , @NamedQuery(name = "CnPacientes.findByObservaciones", query = "SELECT c FROM CnPacientes c WHERE c.observaciones = :observaciones")
    
    , @NamedQuery(name = "CnPacientes.findBySemanagestacion", query = "SELECT c FROM CnPacientes c WHERE c.semanagestacion = :semanagestacion")
    , @NamedQuery(name = "CnPacientes.findByFechainsc", query = "SELECT c FROM CnPacientes c WHERE c.fechainsc = :fechainsc")
   })
public class CnPacientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_paciente")
    private Integer idPaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "identificacion")
    private String identificacion;
    @Size(max = 30)
    @Column(name = "primer_nombre")
    private String primerNombre;
    @Size(max = 30)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Size(max = 30)
    @Column(name = "primer_apellido")
    private String primerApellido;
    @Size(max = 30)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Size(max = 2147483647)
    @Column(name = "barrio")
    private String barrio;
    @Size(max = 80)
    @Column(name = "direccion")
    private String direccion;
//    @Column(name = "nacionalidad")
//    private Integer nacionalidad;
//    @Column(name = "nacionalidades")
//    private Integer nacionalidades;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 300)
    @Column(name = "email")
    private String email;
    @Size(max = 100)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 20)
    @Column(name = "telefonofamiliar")
    private String telefonofamiliar;
    @Size(max = 100)
    @Column(name = "celular")
    private String celular;
    @Size(max = 50)
    @Column(name = "responsable")
    private String responsable;
//    @Column(name = "tipo_identificacionresponsable")
//    private Integer tipoIdentificacionresponsable;
    
    @Column(name = "identificacionresponsable")
    private String identificacionresponsable;
    @Size(max = 20)
    @Column(name = "telefono_responsable")
    private String telefonoResponsable;
    @Size(max = 50)
    @Column(name = "acompanante")
    private String acompanante;
    @Size(max = 100)
    @Column(name = "telefono_acompanante")
    private String telefonoAcompanante;
//    @Column(name = "parentesco_a")
//    private Integer parentescoA;
    @Size(max = 15)
    @Column(name = "historia")
    private String historia;
    @Size(max = 2147483647)
    @Column(name = "observaciones")
    private String observaciones;
//    @Column(name = "id_gestacion")
//    private Integer idGestacion;
    @Size(max = 50)
    @Column(name = "semanagestacion")
    private String semanagestacion;
    @Column(name = "fechainsc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainsc;
    @Column(name = "usuario_creacion")
    private Integer usuarioCreacion;
    @Column(name = "usuario_editar")
    private Integer usuarioEditar;
    @Column(name = "fecha_editar")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEditar;
   
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones nacionalidad;
    @JoinColumn(name = "nacionalidades", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones nacionalidades;
    @JoinColumn(name = "id_gestacion", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones gestacion;
    @JoinColumn(name = "parentesco", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones parentesco;
    @JoinColumn(name = "canton", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones canton;
    @JoinColumn(name = "estado_civil", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones estadoCivil;
    @JoinColumn(name = "etnia", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones etnia;
    @JoinColumn(name = "grupo_sanguineo", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones grupoSanguineo;
    @JoinColumn(name = "identidadgenero", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones identidadgenero;
    @JoinColumn(name = "tipo_identificacion", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoIdentificacion;
    @JoinColumn(name = "tipo_identificacionresponsable", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoIdentificacionrepresentante;
    @JoinColumn(name = "orientacionsexual", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones orientacionsexual;
    @JoinColumn(name = "parroquia", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones parroquia;
    @JoinColumn(name = "provincia", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones provincia;
    @JoinColumn(name = "sexo", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones sexo;
    @JoinColumn(name = "tipo_afiliado", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoAfiliado;
    @JoinColumn(name = "parentesco_a", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones parentescoA;
    
    @OneToMany(mappedBy = "idPaciente")
    private Set<HcArchivos> hcArchivosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPaciente")
    private Set<CtCitas> ctCitasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnPacientes")
    private Set<CnGruposprioritarios> cnGruposprioritariosCollection;
   
    
    @Transient
    String edad; 

    public CnPacientes() {
    }

    public CnPacientes(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public CnPacientes(Integer idPaciente, String identificacion) {
        this.idPaciente = idPaciente;
        this.identificacion = identificacion;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(Integer usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Integer getUsuarioEditar() {
        return usuarioEditar;
    }

    public void setUsuarioEditar(Integer usuarioEditar) {
        this.usuarioEditar = usuarioEditar;
    }

    public Date getFechaEditar() {
        return fechaEditar;
    }

    public void setFechaEditar(Date fechaEditar) {
        this.fechaEditar = fechaEditar;
    }

//    public Integer getNacionalidad() {
//        return nacionalidad;
//    }
//
//    public void setNacionalidad(Integer nacionalidad) {
//        this.nacionalidad = nacionalidad;
//    }
//
//    public Integer getNacionalidades() {
//        return nacionalidades;
//    }
//
//    public void setNacionalidades(Integer nacionalidades) {
//        this.nacionalidades = nacionalidades;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefonofamiliar() {
        return telefonofamiliar;
    }

    public void setTelefonofamiliar(String telefonofamiliar) {
        this.telefonofamiliar = telefonofamiliar;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

//    public Integer getTipoIdentificacionresponsable() {
//        return tipoIdentificacionresponsable;
//    }
//
//    public void setTipoIdentificacionresponsable(Integer tipoIdentificacionresponsable) {
//        this.tipoIdentificacionresponsable = tipoIdentificacionresponsable;
//    }

    public String getIdentificacionresponsable() {
        return identificacionresponsable;
    }

    public void setIdentificacionresponsable(String identificacionresponsable) {
        this.identificacionresponsable = identificacionresponsable;
    }

    public String getTelefonoResponsable() {
        return telefonoResponsable;
    }

    public void setTelefonoResponsable(String telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
    }

    public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

    public String getTelefonoAcompanante() {
        return telefonoAcompanante;
    }

    public void setTelefonoAcompanante(String telefonoAcompanante) {
        this.telefonoAcompanante = telefonoAcompanante;
    }

//    public Integer getParentescoA() {
//        return parentescoA;
//    }
//
//    public void setParentescoA(Integer parentescoA) {
//        this.parentescoA = parentescoA;
//    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

   
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

//    public Integer getIdGestacion() {
//        return idGestacion;
//    }
//
//    public void setIdGestacion(Integer idGestacion) {
//        this.idGestacion = idGestacion;
//    }

    public String getSemanagestacion() {
        return semanagestacion;
    }

    public void setSemanagestacion(String semanagestacion) {
        this.semanagestacion = semanagestacion;
    }

    public Date getFechainsc() {
        return fechainsc;
    }

    public void setFechainsc(Date fechainsc) {
        this.fechainsc = fechainsc;
    }

   

    public CnClasificaciones getTipoIdentificacionrepresentante() {
        return tipoIdentificacionrepresentante;
    }

    public void setTipoIdentificacionrepresentante(CnClasificaciones tipoIdentificacionrepresentante) {
        this.tipoIdentificacionrepresentante = tipoIdentificacionrepresentante;
    }

    public CnClasificaciones getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(CnClasificaciones nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public CnClasificaciones getNacionalidades() {
        return nacionalidades;
    }

    public void setNacionalidades(CnClasificaciones nacionalidades) {
        this.nacionalidades = nacionalidades;
    }

    public CnClasificaciones getGestacion() {
        return gestacion;
    }

    public void setGestacion(CnClasificaciones gestacion) {
        this.gestacion = gestacion;
    }

    public CnClasificaciones getParentesco() {
        return parentesco;
    }

    public void setParentesco(CnClasificaciones parentesco) {
        this.parentesco = parentesco;
    }

    public CnClasificaciones getParentescoA() {
        return parentescoA;
    }

    public void setParentescoA(CnClasificaciones parentescoA) {
        this.parentescoA = parentescoA;
    }

    public CnClasificaciones getCanton() {
        return canton;
    }

    public void setCanton(CnClasificaciones canton) {
        this.canton = canton;
    }

    public CnClasificaciones getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(CnClasificaciones estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public CnClasificaciones getEtnia() {
        return etnia;
    }

    public void setEtnia(CnClasificaciones etnia) {
        this.etnia = etnia;
    }

    public CnClasificaciones getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public void setGrupoSanguineo(CnClasificaciones grupoSanguineo) {
        this.grupoSanguineo = grupoSanguineo;
    }

    public CnClasificaciones getIdentidadgenero() {
        return identidadgenero;
    }

    public void setIdentidadgenero(CnClasificaciones identidadgenero) {
        this.identidadgenero = identidadgenero;
    }

    public CnClasificaciones getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(CnClasificaciones tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public CnClasificaciones getOrientacionsexual() {
        return orientacionsexual;
    }

    public void setOrientacionsexual(CnClasificaciones orientacionsexual) {
        this.orientacionsexual = orientacionsexual;
    }

    public CnClasificaciones getParroquia() {
        return parroquia;
    }

    public void setParroquia(CnClasificaciones parroquia) {
        this.parroquia = parroquia;
    }

    public CnClasificaciones getProvincia() {
        return provincia;
    }

    public void setProvincia(CnClasificaciones provincia) {
        this.provincia = provincia;
    }

    public CnClasificaciones getSexo() {
        return sexo;
    }

    public void setSexo(CnClasificaciones sexo) {
        this.sexo = sexo;
    }

    public CnClasificaciones getTipoAfiliado() {
        return tipoAfiliado;
    }

    public void setTipoAfiliado(CnClasificaciones tipoAfiliado) {
        this.tipoAfiliado = tipoAfiliado;
    }
    
    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }
    
    public String nombreCompleto() {
        String strNombre = "";
        if (primerNombre != null) {
            strNombre = strNombre + primerNombre + " ";
        }
        if (segundoNombre != null) {
            strNombre = strNombre + segundoNombre + " ";
        }
        if (primerApellido != null) {
            strNombre = strNombre + primerApellido + " ";
        }
        if (segundoApellido != null) {
            strNombre = strNombre + segundoApellido;
        }
        return strNombre;
    }

    public String getNombreCompleto() {
        String strNombre = "";
        if (primerNombre != null) {
            strNombre = strNombre + primerNombre + " ";
        }
        if (segundoNombre != null) {
            strNombre = strNombre + segundoNombre + " ";
        }
        if (primerApellido != null) {
            strNombre = strNombre + primerApellido + " ";
        }
        if (segundoApellido != null) {
            strNombre = strNombre + segundoApellido;
        }
        return strNombre.toUpperCase();
    }
    @XmlTransient
    public Set<CnGruposprioritarios> getCnGruposprioritariosCollection() {
        return cnGruposprioritariosCollection;
    }

    public void setCnGruposprioritariosCollection(Set<CnGruposprioritarios> cnGruposprioritariosCollection) {
        this.cnGruposprioritariosCollection = cnGruposprioritariosCollection;
    }
    
    @XmlTransient
    public Set<HcArchivos> getHcArchivosList() {
        return hcArchivosList;
    }

    

    public void setHcArchivosList(Set<HcArchivos> hcArchivosList) {
        this.hcArchivosList = hcArchivosList;
    }

    @XmlTransient
    public Set<CtCitas> getCtCitasList() {
        return ctCitasList;
    }

    public void setCtCitasList(Set<CtCitas> ctCitasList) {
        this.ctCitasList = ctCitasList;
    }

   
   

  @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPaciente != null ? idPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnPacientes)) {
            return false;
        }
        CnPacientes other = (CnPacientes) object;
        if ((this.idPaciente == null && other.idPaciente != null) || (this.idPaciente != null && !this.idPaciente.equals(other.idPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnPacientes[ idPaciente=" + idPaciente + " ]";
    }
}