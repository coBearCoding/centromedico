/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "hc_detalle_tr")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcDetalleTr.findAll", query = "SELECT h FROM HcDetalleTr h")
    , @NamedQuery(name = "HcDetalleTr.findById", query = "SELECT h FROM HcDetalleTr h WHERE h.id = :id")
    , @NamedQuery(name = "HcDetalleTr.findByIdRegistro", query = "SELECT h FROM HcDetalleTr h WHERE h.idRegistro = :idRegistro")
    , @NamedQuery(name = "HcDetalleTr.findByIdCampo", query = "SELECT h FROM HcDetalleTr h WHERE h.idCampo = :idCampo")
    , @NamedQuery(name = "HcDetalleTr.findByValor", query = "SELECT h FROM HcDetalleTr h WHERE h.valor = :valor")})
public class HcDetalleTr implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "id_campo")
    private String idCampo;
    @Column(name = "id_registro")
    private int idRegistro;
    @Column(name = "valor")
    private Float valor;

    public HcDetalleTr() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdCampo() {
        return idCampo;
    }

    public void setIdCampo(String idCampo) {
        this.idCampo = idCampo;
    }

    public int getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(int idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    /*@Override
    public int hashCode() {
    int hash = 0;
    hash += (hcDetallePK != null ? hcDetallePK.hashCode() : 0);
    return hash;
    }
    @Override
    public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof HcDetalleTr)) {
    return false;
    }
    HcDetalleTr other = (HcDetalleTr) object;
    if ((this.hcDetallePK == null && other.hcDetallePK != null) || (this.hcDetallePK != null && !this.hcDetallePK.equals(other.hcDetallePK))) {
    return false;
    }
    return true;
    }*/
    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    /*@Override
    public String toString() {
        return "jpa.modelo.HcDetalleTr[ hcDetallePK=" + hcDetallePK + " ]";
    }*/

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
