/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_clasificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnClasificaciones.findAll", query = "SELECT c FROM CnClasificaciones c")
    , @NamedQuery(name = "CnClasificaciones.findById", query = "SELECT c FROM CnClasificaciones c WHERE c.id = :id")
    , @NamedQuery(name = "CnClasificaciones.findByCodigo", query = "SELECT c FROM CnClasificaciones c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "CnClasificaciones.findByMaestro", query = "SELECT c FROM CnClasificaciones c WHERE c.maestro = :maestro")
    , @NamedQuery(name = "CnClasificaciones.findByDescripcion", query = "SELECT c FROM CnClasificaciones c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "CnClasificaciones.findByObservacion", query = "SELECT c FROM CnClasificaciones c WHERE c.observacion = :observacion")})
public class CnClasificaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 50)
    @Column(name = "maestro")
    private String maestro;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
   
    @OneToMany(mappedBy = "motivoCancelacion")
    private Set<CtCitas> ctCitasList;
    @OneToMany(mappedBy = "tipoCita")
    private Set<CtCitas> ctCitasList1;
    @OneToMany(mappedBy = "codProvincia")
    private Set<CnEmpresa> cnEmpresaList;
    @OneToMany(mappedBy = "codCanton")
    private Set<CnEmpresa> cnEmpresaList1;
    @OneToMany(mappedBy = "tipoDoc")
    private Set<CnEmpresa> cnEmpresaList2;
    @OneToMany(mappedBy = "tipoDocRepLegal")
    private Set<CnEmpresa> cnEmpresaList3;   
    @OneToMany(mappedBy = "especialidad")
    private Set<CnUsuarios> cnUsuariosList;
    @OneToMany(mappedBy = "genero")
    private Set<CnUsuarios> cnUsuariosList1;
    @OneToMany(mappedBy = "tipoUsuario")
    private Set<CnUsuarios> cnUsuariosList3;
    @OneToMany(mappedBy = "Provincia")
    private Set<CnUsuarios> cnUsuariosList4;
    @OneToMany(mappedBy = "Canton")
    private Set<CnUsuarios> cnUsuariosList5;
    @OneToMany(mappedBy = "tipoIdentificacion")
    private Set<CnUsuarios> cnUsuariosList6;
    @OneToMany(mappedBy = "parentesco")
    private Set<CnPacientes> cnPacientesSet;
    @OneToMany(mappedBy = "canton")
    private Set<CnPacientes> cnPacientesSet1;
    @OneToMany(mappedBy = "estadoCivil")
    private Set<CnPacientes> cnPacientesSet2;
    @OneToMany(mappedBy = "etnia")
    private Set<CnPacientes> cnPacientesSet3;
    @OneToMany(mappedBy = "grupoSanguineo")
    private Set<CnPacientes> cnPacientesSet4;
    @OneToMany(mappedBy = "identidadgenero")
    private Set<CnPacientes> cnPacientesSet5;
    @OneToMany(mappedBy = "tipoIdentificacion")
    private Set<CnPacientes> cnPacientesSet6;
    @OneToMany(mappedBy = "orientacionsexual")
    private Set<CnPacientes> cnPacientesSet7;
    @OneToMany(mappedBy = "parroquia")
    private Set<CnPacientes> cnPacientesSet8;
    @OneToMany(mappedBy = "provincia")
    private Set<CnPacientes> cnPacientesSet9;
    @OneToMany(mappedBy = "sexo")
    private Set<CnPacientes> cnPacientesSet10;
    @OneToMany(mappedBy = "tipoAfiliado")
    private Set<CnPacientes> cnPacientesSet11;
  
    @OneToMany(mappedBy = "codEspecialidad")
    private Set<CnConsultorios> cnConsultoriosList;
    
   
    
    public CnClasificaciones() {
    }

    public CnClasificaciones(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMaestro() {
        return maestro;
    }

    public void setMaestro(String maestro) {
        this.maestro = maestro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

     
    @XmlTransient
    public Set<CtCitas> getCtCitasList() {
        return ctCitasList;
    }

    public void setCtCitasList(Set<CtCitas> ctCitasList) {
        this.ctCitasList = ctCitasList;
    }

    @XmlTransient
    public Set<CtCitas> getCtCitasList1() {
        return ctCitasList1;
    }

    public void setCtCitasList1(Set<CtCitas> ctCitasList1) {
        this.ctCitasList1 = ctCitasList1;
    }

    @XmlTransient
    public Set<CnEmpresa> getCnEmpresaList() {
        return cnEmpresaList;
    }

    public void setCnEmpresaList(Set<CnEmpresa> cnEmpresaList) {
        this.cnEmpresaList = cnEmpresaList;
    }

    @XmlTransient
    public Set<CnEmpresa> getCnEmpresaList1() {
        return cnEmpresaList1;
    }

    public void setCnEmpresaList1(Set<CnEmpresa> cnEmpresaList1) {
        this.cnEmpresaList1 = cnEmpresaList1;
    }

    @XmlTransient
    public Set<CnEmpresa> getCnEmpresaList2() {
        return cnEmpresaList2;
    }

    public void setCnEmpresaList2(Set<CnEmpresa> cnEmpresaList2) {
        this.cnEmpresaList2 = cnEmpresaList2;
    }

    @XmlTransient
    public Set<CnEmpresa> getCnEmpresaList3() {
        return cnEmpresaList3;
    }

    public void setCnEmpresaList3(Set<CnEmpresa> cnEmpresaList3) {
        this.cnEmpresaList3 = cnEmpresaList3;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList() {
        return cnUsuariosList;
    }

    public void setCnUsuariosList(Set<CnUsuarios> cnUsuariosList) {
        this.cnUsuariosList = cnUsuariosList;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList1() {
        return cnUsuariosList1;
    }

    public void setCnUsuariosList1(Set<CnUsuarios> cnUsuariosList1) {
        this.cnUsuariosList1 = cnUsuariosList1;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList3() {
        return cnUsuariosList3;
    }

    public void setCnUsuariosList3(Set<CnUsuarios> cnUsuariosList3) {
        this.cnUsuariosList3 = cnUsuariosList3;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList4() {
        return cnUsuariosList4;
    }

    public void setCnUsuariosList4(Set<CnUsuarios> cnUsuariosList4) {
        this.cnUsuariosList4 = cnUsuariosList4;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList5() {
        return cnUsuariosList5;
    }

    public void setCnUsuariosList5(Set<CnUsuarios> cnUsuariosList5) {
        this.cnUsuariosList5 = cnUsuariosList5;
    }

    @XmlTransient
    public Set<CnUsuarios> getCnUsuariosList6() {
        return cnUsuariosList6;
    }

    public void setCnUsuariosList6(Set<CnUsuarios> cnUsuariosList6) {
        this.cnUsuariosList6 = cnUsuariosList6;
    }

  
  
    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet() {
        return cnPacientesSet;
    }

    public void setCnPacientesSet(Set<CnPacientes> cnPacientesSet) {
        this.cnPacientesSet = cnPacientesSet;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet1() {
        return cnPacientesSet1;
    }

    public void setCnPacientesSet1(Set<CnPacientes> cnPacientesSet1) {
        this.cnPacientesSet1 = cnPacientesSet1;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet2() {
        return cnPacientesSet2;
    }

    public void setCnPacientesSet2(Set<CnPacientes> cnPacientesSet2) {
        this.cnPacientesSet2 = cnPacientesSet2;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet3() {
        return cnPacientesSet3;
    }

    public void setCnPacientesSet3(Set<CnPacientes> cnPacientesSet3) {
        this.cnPacientesSet3 = cnPacientesSet3;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet4() {
        return cnPacientesSet4;
    }

    public void setCnPacientesSet4(Set<CnPacientes> cnPacientesSet4) {
        this.cnPacientesSet4 = cnPacientesSet4;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet5() {
        return cnPacientesSet5;
    }

    public void setCnPacientesSet5(Set<CnPacientes> cnPacientesSet5) {
        this.cnPacientesSet5 = cnPacientesSet5;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet6() {
        return cnPacientesSet6;
    }

    public void setCnPacientesSet6(Set<CnPacientes> cnPacientesSet6) {
        this.cnPacientesSet6 = cnPacientesSet6;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet7() {
        return cnPacientesSet7;
    }

    public void setCnPacientesSet7(Set<CnPacientes> cnPacientesSet7) {
        this.cnPacientesSet7 = cnPacientesSet7;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet8() {
        return cnPacientesSet8;
    }

    public void setCnPacientesSet8(Set<CnPacientes> cnPacientesSet8) {
        this.cnPacientesSet8 = cnPacientesSet8;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet9() {
        return cnPacientesSet9;
    }

    public void setCnPacientesSet9(Set<CnPacientes> cnPacientesSet9) {
        this.cnPacientesSet9 = cnPacientesSet9;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet10() {
        return cnPacientesSet10;
    }

    public void setCnPacientesSet10(Set<CnPacientes> cnPacientesSet10) {
        this.cnPacientesSet10 = cnPacientesSet10;
    }

    @XmlTransient
    public Set<CnPacientes> getCnPacientesSet11() {
        return cnPacientesSet11;
    }

    public void setCnPacientesSet11(Set<CnPacientes> cnPacientesSet11) {
        this.cnPacientesSet11 = cnPacientesSet11;
    }

    

    @XmlTransient
    public Set<CnConsultorios> getCnConsultoriosList() {
        return cnConsultoriosList;
    }

    public void setCnConsultoriosList(Set<CnConsultorios> cnConsultoriosList) {
        this.cnConsultoriosList = cnConsultoriosList;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnClasificaciones)) {
            return false;
        }
        CnClasificaciones other = (CnClasificaciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnClasificaciones[ id=" + id + " ]";
    }
    
}
