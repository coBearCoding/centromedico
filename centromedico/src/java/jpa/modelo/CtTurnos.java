/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "ct_turnos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtTurnos.findAll", query = "SELECT c FROM CtTurnos c")
    , @NamedQuery(name = "CtTurnos.findByIdTurno", query = "SELECT c FROM CtTurnos c WHERE c.idTurno = :idTurno")
    , @NamedQuery(name = "CtTurnos.findByFecha", query = "SELECT c FROM CtTurnos c WHERE c.fecha = :fecha")
    , @NamedQuery(name = "CtTurnos.findByHoraIni", query = "SELECT c FROM CtTurnos c WHERE c.horaIni = :horaIni")
    , @NamedQuery(name = "CtTurnos.findByHoraFin", query = "SELECT c FROM CtTurnos c WHERE c.horaFin = :horaFin")
    , @NamedQuery(name = "CtTurnos.findByEstado", query = "SELECT c FROM CtTurnos c WHERE c.estado = :estado")
    
    , @NamedQuery(name = "CtTurnos.findByConcurrencia", query = "SELECT c FROM CtTurnos c WHERE c.concurrencia = :concurrencia")
    , @NamedQuery(name = "CtTurnos.findByContador", query = "SELECT c FROM CtTurnos c WHERE c.contador = :contador")
    , @NamedQuery(name = "CtTurnos.findByFechaCreacion", query = "SELECT c FROM CtTurnos c WHERE c.fechaCreacion = :fechaCreacion")
    })
public class CtTurnos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_turno")
    private Integer idTurno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora_ini")
    @Temporal(TemporalType.TIME)
    private Date horaIni;
    @Column(name = "hora_fin")
    @Temporal(TemporalType.TIME)
    private Date horaFin;
    @Size(max = 2147483647)
    @Column(name = "estado")
    private String estado;
//    @Column(name = "id_medico")
//    private Integer idMedico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "concurrencia")
    private int concurrencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contador")
    private Integer contador;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Column(name = "usuario_creacion")
    private Integer usuarioCreacion;
//    @Column(name = "id_consultorio")
//    private Integer idConsultorio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTurno")
    private List<CtCitas> ctCitasList;
    @JoinColumn(name = "id_horario", referencedColumnName = "id_horario")
    @ManyToOne
    private CnHorario idHorario;
    @JoinColumn(name = "id_medico", referencedColumnName = "id_usuario")
    @ManyToOne
    private CnUsuarios idMedico;
    @JoinColumn(name = "id_consultorio", referencedColumnName = "id_consultorio")
    @ManyToOne
    private CnConsultorios idConsultorio;

    public CtTurnos() {
    }

    public CtTurnos(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public CtTurnos(Integer idTurno, Date fecha, Date horaIni, int concurrencia, Integer contador) {
        this.idTurno = idTurno;
        this.fecha = fecha;
        this.horaIni = horaIni;
        this.concurrencia = concurrencia;
        this.contador = contador;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(Date horaIni) {
        this.horaIni = horaIni;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

//    public Integer getIdMedico() {
//        return idMedico;
//    }
//
//    public void setIdMedico(Integer idMedico) {
//        this.idMedico = idMedico;
//    }

    public int getConcurrencia() {
        return concurrencia;
    }

    public void setConcurrencia(int concurrencia) {
        this.concurrencia = concurrencia;
    }

    public Integer getContador() {
        return contador;
    }

    public void setContador(Integer contador) {
        this.contador = contador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public CnUsuarios getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(CnUsuarios idMedico) {
        this.idMedico = idMedico;
    }

    public CnConsultorios getIdConsultorio() {
        return idConsultorio;
    }

//    public Integer getIdConsultorio() {
//        return idConsultorio;
//    }
//
//    public void setIdConsultorio(Integer idConsultorio) {
//        this.idConsultorio = idConsultorio;
//    }
    public void setIdConsultorio(CnConsultorios idConsultorio) {
        this.idConsultorio = idConsultorio;
    }

    public Integer getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(Integer usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    @XmlTransient
    public CnHorario getIdHorario() {
        return idHorario;
    }
     public void setIdHorario(CnHorario idHorario) {
        this.idHorario = idHorario;
    }

    public List<CtCitas> getCtCitasList() {
        return ctCitasList;
    }

    public void setCtCitasList(List<CtCitas> ctCitasList) {
        this.ctCitasList = ctCitasList;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTurno != null ? idTurno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtTurnos)) {
            return false;
        }
        CtTurnos other = (CtTurnos) object;
        if ((this.idTurno == null && other.idTurno != null) || (this.idTurno != null && !this.idTurno.equals(other.idTurno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CtTurnos[ idTurno=" + idTurno + " ]";
    }
    
}
