/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnUsuarios.findAll", query = "SELECT c FROM CnUsuarios c")
    , @NamedQuery(name = "CnUsuarios.findByIdUsuario", query = "SELECT c FROM CnUsuarios c WHERE c.idUsuario = :idUsuario")
    , @NamedQuery(name = "CnUsuarios.findByIdentificacion", query = "SELECT c FROM CnUsuarios c WHERE c.identificacion = :identificacion")
    , @NamedQuery(name = "CnUsuarios.findByClave", query = "SELECT c FROM CnUsuarios c WHERE c.clave = :clave")
    , @NamedQuery(name = "CnUsuarios.findByLoginUsuario", query = "SELECT c FROM CnUsuarios c WHERE c.loginUsuario = :loginUsuario")
    , @NamedQuery(name = "CnUsuarios.findByEstadoCuenta", query = "SELECT c FROM CnUsuarios c WHERE c.estadoCuenta = :estadoCuenta")
    , @NamedQuery(name = "CnUsuarios.findByObservacion", query = "SELECT c FROM CnUsuarios c WHERE c.observacion = :observacion")
    , @NamedQuery(name = "CnUsuarios.findByPrimerNombre", query = "SELECT c FROM CnUsuarios c WHERE c.primerNombre = :primerNombre")
    , @NamedQuery(name = "CnUsuarios.findBySegundoNombre", query = "SELECT c FROM CnUsuarios c WHERE c.segundoNombre = :segundoNombre")
    , @NamedQuery(name = "CnUsuarios.findByPrimerApellido", query = "SELECT c FROM CnUsuarios c WHERE c.primerApellido = :primerApellido")
    , @NamedQuery(name = "CnUsuarios.findBySegundoApellido", query = "SELECT c FROM CnUsuarios c WHERE c.segundoApellido = :segundoApellido")
    , @NamedQuery(name = "CnUsuarios.findByDireccion", query = "SELECT c FROM CnUsuarios c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "CnUsuarios.findByTelefonoResidencia", query = "SELECT c FROM CnUsuarios c WHERE c.telefonoResidencia = :telefonoResidencia")
    , @NamedQuery(name = "CnUsuarios.findByTelefonoOficina", query = "SELECT c FROM CnUsuarios c WHERE c.telefonoOficina = :telefonoOficina")
    , @NamedQuery(name = "CnUsuarios.findByCelular", query = "SELECT c FROM CnUsuarios c WHERE c.celular = :celular")
    , @NamedQuery(name = "CnUsuarios.findByEmail", query = "SELECT c FROM CnUsuarios c WHERE c.email = :email")
    , @NamedQuery(name = "CnUsuarios.findByCargoActual", query = "SELECT c FROM CnUsuarios c WHERE c.cargoActual = :cargoActual")
    , @NamedQuery(name = "CnUsuarios.findByRegistroProfesional", query = "SELECT c FROM CnUsuarios c WHERE c.registroProfesional = :registroProfesional")
    , @NamedQuery(name = "CnUsuarios.findByFormacionprofesional", query = "SELECT c FROM CnUsuarios c WHERE c.formacionprofesional = :formacionprofesional")
    , @NamedQuery(name = "CnUsuarios.findByCodMsp", query = "SELECT c FROM CnUsuarios c WHERE c.codmsp = :codmsp")
    , @NamedQuery(name = "CnUsuarios.findByNacionalidad", query = "SELECT c FROM CnUsuarios c WHERE c.nacionalidad = :nacionalidad")
    , @NamedQuery(name = "CnUsuarios.findByAutoidentificacion", query = "SELECT c FROM CnUsuarios c WHERE c.autoidentificacion = :autoidentificacion")
    , @NamedQuery(name = "CnUsuarios.findByFechaNacimiento", query = "SELECT c FROM CnUsuarios c WHERE c.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "CnUsuarios.findByFechaCreacion", query = "SELECT c FROM CnUsuarios c WHERE c.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "CnUsuarios.findByVisible", query = "SELECT c FROM CnUsuarios c WHERE c.visible = :visible")})
public class CnUsuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Size(max = 20)
    @Column(name = "identificacion")
    private String identificacion;
    @Size(max = 50)
    @Column(name = "clave")
    private String clave;
    @Size(max = 50)
    @Column(name = "login_usuario")
    private String loginUsuario;   
    @Size(max = 500)
    @Column(name = "observacion")
    private String observacion;
    @Size(max = 60)
    @Column(name = "primer_nombre")
    private String primerNombre;
    @Size(max = 60)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Size(max = 60)
    @Column(name = "primer_apellido")
    private String primerApellido;
    @Size(max = 60)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    @Size(max = 80)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 10)
    @Column(name = "telefono_residencia")
    private String telefonoResidencia;
    @Size(max = 10)
    @Column(name = "telefono_oficina")
    private String telefonoOficina;
    @Size(max = 20)
    @Column(name = "celular")
    private String celular;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 100)
    @Column(name = "cargo_actual")
    private String cargoActual;
    @Size(max = 30)
    @Column(name = "registro_profesional")
    private String registroProfesional;   
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "visible")
    private Boolean visible;
     @Column(name = "estado_cuenta")
    private Boolean estadoCuenta;
    @Column(name = "nacionalidad")
    private Integer nacionalidad;
    @Column(name = "autoidentificacion")
    private Integer autoidentificacion;
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Column(name = "formacionprofesional")
    private String formacionprofesional;
    @Column(name = "cod_msp")
    private String codmsp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMedico")
    private List<CtCitas> ctCitasList;
    
    @JoinColumn(name = "especialidad", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones especialidad;
    @JoinColumn(name = "genero", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones genero;
    @JoinColumn(name = "tipo_usuario", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoUsuario;
    @JoinColumn(name = "provincia", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones Provincia;
    @JoinColumn(name = "canton", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones Canton;
    @JoinColumn(name = "tipo_identificacion", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones tipoIdentificacion;    
    @JoinColumn(name = "id_perfil", referencedColumnName = "id_perfil")
    @ManyToOne(optional = false)
    private CnPerfilesUsuario idPerfil;
   
    @OneToMany(mappedBy = "idMedico")
    private List<CtTurnos> ctTurnosList;

    public CnUsuarios() {
    }

    public CnUsuarios(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

   
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoResidencia() {
        return telefonoResidencia;
    }

    public void setTelefonoResidencia(String telefonoResidencia) {
        this.telefonoResidencia = telefonoResidencia;
    }

    public String getTelefonoOficina() {
        return telefonoOficina;
    }

    public void setTelefonoOficina(String telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCargoActual() {
        return cargoActual;
    }

    public void setCargoActual(String cargoActual) {
        this.cargoActual = cargoActual;
    }

    public String getRegistroProfesional() {
        return registroProfesional;
    }

    public void setRegistroProfesional(String registroProfesional) {
        this.registroProfesional = registroProfesional;
    }

    public Integer getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Integer nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Integer getAutoidentificacion() {
        return autoidentificacion;
    }

    public void setAutoidentificacion(Integer autoidentificacion) {
        this.autoidentificacion = autoidentificacion;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

   

    public String getFormacionprofesional() {
        return formacionprofesional;
    }

    public void setFormacionprofesional(String formacionprofesional) {
        this.formacionprofesional = formacionprofesional;
    }

    public String getCodmsp() {
        return codmsp;
    }

    public void setCodmsp(String codmsp) {
        this.codmsp = codmsp;
    }

    public Boolean getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(Boolean estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }   

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

     public String nombreCompleto() {
        String strNombre = "";
        if (primerNombre != null) {
            strNombre = strNombre + primerNombre + " ";
        }
        if (segundoNombre != null) {
            strNombre = strNombre + segundoNombre + " ";
        }
        if (primerApellido != null) {
            strNombre = strNombre + primerApellido + " ";
        }
        if (segundoApellido != null) {
            strNombre = strNombre + segundoApellido;
        }
        return strNombre;
    }

      public String getNombreCompleto() {
        String strNombre = "";
        if (primerNombre != null) {
            strNombre = strNombre + primerNombre + " ";
        }
        if (segundoNombre != null) {
            strNombre = strNombre + segundoNombre + " ";
        }
        if (primerApellido != null) {
            strNombre = strNombre + primerApellido + " ";
        }
        if (segundoApellido != null) {
            strNombre = strNombre + segundoApellido;
        }
        return strNombre.toUpperCase();
    }
      
    @XmlTransient
    public List<CtCitas> getCtCitasList() {
        return ctCitasList;
    }

    public void setCtCitasList(List<CtCitas> ctCitasList) {
        this.ctCitasList = ctCitasList;
    }

   
    public List<CtTurnos> getCtTurnosList() {
        return ctTurnosList;
    }

    public void setCtTurnosList(List<CtTurnos> ctTurnosList) {
        this.ctTurnosList = ctTurnosList;
    }

    public CnClasificaciones getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(CnClasificaciones especialidad) {
        this.especialidad = especialidad;
    }

    public CnClasificaciones getGenero() {
        return genero;
    }

    public void setGenero(CnClasificaciones genero) {
        this.genero = genero;
    }

    public CnClasificaciones getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(CnClasificaciones tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public CnClasificaciones getProvincia() {
        return Provincia;
    }

    public void setProvincia(CnClasificaciones Provincia) {
        this.Provincia = Provincia;
    }

    public CnClasificaciones getCanton() {
        return Canton;
    }

    public void setCanton(CnClasificaciones Canton) {
        this.Canton = Canton;
    }

    public CnClasificaciones getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(CnClasificaciones tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

   

    public CnPerfilesUsuario getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(CnPerfilesUsuario idPerfil) {
        this.idPerfil = idPerfil;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnUsuarios)) {
            return false;
        }
        CnUsuarios other = (CnUsuarios) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnUsuarios[ idUsuario=" + idUsuario + " ]";
    }
    
}
