/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dante
 */
@Entity
@Table(name = "hc_tf_tratamientos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcTfTratamientos.findAll", query = "SELECT h FROM HcTfTratamientos h")
    , @NamedQuery(name = "HcTfTratamientos.findById", query = "SELECT h FROM HcTfTratamientos h WHERE h.id = :id")
    , @NamedQuery(name = "HcTfTratamientos.findByIdTratamiento", query = "SELECT h FROM HcTfTratamientos h WHERE h.idTratamiento = :idTratamiento")
    , @NamedQuery(name = "HcTfTratamientos.findByCodTratamientos", query = "SELECT h FROM HcTfTratamientos h WHERE h.codTratamientos = :codTratamientos")})
public class HcTfTratamientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "id_tratamiento")
    private Long idTratamiento;
    @Column(name = "activo")
    private Boolean activo;
    
    @JoinColumn(name = "cod_tratamientos", referencedColumnName = "id")
    @ManyToOne
    private CnClasificaciones codTratamientos;

    public HcTfTratamientos() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Long idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public CnClasificaciones getCodTratamientos() {
        return codTratamientos;
    }

    public void setCodTratamientos(CnClasificaciones codTratamientos) {
        this.codTratamientos = codTratamientos;
    }

}
