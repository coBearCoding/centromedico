/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_perfiles_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnPerfilesUsuario.findAll", query = "SELECT c FROM CnPerfilesUsuario c")
    , @NamedQuery(name = "CnPerfilesUsuario.findByIdPerfil", query = "SELECT c FROM CnPerfilesUsuario c WHERE c.idPerfil = :idPerfil")
    , @NamedQuery(name = "CnPerfilesUsuario.findByNombrePerfil", query = "SELECT c FROM CnPerfilesUsuario c WHERE c.nombrePerfil = :nombrePerfil")})
public class CnPerfilesUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_perfil")
    private Integer idPerfil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_perfil")
    private String nombrePerfil;    
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cnPerfilesUsuario")
//    private List<CnOpcionesMenuPerfilUsuario> cnOpcionesMenuPerfilUsuarioList;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "cn_opciones_menu_perfil_usuario", joinColumns = {
        @JoinColumn(name = "id_perfil_usuario")}, inverseJoinColumns = {
        @JoinColumn(name = "id_opcion_menu")})   
    private Set<CnOpcionesMenu> cnOpcionesMenuList;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "idPerfil")
    private List<CnUsuarios> cnUsuariosList;

    public CnPerfilesUsuario() {
    }

    public CnPerfilesUsuario(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public CnPerfilesUsuario(Integer idPerfil, String nombrePerfil) {
        this.idPerfil = idPerfil;
        this.nombrePerfil = nombrePerfil;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombrePerfil() {
        return nombrePerfil;
    }

    public void setNombrePerfil(String nombrePerfil) {
        this.nombrePerfil = nombrePerfil;
    }
   
    
    public Set<CnOpcionesMenu> getCnOpcionesMenuList() {
        return cnOpcionesMenuList;
    }

//    @XmlTransient
//    public List<CnOpcionesMenuPerfilUsuario> getCnOpcionesMenuPerfilUsuarioList() {
//        return cnOpcionesMenuPerfilUsuarioList;
//    }
//
//    public void setCnOpcionesMenuPerfilUsuarioList(List<CnOpcionesMenuPerfilUsuario> cnOpcionesMenuPerfilUsuarioList) {
//        this.cnOpcionesMenuPerfilUsuarioList = cnOpcionesMenuPerfilUsuarioList;
//    }
//      @XmlTransient
//      public List<CnOpcionesMenu> getCnOpcionesMenuList() {
//        return cnOpcionesMenuList;
//      }
//      
//      public void setCnOpcionesMenuList(List<CnOpcionesMenu> cnOpcionesMenuList) {
//        this.cnOpcionesMenuList = cnOpcionesMenuList;
//      }
    public void setCnOpcionesMenuList(Set<CnOpcionesMenu> cnOpcionesMenuList) {
        this.cnOpcionesMenuList = cnOpcionesMenuList;
    }

    @XmlTransient
    public List<CnUsuarios> getCnUsuariosList() {
        return cnUsuariosList;
    }

    public void setCnUsuariosList(List<CnUsuarios> cnUsuariosList) {
        this.cnUsuariosList = cnUsuariosList;
    }
        
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPerfil != null ? idPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnPerfilesUsuario)) {
            return false;
        }
        CnPerfilesUsuario other = (CnPerfilesUsuario) object;
        if ((this.idPerfil == null && other.idPerfil != null) || (this.idPerfil != null && !this.idPerfil.equals(other.idPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnPerfilesUsuario[ idPerfil=" + idPerfil + " ]";
        //return idPerfil + " - " + nombrePerfil;
    }
    
}
