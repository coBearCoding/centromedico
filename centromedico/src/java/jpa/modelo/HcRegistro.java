/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "hc_registro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HcRegistro.findAll", query = "SELECT h FROM HcRegistro h")
    , @NamedQuery(name = "HcRegistro.findByIdRegistro", query = "SELECT h FROM HcRegistro h WHERE h.idRegistro = :idRegistro")
    , @NamedQuery(name = "HcRegistro.findByFechaReg", query = "SELECT h FROM HcRegistro h WHERE h.fechaReg = :fechaReg")
    , @NamedQuery(name = "HcRegistro.findByFechaSis", query = "SELECT h FROM HcRegistro h WHERE h.fechaSis = :fechaSis")
    , @NamedQuery(name = "HcRegistro.findByIdCita", query = "SELECT h FROM HcRegistro h WHERE h.idCita = :idCita")
    , @NamedQuery(name = "HcRegistro.findByFolio", query = "SELECT h FROM HcRegistro h WHERE h.folio = :folio")})
public class HcRegistro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Column(name = "fecha_reg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReg;
    @Column(name = "fecha_sis")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSis;
    @Column(name = "id_cita")
    private Integer idCita;
    @Column(name = "folio")
    private Integer folio;
    @Column(name = "idfichamedica")
    private Integer idfichamedica;
    @Column(name = "id_usuario_editar")
    private Integer idUsuarioEditar;
    @Column(name = "fecha_editar")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEditar;
    @Column(name = "id_tratamiento")
    private Integer idtratamiento;
    @Column(name = "sesion")
    private Integer sesion;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hcRegistro",fetch = FetchType.EAGER)
    private Set<HcDetalle> hcDetalleList;
    @JoinColumn(name = "id_tipo_reg", referencedColumnName = "id_tipo_reg")
    @ManyToOne
    private HcTipoReg idTipoReg;
    @JoinColumn(name = "id_medico", referencedColumnName = "id_usuario")
    @ManyToOne
    private CnUsuarios idMedico;
    @JoinColumn(name = "id_paciente", referencedColumnName = "id_paciente")
    @ManyToOne
    private CnPacientes idPaciente;

    public HcRegistro() {
    }

    public HcRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Date getFechaSis() {
        return fechaSis;
    }

    public void setFechaSis(Date fechaSis) {
        this.fechaSis = fechaSis;
    }

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public HcTipoReg getIdTipoReg() {
        return idTipoReg;
    }

    public void setIdTipoReg(HcTipoReg idTipoReg) {
        this.idTipoReg = idTipoReg;
    }

    public CnUsuarios getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(CnUsuarios idMedico) {
        this.idMedico = idMedico;
    }

    public CnPacientes getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(CnPacientes idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Integer getIdfichamedica() {
        return idfichamedica;
    }

    public void setIdfichamedica(Integer idfichamedica) {
        this.idfichamedica = idfichamedica;
    }

    public Integer getIdUsuarioEditar() {
        return idUsuarioEditar;
    }

    public void setIdUsuarioEditar(Integer idUsuarioEditar) {
        this.idUsuarioEditar = idUsuarioEditar;
    }

    public Date getFechaEditar() {
        return fechaEditar;
    }

    public void setFechaEditar(Date fechaEditar) {
        this.fechaEditar = fechaEditar;
    }

    public Integer getIdtratamiento() {
        return idtratamiento;
    }

    public void setIdtratamiento(Integer idtratamiento) {
        this.idtratamiento = idtratamiento;
    }

    public Integer getSesion() {
        return sesion;
    }

    public void setSesion(Integer sesion) {
        this.sesion = sesion;
    }

   

    @XmlTransient
    public Set<HcDetalle> getHcDetalleList() {
        return hcDetalleList;
    }

    public void setHcDetalleList(Set<HcDetalle> hcDetalleList) {
        this.hcDetalleList = hcDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HcRegistro)) {
            return false;
        }
        HcRegistro other = (HcRegistro) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.HcRegistro[ idRegistro=" + idRegistro + " ]";
    }
    
}
