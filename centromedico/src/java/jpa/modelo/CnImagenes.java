/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_imagenes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnImagenes.findAll", query = "SELECT c FROM CnImagenes c")
    , @NamedQuery(name = "CnImagenes.findById", query = "SELECT c FROM CnImagenes c WHERE c.id = :id")
    , @NamedQuery(name = "CnImagenes.findByNombre", query = "SELECT c FROM CnImagenes c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "CnImagenes.findByNombreEnServidor", query = "SELECT c FROM CnImagenes c WHERE c.nombreEnServidor = :nombreEnServidor")
    , @NamedQuery(name = "CnImagenes.findByUrlImagen", query = "SELECT c FROM CnImagenes c WHERE c.urlImagen = :urlImagen")})
public class CnImagenes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "nombre_en_servidor")
    private String nombreEnServidor;
    @Size(max = 2147483647)
    @Column(name = "url_imagen")
    private String urlImagen;
    @OneToMany(mappedBy = "logo")
    private List<CnEmpresa> cnEmpresaList;
    
//    @OneToMany(mappedBy = "foto")
//    private List<CnPacientes> cnPacientesList;
//    @OneToMany(mappedBy = "firma")
//    private List<CnPacientes> cnPacientesList1;

    public CnImagenes() {
    }

    public CnImagenes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreEnServidor() {
        return nombreEnServidor;
    }

    public void setNombreEnServidor(String nombreEnServidor) {
        this.nombreEnServidor = nombreEnServidor;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @XmlTransient
    public List<CnEmpresa> getCnEmpresaList() {
        return cnEmpresaList;
    }

    public void setCnEmpresaList(List<CnEmpresa> cnEmpresaList) {
        this.cnEmpresaList = cnEmpresaList;
    }

   

//    @XmlTransient
//    public List<CnPacientes> getCnPacientesList() {
//        return cnPacientesList;
//    }
//
//    public void setCnPacientesList(List<CnPacientes> cnPacientesList) {
//        this.cnPacientesList = cnPacientesList;
//    }
//
//    @XmlTransient
//    public List<CnPacientes> getCnPacientesList1() {
//        return cnPacientesList1;
//    }
//
//    public void setCnPacientesList1(List<CnPacientes> cnPacientesList1) {
//        this.cnPacientesList1 = cnPacientesList1;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnImagenes)) {
            return false;
        }
        CnImagenes other = (CnImagenes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnImagenes[ id=" + id + " ]";
    }
    
}
