/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenlly
 */
@Entity
@Table(name = "cn_items_horario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CnItemsHorario.findAll", query = "SELECT c FROM CnItemsHorario c")
    , @NamedQuery(name = "CnItemsHorario.findByIdItemHorario", query = "SELECT c FROM CnItemsHorario c WHERE c.idItemHorario = :idItemHorario")
    , @NamedQuery(name = "CnItemsHorario.findByDia", query = "SELECT c FROM CnItemsHorario c WHERE c.dia = :dia")
    , @NamedQuery(name = "CnItemsHorario.findByHoraInicio", query = "SELECT c FROM CnItemsHorario c WHERE c.horaInicio = :horaInicio")
    , @NamedQuery(name = "CnItemsHorario.findByHoraFinal", query = "SELECT c FROM CnItemsHorario c WHERE c.horaFinal = :horaFinal")
    , @NamedQuery(name = "CnItemsHorario.findByNombredia", query = "SELECT c FROM CnItemsHorario c WHERE c.nombredia = :nombredia")})
public class CnItemsHorario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_item_horario")
    private Integer idItemHorario;
    @Column(name = "dia")
    private Short dia;
    @Column(name = "hora_inicio")
    @Temporal(TemporalType.TIME)
    private Date horaInicio;
    @Column(name = "hora_final")
    @Temporal(TemporalType.TIME)
    private Date horaFinal;
    @Size(max = 20)
    @Column(name = "nombredia")
    private String nombredia;
    @JoinColumn(name = "id_horario", referencedColumnName = "id_horario")
    @ManyToOne
    private CnHorario idHorario;

    public CnItemsHorario() {
    }

    public CnItemsHorario(Integer idItemHorario) {
        this.idItemHorario = idItemHorario;
    }

    public Integer getIdItemHorario() {
        return idItemHorario;
    }

    public void setIdItemHorario(Integer idItemHorario) {
        this.idItemHorario = idItemHorario;
    }

    public Short getDia() {
        return dia;
    }

    public void setDia(Short dia) {
        this.dia = dia;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Date horaFinal) {
        this.horaFinal = horaFinal;
    }

    public String getNombredia() {
        return nombredia;
    }

    public void setNombredia(String nombredia) {
        this.nombredia = nombredia;
    }

    public CnHorario getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(CnHorario idHorario) {
        this.idHorario = idHorario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idItemHorario != null ? idItemHorario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CnItemsHorario)) {
            return false;
        }
        CnItemsHorario other = (CnItemsHorario) object;
        if ((this.idItemHorario == null && other.idItemHorario != null) || (this.idItemHorario != null && !this.idItemHorario.equals(other.idItemHorario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.modelo.CnItemsHorario[ idItemHorario=" + idItemHorario + " ]";
    }
    
}
