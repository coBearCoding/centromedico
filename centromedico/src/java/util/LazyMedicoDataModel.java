
package util;

import org.primefaces.model.SortOrder;
import java.util.Map;
import jpa.modelo.CnUsuarios;
import java.util.List;
import jpa.facade.CnUsuariosFacade;
import org.primefaces.model.LazyDataModel;
/**
 *
 * @author kenlly
 */
public class LazyMedicoDataModel extends LazyDataModel<CnUsuarios> {

    private final CnUsuariosFacade cnUsuariosFacade;
    private List<CnUsuarios> datasource;

    public LazyMedicoDataModel(CnUsuariosFacade cnUsuariosFacade) {
        this.cnUsuariosFacade = cnUsuariosFacade;
    }

    @Override
    public Object getRowKey(CnUsuarios medico) {
        return medico.getIdUsuario();
    }
    
    @Override
    public CnUsuarios getRowData(String rowKey) {
        
        int intRowKey = Integer.parseInt(rowKey);
        return cnUsuariosFacade.find(intRowKey);
    }


    @Override
    public List<CnUsuarios> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        List<CnUsuarios> data = loadMedicos(first, pageSize, filters);

        
        int dataSize = data.size();

        
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }

    private List<CnUsuarios> loadMedicos(int offset, int limit, Map<String, Object> filters) {
        List<CnUsuarios> data;
        String sql = "";
        if (filters.isEmpty()) {
            String query = "SELECT p FROM CnUsuarios p WHERE p.tipoUsuario.codigo = '2' AND p.estadoCuenta = true";
            String queryCount = "SELECT COUNT (p) FROM CnUsuarios p WHERE p.tipoUsuario.codigo = '2' AND p.estadoCuenta = true";
            this.setRowCount(cnUsuariosFacade.totalMedicosLazy(queryCount, 0, ""));
            data = cnUsuariosFacade.findMedicosLazy(query, limit, offset, 0, "");
        } else {
            String medico = "";
            int especialidad = 0;
            int i = 0;
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                String filterProperty = entry.getKey();
                if (i > 0) {
                    sql += " AND";
                }
                switch (filterProperty) {
                    case "especialidad.id":
                        sql += " p.especialidad.id = ?1";
                        especialidad = Integer.parseInt(entry.getValue().toString());
                        break;
                    default:
                        sql += " (LOWER(p.primerNombre) LIKE ?3 OR LOWER(p.segundoNombre) LIKE ?3 OR LOWER(p.primerApellido) LIKE ?3 OR LOWER(p.segundoApellido) LIKE ?3 OR CONCAT(LOWER(p.primerNombre), ' ', LOWER(p.segundoNombre)) LIKE ?3 OR CONCAT(LOWER(p.primerApellido), ' ', LOWER(p.segundoApellido)) LIKE ?3 OR CONCAT(LOWER(p.primerNombre), ' ', LOWER(p.segundoNombre), ' ', LOWER(p.primerApellido), ' ', LOWER(p.segundoApellido)) LIKE ?3 OR CONCAT(LOWER(p.primerNombre), ' ', LOWER(p.segundoNombre), ' ', LOWER(p.primerApellido)) LIKE ?3 )";                        
                        medico = entry.getValue().toString().toLowerCase();
                        break;
                }
                i++;

            }
            String queryCount = "SELECT COUNT (p) FROM CnUsuarios p WHERE p.tipoUsuario.codigo = '2' AND p.estadoCuenta = true AND".concat(sql);
            this.setRowCount(cnUsuariosFacade.totalMedicosLazy(queryCount, especialidad, medico));
            String query = "SELECT p FROM CnUsuarios p WHERE p.tipoUsuario.codigo = '2' AND p.estadoCuenta = true AND".concat(sql);
            data = cnUsuariosFacade.findMedicosLazy(query, limit, offset, especialidad, medico);
        }
        return data;
    }
}

