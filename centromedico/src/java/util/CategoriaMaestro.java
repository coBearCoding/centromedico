
package util;

/**
 *
 * @author kenlly
 */
public enum CategoriaMaestro {
   
    CategoriaPaciente,//Ingresos en SMLMV
    CausaExterna,//Identificador de la causa externa que origina el servicio de salud"
    Canton,
    Provincia,//Provincia
    Enfermedades, //cie10
    Afiliado,//IESS, etc
    Especialidad,//Especialidades de médicos
    EstadoCivil,//Tipo de estado Civil
   
    Etnia,//Etnia    
    
    //Finalidad,//Finalidad del registro clinico"
    FinalidadConsulta,//Finalidad de una consulta"
    FinalidadProcedimiento,//Finalidad de un servicio"
    
    GrupoSanguineo,//Grupo Sanguíneo
   
    GrupoPrioritario,
    Icono,//Iconos de la aplicación
    MotiCancCitas,//Motivo cancelacion citas
    MotivoConsulta,//Mtivo de consulta
   
  
    Parentesco,//Parentezco familiar
   
    Sexo,
   
    TipoAfiliado,//Tipos de afiliados
    TipoConsulta,//Tipos de consultas
   
    TipoEdad,//Tipo de edad
   
    TipoIdentificacion,//Tipos de identificacion
 
    TipoUsuario,//Tipos de usuarios,medico,administrador..
    Zona,//Zonas urbana,rural,
   
    Gestacion,
    Parroquia,
    
    IdentidadGenero,
    OrigenAtencion, //Origen de Atención manejado por Formulario Anexo 2 Atencion de Urgencias 
    OrientacionSexual,
    //-----------------------------------------------------
    //no contenidas en cn_clasificaciones (son tablas independientes)
    //-----------------------------------------------------
    PerfilesUsuario, //                    permisos de un determinado usuario
   
    Medicos, //                        solo medicos    
    Usuarios, //                          usuarios y medicos
    //TipoRegistroClinico,//                  formatos de historas de la entidad
    TipoRegistroClinico,//                  formatos de historas de la entidad
   
    Meses,
  
    Nacionalidad,
    Nacionalidades,
   
    NOVALUE;

    public static CategoriaMaestro convert(String str) {
        try {
            return valueOf(str);
        } catch (IllegalArgumentException ex) {
            return NOVALUE;
        }
    }
}

