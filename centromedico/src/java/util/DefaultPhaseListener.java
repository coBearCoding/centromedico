
package util;

import java.io.IOException;
import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import controller.seguridad.GenericoController;
import controller.seguridad.IndexController;

/**
 *
 * @author kenlly
 */

public class DefaultPhaseListener implements PhaseListener {

    private GenericoController genericoController;
    private IndexController indexController;
    FacesContext facesContext;
    ExternalContext ext;
    String paginaActual;

    @Override
    public void afterPhase(PhaseEvent event) {
        facesContext = event.getFacesContext();
        paginaActual = facesContext.getViewRoot().getViewId();
        if (!paginaActual.endsWith("xhtml")) {
            return;
        }
        try {
            ext = facesContext.getExternalContext();
            
            indexController = (IndexController) facesContext.getApplication().evaluateExpressionGet(facesContext, "#{indexController}", IndexController.class);
            if (indexController.isAutenticado()) {
             
                genericoController = (GenericoController) ext.getApplicationMap().get("genericoController");
                if (!genericoController.buscarPorIdSesion(indexController.getIdSession())) {
                    
                    indexController.setAutenticado(false);
                    ((HttpSession) ext.getSession(false)).invalidate();
                    if (paginaActual.contains("home.xhtml")) {
                        ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "//index.html?v=close");//System.out.println("enviado a: " + ctxPath + "/index.html?v=timeout");
                    } else {
                        ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "/inactividad.html");//System.out.println("enviado a: " + ctxPath + "/index.html?v=timeout");
                    }
                    return;
                }
            }
           
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
            if (!paginaActual.endsWith("login.xhtml")) {
                if (session == null) {
                    indexController.setAutenticado(false);
                    genericoController.removeSession(indexController.getIdSession());
                    if (paginaActual.contains("home.xhtml")) {
                        ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "/index.html?v=timeout");
                    } else {
                        ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "/inactividad.html");
                    }
                } else {
                    Object currentUser = session.getAttribute("username");
                    if (currentUser == null || currentUser == "") {
                        indexController.setAutenticado(false);
                        genericoController.removeSession(indexController.getIdSession());
                        if (paginaActual.contains("home.xhtml")) {
                            ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "/index.html?v=nosession");
                        } else {
                            ext.redirect(((ServletContext) ext.getContext()).getContextPath() + "/inactividad.html");
                        }
                    }
                }
            } 
        } catch (ELException | IOException t) {
            throw new FacesException("Session timed out", t);
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
