
package util;

import jpa.modelo.CnPacientes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import jpa.facade.CnPacientesFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
/**
 *
 * @author kenlly
 */
public class LazyPacienteDataModel extends LazyDataModel<CnPacientes> {

    private CnPacientesFacade pacientesFacade;
    private List<CnPacientes> dataSource;

    public LazyPacienteDataModel(CnPacientesFacade pacFacade) {
        pacientesFacade = pacFacade;
    }

    @PostConstruct
    private void numeroRegistros() {
        this.setRowCount(pacientesFacade.numeroTotalRegistros());
    }

    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    @Override
    public CnPacientes getRowData(String rowKey) {
        for (CnPacientes paciente : dataSource) {
            if (paciente.getIdPaciente().toString().equals(rowKey)) {
                return paciente;
            }
        }    
        return null;
    }

    @Override
    public Object getRowKey(CnPacientes paciente) {
        return paciente.getIdPaciente();
    }

    @Override
    public List<CnPacientes> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        dataSource = new ArrayList<>();
        String where = "";
        if (filters != null && !filters.isEmpty()) {
            for (Map.Entry<String, Object> entrySet : filters.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue().toString();
                if (key.contains("identificacion")) {
                    where = where + " identificacion ilike '%" + value + "%' AND ";
                } else {
                    where = where + " replace(array_to_string(array [primer_nombre || ' ', segundo_nombre || ' ', primer_apellido || ' ', segundo_apellido],'',''),'  ',' ') ilike '%" + value + "%' AND ";
                }
            }
            if (where.length() != 0) {
                where = "WHERE " + where.substring(0, where.length() - 4);
            }
        }
        String sql = "SELECT * FROM cn_pacientes " + where + " ORDER BY primer_nombre LIMIT " + String.valueOf(pageSize) + " OFFSET " + String.valueOf(first);
        dataSource = pacientesFacade.consultaNativaPacientes(sql);
        sql = "SELECT COUNT(*) FROM cn_pacientes " + where;
        this.setRowCount(pacientesFacade.consultaNativaConteo(sql));
        return dataSource;
    }

    public List<CnPacientes> getDatasource() {
        return dataSource;
    }

    public void setDatasource(List<CnPacientes> datasource) {
        this.dataSource = datasource;
    }

}

