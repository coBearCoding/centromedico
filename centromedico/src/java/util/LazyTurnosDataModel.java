//
//package util;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import jpa.modelo.CtTurnos;
//import jpa.facade.CtTurnosFacade;
//import org.primefaces.model.LazyDataModel;
//import org.primefaces.model.SortOrder;
///**
// *
// * @author kenlly
// */
//public class LazyTurnosDataModel extends LazyDataModel<CtTurnos> {
//
//    private CtTurnosFacade ctTurnosFacade;
//    private List<Integer> idsmedicos;
//    private Date horaIni;
//    private Date horaFin;
//    private List diasSemana;
//
//    public LazyTurnosDataModel() {
//    }
//
//    public LazyTurnosDataModel(CtTurnosFacade ctTurnosFacade, List<Integer> idsmedicos, Date horaIni, Date horaFin, List diasSemana) {
//        this.ctTurnosFacade = ctTurnosFacade;
//        this.idsmedicos = idsmedicos;
//        this.horaIni = horaIni;
//        this.horaFin = horaFin;
//        this.diasSemana = diasSemana;
//    }
//    
//    @Override
//    public Object getRowKey(CtTurnos turno) {
//        return turno.getIdTurno();
//    }
//
//    @Override
//    public List<CtTurnos> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
//        //filter
//        List<CtTurnos> data = loadTurnos(first, pageSize, filters);
//
//        //rowCount
//        int dataSize = data.size();
//
//        //paginate
//        if (dataSize > pageSize) {
//            try {
//                return data.subList(first, first + pageSize);
//            } catch (IndexOutOfBoundsException e) {
//                return data.subList(first, first + (dataSize % pageSize));
//            }
//        } else {
//            return data;
//        }
//    }
//
//    private List<CtTurnos> loadTurnos(int offset, int limit, Map<String, Object> filters) {
//        List<CtTurnos> data;
//        String sql = "";
//        SimpleDateFormat ft = new SimpleDateFormat("HH:mm");
//        if (horaIni != null) {
//            sql += " AND hora_ini >= '" + ft.format(horaIni) + "'";
//        }
//        if (horaFin != null) {
//            sql += " AND hora_fin <= '" + ft.format(horaFin) + "'";
//        }
//        if (diasSemana != null) {
//            if (diasSemana.size() > 0) {
//                sql += " AND EXTRACT(DOW FROM fecha) IN (";
//                int i = diasSemana.size();
//                int index = 1;
//                for (Object dia : diasSemana) {
//                    sql += dia.toString();
//                    if (index < i) {
//                        sql += ", ";
//                    }
//                    index++;
//                }
//                sql += ")";
//            }
//        }
//        String query;
//        String queryCount;
//        query = "SELECT * FROM ct_turnos JOIN cn_consultorios ON cn_consultorios.id_consultorio = ct_turnos.id_consultorio JOIN cn_usuarios ON id_medico = id_usuario WHERE id_medico IN (";
//        queryCount = "SELECT COUNT(id_turno) FROM ct_turnos JOIN cn_consultorios ON cn_consultorios.id_consultorio = ct_turnos.id_consultorio JOIN cn_usuarios ON id_medico = id_usuario WHERE id_medico IN (";
//        int i = idsmedicos.size();
//        int indx = 1;
//        for (Integer id : idsmedicos) {
//            query += id;
//            queryCount += id;
//            if (indx < i) {
//                query += ", ";
//                queryCount += ", ";
//            }
//            indx++;
//        }
//        query += ") AND estado = 'disponible' AND fecha >= current_date";
//        queryCount += ") AND estado = 'disponible' AND fecha >= current_date";
//
//        if (filters.isEmpty()) {
//            data = ctTurnosFacade.findTurnosDisponiblesByMedicosLazyNative(query.concat(sql), idsmedicos, 0, horaIni, horaFin, diasSemana, offset, limit);
//            int elementos = ctTurnosFacade.totalTurnosByMedicosLazyNative(queryCount.concat(sql), idsmedicos, 0, horaIni, horaFin, diasSemana);
//            this.setRowCount(elementos);
//        } else {
//            int especialidad = 0;
//            for (Map.Entry<String, Object> entry : filters.entrySet()) {
//                String filterProperty = entry.getKey();
//                switch (filterProperty) {
//                    case "idMedico.especialidad.id":
//                        especialidad = Integer.parseInt(entry.getValue().toString());
//                        sql += " AND cn_usuarios.especialidad = " + especialidad;
//                        break;
//                    case "fecha":
//                        ft = new SimpleDateFormat("dd/MM/yyyy");
//                        try {
//                            String aux = entry.getValue().toString();
//                            Date fecha = ft.parse(aux);
//                            ft = new SimpleDateFormat("yyyy-MM-dd");
//                            sql += " AND ct_turnos.fecha = '"+ft.format(fecha)+"'";
//                        } catch (ParseException pe){
//                            sql += "";
//                        }
//                        break;
//                }
//            }
//            int elementos = ctTurnosFacade.totalTurnosByMedicosLazyNative(queryCount.concat(sql), idsmedicos, especialidad, horaIni, horaFin, diasSemana);
//            this.setRowCount(elementos);
//            data = ctTurnosFacade.findTurnosDisponiblesByMedicosLazyNative(query.concat(sql), idsmedicos, especialidad, horaIni, horaFin, diasSemana, offset, limit);
//        }
//        return data;
//    }
//}
