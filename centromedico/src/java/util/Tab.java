
package util;

/**
 *
 * @author kenlly
 */
public class Tab {
    
    private String titulo;
    private String url;

    public Tab(String titulo, String url) {
        this.titulo = titulo;
        this.url = url;
    }    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }    
}

