/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.utilidades;

import java.util.Date;

/**
 *
 * @author dante
 */
public class SesionTrU {
    private String empresa;
    private String value;
    private String paciente;
    private String factura;
    private String valEmpresa;
    private String valTotal;
    private String copago;
    private String sede;
    private String sedeDir;
    private String sedeTel;
    private int idCita;
    private Date fecha;
    private Date hora;
    private String consultorio;
    private int idPrestador;
    private String prestadorPN;
    private String prestadorSN;
    private String prestadorPA;
    private String prestadorSA;
    private String prestadorEspecialidad;
    private int idPaciente;
    private String pacientePN;
    private String pacienteSN;
    private String pacientePA;
    private String pacienteSA;
    private String pacienteTipoDoc;
    private String pacienteNumDoc;
    private String administradora;
    private String empresaTelefono;
    private String empresaDireccion;
    private String servicio;
    private String programa;
    private String actividad;
    private String noTurno;
    private String codAdministradora;
    private String observaciones;
    private String motivoConsulta;
    private String motivoCancelacion;
    private Date fechaRegistro;
    private Date fechaCancelacion;
    private boolean atendida;
    private boolean cancelada;
    private String subsecuente;
    private String fechaimpresion;
    private int conteo;

    public SesionTrU() {
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getValEmpresa() {
        return valEmpresa;
    }

    public void setValEmpresa(String valEmpresa) {
        this.valEmpresa = valEmpresa;
    }

    public String getValTotal() {
        return valTotal;
    }

    public void setValTotal(String valTotal) {
        this.valTotal = valTotal;
    }

    public String getCopago() {
        return copago;
    }

    public void setCopago(String copago) {
        this.copago = copago;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getSedeDir() {
        return sedeDir;
    }

    public void setSedeDir(String sedeDir) {
        this.sedeDir = sedeDir;
    }

    public String getSedeTel() {
        return sedeTel;
    }

    public void setSedeTel(String sedeTel) {
        this.sedeTel = sedeTel;
    }

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getConsultorio() {
        return consultorio;
    }

    public void setConsultorio(String consultorio) {
        this.consultorio = consultorio;
    }

    public int getIdPrestador() {
        return idPrestador;
    }

    public void setIdPrestador(int idPrestador) {
        this.idPrestador = idPrestador;
    }

    public String getPrestadorPN() {
        return prestadorPN;
    }

    public void setPrestadorPN(String prestadorPN) {
        this.prestadorPN = prestadorPN;
    }

    public String getPrestadorSN() {
        return prestadorSN;
    }

    public void setPrestadorSN(String prestadorSN) {
        this.prestadorSN = prestadorSN;
    }

    public String getPrestadorPA() {
        return prestadorPA;
    }

    public void setPrestadorPA(String prestadorPA) {
        this.prestadorPA = prestadorPA;
    }

    public String getPrestadorSA() {
        return prestadorSA;
    }

    public void setPrestadorSA(String prestadorSA) {
        this.prestadorSA = prestadorSA;
    }

    public String getPrestadorEspecialidad() {
        return prestadorEspecialidad;
    }

    public void setPrestadorEspecialidad(String prestadorEspecialidad) {
        this.prestadorEspecialidad = prestadorEspecialidad;
    }

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getPacientePN() {
        return pacientePN;
    }

    public void setPacientePN(String pacientePN) {
        this.pacientePN = pacientePN;
    }

    public String getPacienteSN() {
        return pacienteSN;
    }

    public void setPacienteSN(String pacienteSN) {
        this.pacienteSN = pacienteSN;
    }

    public String getPacientePA() {
        return pacientePA;
    }

    public void setPacientePA(String pacientePA) {
        this.pacientePA = pacientePA;
    }

    public String getPacienteSA() {
        return pacienteSA;
    }

    public void setPacienteSA(String pacienteSA) {
        this.pacienteSA = pacienteSA;
    }

    public String getPacienteTipoDoc() {
        return pacienteTipoDoc;
    }

    public void setPacienteTipoDoc(String pacienteTipoDoc) {
        this.pacienteTipoDoc = pacienteTipoDoc;
    }

    public String getPacienteNumDoc() {
        return pacienteNumDoc;
    }

    public void setPacienteNumDoc(String pacienteNumDoc) {
        this.pacienteNumDoc = pacienteNumDoc;
    }

    public String getAdministradora() {
        return administradora;
    }

    public void setAdministradora(String administradora) {
        this.administradora = administradora;
    }

    public String getEmpresaTelefono() {
        return empresaTelefono;
    }

    public void setEmpresaTelefono(String empresaTelefono) {
        this.empresaTelefono = empresaTelefono;
    }

    public String getEmpresaDireccion() {
        return empresaDireccion;
    }

    public void setEmpresaDireccion(String empresaDireccion) {
        this.empresaDireccion = empresaDireccion;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getNoTurno() {
        return noTurno;
    }

    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

    public String getCodAdministradora() {
        return codAdministradora;
    }

    public void setCodAdministradora(String codAdministradora) {
        this.codAdministradora = codAdministradora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public String getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public boolean isAtendida() {
        return atendida;
    }

    public void setAtendida(boolean atendida) {
        this.atendida = atendida;
    }

    public boolean isCancelada() {
        return cancelada;
    }

    public void setCancelada(boolean cancelada) {
        this.cancelada = cancelada;
    }

    public String getSubsecuente() {
        return subsecuente;
    }

    public void setSubsecuente(String subsecuente) {
        this.subsecuente = subsecuente;
    }

    public String getFechaimpresion() {
        return fechaimpresion;
    }

    public void setFechaimpresion(String fechaimpresion) {
        this.fechaimpresion = fechaimpresion;
    }

    public int getConteo() {
        return conteo;
    }

    public void setConteo(int conteo) {
        this.conteo = conteo;
    }
    
    
}
