
package util;

import java.util.Date;

/**
 *
 * @author kenlly
 */
public class CitaReporte {
    private String empresa;
    private int idCita;
    private Date fecha;
    private Date hora;
    private String medicoPN;
    private String medicoSN;
    private String medicoPA;
    private String medicoSA;
    private String pacientePN;
    private String pacienteSN;
    private String pacientePA;
    private String pacienteSA;
    private String pacienteTipoDoc;
    private String pacienteNumDoc;
    private String empresaTelefono;
    private String empresaDireccion;
    private String servicio;
    private String noTurno;
    private String motivoCancelacion;
    private Date fechaCancelacion;
    private boolean atendida;
    private boolean cancelada;
    private String subsecuente;
    private int conteoTf;
    private int conteoTr;
    private String fechaimpresion;

    public CitaReporte() {
    }

    /**
     * @return the empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the citaId
     */
    public int getIdCita() {
        return idCita;
    }

    /**
     * @param citaId the citaId to set
     */
    public void setIdCita(int citaId) {
        this.idCita = citaId;
    }
    /**
     * @return the noTurno
     */
    public String getNoTurno() {
        return noTurno;
    }
    /**
     * @param noTurno the num turno to set
     */
    public void setNoTurno(String noTurno) {
        this.noTurno = noTurno;
    }

    /**
     * @return the medicoPN
     */
    public String getMedicoPN() {
        return medicoPN;
    }

    /**
     * @param medicoPN the medicoPN to set
     */
    public void setMedicoPN(String medicoPN) {
        this.medicoPN = medicoPN;
    }

    /**
     * @return the medicoSN
     */
    public String getMedicoSN() {
        return medicoSN;
    }

    /**
     * @param medicoSN the medicoSN to set
     */
    public void setMedicoSN(String medicoSN) {
        this.medicoSN = medicoSN;
    }

    /**
     * @return the medicoPA
     */
    public String getMedicoPA() {
        return medicoPA;
    }

    /**
     * @param medicoPA the medicoPA to set
     */
    public void setMedicoPA(String medicoPA) {
        this.medicoPA = medicoPA;
    }

    /**
     * @return the medicoSA
     */
    public String getMedicoSA() {
        return medicoSA;
    }

    /**
     * @param medicoSA the medicoSA to set
     */
    public void setMedicoSA(String medicoSA) {
        this.medicoSA = medicoSA;
    }

    /**
     * @return the pacientePN
     */
    public String getPacientePN() {
        return pacientePN;
    }

    /**
     * @param pacientePN the pacientePN to set
     */
    public void setPacientePN(String pacientePN) {
        this.pacientePN = pacientePN;
    }

    /**
     * @return the pacienteSN
     */
    public String getPacienteSN() {
        return pacienteSN;
    }

    /**
     * @param pacienteSN the pacienteSN to set
     */
    public void setPacienteSN(String pacienteSN) {
        this.pacienteSN = pacienteSN;
    }

    /**
     * @return the pacientePA
     */
    public String getPacientePA() {
        return pacientePA;
    }

    /**
     * @param pacientePA the pacientePA to set
     */
    public void setPacientePA(String pacientePA) {
        this.pacientePA = pacientePA;
    }

    /**
     * @return the pacienteSA
     */
    public String getPacienteSA() {
        return pacienteSA;
    }

    /**
     * @param pacienteSA the pacienteSA to set
     */
    public void setPacienteSA(String pacienteSA) {
        this.pacienteSA = pacienteSA;
    }

    /**
     * @return the fechaimpresion
     */
    public String getFechaimpresion() {
        return fechaimpresion;
    }
    /**
     * @param fechaimpresion the fecha impresion to set
     */
    public void setFechaimpresion(String fechaimpresion) {
        this.fechaimpresion = fechaimpresion;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the Hora
     */
    public Date getHora() {
        return hora;
    }

    /**
     * @param Hora the Hora to set
     */
    public void setHora(Date Hora) {
        this.hora = Hora;
    }

    /**
     * @return the pacienteTipoDoc
     */
    public String getPacienteTipoDoc() {
        return pacienteTipoDoc;
    }

    /**
     * @param pacienteTipoDoc the pacienteTipoDoc to set
     */
    public void setPacienteTipoDoc(String pacienteTipoDoc) {
        this.pacienteTipoDoc = pacienteTipoDoc;
    }

    /**
     * @return the pacienteNumDoc
     */
    public String getPacienteNumDoc() {
        return pacienteNumDoc;
    }

    /**
     * @param pacienteNumDoc the pacienteNumDoc to set
     */
    public void setPacienteNumDoc(String pacienteNumDoc) {
        this.pacienteNumDoc = pacienteNumDoc;
    }

    /**
     * @return the motivoCancelacion
     */
    public String getMotivoCancelacion() {
        return motivoCancelacion;
    }

    /**
     * @param motivoCancelacion the motivoCancelacion to set
     */
    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    /**
     * @return the subsecuente
     */
    public String getSubsecuente() {
        return subsecuente;
    }
    /**
     * @param subsecuente 
     */
    public void setSubsecuente(String subsecuente) {
        this.subsecuente = subsecuente;
    }

    public String getEmpresaDireccion() {
        return empresaDireccion;
    }

    public void setEmpresaDireccion(String empresaDireccion) {
        this.empresaDireccion = empresaDireccion;
    }

    public String getEmpresaTelefono() {
        return empresaTelefono;
    }

    public void setEmpresaTelefono(String empresaTelefono) {
        this.empresaTelefono = empresaTelefono;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public boolean isAtendida() {
        return atendida;
    }

    public void setAtendida(boolean atendida) {
        this.atendida = atendida;
    }

    public boolean isCancelada() {
        return cancelada;
    }

    public void setCancelada(boolean cancelada) {
        this.cancelada = cancelada;
    }

    
     public int getConteoTf() {
        return conteoTf;
    }

    public void setConteoTf(int conteoTf) {
        this.conteoTf = conteoTf;
    }

    public int getConteoTr() {
        return conteoTr;
    }

    public void setConteoTr(int conteoTr) {
        this.conteoTr = conteoTr;
    }
    
 }
