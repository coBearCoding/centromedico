
package util;

import com.google.common.io.Files;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import jpa.modelo.CnClasificaciones;
import jpa.facade.CnClasificacionesFacade;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.primefaces.model.UploadedFile;
import org.apache.poi.xssf.usermodel.*;

/**
 *
 * @author kenlly
 */
public class MetodosGenerales {

    @EJB
    CnClasificacionesFacade clasFacade;

    List<String> listaTagsValidos = Arrays.asList("<strong>", "</strong>", "<span style=\"font-weight: bold;\">", "<span>", "</span>", "<p>", "</p>", "<b>", "</b>", "<ol>", "</ol>",
            "<li>", "</li>", "<div>", "</div>", "<br>", "<div style=\"text-align: justify;\">");

 
    public void createCell(XSSFCellStyle cellStyle, XSSFRow fila, int position, String value) {
        XSSFCell cell;
        cell = fila.createCell((short) position);// Se crea una cell dentro de la fila                        
        cell.setCellValue(new XSSFRichTextString(value));
        cell.setCellStyle(cellStyle);
    }

    //CREA UNA CELDA PARA UN DOCUMENTO TIPO EXCEL
    public void createCell(XSSFRow fila, int position, String value) {
        XSSFCell cell;
        cell = fila.createCell((short) position);// Se crea una cell dentro de la fila                        
        cell.setCellValue(new XSSFRichTextString(value));
    }


    public String obtenerCadenaNoNula(String txt1) {
        //se entrega una cadena que no contenga null
        String strReturn = "";
        if (txt1 != null && txt1.trim().length() != 0) {
            strReturn = strReturn + txt1;
        }
        return strReturn;
    }

    public String obtenerCadenaNoNula(String txt1, String txt2) {
        //se entrega una cadena que no contenga null
        String strReturn = "";
        if (txt1 != null && txt1.trim().length() != 0) {
            strReturn = strReturn + txt1;
        }
        if (txt2 != null && txt2.trim().length() != 0) {
            strReturn = strReturn + " " + txt2;
        }

        return strReturn;
    }

    public String obtenerCadenaNoNula(String txt1, String txt2, String txt3) {
        //se entrega una cadena que no contenga null
        String strReturn = "";
        if (txt1 != null && txt1.trim().length() != 0) {
            strReturn = strReturn + txt1;
        }
        if (txt2 != null && txt2.trim().length() != 0) {
            strReturn = strReturn + " " + txt2;
        }
        if (txt3 != null && txt3.trim().length() != 0) {
            strReturn = strReturn + " " + txt3;
        }
        return strReturn;
    }

    public String obtenerCadenaNoNula(String txt1, String txt2, String txt3, String txt4) {
        //se entrega una cadena que no contenga null
        String strReturn = "";
        if (txt1 != null && txt1.trim().length() != 0) {
            strReturn = strReturn + txt1;
        }
        if (txt2 != null && txt2.trim().length() != 0) {
            strReturn = strReturn + " " + txt2;
        }
        if (txt3 != null && txt3.trim().length() != 0) {
            strReturn = strReturn + " " + txt3;
        }
        if (txt4 != null && txt4.trim().length() != 0) {
            strReturn = strReturn + " " + txt4;
        }

        return strReturn;
    }

    public String calcularEdad(Date fechaNacimiento) {//calcular la edad a partir de la fecha de nacimiento    
        Period periodo = new Period(new DateTime(fechaNacimiento), new DateTime(new Date()));
        return String.valueOf(periodo.getYears()) + "A " + String.valueOf(periodo.getMonths()) + "M ";
    }

    public int calcularEdadInt(Date fechaNacimiento) {//calcular la edad en años
        Period periodo = new Period(new DateTime(fechaNacimiento), new DateTime(new Date()));
        if (periodo.getYears() == 0) {
            return 1;
        } else {
            return periodo.getYears();
        }
    }

    public String calcularEdadMes(Date fechaNacimiento) {//calcular la edad en meses Cores
        Period periodo = new Period(new DateTime(fechaNacimiento), new DateTime(new Date()));
        return String.valueOf(periodo.getMonths());
    }


    public boolean validarNoVacio(String valor) {
        //validas si una cadena no es vacia //true= no vacio //false=vacio
        if (valor == null) {
            return false;
        }
        return valor.trim().length() != 0;
    }

    public boolean validacionCampoVacio(String valor, String nombre) {
        /*false = lleno, true = vacio pero ademas imprime el mensaje de error
         */
        if (valor == null || valor.trim().length() == 0) {
            imprimirMensaje("Error", "El campo " + nombre + " es obligatorio", FacesMessage.SEVERITY_ERROR);
            return true;
        }
        return false;
    }

    public void imprimirMensaje(String titulo, String mensaje, Severity tipo) {
        if (tipo == FacesMessage.SEVERITY_INFO) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, mensaje));
        } else if (tipo == FacesMessage.SEVERITY_WARN) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, mensaje));
        } else if (tipo == FacesMessage.SEVERITY_ERROR) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, mensaje));
            //System.out.println("ERROR: ############### " + titulo + " ---- " + mensaje);
        } else if (tipo == FacesMessage.SEVERITY_FATAL) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, titulo, mensaje));
        }
    }

    public boolean uploadArchivo(UploadedFile archivoCargado, String rutaDestino, String nombrearchivo) {
        String absoluteWebPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        final String project_path = absoluteWebPath + "recursos/tmp/" + nombrearchivo;
        rutaDestino = rutaDestino + nombrearchivo;
        File tmp = new File(project_path);
        File fichero = new java.io.File(rutaDestino);
        if (fichero.exists()) {//si existe se borra
            fichero.delete();
        }
        fichero = new java.io.File(rutaDestino);
        try (FileOutputStream fileOutput = new FileOutputStream(fichero)) {
            InputStream inputStream = archivoCargado.getInputstream();
            byte[] buffer = new byte[1024];
            int bufferLength;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
            }
            // Files.copy(fichero, tmp);
        } catch (Exception e) {
            System.out.println("Error 01: " + e.getMessage());
            imprimirMensaje("Error 01", e.getMessage(), FacesMessage.SEVERITY_ERROR);
            return false;
        }
        return true;
    }

    public void CopyFilePath(String rutaOrigen, Integer foto) {

        String absoluteWebPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        final String rutaDestino = absoluteWebPath + "recursos/" + "/tmp/" + foto + ".jpg";

        File origen = new File(rutaOrigen);
        File destino = new File(rutaDestino);

        try {
            Files.copy(origen, destino);
        } catch (IOException ex) {
            System.out.println("Error 01: " + ex.getMessage());
            imprimirMensaje("Error 01", ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }

    }

    public List<SelectItem> cargarCategoria(String maestro) {
        List<SelectItem> listaRetorno = new ArrayList<>();
        List<CnClasificaciones> listaCategorias = clasFacade.buscarPorMaestro(maestro);
        for (CnClasificaciones clasificacion : listaCategorias) {
            listaRetorno.add(new SelectItem(clasificacion.getId(), clasificacion.getDescripcion()));
        }
        return listaRetorno;
    }

   
}
