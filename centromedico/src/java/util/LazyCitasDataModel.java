
package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jpa.modelo.CtCitas;
import jpa.facade.CtCitasFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
/**
 *
 * @author kenlly
 */
public class LazyCitasDataModel extends LazyDataModel<CtCitas> {

    private CtCitasFacade citasFacade;
    private String opcion;
    private int identificador;
    private List<CtCitas> data;  
    
    public LazyCitasDataModel(CtCitasFacade citasFacade, String opcion, int identificador) {
        this.citasFacade = citasFacade;
        this.opcion = opcion;
        this.identificador = identificador;
    }


    @Override
    public Object getRowKey(CtCitas cita) {
        return cita.getIdCita();
    }

    @Override
    public List<CtCitas> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        int rows = 0;
        switch (opcion) {
            case "paciente":
                data = citasFacade.findCitasByPacienteVigentes(identificador, first, pageSize);
                rows = citasFacade.TotalCitasVigentesByPaciente(identificador);
                break;
            case "medico":
                data = citasFacade.findCitasByMedicoVigentes(identificador, first, pageSize);
                rows = citasFacade.TotalCitasVigentesByMedico(identificador);
                break;
            default:
                data = new ArrayList();
        }
        
        this.setRowCount(rows);
        int dataSize = data.size();

        
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }
}
