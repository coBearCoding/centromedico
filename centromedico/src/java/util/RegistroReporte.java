
package util;

/**
 *
 * @author kenlly
 */
public class RegistroReporte {
    private String nombre ;
    private String identificacion ; 
    private String sexo ;
    private String orientacionsexual ; 
    private String identidadgenero ;
    private String fecha_nacimiento ;
    private String identificacionresponsable ; 
    private String nacionalidad ;
    private String etnia ;
    private String nacionalidades ;
    private String afiliado ; 
    private String gruposprioritarios ;
    private String semanagestacion ; 
    private String provincia ;
    private String canton ; 
    private String parroquia;
    private String barrio ;
    private String enfermedad ;
    private String codigo ;             
    private String tipoatencion;

    public RegistroReporte() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getOrientacionsexual() {
        return orientacionsexual;
    }

    public void setOrientacionsexual(String orientacionsexual) {
        this.orientacionsexual = orientacionsexual;
    }

    public String getIdentidadgenero() {
        return identidadgenero;
    }

    public void setIdentidadgenero(String identidadgenero) {
        this.identidadgenero = identidadgenero;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getIdentificacionresponsable() {
        return identificacionresponsable;
    }

    public void setIdentificacionresponsable(String identificacionresponsable) {
        this.identificacionresponsable = identificacionresponsable;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getEtnia() {
        return etnia;
    }

    public void setEtnia(String etnia) {
        this.etnia = etnia;
    }

    public String getNacionalidades() {
        return nacionalidades;
    }

    public void setNacionalidades(String nacionalidades) {
        this.nacionalidades = nacionalidades;
    }

    public String getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(String afiliado) {
        this.afiliado = afiliado;
    }

    public String getGruposprioritarios() {
        return gruposprioritarios;
    }

    public void setGruposprioritarios(String gruposprioritarios) {
        this.gruposprioritarios = gruposprioritarios;
    }

    public String getSemanagestacion() {
        return semanagestacion;
    }

    public void setSemanagestacion(String semanagestacion) {
        this.semanagestacion = semanagestacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipoatencion() {
        return tipoatencion;
    }

    public void setTipoatencion(String tipoatencion) {
        this.tipoatencion = tipoatencion;
    }

    
}
