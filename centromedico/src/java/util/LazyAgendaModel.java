
package util;
import java.util.Date;
import java.util.List;
import jpa.modelo.CtCitas;
import jpa.modelo.CtTurnos;
import jpa.facade.CtCitasFacade;
import jpa.facade.CtTurnosFacade;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;

/**
 *
 * @author kenlly
 */
public class LazyAgendaModel extends LazyScheduleModel {
    
    private final int idMedico;
    private final CtTurnosFacade turnosFacade;
    private final CtCitasFacade citasFacade;
    private List<CtTurnos> listaTurnos;
    private final String opcion;

    public LazyAgendaModel(int idMedico, CtTurnosFacade turnosFacade, CtCitasFacade citasFacade, String opcion) {
        this.idMedico = idMedico;
        this.turnosFacade = turnosFacade;
        this.citasFacade = citasFacade;
        this.opcion = opcion;
    }

    @Override
    public void loadEvents(Date start, Date end) {
        clear();
        listaTurnos = turnosFacade.obtenerTurnosLazy(idMedico, start, end);
        if (listaTurnos != null) {
            if (!listaTurnos.isEmpty()) {
                switch (opcion) {
                    case "cita":
                        funcionalidadAgendaCita();
                        break;
                    case "generarHorario":
                        funcionalidadAgendaCrearTurnos();
                        break;
                    case "enfermeriaTurnos":
                        listaTurnos = turnosFacade.obtenerTurnosEnfermeriaLazy(start, end);
                        funcionalidadOtrasAgendas();
                    break;
                    default:
                        funcionalidadOtrasAgendas();
                }
            }else{
                switch (opcion) {
                    case "enfermeriaTurnos":
                            listaTurnos = turnosFacade.obtenerTurnosEnfermeriaLazy(start, end);
                            funcionalidadOtrasAgendas();
                        break;
            }
                    
        }
    }
    }

    private void funcionalidadAgendaCita() {
        for (CtTurnos turno : listaTurnos) {
            Date inicial = new Date(turno.getFecha().getTime());
            inicial.setHours(turno.getHoraIni().getHours());
            inicial.setMinutes(turno.getHoraIni().getMinutes());
            Date finaliza = new Date(turno.getFecha().getTime());
            String estilo;
            String title = turno.getIdTurno().toString();
            if (turno.getEstado().equals("disponible")) {
                estilo = "disponible";
            } else if (turno.getEstado().equals("asignado") || turno.getEstado().equals("en_espera") || turno.getEstado().equals("atendido")) {
                estilo = "asignado";
            } else if (turno.getEstado().equals("no_disponible")) {
                estilo = "no_disponible";
            } else {
                estilo = "reservado";
            }
            CtCitas c = citasFacade.findCitasByTurno(turno.getIdTurno());
            if (c != null) {
                title = title.concat(" - " + c.getIdPaciente().nombreCompleto());
            }
            finaliza.setHours(turno.getHoraFin().getHours());
            finaliza.setMinutes(turno.getHoraFin().getMinutes());
            addEvent(new DefaultScheduleEvent(title, inicial, finaliza, estilo));
        }
    }

    private void funcionalidadAgendaCrearTurnos() {
        String title;
        for (CtTurnos turno : listaTurnos) {
            Date inicial = new Date(turno.getFecha().getTime());
            inicial.setHours(turno.getHoraIni().getHours());
            inicial.setMinutes(turno.getHoraIni().getMinutes());
            Date finaliza = new Date(turno.getFecha().getTime());
            String estilo;
            if (turno.getEstado().equals("no_disponible")) {
                estilo = "no_disponible";
            } else {
                estilo = "disponible";
            }
            finaliza.setHours(turno.getHoraFin().getHours());
            finaliza.setMinutes(turno.getHoraFin().getMinutes());
            title = turno.getIdTurno().toString();
            if (turno.getIdHorario() != null) {
                title = title.concat(" - " + turno.getIdHorario().getDescripcion());
            }
            addEvent(new DefaultScheduleEvent(title, inicial, finaliza, estilo));
        }
    }

 
    private void funcionalidadOtrasAgendas() {
        for (CtTurnos turno : listaTurnos) {
            Date inicial = new Date(turno.getFecha().getTime());
            inicial.setHours(turno.getHoraIni().getHours());
            inicial.setMinutes(turno.getHoraIni().getMinutes());
            Date finaliza = new Date(turno.getFecha().getTime());
            String estilo;
            String title;
            CtCitas cita = citasFacade.findCitasByTurno(turno.getIdTurno());
            if (cita != null) {
                title = turno.getIdTurno().toString().concat(" - " + cita.getIdPaciente().nombreCompleto());
                if (turno.getEstado().equals("asignado") && !cita.getAtendida()) {
                    estilo = "no_atendido";
                } else if (turno.getEstado().equals("atendido") || cita.getAtendida()) {
                    estilo = "atendido";
                } else if (turno.getEstado().equals("en_espera")) {
                    estilo = "en_espera";
                } else {
                    estilo = "libre";
                }
            } else {
                estilo = "libre";
                title = turno.getIdTurno().toString();
            }
            finaliza.setHours(turno.getHoraFin().getHours());
            finaliza.setMinutes(turno.getHoraFin().getMinutes());
            addEvent(new DefaultScheduleEvent(title, inicial, finaliza, estilo));
        }
    }

    public List<CtTurnos> getListaTurnos() {
        return listaTurnos;
    }
}

